#NOTE:
#Computes the Reynolds drag coefficient
#
import sys, math
import std_atm
import Biparabolic1
#
#There are only 2 tables used for REYNREF for all Models, 
#1 for all flaps Up CONFIGs (1 and 2) and 1 for all flaps 
#Down CONFIGs (3,4,5,6).
#
def reynolds(fwt,fmach,falt,ftemperature):
 rey_wod = []
 rey_alt = []
#
#For 37800W
#DEGRN01       = 6
#ACDF01        = 24.41
#BCDF01        = -63.99
#H
#/TBLU
#REYNREF01
# .12000 .15000 .20000 .25000 .30000 .36000 .44000 .48000 .52000 .56000  wod
# .60000 .64000 .68000 .72000 .80000                                    1wod
# -1.100  4.875 12.200 17.600 21.893 26.061 30.553 32.525 34.395 36.002
# 36.832 37.404 37.883 38.314 39.097
#
#For P8Clean
#DEGRN01       =   6
#ACDF01        =  24.41
#BCDF01        = -63.99
#H
#/TBLU
#REYNREF01
# .12000 .15000 .20000 .25000 .30000 .36000 .44000 .48000 .52000 .56000  wod
# .60000 .64000 .68000 .72000 .80000                                    1wod
# -1.100  4.875 12.200 17.600 21.893 26.061 30.553 32.525 34.395 36.002
# 36.832 37.404 37.883 38.314 39.097
#
 reynref01 = {"0.12000":-1.1,"0.15000":4.875,"0.20000":12.2,"0.25000":17.6,\
             "0.30000":21.893,"0.36000":26.061,"0.44000":30.553,"0.48000":32.525,\
             "0.52000":34.395,"0.56000":36.002,"0.60000":36.832,"0.64000":37.404,\
             "0.68000":37.883,"0.72000":38.314,"0.80000":39.097}
 reynref02 = {"0.12000":-1.1,"0.15000":4.875,"0.20000":12.2,"0.25000":17.6,\
             "0.30000":21.893,"0.36000":26.061,"0.44000":30.553,"0.48000":32.525,\
             "0.52000":34.395,"0.56000":36.002,"0.60000":36.832,"0.64000":37.404,\
             "0.68000":37.883,"0.72000":38.314,"0.80000":39.097}                                    
#
 for i in reynref01.keys():
  rey_wod.append(float(i))
  rey_alt.append(reynref01[i])
 rey_wod.sort()
 rey_alt.sort()
 #print("rey_wod ",rey_wod)
 #print("rey_alt ",rey_alt)
#
 dlta_std = std_atm.alt2press_ratio(falt)
 theta_std = std_atm.alt2temp_ratio(falt)
 theta_ambient = (ftemperature + 273.15)/288.15
 #print ("dlta_std theta_std wod ",dlta_std," ",theta_std)
 #print ("fwt = ",fwt," dlta_std = ",dlta_std)
 wod = (fwt/dlta_std)*0.000001
 #print ("wod = ",wod) 
 #print ("theta_ambient = ",theta_ambient)
 refalt = Biparabolic1.Biparabolic1(rey_wod,rey_alt,wod,'reyrefalt')*1000.00
 #print ("refalt = ",refalt)
 dlta_refalt = std_atm.alt2press_ratio(refalt)
 theta_refalt = std_atm.alt2temp_ratio(refalt)
 REovrMnom = 5.13384*1000000.00*((theta_refalt+0.38312)/theta_refalt**2)*dlta_refalt
 REovrM = 5.13384*1000000.00*((theta_ambient+0.38312)/theta_ambient**2)*dlta_std
 CDrey = -63.99 * 0.0001 * math.log10(REovrM/REovrMnom)
 #print ("REovrMnom REovrM CDrey ",REovrMnom," ",REovrM," ",CDrey)
#
#Eriks way
 cd1 = 24.41 - (63.99 * math.log10(REovrMnom))
 cd1 = cd1*0.0001
 cd2 = 24.41 - (63.99 * math.log10(REovrM))
 cd2 = cd2*0.0001 
 CDrey_erik = cd2 - cd1
 #print ("CDrey_erik = ",CDrey_erik)

 return(CDrey)
