#### Purpose ####
#Initial implementation of Linear interpolation function

# Date created: July 2nd, 2015

import numpy as np
from bisect import bisect_left

# 2 sets of coefficients, one for each parabolic equation
a1 = 2
b1 = 4
c1 = 22

a2 = 1
b2 = 6
c2 = 33

# Set fixed variables
xwant = 16.72
print("xwant is", xwant)

# List of x values
xlist1 = [2,4,6,8,10,12,14,18,24]
print("xlist1 is", xlist1)

# Perform Linear interpolation 
class Interpolate(object):
    def __init__(self, xlist, ylist):
        for x, y in zip(xlist, xlist[1:]):
            if any(y - x <= 0):
                raise ValueError("xlist must be in ascending order!")
        # Map to float so that integer division (python <= 2.7) won't kick in 
        # and ruin things if x1, x2, y1 and y2 are all integers for some iterval.
        xlist = self.xlist = map(float, xlist)
        ylist = self.ylist = map(float, ylist)
        # zip makes an iterator that aggregates elements from each of the iterables.
        intervals = zip(xlist, xlist[1:], ylist, ylist[1:])
        self.slopes = [(y2 - y1)/(x2 - x1) for x1, x2, y1, y2 in intervals]

    # Create function __getitem__ that takes advantage of the fact that self.x_list 
    # is sorted in ascending order by using bisect_left to (very) quickly find the 
    # index of the largest element smaller than x in self.x_list.
    def __getitem__(self, x):
        i = bisect_left(self.xlist, x) - 1
        return self.ylist[i] + self.slopes[i] * (x - self.xlist[i])