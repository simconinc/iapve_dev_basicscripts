def Biparabolic1(xlist, ylist, xwant, whatfor):
#
#### Purpose ####
#Prepare two sets of three point arrays. 
#Use these two sets to call UnivariateSpline to get 
#two parabolic objects from which to determine the ywant
#value for xwant and then perform a weighted average 
#between these two values.
#
# whatfor can be 'cd' or 'thrust' or 'fuelflow' or 'reyrefalt'
#
    import numpy as np
    from scipy.interpolate import UnivariateSpline
    from bisect import bisect_left
    from Linear import Linear
#
    # First determine if we have a direct match on
    # xwant within xlist
    pflag = 0
    print("start of Biparabolic1 with requested x value = ",xwant)
    #print("xlist ",xlist)
    #print("ylist ",ylist)
    il_cnt = 0
    for ilook in xlist:
     if (ilook == xwant):
      #print ("ilook == xwant which is ",xwant," so returning ylist[il_cnt] = ",ylist[il_cnt],"with il_cnt = ",il_cnt)
      return(ylist[il_cnt])
     il_cnt = il_cnt + 1
    if (len(xlist) <= 2):
     #print ("len of xlist is <= 2 so returning based on linear interpolation")
     ywant = Linear(xlist,ylist,xwant)
     return ywant
    #
    #No direct match so proceed with parabolic
    xpos = bisect_left(xlist, xwant)
    #print ("xpos len(xlist) ",xpos," ",len(xlist))
    #print ("xpos len(ylist) ",xpos," ",len(ylist))
    #
    extrapolate = False
    if (xpos == len(xlist)):
      extrapolate = True
      #for aga in ylist:
       #print("aga ylist ",aga)
      #print ("returning ylist[xpos-1] where xpos = ",xpos," and ylist[xpos-1] = ",ylist[xpos-1])
      #return ylist[xpos-1]
      print('Warning, xwant greater than largest value in xlist. ywant will be the result of extrapolation')
    # test for edge cases.
    if not xlist[0] < xwant < xlist[len(xlist) - 1] :
      extrapolate = True
      print('Warning, xwant outside the range of xlist. ywant will be the result of extrapolation')
    if (xpos == 0):
      extrapolate = True
      print('Warning, xpos = 0. ywant will be the result of extrapolation')
      #print ("returning ylist[0] where ylist[0] = ",ylist[0])
      #return ylist[0]
    #
    # We need 3 points for each curve with the biparabolic method, so grab two 
    # two points before xwant and one point after it.
    # Build X1 and Y1 arrays of 3 points each
    # For building yarray, we don't create ypos because we can use xpos
    # We use the same positional values in the list as xpos so point match up
    #
    # Build the arrays
    if (not extrapolate):
     if ((xpos + 1) == len(xlist)):
      x1p1 = xlist[xpos - 2]
      x1p2 = xlist[xpos - 1]
      x1p3 = xlist[xpos]
      y1p1 = ylist[xpos - 2]
      y1p2 = ylist[xpos - 1]
      y1p3 = ylist[xpos]
      xlist1 = [x1p1,x1p2,x1p3]
      ylist1 = [y1p1,y1p2,y1p3]
     else:
      x1p1 = xlist[xpos - 1]
      x1p2 = xlist[xpos]
      x1p3 = xlist[xpos + 1]
      y1p1 = ylist[xpos - 1]
      y1p2 = ylist[xpos]
      y1p3 = ylist[xpos + 1]
      xlist1 = [x1p1,x1p2,x1p3]
      ylist1 = [y1p1,y1p2,y1p3]     
    #
    #Build xlist2 and ylist2 
    #print ("xpos-2 ",(xpos-2))
     if ((xpos-2) < 0):
      x2p1 = xlist[xpos - 1]
      x2p2 = xlist[xpos] 
      x2p3 = xlist[xpos + 1] 
      y2p1 = ylist[xpos - 1]
      y2p2 = ylist[xpos] 
      y2p3 = ylist[xpos + 1] 
      xlist2 = [x2p1,x2p2,x2p3]
      ylist2 = [y2p1,y2p2,y2p3]
     else: 
      x2p1 = xlist[xpos - 2]
      x2p2 = xlist[xpos - 1]
      x2p3 = xlist[xpos]
      y2p1 = ylist[xpos - 2]
      y2p2 = ylist[xpos -1] 
      y2p3 = ylist[xpos] 
      xlist2 = [x2p1,x2p2,x2p3]
      ylist2 = [y2p1,y2p2,y2p3]
#
    #print ("xlist1 and xlist2 ",xlist1," ",xlist2)
     xarray1 = np.array(xlist1)
     xpos1 = (np.abs(xarray1 - xwant)).argmin()
    #print ("xpos1 = ",xpos1," xarray1 = ",xarray1)
     xarray2 = np.array(xlist2)
     xpos2 = (np.abs(xarray2 - xwant)).argmin()
    #print ("xpos2 = ",xpos2," xarray2 = ",xarray2)
#
     threepoint_xarr1 = np.array([x1p1, x1p2, x1p3])
     threepoint_yarr1 = np.array([y1p1, y1p2, y1p3])
     threepoint_xarr2 = np.array([x2p1, x2p2, x2p3])
     threepoint_yarr2 = np.array([y2p1, y2p2, y2p3])

     #print("threepoint_xarr1 is ", threepoint_xarr1)   
     #print("threepoint_yarr1 is ", threepoint_yarr1)         
     #print("threepoint_xarr2 is ", threepoint_xarr2)         
     #print("threepoint_yarr2 is ", threepoint_yarr2)

# Generate splines and use them to calculate ywants
     parabspline1 = UnivariateSpline(threepoint_xarr1,threepoint_yarr1,k=2,s=0,ext=0)
     parabspline2 = UnivariateSpline(threepoint_xarr2,threepoint_yarr2,k=2,s=0,ext=0)
     ywant1 = parabspline1(xwant)
     ywant2 = parabspline2(xwant)
     #print("ywant1 is ", ywant1)    
     #print("ywant2 is ", ywant2)
    
    #Now use the two ywant points to calculate a weighted average
  
    #print ("Weighting Section xwant xarray1[xpos1] xarray2[xpos2] ",xwant," ",xarray1[xpos1]," ",xarray2[xpos2])
    #if xwant <= xarray1[xpos1]:       #xwant is smaller than xpos1, the so other point surrounding xwant is xpos - 1
        #x1_weight = 1 - ((xarray1[xpos1] - xwant)/(xarray1[xpos1] - xarray1[xpos1 - 1])) 
        #print("xwant <= xarray1[xpos1]")
        #print("x1_weight = ",x1_weight)
    #else:  #xwant is greater than xpos1, the so other point surrounding xwant is xpos1 + 1
        #x1_weight = 1 - ((xwant - xarray1[xpos1])/(xarray1[xpos1 + 1] - xarray1[xpos1])) 
        #print("xwant > xarray1[xpos1]")
        #print("x1_weight = ",x1_weight)
    #if xwant <= xarray2[xpos2]:       #xwant is smaller than xpos2, the so other point surrounding xwant is xpos2 - 1
        #x2_weight = 1 - ((xarray2[xpos2] - xwant)/(xarray2[xpos2] - xarray2[xpos2 - 1])) 
        #print("xwant <= xarray2[xpos2]") 
        #print("x2_weight = ",x2_weight)
    #else:  #xwant is greater than xpos2, the so other point surrounding xwant is xpos + 1
        #x2_weight = 1 - ((xwant - xarray2[xpos2])/(xarray2[xpos2 + 1] - xarray2[xpos2])) 
        #print("xwant > xarray2[xpos2]")
        #print("x2_weight = ",x2_weight)       
    
     xdiff1 = abs(xwant-threepoint_xarr1[1])
     xdiff2 = abs(xwant-threepoint_xarr2[1])
     #print ("xwant xdiff1 xdiff2 ",xwant," ",xdiff1," ",xdiff2)
     totdiff = xdiff1 + xdiff2
     yweight1 = 1.0 - (xdiff1/totdiff)
     yweight2 = 1.0 - (xdiff2/totdiff)
     #print ("totdiff yweight1 yweight2 ",totdiff," ",yweight1," ",yweight2," ")
    
#Try to deal with extreme cases

#Put Kirk and Spock method for dealing with clift cases aside
    #if (ya1_fact2 != 0):
     #kirk = ya1_fact1/ya1_fact2
    #else: 
     #kirk = 1
    #if (ya2_fact2 !=0):
     #spock = ya2_fact1/ya2_fact2
    #else:
     #spock = 1
    #if (kirk < 0.3 or kirk > 10):
     #x1_weight = 0
    #if (spock < 0.3 or spock > 10):
     #x2_weight = 0
    #print ("kirk and spock ",kirk," ",spock," ",ya1_fact1," ",ya1_fact2," ",ya2_fact1," ",ya2_fact2)
#
# Try DB's method for dealing with clift cases
     if (whatfor == 'fuelflow' or whatfor == 'fuelflow_3rd' or whatfor == 'cd_high'\
         or whatfor == 'cd' or whatfor == 'fuelflow_high' or whatfor =='maxclimbthrust'):
      if (pflag):
       print ("whatfor = ",whatfor)
       print ("y1p1 y2p2 ",y1p1," ",y2p2)
      #print ("threepoint_yarr1[0] threepoint_yarr2[1] ",threepoint_yarr1[0]," ",\
      #        threepoint_yarr2[1])
       print ("ywant1 and threepoint_yarr1[0] ",ywant1," ",threepoint_yarr1[0]) 
      if (ywant1 <= threepoint_yarr1[0]):
       #print("HERE IS THE TRUTH")
       yweight1 = 0
       yweight2 = 1.0
      #print ("ywant2 and threepoint_yarr2[1] ",ywant2," ",threepoint_yarr2[1])
      if (ywant2 <= threepoint_yarr2[1]): 
       #print("HERE IS THE TRUTH AGAIN")
       yweight2 = 0
       yweight1 = 1.0
      if (ywant1 <= threepoint_yarr1[0] and ywant2 <= threepoint_yarr2[1]): 
       #print("HERE IS THE REAL TRUTH")
       yweight1 = 0
       yweight2 = 0
      if ((ywant1*2.0) < threepoint_yarr1[0]): 
       #print("HERE IS THE NOT SO REAL TRUTH1")
       yweight1 = 0
       yweight2 = 1.0
      if ((ywant2*2.0) < threepoint_yarr1[1]): 
       #print("HERE IS THE NOT SO REAL TRUTH2")
       yweight1 = 1.0
       yweight2 = 0
#
     if (yweight1 == 0 and yweight2 == 0):
      weighted_ave = (ywant1 + ywant2)/2.0
     else:
     #weighted_ave = ((x1_weight * ywant1) + (x2_weight * ywant2)) / (x1_weight + x2_weight)
     #weighted_ave = ywant1*yweight1 + ywant2*yweight2
      weighted_ave = ywant1*yweight1 + ywant2*yweight2
      if (pflag):
       print ("weighted_ave at whatfor == ",whatfor," weighted_ave = ",weighted_ave)
     #if (whatfor == 'fuelflow_3rd' or whatfor == 'cd_high' or whatfor == 'fuelflow_high'):
      if (whatfor == 'highside'):
       if (ywant1 > ywant2):
        wa1 = ywant1
        wa2 = weighted_ave
       #weighted_ave = (wa1 + wa2)/2.0
        weighted_ave = wa1
       if (ywant2 > ywant1):
        wa1 = ywant2
        wa2 = weighted_ave
       #weighted_ave = (wa1 + wa2)/2.0
        weighted_ave = wa1
     print("weighted_ave is ", weighted_ave)
    else:
     threepoint_xarr1 = np.array(xlist)
     threepoint_yarr1 = np.array(ylist)
     print ("EXTRAPOLATE for ",xwant," ",threepoint_xarr1," ",threepoint_yarr1)
     parabspline = UnivariateSpline(threepoint_xarr1,threepoint_yarr1,k=2,s=0,ext=0)
     wa = parabspline(xwant)
     weighted_ave = wa
     print ("weighted_ave = ",weighted_ave)
    return(weighted_ave)
