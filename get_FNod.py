#NOTE:
#keys obtained from dictionaries loaded via json.loads
#are always strings. But the value associated with any 
#given key maintains its original type
#
import json, sys
import std_atm
import getCDWF
import reynolds
import CLBuffet
import getFNCNT
import get_CDWM
import get_yawdragcoefficient
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2
P8clean = False
#P8clean = True
P8podup = False
P8poddn = False
JBx37800W = True
#JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
print ("TIRGRE THE GOOD ac_model_config ",ac_model_config)
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
#
S = 1341.0

def get_FNod(ftemperature,fmach,falt,fweight,fcl,aircraft_config):
 print ("JUST STARTING get_FNod and aircraft_config = ",aircraft_config)
 print ("BLACK JAGUAR CAT JBx37800W = ",JBx37800W)
 print ("P8clean = ",P8clean)
 print ("P8podup = ",P8podup)
 exact_match = False
 CD = {}
#WF = {}
#
#CDBASE TABLES ARE UNIQUE FOR EACH MODEL 
#FOR A GIVEN MODEL, CONFIG 1&2 USE THE SAME CDBASE TABLE AND
#FLAPS 1, 5, 10, 15 (CONFIGS 3,4,5,6) USE UNIQUE TABLES
#
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8clean_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8clean_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8clean_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8clean_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8clean_CDBASE06_JSON.txt')
  #WF_file = open('P8clean_WFTAB101_JSON.txt')
#
 if (P8podup):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8podup_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8podup_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8podup_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8podup_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8podup_CDBASE06_JSON.txt')
  #WF_file = open('P8podup_WFTAB111_JSON.txt')
#
 if (P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8poddn_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8poddn_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8poddn_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8poddn_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8poddn_CDBASE06_JSON.txt')
  #WF_file = open('P8podup_WFTAB111_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('JBx37800W_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('JBx37800W_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('JBx37800W_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('JBx37800W_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('JBx37800W_CDBASE06_JSON.txt')
  #WF_file = open('JBx37800W_WFTAB101_JSON.txt')
#
 a1string = CD_file.readline()
 CD = json.loads(a1string)
 #a2string = WF_file.readline()
 #WF = json.loads(a2string)
 theta_ambient = (ftemperature + 273.15)/288.15
#fmach = ftas/(661.4786 * (theta_ambient ** (0.5)))
 int_mach = int((fmach*100.0) + .5)
 mach = str(int_mach)
 int_alt = int(falt)
 alt = str(int_alt)
#Compute the Lift Coefficient cl
#Use Eqn 9 of Chapter 9 in Jet Transport Performance Methods.pdf
 L =  fweight
 dlta = std_atm.alt2press_ratio(falt)
 print("L falt dlta fmach S = ",L," ",falt," ",dlta," ",fmach," ",S)
 if (fcl == 0.0):
  try:
   cl = L/(1481.4*(fmach**2)*dlta*S)
  except:
   cl = 1.80
  print ("COMPUTED CL = ",cl)
 else:
  cl = fcl
  print ("CL from calling program = ",cl)
 if (P8clean or P8podup or P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffetP8_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffetP8_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffetP8_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffetP8_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffetP8_06(cl,fmach)
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffet37800W_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffet37800W_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffet37800W_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffet37800W_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffet37800W_06(cl,fmach)
# 
 print ("clmax = ",clmax)
 if (clmax >= (cl*0.001)):
  cl_list = []
  cd_list = []
  available_CDmachs = []
  CDvalue_list = []
  available_CDmachs = getCDWF.getmach_fromCDBASE(aircraft_config)
  print(" ")
  print("available_CDmachs = ",available_CDmachs)
  for mgrab in available_CDmachs:
#  print("From CDBASE01 ")
#  print ("CD[mgrab] ",CD[str(mgrab)])
   cl_list = CD[str(mgrab)][0]
   cdho_list = CD[str(mgrab)][1]
   cd_list = [xcd*0.000001 for xcd in cdho_list]
   print ("cl = ",cl," ahead of getting CDvalue from Biparabolic1")
   if (isinstance(cl_list[0],str)):
    print ("ripper cat")
    clfloat = []
    for ichg in cl_list:
     clfloat.append(float(ichg))
    cl_list = clfloat
   print ("GOBLUE ",cl_list," ",cd_list)
   CDvalue = Biparabolic1(cl_list,cd_list,cl,'cd')
   CDvalue_list.append(CDvalue)
   print("For mgrab = ",mgrab," CDvalue from Biparabolic1 = ",CDvalue)
   print(" ")
 else:
  CDvalue_list = [-888]
#CDvalue = 0.22285
#
 if (CDvalue_list[0] == -888):
  theCDvalue = -888
  FFNod = -888
  D = -888
  return(cl,theCDvalue,L,D,FFNod)
 else:
  print("NOODLE call to Biparabolic1 where cl = ",cl)
  fmachable_list =[(float(xv)*0.001) for xv in available_CDmachs]
  print("fmachable_list = ",fmachable_list) 
  print("fmach and CDvalue_list = ",fmach," ",CDvalue_list)
  theCDBASEvalue = Biparabolic1(fmachable_list,CDvalue_list,fmach,'cd')
  #print("theCDBASEvalue = ",theCDBASEvalue)
  if (aircraft_config <= 6):
   theReyCDvalue = reynolds.reynolds(fweight,fmach,falt,ftemperature)
  else:
   print ("NO REYNOLDS CORRECTION FOR THIS CONFIG")
   theReyCDvalue = 0.0
  print("theReyCDvalue = ",theReyCDvalue)
  CDbasic = theCDBASEvalue + theReyCDvalue
#aircraft_config ==2 means 1LE
  if (aircraft_config == 2):
   CDwm = get_CDWM.get_CDWM(fmach,falt,cl,aircraft_config)
   CDbasic_plus_CDWM = CDbasic + CDwm
   print ("CDbasic_plus_CDWM CDbasic CDwm ",CDbasic_plus_CDWM," ",CDbasic," ",CDwm)
   estDrag = CDbasic_plus_CDWM * (1481.4*(fmach**2)*dlta*S)
   wingspan = 112.58
   moment_arm = 16.14
   #sqb =  1481.4*(fmach**2)*dlta*S*wingspan
   CDyaw = get_yawdragcoefficient.get_ydc(estDrag,dlta,S,wingspan,moment_arm,fmach,falt,aircraft_config)
   #if (fmach > 0.45):
    #theCDvalue = theCDvalue*1.061
   #else:
    #theCDvalue = theCDvalue*1.07
   theCDvalue = CDbasic_plus_CDWM + CDyaw
   print ("END OF 1LE section in get_FNod and theCDvalue = ",theCDvalue,\
          " CDbasic = ",CDbasic," CDwm = ",CDwm,\
          " CDbasic_plus_CDWM = ",CDbasic_plus_CDWM," CDyaw = ",CDyaw)
  else:
   CDwm = 0.0
   CDyaw = 0.0
   theCDvalue = CDbasic
#
  print ("theCDvalue theCDBASEvalue theReyCDvalue ",theCDvalue," ",theCDBASEvalue," ",theReyCDvalue)
  print("AGAIN L falt dlta fmach = ",L," ",falt," ",dlta," ",fmach)
#Now compute the Drag
#See A-15 in Jet Transport Performance Methods.pdf
#
  D = theCDvalue * (1481.4*(fmach**2)*dlta*S) 
  print ("Drag = ",D)
#For 1LE (aircraft_config = 2), the total DRAG must be compensated by one engine
#For non 1LE (aircraft_config != 2), the total DRAG compensation is split evenly between engines
  if (aircraft_config == 2):
   FFNod = (D/dlta)
  else:
   FFNod = (D/dlta)/2.0
  print ("Cole Younger FFNod from get_FNod = ",FFNod,)
  #FFNod = 27000.00
  return(cl,theCDvalue,L,D,FFNod)
