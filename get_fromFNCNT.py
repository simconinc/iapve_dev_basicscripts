import json,sys
import std_atm
import math
#P8clean = False
P8clean = True
P8podup = False
P8poddn = False
#JBx37800W = True
JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
#
def get_lists_fromFNCNTod(strdisa,stralt,aircraft_config):
 FNCNT = {}
 mach_list = {}
 FNCNTod_list = {}
#
#FNCNT104 IS COMMON ACROSS ALL P8 MODELS
#IT IS ONLY USED FOR 1LE (config 2)
#
 if (JBx37800W):
  FNCNT_file = open('JBx37800W_FNCNT104_JSON.txt')
 if (P8clean or P8podup or P8poddn):
  FNCNT_file = open('P8clean_FNCNT104_JSON.txt')
 #if (P8podup):
  #FNCNT_file = open('P8podup_FNCNT104_JSON.txt')
#
 a2string = FNCNT_file.readline()
 FNCNT = json.loads(a2string)
 print ("In get_lists_fromFNCNTod and stralt strdisa = ",stralt," ",strdisa)
 try:
  mach_list = FNCNT[strdisa][stralt][0]
  FNCNTod_list = FNCNT[strdisa][stralt][1]
 except:
  mach_list = [-888]
  FNCNTod_list = [-888]
 print("AT THE END OF get_max_FNCNTod for these strdisa and stralt values ",\
        strdisa," ",stralt)
 return(mach_list,FNCNTod_list)
#
def getdisa_fromFNCNT104():
#Returns the dictionary available_alts_dict that is structured as follows:
#top level keys are disa values such as '-72'
#The value associated with each top level key is a list of the available alts for that
#altitude. The alts in each list are each strings such as '37000' representing. 
#The reason they are strings is that they themselves will serve as keys
# 
 diag = 3
 #P8clean = True
 #JBx37800W = False
 #P8podup = False
 FNCNT = {}
 available_disa = [] 
 available_alts_list = []
 available_alts_dict = {}
 if (P8clean):
  FNCNT_file = open('P8clean_FNCNT104_JSON.txt') 
 if (P8podup):
  FNCNT_file = open('P8podup_FNCNT104_JSON.txt') 
 if (JBx37800W):
  FNCNT_file = open('JBx37800W_FNCNT104_JSON.txt')
 a1string = FNCNT_file.readline()
 FNCNT = json.loads(a1string)
 available_disas = FNCNT.keys()
 #str_disa = []
 #for strdisas in available_disas:
  #str_disa.append(strdisas)
 for jdisas in available_disas:
  print("FNCNT[jdisas]keys = ",FNCNT[jdisas].keys())
  for striaa in FNCNT[jdisas].keys():
   if (diag == 4):
    print ("striaa = ",iaa)
   available_alts_list.append(striaa)
   if (diag == 4): 
    print ("available_alts_list  ",available_alts_list)
  available_alts_dict.update({jdisas:available_alts_list})
  available_alts_list = []
  print ("jdisas & available_alts_dict[jdisas] = ",jdisas," ",available_alts_dict[jdisas])
 return(available_alts_dict)

