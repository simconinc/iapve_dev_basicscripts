def Linear(xlist, ylist, xwant):
#
#### Purpose ####
#Linear interpolation for y based on two points in x and y
#This is a quick implementation of linear interpolation; might need to be replaced by 
#a more comprehensive version in the future
#
    from bisect import bisect_left
#
    # First determine if we have a direct match on
    # xwant within xlist
    print("start of Linear with requested x value = ",xwant)
    #print("xlist ",xlist)
    #print("ylist ",ylist)
    if (len(xlist) == 1):
     #print ("len of xlist is 1 so returning ylist[0] = ",ylist[0])
     return (ylist[0])
    il_cnt = 0
    for ilook in xlist:
     if (ilook == xwant):
      #print ("ilook == xwant which is ",xwant," so returning ylist[il_cnt] = ",ylist[il_cnt],"with il_cnt = ",il_cnt)
      return(ylist[il_cnt])
     il_cnt = il_cnt + 1
    #
    #No direct match so proceed with linear interpolation
    xpos = bisect_left(xlist, xwant)
    #print ("xpos len(xlist) ",xpos," ",len(xlist))
    #print ("xpos len(ylist) ",xpos," ",len(ylist))
    #
    if (xpos >= len(xlist)):
     return(ylist[-1])
    if (xpos < len(xlist) and xpos > 0):
     yval = ylist[xpos-1] + (ylist[xpos]-ylist[xpos-1])*((xwant - xlist[xpos-1])/(xlist[xpos]-xlist[xpos-1]))
     return(yval)
    if (xpos == 0):
     print ("returning ylist[0] where ylist[0] = ",ylist[0])
     return(ylist[0])
    