#table_maker.py
#This program reads from an INFLT .dat file and parses certain tables
#It writes each section of a parsed table data to a corresponding ..._tbl.txt file.
#The sections are delimited by '*'.  Lines within each section are delimited by
#a small character sequence such as '1#'.
#table_maker.py is meant to be run stand alone to produce ..._tbl.txt files.
#However, the table making code sections of table_maker.py can be found
#within trialobyte.py as one of the features of trialobyte.py is that
#it creates ..._tbl.txt files anew each time it is run.  

import json
import sys
import re

import csv
import numpy as np

#data = numpy.loadtxt('JBx37800W.dat')
#dest_path = "C:/python276/WFTAB101_table.txt"
#dest_path = "C:/python276/P8Clean_WFTAB101_table.txt"

#newarray = np.fromfile('JBx37800W.dat',dtype=int)
#with open(newarray,'r'):
#    data_iter = csv.reader(dest_path, 
#                           delimiter = delimiter, 
#                           quotechar = '11/n')
#    data = [data for data in data_iter]
#data_array = np.asarray(data, dtype=None, order=None)
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE input file
INFLT_file = open('JBx37800W.dat','rU')
#INFLT_file = open('model_clean_v2p0_JBe.dat','rU')
#INFLT_file = open('model_podup_v2p0_JBe.dat','rU')
#INFLT_file = open('37800WCFM56_57.dat','rU')
#INFLT_file = open('model_poddn_v1p2_JBe.dat','rU')
#***
#
i = 1
foundtable = False
#astr is where each line of a given table is appended to in order to create
#one long string making up that table.  astr is always initialized to null
astr = ""

tables = []  #for storing a list of tables, each a string
tbl_dict = []   #for storing a list of dictionaries
subtab = []
independent_vars = []
index11 = []
fuel_table = []
WFlist = []
dict = {}
crawdad = {}
k = 0

#the following while loop extracts certain tables and stores them as 
#strings in the tables list
#The while loop below stays in effect until i is set to 0
while(i):
    astring = INFLT_file.readline()
#   print "astring = ",astring
#Check if astring contains a start indicator for a table
#The string /TBLU indicates the start of the very 1st table
#An H or a / indicates end of a table and the possible start of a new table
    if (not foundtable and astring[0:5] == "/TBLU"):
        foundtable = True 
#       print "FOUND TBLU"
    elif(foundtable and (astring[0:1] == "H" or astring[0:1] == '/')):
        tables.append(astr)
#       print "astr[0:7] and tables[k] ",astr[0:7]," ",tables[k][0:27]
        foundtable = False 
#       print "foundtable astring[0:1] = H or / ", foundtable," ",astring[0:1]
        k = k + 1
        astr = ""
    elif(foundtable and astring != ""):
        astr = astr + astring
#astring == "" means we have reached the end of the file 
    if (astring == ""):
        tables.append(astr)
        i = 0
#print "fplans equals", fplans           
#print "out of while loop"

for t in tables:
    print ("START of t in tables")
# when you specify '\n' python treats it as hex0a
# when you specify '\r' python treats it as hex0D
# therefore
# p.replace('\n', ' ') replaces all occurrences of hex0A with hex20
# p.replace('\r', ' ') replaces all occurrences of hex0D with hex20
# it is necessary to replace in the string p all occurences of hex0A with hex20 and all occurences hex0D with hex20 in order for json.loads(p) to work properly
#   t = t.replace('\n', ' ').replace('\r', ' ')
    print ("t ",t[0:272])
    print ("END OF THE TABLE t")   
print ("Finished reading the file")  
#
#At this point in the process each member of the tables list is a string consisting
#of an entire table starting with the table identifier (e.g. CDBASE01)
#
#Now we are going to find the member of the tables list containing the string
#for the table WFTAB101 and process that table 
#we know from the INFLT User Manual that WFTAB101 is based on WF = F(W/delta,mach,alt)
#
#We want to split the WFTAB101 table into 4 parts; part 1 consists of the independent variable 
#FNod values, part 2 consists of the independent variable mach values, part 3 consists of the
#independent variable altitude values, and part 4 consists of the dependent variable WF values.
#
#open file to write the WFTAB101 table to 
for w in tables:
 #if (w[0:8] == 'WFTAB101' or w[0:8] == 'WFTAB111' or w[0:8] == 'WFTAB104'):
  #if (w[0:8] == 'WFTAB101'):
   #print ("found WFTAB101")
  #if (w[0:8] == 'WFTAB111'):
   #print ("found WFTAB111")
  #if (w[0:8] == 'WFTAB114'):
   #print ("found WFTAB114")
 if (w[0:8] == 'WFTAB104'):
  print ("found WFTAB104")
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for WFTAB101
  #fn = "JBx37800W_WFTAB104_tbl.txt"
  #fn = "notneededP8clean_WFTAB101_tbl.txt"
  #fn = "notneededJBx37800W_WFTAB101_tbl.txt"
  fn = "notneeded.txt"
  #fn = "37800WCFM56_57_WFTAB101_tbl.txt"
  #fn = "P8podup_WFTAB111_tbl.txt"
  #fn = "P8podup_WFTAB114_tbl.txt"
#***
#
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  print ("indpendent_var[2] ",independent_vars[2])
  FNod = independent_vars[0]
  FNod = FNod.replace('1\n', '1#').replace('\r','')
  FNod = FNod.replace('\n','')
  target.write(FNod)
  target.write('*\n')
  mach = independent_vars[1]
  print ("the initial entire mach string ",mach)
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  print ("the mach string after replacements ",mach)
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[2]
  alt = alt.replace('\n','').replace('\r','')
  target.write(alt)
  target.write('*\n') 
  print ("FNod ",FNod)
  print ("mach ",mach)
  print ("alt ",alt)

#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS")
  print ("index11[2] ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  #WF = w[index11[3]:]  SHOULD NEVER HAVE BEEN w[index11[3]:] for any of the WFTABij's
  WF = w[index11[2]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(WF)
  target.write('*')
  print ("WF VALUES ")
  print (WF)
  print ("DONE PRINTING WF VALUES")
# print "Complete Fuel Table"
# print fuel_table
#
#!#%
#BEGIN FNCLB101
#open file to write the FNCLB101 table to 
for w in tables:
 if (w[0:8] == 'FNCLB101'): 
  print ("found FNCLB101")
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for FNCLB101
  #fn = "notneededJBx37800W_FNCLB101_tbl.txt"
  fn = "notneeded.txt"
  #fn = "P8podup_FNCLB101_tbl.txt"
#***
#
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  print ("indpendent_var[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the FNCLBod values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS AND PANTHERS")
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMEJAGS ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  FNCLB = w[index11[2]:]
  target.write(FNCLB)
  target.write('*')
  print ("FNCLB VALUES ")
  print (FNCLB)
  print ("DONE PRINTING FNCLB VALUES")
#!#%
#
#BEGIN FNCRU101
#open file to write the FNCRU101 table to 
for w in tables:
 if (w[0:8] == 'FNCRU101'): 
  print ("found FNCRU101")
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for FNCRU101
  fn = "JBx37800W_FNCRU101_tbl.txt"
  #fn = "P8podup_FNCRU101_tbl.txt"
  #fn = "notneeded.txt"

#***
#
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  print ("indpendent_var[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the FNCLBod values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are more JAGUARS AND PANTHERS")
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMEJAGS ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  FNCRU = w[index11[2]:]
  target.write(FNCRU)
  target.write('*')
  print ("FNCRU VALUES ")
  print (FNCRU)
  print ("DONE PRINTING FNCRU VALUES")
#!#%
#
# FNCNT104
#
#BEGIN FNCNT104
#open file to write the FNCNT101, 04 .. table to 
for w in tables:
 if (w[0:8] == 'FNCNT104'): 
  print ("found FNCNT104")
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for FNCRU101
  #fn = "JBx37800W_FNCNT104_tbl.txt"
  #fn = "P8podup_FNCNT104_tbl.txt"
  fn = "notneeded.txt"
#***
#
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  print ("indpendent_var[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the FNCNTod values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are more JAGUARS AND PANTHERS")
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMEJAGS ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  FNCNT = w[index11[2]:]
  target.write(FNCNT)
  target.write('*')
  print ("FNCNT VALUES ")
  print (FNCNT)
  print ("DONE PRINTING FNCNT VALUES")
#!#%
#
#END FNCNT104
#
#
# APUWF01
#
#BEGIN APUWF01
#open file to write the APUWF01 table to 
for w in tables:
 #if (w[0:7] == 'APUWF01'): 
 if (w[0:7] == 'APUWF27'): 
  print ("found APUWF27")
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for FNCRU101
  #fn = "P8clean_APUWF01_tbl.txt"
  #fn = "P8podup_APUWF01_tbl.txt"
  fn = "notneeded.txt"
#***
#
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  #print ("indpendent_var[2] ",independent_vars[2])
  alt = independent_vars[0] + "1#"
  alt = alt.replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  print ("alt ",alt)
  mach = independent_vars[1] + "1#"
  #mach = mach.replace('11\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  print ("mach ",mach)
#
#Now we want to find the index in w which is the start of the APUWF values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are more JAGUARS AND PANTHERS")
  print ("index11[0] index11[1] ",index11[0],\
         " ",index11[1])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  #outer2 = index11[2] + 100
  print ("HOMEJAGS ",w[index11[1]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  #print ('2 ',w[index11[2]:outer2]) 
  APUWF = w[index11[1]:]
  target.write(APUWF)
  target.write('*')
  print ("APUWF VALUES ")
  print (APUWF)
  print ("DONE PRINTING APUWF VALUES")
#!#%
#
#END APUWF
#
#
#BEGIN WFIDL101
#open file to write the WFIDL101 table to 
for w in tables:
 if (w[0:8] == 'WFIDL101'): 
  print ("found WFIDL101")
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for WFIDL101
  #fn = "notneeded_JBx37800W_WFIDL101_tbl.txt"
  #fn = "P8clean_WFIDL101_tbl.txt"
  #fn = "P8podup_WFIDL101_tbl.txt"
  fn = "notneeded.txt"
#***
#
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  #independent_vars = w[22:].split("11\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  print ("indpendent_var[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS AND ALIENS")
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMER ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  WFIDL = w[index11[2]:]
  target.write(WFIDL)
  target.write('*')
  print ("WFIDL VALUES ")
  print (WFIDL)
  print ("DONE PRINTING WFIDL VALUES")
#!#%
#
#END WFIDL101 section
#
 if (w[0:8] == 'CDBASE05'):
  print ("found CDBASE05")
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for CDBASE01
  #fn = "JBx37800W_CDBASE04_tbl.txt"
  #fn = "notneededJBx37800W_CDBASE01_tbl.txt"
  #fn = "notneededP8clean_CDBASE01_tbl.txt"
  #fn = "P8clean_CDBASE06_tbl.txt"
  #fn = "P8poddn_CDBASE06_tbl.txt"
  #fn = "P8poddn_CDBASE01_tbl.txt"
  #fn = "P8clean_CDBASE04_tbl.txt"
  #fn = "P8clean_CDBASE05_tbl.txt"
  #fn = "P8podup_CDBASE05_tbl.txt"
  #fn = "37800WCFM56_57_CDBASE01_tbl.txt"
  #fn = "P8podup_CDBASE06_tbl.txt"
  #fn = "P8clean_CDBASE04_tbl.txt"
  fn = "notneeded.txt"
  target = open(fn,'w')
  target.truncate()
#***
#
  independent_vars = w[9:].split("1cl\n")
  cl = independent_vars[0]
  cl = cl.replace('cl\n', 'cl#').replace('\r','')
  print ("cl = ",cl)
  target.write(cl)
  target.write('*\n')

  mach_and_cd = independent_vars[1].split("1mach\n")
  mach = mach_and_cd[0]
  print ("mach_and_cd = ",mach_and_cd)
  print ("mach = ",mach)
  mach = mach.replace('mach\n', 'mach#').replace('\r','')
  target.write(mach)
  target.write('*\n')
  
  cd = mach_and_cd[1]
  target.write(cd)
  target.write('*') 
#
#Now we are going to find the member of the tables list containing the string
#for the table FNBLDF01 and process that table 
#we know from the INFLT User Manual that FNBLDF01 is based on FN = F(mach,alt)
#
#We want to split the FNBLDF01 table into 3 parts; part 1 consists of the independent variable 
#mach values, part 2 consists of the independent variable alt values, and part 3 consists of the 
#dependent variable FN values.
#
#open file to write the FNBLDF01 table to 
independent_vars = []
index11 = []
for w in tables:
 if (w[0:8] == 'FNBLDF01'):
  print ("found FNBLDF01")
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for FNBLDF01
  #fn = "JBx37800W_FNBLDF01_tbl.txt"
  #fn = "P8podup_FNBLDF01_tbl.txt"
  #fn = "notneeded_JBx37800W_FNBLDF01_tbl.txt"
  #fn = "notneeded_P8clean_FNBLDF01_tbl.txt"
  #fn = "37800WCFM56_57_WFTAB101_tbl.txt"
  fn = "notneeded.txt"
#***
#
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  print ("HERE IS ALT ",alt) 
  #alt = mach.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  index11 = []
#Now we want to find the index in w which is the start of the FN values
#this would be right after the 2nd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
# print "index11[2] ",index11[2]
# print "Here"  
# outer = index11[2] + 100
# print w[index11[2]:outer]  
  FNbld = w[index11[1]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(FNbld)
  target.write('*')
  print ("FNbld Values")
  print (FNbld)
  print ("DONE PRINTING FNbld VALUES")
#!!!
#
#Now we are going to find the member of the tables list containing the string
#for the table TABCORF01 and process that table 
#we know from the INFLT User Manual that TABCORF01 is based on WFdelta = F(W/delta,mach,alt)
#
#We want to split the TABCORF01 table into 4 parts; part 1 consists of the independent variable 
#FNod values, part 2 consists of the independent variable mach values, part 3 consists of the
#independent variable altitude values, and part 4 consists of the dependent variable WF values.
#
#open file to write the TABCORF01 table to 
independent_vars = []
index11 = []
fuel_table = []
CORFlist = []
for w in tables:
 print ("TRYING TO FIND TABCORF01 and w = ",w)
 print ("w[0:9]",w[0:9]) 
 if (w[0:9] == 'TABCORF01'):
  print ("found TABCORF01")
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file for TABCORF01
  fn = "notneeded.txt"
  #fn = "P8podup_TABCORF01_tbl.txt"
  #fn = "37800WCFM56_57_WFTAB101_tbl.txt"
  #fn = "notneededP8clean_TABCORF01_tbl.txt"
#***
#
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[9:].split("11\n")
  FNod = independent_vars[0]
  FNod = FNod.replace('1\n', '1#').replace('\r','')
  FNod = FNod.replace('\n','')
  target.write(FNod)
  target.write('*\n')
  mach = independent_vars[1]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[2]
  alt = alt.replace('\n','').replace('\r','')
  target.write(alt)
  target.write('*\n') 
  print ("FNod ",FNod)
  print ("mach ",mach)
  print ("alt ",alt)

#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
# print "index11[2] ",index11[2]
# print "Here"  
# outer = index11[2] + 100
# print w[index11[2]:outer]  
  CORF = w[index11[2]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(CORF)
  target.write('*')
  print ("CORF VALUES ")
  print (CORF)
  print ("DONE PRINTING CORF VALUES")
# print "Complete Fuel Table"
# print fuel_table
#
#
#Now we are going to find the member of the tables list containing the string
#for the table CDPSI01 and process that table 
#we know from the INFLT User Manual that CDPSI01 is based on CD = f(CN) 
#where CN = yawing moment coefficient
#The CDPSI01 table has only one part because there is only 1 indpendent variable 
#
#CDPSI01 is the table header for PSI data
independent_vars = []
for w in tables:
 print ("TRYING TO FIND CDPSI01 and w = ",w)
 print ("w[0:7]",w[0:7]) 
 if (w[0:7] == 'CDPSI01'):
  print ("found CDPSI01")
#
#the parser.get command is what will be used when trialobyte takes over
  #tbl_out_CDPSI01 = parser.get('table_maker_output_CDPSI01', 'tbl_out_CDPSI01')
  #tbl_out_CDPSI01 = "P8clean_CDPSI01_tbl.txt"
  tbl_out_CDPSI01 = "notneeded.txt"
#open file to write the CDPSI01 table to 
  target = open(tbl_out_CDPSI01,'w')
  target.truncate()
#
  print ("w 8: ",w[8:])
  independent_vars = w[8:].split("1cn\n")
  cn = independent_vars[0]
  cn = cn.replace('  cn\n','').replace('\r','')
  print ("cn = ",cn)
  target.write(cn)
  target.write('*\n')
  cdpsi = independent_vars[1]
  cdpsi = cdpsi.replace('\n','').replace('\r','')
  print ("cdpsi = ",cdpsi)
  target.write(cdpsi)
#
#Now we are going to find the member of the tables list containing the string
#for the table CDWM01 and process that table 
#The CDWM01 table has two one parts because there are 2 indpendent variables 
#
#open file to write the CDWM01 table to
#BEGIN CDWM01, this is the table header for Yaw Drag data
#open file to write the CDWM01 table to 
independent_vars = []
index11 = []
#
for w in tables:
 if (w[0:6] == 'CDWM01'):
  print ("found CDWM01")
#the parser.get command is what will be used when trialobyte takes over 
  #tbl_out_CDWM01 = parser.get('table_maker_output_CDWM01', 'tbl_out_CDWM01')
  #tbl_out_CDWM01 = "P8clean_CDWM01_tbl.txt"
  #tbl_out_CDWM01 = "P8poddn_CDWM01_tbl.txt"
  #tbl_out_CDWM01 = "JBx37800W_CDWM01_tbl.txt"
  tbl_out_CDWM01 = "notneeded.txt"
  target = open(tbl_out_CDWM01,'w')
  target.truncate()
  independent_vars = w[7:].split("11\n")  
  print ("independent_vars[0] ",independent_vars[0])
  CL = independent_vars[0]
  CL = CL.replace('1\n', '1#').replace('\r','')
  CL = CL.replace('\n','')
  target.write(CL)
  target.write('*\n')
  mach = independent_vars[1]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
#Now we want to find the index in w which is the start of the CD values
#this would be right after the 2nd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS")
  print ("index11[1] ",index11[1])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  #outer2 = index11[2] + 100
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  #print ('2 ',w[index11[2]:outer2]) 
  CD = w[index11[1]:]
  print ("CD VALUES ")
  print (CD)
  target.write(CD)
  target.write('*')
  print ("DONE PRINTING CD VALUES")
# print "Complete CDWM Table"
#
#!!!
#At this point in the process we have written out four string objects
#to the files WFTAB101_tbl.txt and CDBASE01_tbl.txt or to similarly named
#output files.
#For WFTAB101 these four string objects are:
#   FNod - contains the entire set of thrust over delta values as one string
#   mach - contains the entire set of mach values as one string
#   alt - contains all of the altitude values as one string
#   WF - contains the entire set of fuel flow values as one string
#Within WFTAB101_tbl.txt, the four strings are separated by the delimiter '*'
#

