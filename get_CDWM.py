#NOTE:
#keys obtained from dictionaries loaded via json.loads
#are always strings. But the value associated with any 
#given key maintains its original type
#
import json, sys
import std_atm
import getCDWF
import reynolds
import CLBuffet
import getFNCNT
import Linear
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2
P8clean = False
#P8clean = True
P8podup = False
P8poddn = False
JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
print ("TIRGRE THE GOOD ac_model_config ",ac_model_config)
#
if (ac_model_config[0:2] == "p8"):
 P8 = True
else:
 P8 = False
#
if (ac_model_config == "37800W\n"):
 P8 = False
 JBx37800W = True
#
S = 1341.0
#
def get_CDWM(fmach,falt,fcl,aircraft_config):
 if (aircraft_config != 2):
  print ("YOU HAVE NO BUSINESS CALLING get_CDWM since aircraft_config not= 2")
  return(0,0,0,0,0)
 print ("JUST STARTING get_CDWM and aircraft_config = ",aircraft_config)
 print ("BLACK JAGUAR CAT JBx37800W = ",JBx37800W)
 print ("P8 = ",P8)
 CD = {}
#
 if (P8):
  if (aircraft_config == 2):
   CD_file = open('P8clean_CDWM01_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 2):
   CD_file = open('JBx37800W_CDWM01_JSON.txt')
#
 a1string = CD_file.readline()
 CD = json.loads(a1string)
 print ("JIGTIME CD = ",CD)
 int_mach = int((fmach*100.0) + .5)
 mach = str(int_mach)
 int_alt = int(falt)
 alt = str(int_alt)
 cl = fcl
 print ("CL from calling program = ",cl)
#
 cl_list = []
 cd_list = []
 available_CDmachs = []
 CDvalue_list = []
 available_CDmachs = getCDWF.getmach_fromCDWM(aircraft_config)
 print(" ")
 print("available_CDmachs = ",available_CDmachs)
 for mgrab in available_CDmachs:
  cl_list = CD[str(mgrab)][0]
  cdho_list = CD[str(mgrab)][1]
  cd_list = [xcd*0.000001 for xcd in cdho_list]
  print ("cl = ",cl," ahead of getting CDvalue from Linear")
  if (isinstance(cl_list[0],str)):
   print ("ripper cat")
   clfloat = []
   for ichg in cl_list:
    clfloat.append(float(ichg))
   cl_list = clfloat
  print ("GOBLUE ",cl_list," ",cd_list)
  #CDvalue = Biparabolic1(cl_list,cd_list,cl,'cd')
  CDvalue = Linear.Linear(cl_list,cd_list,cl)
  CDvalue_list.append(CDvalue)
  print("For mgrab = ",mgrab," CDvalue from Linear = ",CDvalue)
  print(" ")
#CDvalue = 0.22285
#
 print ("CDvalue_list = ",CDvalue_list)
 if (CDvalue_list[0] == -888):
  theCDvalue = -888
  return(theCDvalue)
 else:
  print("call to Linear where cl = ",cl)
  fmachable_list =[(float(xv)*0.001) for xv in available_CDmachs]
  print("fmachable_list = ",fmachable_list) 
  print("fmach and CDvalue_list = ",fmach," ",CDvalue_list)
  theCDvalue = Linear.Linear(fmachable_list,CDvalue_list,fmach)
  print ("theCDvalue = ",theCDvalue)
  return(theCDvalue)
