#NOTE:
#keys obtained from dictionaries loaded via json.loads
#are always strings. But the value associated with any 
#given key maintains its original type
#
import json, sys
import std_atm
import getfromAPUWF
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2
import Linear
P8clean = False
#P8clean = True
P8podup = False
P8poddn = False
JBx37800W = True
#JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
print ("TIRGRE THE GOOD ac_model_config ",ac_model_config)
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
#
S = 1341.0

def get_APUwf(fmach,falt,aircraft_config):
 print ("JUST STARTING get_APUwf and aircraft_config = ",aircraft_config)
 print ("BLACK JAGUAR CAT JBx37800W = ",JBx37800W)
 print ("P8clean = ",P8clean)
 print ("P8podup = ",P8podup)
 exact_match = False
 WF = {}
#
 if (P8clean):
  if (aircraft_config == 2):
   WF_file = open('P8clean_APUWF01_JSON.txt')
#
 if (P8podup or P8poddn):
  if (aircraft_config >= 0 and aircraft_config <= 6):
   WF_file = open('P8podup_APUWF01_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 2):
   WF_file = open('JBx37800W_APUWF01_JSON.txt')
  #if (aircraft_config == 3):
   #WF_file = open('JBx37800W_APUWF03_JSON.txt')
  #if (aircraft_config == 4):
   #WF_file = open('JBx37800W_APUWF04_JSON.txt')
  #if (aircraft_config == 5):
   #WF_file = open('JBx37800W_APUWF05_JSON.txt')
  #if (aircraft_config == 6):
   #WF_file = open('JBx37800W_APUWF06_JSON.txt')
#
 a1string = WF_file.readline()
 WF = json.loads(a1string)
 int_mach = int((fmach*100.0) + .5)
 mach = str(int_mach)
 if (int_mach > 0):
  alt_list = []
  WF_list = []
  available_WFmachs = []
  WFvalue_list = []
  available_WFmachs = getfromAPUWF.getmach_fromAPUWF(aircraft_config)
  print(" ")
  print("available_WFmachs = ",available_WFmachs)
  for mgrab in available_WFmachs:
#  print("From APUWF01 ")
#  print ("WF[mgrab] ",WF[str(mgrab)])
   alt_list = WF[str(mgrab)][0]
   WF_list = WF[str(mgrab)][1]
   WF_list = [xWF*1.0 for xWF in WF_list]
   print ("falt = ",falt," ahead of getting WFvalue from Biparabolic1")
   if (isinstance(alt_list[0],str)):
    print ("ripper cat")
    altfloat = []
    for ichg in alt_list:
     altfloat.append(float(ichg))
    alt_list = altfloat
   print ("GOBLUE ",alt_list," ",WF_list)
   WFvalue = Linear.Linear(alt_list,WF_list,falt)
   WFvalue_list.append(WFvalue)
   print("For mgrab = ",mgrab," WFvalue from Biparabolic1 = ",WFvalue)
   print(" ")
 else:
  WFvalue_list = [-888]
#WFvalue = 0.22285
#
 if (WFvalue_list[0] == -888):
  theWFvalue = -888
  return(theWFvalue)
 else:
  print("NOODLE call to Biparabolic1 where falt = ",falt)
  fmachable_list =[(float(xv)*0.001) for xv in available_WFmachs]
  print("fmachable_list = ",fmachable_list) 
  print("fmach and WFvalue_list = ",fmach," ",WFvalue_list)
  theAPUWFvalue = Linear.Linear(fmachable_list,WFvalue_list,fmach)
  print ("theAPUWFvalue = ",theAPUWFvalue)
  print("AGAIN falt fmach = ",falt," ",fmach)
  return(theAPUWFvalue)
