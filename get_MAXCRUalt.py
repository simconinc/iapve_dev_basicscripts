import get_aeropfm
import std_atm
import math
#def get_MAXCRUalt(ftemperature,ftas,fmach,falt,fweight,aircraft_model,aircraft_config,pflag):
def get_MAXCRUalt(disa,ftas,fmach,falt,fweight,aircraft_model,aircraft_config,pflag):
#G is the acceleration of gravity in ft/sec**2
 G = 32.17405
 G_ft_per_min_sqrd = 32.17405*3600.00
 thrust_diff = 100000.00
#
 ftascMAXCRUalt = True
 #ftascMAXCRUalt = False
 ftemperature = std_atm.isa2temp(disa,falt)
 pflag = 0
 pflag = 1
 if (pflag): print ('disa ftemperature ',disa,' ',ftemperature)
 cMAXCRUalt = falt
 if (aircraft_config == 2):
  MAXCRU_alt_delta = 32000.00 - falt
 else:
  MAXCRU_alt_delta = 43000.00 - falt
 print ("MAXCRU_alt_delta = ",MAXCRU_alt_delta)
 higher = False
 lower = False
 i = 1
 while_cnt = 0
#ITERATE ON ALTITUDE
 while (i==1 and while_cnt < 25):
  while_cnt = while_cnt + 1
  if (pflag): print ("cMAXCRUalt at top = ",cMAXCRUalt)
  ftemperature = std_atm.isa2temp(disa,cMAXCRUalt)
  if (pflag): print ('disa ftemperature ',disa,' ',ftemperature)
#
  if (aircraft_config == 2):
   maxthrustod = get_aeropfm.get_maxconthrust(ftemperature,cMAXCRUalt,fmach,aircraft_model,aircraft_config,pflag)
  else:
   maxthrustod = get_aeropfm.get_maxcruisethrust(ftemperature,cMAXCRUalt,fmach,aircraft_model,aircraft_config,pflag)
  if (pflag): print ("MAXIMUS")
  dlta = std_atm.alt2press_ratio(cMAXCRUalt)
  if (pflag): print ("dlta = ",dlta)
  maxthrust = maxthrustod * dlta
  if (pflag): print("maxthurstod maxthrust ",maxthrustod," ",maxthrust)
#
  fcl = 0.0
  #geefactor = 1.035
  geefactor = 1.03528
  (lift_coeff,clmax,drag_coeff,Lift,Drag,ffnod) = get_aeropfm.get_FNod_cruise(geefactor,ftemperature,fmach,\
   cMAXCRUalt,fweight,fcl,aircraft_model,aircraft_config,pflag)
  Lift_15degbank = Lift
  Drag_15degbank = Drag
  pflag = 1
  if (pflag): print ("below are lift_coeff,clmax,drag_coeff,Lift_15degbank,Drag_15degbank")
  if (pflag): print (lift_coeff," ",clmax," ",drag_coeff," ",Lift," ",Drag)
  if (pflag): print("maxthrust Drag ",maxthrust," ",Drag)
  if (pflag): print ("ftas fweight ",ftas," ",fweight)
  pflag = 0
#
  try:
   CL_15degbank = lift_coeff
  except:
   CL_15degbank = 0.0
  #cl = 0.0
  #(dummy,lift_coeff,drag_coeff,Lift,Drag,ffnod) = \
   #get_aeropfm.get_FNod_cruise(gfactor,ftemperature,\
   #fmach,cMAXCRUalt,fweight,cl,aircraft_model,aircraft_config,pflag)
  #if (pflag): print("maxthrust Drag_15degbank ",maxthrust," ",Drag_15degbank)
#
  #thrust_diff = maxthrust - Drag_15degbank
  thrust_diff = maxthrust - Drag_15degbank
#
  pflag = 1
  if (pflag): print ("MAIN STOP cMAXCRUalt CL_15degbank thrust_diff ",cMAXCRUalt," ",CL_15degbank," ",thrust_diff)
  #if (abs(thrust_diff) < 0.5):
  #if (abs(thrust_diff) <= 25.0):
  #if (abs(thrust_diff) <= 6.0):
  if (abs(thrust_diff) <= 0.5):
   i = 0
  else:
   if (thrust_diff > 1.5 and Drag >0.0):
    cMAXCRUalt = cMAXCRUalt + MAXCRU_alt_delta
    higher = True
   if (thrust_diff < -1.5 or Drag <= 0.0):
    cMAXCRUalt = cMAXCRUalt - MAXCRU_alt_delta
    lower = True
   if (pflag): print ("cMAXCRUalt = ",cMAXCRUalt)
   MAXCRU_alt_delta = MAXCRU_alt_delta/2.0
  if (pflag): print ("PERRY MASON MAXCRU_alt_delta = ",MAXCRU_alt_delta)
  if (pflag): print ("BEFORE RETURN FROM get_MAXCRUalt and lift_coeff = ",lift_coeff)
  if (abs(MAXCRU_alt_delta) < 0.1 and abs(thrust_diff) > 25.0):
   print ("MAXCRUALT THROWING IN THE TOWL ON CONVERGENCE with cMAXCRUalt = ",cMAXCRUalt," MAXCRU_alt_delta = ",MAXCRU_alt_delta) 
   cMAXCRUalt = -999
   i = 0
 pflag = 0
 return(cMAXCRUalt,maxthrust,lift_coeff,clmax,drag_coeff,Lift_15degbank,Drag_15degbank)


