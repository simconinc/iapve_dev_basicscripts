import std_atm
from Biparabolic1 import Biparabolic1
import get_lists_forWF
import getCDWF
#This can be used for getting the WF and FuelFlow values for a given combination
#of altitude, mach (expressed as mach*10**3), temperature (in deg C) and thrust over delta
def get_WF_FuelFlow(ialt,imach,ftemperature,Tovrdlt):
#
#Establish WFalts_mach_dict dictionary for use further down
#
   """
   available_WFalts_machs_dict = {}
   available_WFalts_machs_dict = getCDWF.getalt_fromWFTAB101()
#
   sensible_mach_list = []
   sensible_alt_list = []
#
   for tryalt in available_WFalts_machs_dict.keys():
    checkalt = float(tryalt)
    differ = abs(falt - checkalt)
    if (differ <= 10000.00):
     sensible_alt_list.append(tryalt)
   print ("sensible_alt_list = ",sensible_alt_list)
#  for ialt in available_WFalts_machs_dict.keys():
#
   for ialt in sensible_alt_list:
    if (ffnod == 0):
     print("BREAK THE FOR LOOP RIGHT HERE SINCE ffnod = 0")
     break
   for trymach in available_WFalts_machs_dict[ialt]:
    checkmach = float(trymach)/1000.00
    differ = abs(fmach - checkmach)
    if (differ <= 0.25): 
     sensible_mach_list.append(trymach)
   print("sensible_mach_list = ",sensible_mach_list)
#
   """
   ffnodlist,wfvaluelist = get_lists_forWF.get_lists_forWF(ialt,imach)
   print("below are ffnodlist and WFvaluelist")
   print (ffnodlist)
   print (wfvaluelist)
   fnoodle =[float(xv) for xv in ffnodlist]
   ffnod = Tovrdlt/2.0
   WFvalue_mach = Biparabolic1(fnoodle,wfvaluelist,ffnod,'fuelflow')
#
   theta_ambient = (ftemperature + 273.15)/288.15
   falt = float(ialt)
   fmach = float(imach)*0.001
   dlta = std_atm.alt2press_ratio(falt)
   print ("theta_ambient dlta fmach ",theta_ambient," ",dlta," ",fmach)
   total_theta = theta_ambient*(1 + 0.2*(fmach**2))
   tpm = (1 + 0.2*(fmach**2))
   total_dlta = dlta*(tpm**3.5)
   print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
   #Fuel_flow = theWFvalue * ((total_theta**0.60) * total_dlta) * 2.0
   Fuel_Flow = WFvalue_mach * ((total_theta**0.60) * total_dlta) * 2.0
   print ("WFvalue_mach = ",WFvalue_mach," Fuel_Flow = ",Fuel_Flow)
   return(Fuel_Flow)
