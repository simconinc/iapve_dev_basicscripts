#get_CD_FN_forR
import sys
import get_aeropfm
#
#Run like this:
#python get_CD_FN_forR.py ftemperature fmach falt fweight fcl aircraft_model aircraft_config type 
#Where:
#ftemperature is the ISA temperature deviation which is found in Col E of the IAPVE spread sheet
#fmach is the mach speed which is found in Col H of the IAPVE spread sheet
#falt is the flight level in feet which is found in Col M of the IAPVE spread sheet
#fweight is the aircraft weight which is found in Col G of the IAPVE spread sheet
#fcl is the lift coefficient. Note the following:
#  Set fcl to 0.0 if you want get_FNod to compute the lift coefficient for you based on the weight, mach speed, and
#  value for S.
#  Set fcl to a known value of lift coefficient if you want the lift, drag coefficient, and drag computed based on it
#aircraft_model is the aircraft model found in Col AR of the spread sheet
#aircraft_configuration is the aircraft configuration found in Col AS of the spread sheet
#type is the type of computation. Current choices are:
#
#  'cruisethrust' - this causes get_FNod_cruise to be called which returns the following parameters:
#     (lift_coeff,cl_max,drag_coeff,Lift,Drag,ffnod) 
#      where:
#       ffnod = required_cruisethrust/dlta-engine
#     Each of the returned parameters is written to a separate line of the file aerodata_for_R.txt 
#     in the order given above.
#
#  'maxclimbthrust' - this causes get_maxclimbthrust to be called which returns the following parameter:
#      total_available_climb_thrust/dlta -  This is written to a single line of the
#      file aerodata_for_R.txt.
#
#NOTE 1:
#    The value of required_cruisethrust/dlta-engine is the required thrust/dlta per engine necessary 
#    to maintain constant speed and level altitude for the conditions specified by the input
#    parameters to get_FNod_cruise.
#
#NOTE 2:
#    The value of total_available_climb_thrust/dlta-engine is the maximum climb thrust/dlta available assuming 
#    max climb thrust contributions from all operating engine.
#
arglist = sys.argv
print ("arglist = ",arglist)
#
ftemperature = float(arglist[1])
fmach = float(arglist[2])
falt = float(arglist[3])
fweight = float(arglist[4])
fcl = float(arglist[5])
aircraft_model = arglist[6]
aircraft_config = int(arglist[7])
type = arglist[8]
#
print (ftemperature," ",fmach," ",falt," ",fweight," ",fcl," ",\
       aircraft_model," ",aircraft_config)
#
if (type == 'cruisethrust'):
 (lift_coeff,cl_max,drag_coeff,Lift,Drag,ffnod) = get_aeropfm.get_FNod_cruise(ftemperature,fmach,falt,fweight,fcl,aircraft_model,aircraft_config)
 print(lift_coeff," ",drag_coeff," ",Lift," ",Drag," ",ffnod)
#Now write the results to a file that can be read by an application such as the R program VerCat
 with open('aerodata_for_R.txt','w') as aero:
  aero.write(str(lift_coeff)+"\n")
  aero.write(str(cl_max)+"\n")
  aero.write(str(drag_coeff)+"\n")
  aero.write(str(Lift)+"\n")
  aero.write(str(Drag)+"\n")
  aero.write(str(ffnod)+"\n")
#
if (type == 'maxclimbthrust'):
 totalavlbclimbthrustod = get_aeropfm.get_maxclimbthrust(falt,fmach,ftemperature,aircraft_model,aircraft_config)
 print("totalavlbclimbthrustod = ",totalavlbclimbthrustod)
#Now write the results to a file that can be read by an application such as the R program VerCat
 with open('aerodata_for_R.txt','w') as aero:
  aero.write(str(totalavlbclimbthrustod)+"\n")

#maxconthrust section is not ready yet
#if (type == 'maxconthrust')
 #totalavlbconthrustod = get_aeropfm.get_maxconthrust(falt,fmach,ftemperature,aircraft_model,aircraft_config)
 #print("totalavlbconthrustod = ",totalavlbconthrustod)
 #with open('aerodata_for_R.txt','w') as aero:
  #aero.write(str(totalavlbconthrustod)+"\n")
#(lift_coeff,drag_coeff,Lift,Drag,ffnod) = get_aeropfm.get_maxconthrust(ftemperature,fmach,falt,fweight,fcl,aircraft_model,aircraft_config)
 #print(lift_coeff," ",drag_coeff," ",Lift," ",Drag," ",ffnod)
#Now write the results to a file that can be read by an application such as the R program VerCat
 #with open('aerodata_for_R.txt','w') as aero:
  #aero.write(str(lift_coeff)+"\n")
  #aero.write(str(drag_coeff)+"\n")
  #aero.write(str(Lift)+"\n")
  #aero.write(str(Drag)+"\n")
  #aero.write(str(ffnod)+"\n")
#
#Some documentation
#The with open code represents creation and use of a context manager object.
#The 'open(....) ...' portion of the 'with open(...) ...' command constructs a 
#context manager object that wraps the file 'aerodata_for_R.txt'.
#The context manager object includes:
#    an __enter__ method
#    an __exit__ method
#    a read method
#    a readline method
#    a write method
#    many other methods
#The 'as aero' portion of the 'with open(...) ...' command causes aero to be
#the reference to the context manager object.
#The with portion of the 'with open(...) ...' command also causes the context 
#manager object's __enter__ method to be executed.
#aero.write(...) causes the context manager object's write method to be executed 
#and when the end of the with block is reached the context manager object's __exit__
#method gets executed.
#The above with code block is equivalent to:
#   aero = open('aerodata_for_R.txt','w')
#   try:
#       aero.write(str(ffnod)+"\n")
#       aero.write(str(drag_coeff)+"\n")
#       ....
#   finally:
#       aero.close()
#
