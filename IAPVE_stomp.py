#This is the main driver program for reading from the INFLT tables stored as JSON strings 
#and for reading from the flight plan output file stored as JSON output

import sys, json
import csv
import get_INFLTtables_storedasJSON
import std_atm
import getCDWF
import get_FNod
import get_lists_forWF
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2
#
#outfile = open('jaguar.csv','a',newline='')
#outfile = open('tol_jaguar.csv','a',newline='')
#outfile = open('sjc_tol_jaguar.csv','a',newline='')
outfile = open('multiple_city_pairs_737-800W.csv','a',newline='')
#outfile = open('multiple_city_pairs_P8clean.csv','a',newline='')
writer = csv.writer(outfile,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
header_row = ["PointID","WFvalue","Fuel_flow","Temperature","Weight","Mach","Altitiude","CL",\
              "CD","Lift","Drag","Thrust-per-eng/delta","Total_Thrust/delta","Plan_fuel_flow","Delta_fuel_flow","% diff"] 
writer.writerow(header_row)
#
#Read in INFLT table data formatted as JSON strings
logDebug = 1

#Read in all of the text from the file that contains SULU2 flight plan output
#for a single plan and do the following:
#    Build up a single string from the text and in so doing
#    eliminate line feeds (/n) and carriage returns (/r) 
#
diag = 3
mydata = []
i = 1
fpData = {}
astring = ''
fp_json_string = ''
kite = 1
climb_or_descent = False
#fn_of_flightplan = 'jsonfpdata.txt'
#fn_of_flightplan = 'ktol_kmia_json.txt'
#fn_of_flightplan = 'ksjc_ktol_json.txt'
#fn_of_flightplan = 'kbos_eham_M75_json.txt'
#fn_of_flightplan = 'panc_zbaa_ci_json.txt'
fn_of_flightplan = 'ebbr_llbg_ci_json.txt'
#fn_of_flightplan = 'ksjc_ktol_m78_0wind0disa_json.txt'
#fn_of_flightplan = 'kewr_ksea_m77_0wind0disa_json.txt'
#fn_of_flightplan = 'kbos_eham_m75_0wind0disa_json.txt'
#fn_of_flightplan = 'kdfw_cyyg_ci_0wind0disa_json.txt'
#fn_of_flightplan = 'P8clean_kdfw_cyyg_ci_json.txt'
#fn_of_flightplan = 'P8clean_kbos_eham_ci_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_ci_json.txt'
#fn_of_flightplan = 'P8clean_vabb_egkk_ci_json.txt'
#fn_of_flightplan = 'P8clean_klax_kmsp_ci_json.txt'
#
reynfactor = 1.0
json_file = open(fn_of_flightplan)
#
while(i):
   astring = json_file.readline()
   if (astring == ''):
      i = 0
   elif (astring != ""):
      fp_json_string += astring
#
fp_json_string.replace('\n',' ').replace('\r',' ')
fpData = json.loads(fp_json_string)
#
print ("HERE ARE KEYS")
print (fpData.keys())
print ("DONE WITH KEYS")
print ("HERE ARE ITEMS")
print (fpData.items())
print ("DONE WITH ITEMS")

log2Data = []
pod = fpData["pod"]
poa = fpData["poa"]
oew = fpData["ac"]["oew"]
print ("pod poa owe ",pod," ",poa," ",oew)
data_set = {}

profile = fpData["plan"]["profile"]
for checkpoint in fpData["plan"]["log2"]:
      print (" ")
      print ("CHECKER checkpoint ",checkpoint)
#     print ("checkpoint type = ",checkpoint["type"])
      id  = checkpoint["id"]
      typee = checkpoint["type"]
      print ("id & type ",id," ",typee)
      if (checkpoint["type"] == "APT"):
                 print ("CHECKS OUT AS AN AIRPORT")
                 try:
                               print ("TRYING")
                               payload = checkpoint["payload"]
                               print ("payload ",payload)
                               deptfuel = checkpoint["fuel"]
                               print ("deptfuel ",deptfuel)
                               deptwgt = oew + payload + deptfuel
                               print ("deptwgt ",deptwgt)
                               set_type = checkpoint["type"]
                               id = checkpoint["id"]
                               if (id == pod):
                                weight = deptwgt*2.20462
                               elif (id == poa):
                                inpayload = fpData["plan"]["in"]["payload"]
                                infuel = fpData["plan"]["in"]["fuel"]
                                weight = (oew + inpayload + infuel)*2.20462
                                print ("EUREKA POA inpayload infuel weight ",inpayload," ",infuel," ",weight)
                               apt_temperature = checkpoint["temperature"]
                               apt_elevation = checkpoint["elevation"]
                               data_set = {"APTid":id,"Payload":payload,"Deptfuel":deptfuel,"DeptWgt":deptwgt,"SetType":set_type} 
                               print ("data_set for APT")
                               print (data_set)  
                               log2Data.append(data_set)
                               prevalt = 0.0
                               row = [id," "," ",apt_temperature,weight," ",apt_elevation," ",\
                               " "," "," "," "," "," "," "] 
                               print ("row = ",row)
                               writer.writerow(row)
#

                 except:
                               if False:
                                      print ("no payload or fuel at this airport")

      if (str(checkpoint["type"]) == "FIX" or \
           str(checkpoint["type"]) == "VOR"):
                 print ("CHECKS OUT AS A FIX OR VOR")
                 alt = checkpoint["altitude"]
                 print ("ALT = ",alt)
                 if (kite == 1):
                               alt = checkpoint["altitude"]
                               print ("alt = ",alt," ",type(alt)," prevalt = ",prevalt)
                               if (alt != prevalt):
                                climb_or_descent = True
                               else:
                                climb_or_descent = False
                               prevalt = alt
                               tas = checkpoint["tas"]
                               fuelflow = checkpoint["fuelflow"]
                               burn = checkpoint["burn"]
                               temperature = checkpoint["temperature"]
                               fuel = checkpoint["fuel"]
                               weight = oew + payload + fuel
                               set_type = checkpoint["type"]
                               id = checkpoint["id"]
#
# See Jet Transport Performance Methods.pdf Appendix pages A7-A10 for descriptions of Theta and Mach
# and how Theta is determined in the context of Jet Transport. I refer to this Theta as theta_ambient
# because it is determined as outside (aka ambient) air temperture in deg K at a given flight level divided
# by ISA temperature at sea level which is 288.15 deg K.  
# Note - FPC outputs temperature in deg C so the ambient air temperature in deg K is temperature + 273.15
#
# By convention, delta is pressure at flight level divided by pressure at sea level based on standard atmosphere
                               press_ratio = std_atm.alt2press_ratio(alt)
                               theta_ambient = (temperature + 273.15)/288.15
                               mach_nbr = tas/(661.4786 * (theta_ambient ** (0.5)))
                               data_set = {"PointID":id,"Altitude":alt,"Temperature":temperature,"TAS":tas,\
                                           "Fuelflow":fuelflow,"Weight":weight,"Delta":press_ratio,\
                                           "ThetaAmbient":theta_ambient,"Mach":mach_nbr,"SetType":set_type,"Climb_or_Descent":climb_or_descent}
                               print ("data_set for FIX or VOR")# See Jet Transport Performance Methods.pdf Appendix pages A7-A10 for descriptions of Theta and Mach
                               print (data_set)
                               log2Data.append(data_set)
#                except:
#                              if False:
#                                     print ("no alt or tas or fuelflow or burn or temperature or fuel at this point")
      
      if (str(checkpoint["type"]) == 'SID' or \
           str(checkpoint["type"]) == 'STAR' or \
            str(checkpoint["type"]) == 'DCT' or \
              str(checkpoint["type"])== 'AWY'):
                 print ("CHECKS OUT AS A SID STAR DCT or AIRWAY")
                 try:
                   for fix in checkpoint["snapshots"]:
                     try:
                               alt = fix["altitude"]      
                               print ("alt = ",alt," ",type(alt))
                               tas = fix["tas"]
                               fuelflow = fix["fuelflow"]
                               burn = fix["burn"]
                               temperature = fix["temperature"]
                               fuel = fix["fuel"]
                               weight = oew + payload + fuel
                               set_type = checkpoint["type"]
                               id = checkpoint["id"]
# See Jet Transport Performance Methods.pdf Appendix pages A7-A10 for descriptions of Theta and Mach
# and how Theta is determined in the context of Jet Transport. I refer to this Theta as theta_ambient
# because it is determined as outside (aka ambient) air temperture in deg K at a given flight level divided
# by ISA temperature at sea level which is 288.15 deg K.  
# Note - FPC outputs temperature in deg C so the ambient air temperature in deg K is temperature + 273.15
                               theta_ambient = (temperature + 273.15)/288.15
                               mach_nbr = tas/(661.4786 * (theta_ambient ** (0.5)))
# By convention, delta is pressure at flight level divided by pressure at sea level based on standard atmosphere
                               press_ratio = std_atm.alt2press_ratio(alt)
                               data_set = {"PointID":id,"Altitude":alt,"Temperature":temperature,"TAS":tas,\
                                           "Fuelflow":fuelflow,"Weight":weight,"Delta":press_ratio,\
                                           "ThetaAmbient":theta_ambient,"Mach":mach_nbr,"SetType":set_type}
                               print ("data_set for SID STAR DCT or AWY")
                               print (data_set)
                               log2Data.append(data_set)
                     except:
                               if False:
                                    print ("This segment does not have all of the elements it is supposed to have")             
                 except:
                   print ("This segment does not incur an altitude change and therefore there is no snapshots list")
                   tas = fix["tas"]
                   alt = fix["altitude"]
#                  No fuel flow or fuel onboard values are available when on a segment without snapshots
#                  so we set fuelflow to -1 as a signature that there is no fuel flow available
                   fuelflow = -1
                   burn = fix["burn"]
                   weight = oew + payload + (deptfuel - burn)
                   temperature = fix["temperature"]
                   set_type = checkpoint["type"]
                   id = checkpoint["id"]
# See Jet Transport Performance Methods.pdf Appendix pages A7-A10 for descriptions of Theta and Mach
# and how Theta is determined in the context of Jet Transport. I refer to this Theta as theta_ambient
# because it is determined as outside (aka ambient) air temperture in deg K at a given flight level divided
# by ISA temperature at sea level which is 288.15 deg K.  
# Note - FPC outputs temperature in deg C so the ambient air temperature in deg K is temperature + 273.15
                   theta_ambient = (temperature + 273.15)/288.15
                   mach_nbr = tas/(661.4786 * (theta_ambient ** (0.5)))
# By convention, delta is pressure at flight level divided by pressure at sea level based on standard atmosphere
                   press_ratio = std_atm.alt2press_ratio(alt)
                   data_set = {"PointID":id,"Altitude":alt,"Temperature":temperature,"TAS":tas,\
                               "Weight":weight,"Delta":press_ratio,"FuelFlow":fuelflow, \
                               "ThetaAmbient":theta_ambient,"Mach":mach_nbr,"SetType":set_type}
                   print ("data_set for SID STAR DCT or AWY")
                   print (data_set)
                   log2Data.append(data_set)

#So now log2Data is a list of dictionary objects.  Each dictionary object contains log2 data for an 
#indivdual element (airport, fix, or segment) from with the JSON log2 element.   
#
#STOMP THE YARD
#
print ("HERE IS log2Data")
print (log2Data)
print(" ")
available_WFalts_machs_dict = {}
available_WFalts_machs_dict = getCDWF.getalt_fromWFTAB101()
print(" ")
print("IAPVE available_WFalts_machs_dict KEYS and VALUES")
if (diag == 4):
 for iug in available_WFalts_machs_dict.keys():
  print ("available_WFalts_machs_dict key ",iug)
  print ("available_WFalts_machs_dict value",available_WFalts_machs_dict[iug])
ai = 0
machmindiff = 0.75
altmindiff = 5000

mach_list = []
alt_list = []
ffnodlist = []
wfvaluelist = []
WFvalue_mach_list = []
WFvalue_alt_mach_list = []
alt_dict_forprint = {}
mach_dict_forprint = {}

for adict in log2Data:
 print ("ai and adict ",ai," ",adict)
 if (adict["SetType"] == 'FIX' or adict["SetType"] == 'VOR'):
  inclb_or_dsct = False
  print ("THE FIX IS IN ",ai," ",adict["PointID"]," ",adict)
  print ("Note that profile = ",profile)
  findseq = "/" + adict["PointID"]
  findprofchg = profile.find(findseq)
  if (findprofchg >= 0):
   print ("SORRY WILL NOT USE THIS FIX BECAUSE THERE IS AN ALT CHANGE BEFORE OR AFTER IT")
   inclb_or_dsct = True
  if (adict["Climb_or_Descent"]):
   print ("SORRY WILL NOT USE THIS FIX BECAUSE IT OCCURS WITHIN A CLIMB OR DESCENT")
   inclb_or_dsct = True
 if ((adict["SetType"] == 'FIX' or adict["SetType"] == 'VOR') and not inclb_or_dsct):
  print ("THE USEABLE FIX FOR LEVEL FLIGHT IS ",adict["PointID"])  
  ftemperature = adict["Temperature"]
  theta_ambient = adict["ThetaAmbient"]
  ftas = adict["TAS"]
  fweight = adict["Weight"]*2.20462
  print ("adict[Altitude] = ",adict["Altitude"]," and its type is ",type(adict["Altitude"]))
  falt = adict["Altitude"]
  fmach = adict["Mach"]
  print ("AND THE Temperature Weight Altitude and Mach are ",ftemperature," ",fweight," ",falt," ",fmach," ") 
  mdiff = 888888.00
  adiff = 888888.00
#  
  mach_vals = []
  ffnod_master_list = []
  alt_cnt = 0
  mach_cnt = 0
  for ialt in available_WFalts_machs_dict.keys():
   print ("type of ialt is ",type(ialt))
   ffnod_master_list.append([])
   mach_vals = available_WFalts_machs_dict[ialt]
   for imach in mach_vals:
    print ("ialt & imach ",ialt," ",imach)
    fimach = float(imach)/1000.00
    (lift_coeff,drag_coeff,Lift,Drag,ffnod) = get_FNod.get_FNod(ftemperature,fimach,float(ialt),fweight)
    mach_dict_forprint[str(imach)] = ffnod
    print ("ffnod mach_cnt ",ffnod," ",mach_cnt)
#   
    ffnod_master_list[alt_cnt].append(ffnod)
    if (ialt == '41000'):
     print ("GARN alt_cnt ialt imach ",alt_cnt," ",ialt," ",imach," and ffnod = ",ffnod)
     print ("UGLUK AND THE FIX IS ",adict["PointID"])  
#
    mach_list.append(fimach)
    print ("Here is what we have for mach_cnt = ",mach_cnt)
    print (available_WFalts_machs_dict[ialt][mach_cnt])
    ffnodlist,wfvaluelist = get_lists_forWF.get_lists_forWF(ialt,imach)
    print ("Ahead of the 1st BiParabolic call")
    print("ffnod ffnodlist ",ffnod," ",ffnodlist)
    print("wfvaluelist ",wfvaluelist)
    fnoodle =[float(xv) for xv in ffnodlist]
    WFvalue_mach = Biparabolic1(fnoodle,wfvaluelist,ffnod)
#   WFvalue_mach = 9988.00
    print ("After 1st Biparabolic call and WFvalue_mach = ",WFvalue_mach)
    if (imach == 700 and float(ialt) == falt):
       print ("FOR COMPARISION WITH BPS REPORTS:")
       floatalt = float(ialt)
       floatmach = float(imach)*0.001
       dlta = std_atm.alt2press_ratio(floatalt)
       print ("theta_ambient dlta floatalt floatmach ",theta_ambient," ",dlta," ",floatalt," ",floatmach)
       print ("fweight = ",fweight," ftemperature = ",ftemperature)
       print ("lift coefficient = ",lift_coeff," drag coefficient = ",drag_coeff," Lift = ",Lift," Drag = ",Drag) 
       print ("Thrust over delta = ",ffnod)
       total_theta = theta_ambient*(1 + 0.2*(floatmach**2))
       tpm = (1 + 0.2*(floatmach**2))
       total_dlta = dlta*(tpm**3.5)
       print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
       Fuel_flow = WFvalue_mach * ((total_theta**0.62) * total_dlta)
       print ("Fuel_flow = ",Fuel_flow," for floatalt = ",floatalt," floatmach = ",floatmach)
       print (" ")
    WFvalue_mach_list.append(WFvalue_mach)
    mach_cnt = mach_cnt + 1
    print ("AT THE END OF THE INNER LOOP for ialt and imach ",ialt," ",imach)
    print ("  ") 
   print ("Ahead of the 2nd Biparabolic call and ialt = ",ialt)
   print ("AND HERE ARE THE FFNOD VALUES THAT CAME INTO PLAY")
   for mykey in sorted(mach_dict_forprint):
    print (mykey," ",mach_dict_forprint[mykey]) 
   print ("fmach mach_list ",fmach," ",mach_list)
   print ("WFvalue_mach_list ",WFvalue_mach_list)
   WFvalue_alt = Biparabolic1(mach_list,WFvalue_mach_list,fmach)
   print ("After 2nd Biparabolic call WFvalue_alt = ",WFvalue_alt)
   alt_dict_forprint[str(ialt)] = mach_dict_forprint  
   print ("STOMP THE YARD")
   print (" ")
#  WFvalue_alt = 10881.00
   WFvalue_alt_mach_list.append(WFvalue_alt)
   mach_list = []
   WFvalue_mach_list = []
   alt_list.append(ialt)
   alt_cnt = alt_cnt + 1
   mach_cnt = 0
  
  print (" ")
  print ("JAGUAR TERRITORY")
  print ("alt_list = ",alt_list)
  falt_list =[float(xv) for xv in alt_list]
  falt_list.sort()
  WFvalue_alt_mach_list.sort()
  print ("Ahead of the 3rd BiParabolic call")
  print ("falt falt_list WFvalue_alt_mach_list ",falt," ",falt_list," ",WFvalue_alt_mach_list)
  print ("AND THE Temperature Weight Mach are ",ftemperature," ",fweight," ",fmach) 
  print ("AND FURTHERMORE ffnod_master_list = ",ffnod_master_list)
  print ("ffnod_master_list[14] = ",ffnod_master_list[14])
  theWFvalue = Biparabolic1(falt_list,WFvalue_alt_mach_list,falt)  
# theWFvalue = 11999.0
  print (" ")  
  print ("FOR ",adict["PointID"])  
  print ("theWFvalue =  ",theWFvalue)
#
  theta_ambient = (ftemperature + 273.15)/288.15
  dlta = std_atm.alt2press_ratio(falt)
  print ("theta_ambient dlta fmach ",theta_ambient," ",dlta," ",fmach)
  total_theta = theta_ambient*(1 + 0.2*(fmach**2))
  tpm = (1 + 0.2*(fmach**2))
  total_dlta = dlta*(tpm**3.5)
  print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
  Fuel_flow = theWFvalue * ((total_theta**0.62) * total_dlta) * 2.0
# fudge factor to account for reynolds drag
  Fuel_flow = Fuel_flow*reynfactor
#
  print ("Fuel_flow = ",Fuel_flow)
  plan_Fuel_flow = adict["Fuelflow"] * 2.20462
  Delta_ff = plan_Fuel_flow - Fuel_flow
  percent_diff = Delta_ff/Fuel_flow
#
#  if sys.version_info >= (3,0,0):
#   outfile = open('jaguar.csv','w',newline='')
#  else:
#   outfile = open('jaguar.csv','wb')
  (the_lift_coeff,the_drag_coeff,the_Lift,the_Drag,the_ffnod) = get_FNod.get_FNod(ftemperature,fmach,falt,fweight)
  the_total_ffnod = the_ffnod * 2.0
  row = [adict["PointID"],theWFvalue,Fuel_flow,ftemperature,fweight,fmach,falt,the_lift_coeff,\
         the_drag_coeff,the_Lift,the_Drag,the_ffnod,the_total_ffnod,plan_Fuel_flow,Delta_ff,percent_diff] 
  writer.writerow(row)
#
  print (" ")
  alt_list = []
  falt_list = []
  ffnod_master_list = []
  WFvalue_alt_mach_list = []
  alt_cnt = 0
  ai = ai + 1
ender_row = [" "," "," "," "," "," "," "," ",\
              " "," "," "," "," "," "," "] 
writer.writerow(ender_row)



