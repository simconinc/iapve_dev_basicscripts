import get_CDYAW
def get_ydc(Bsic_plus_WM_Drag,S,dlta,wingspan,moment_arm,fmach,falt,aircraft_config):
#dp is dynamic pressure
#Bsic_plus_WM_Drag as sent from the calling program is the sum of basic drag and windmill drag
 dp = 1481*(fmach**2)*dlta
 Sqb = S*dp*wingspan 
 currentDrag = Bsic_plus_WM_Drag
 print ("currentDrag at top = ",currentDrag)
 i = 1
 while (i==1):
  estCN = (currentDrag * moment_arm)/Sqb
  print ("estCN = ",estCN," Sqb = ",Sqb)
  estCDYAW = get_CDYAW.get_CDYAW(estCN,aircraft_config)
  estYawDrag = estCDYAW * (dp*S)
  print ("estCDYAW = ",estCDYAW," estYawDrag = ",estYawDrag)
  newDrag = Bsic_plus_WM_Drag + estYawDrag
  print ("newDrag = ",newDrag)
  if (abs(newDrag - currentDrag) <= 10.0):
   i = 0
  currentDrag = newDrag
 CDYAW = estCDYAW
 return(CDYAW)
# 
#FROM INFLT
#� � � DO 100 I = 1,20
#Computing the total drag and thrust/engine required:
#Estimate CN
#
#�
#� � � � � � CDX = DFACT * DRGFCT * (CDBSIC + CDCOR + CDYAW + CDWM +
#� � �$ � � � � � � � � �CDSP + CDGEAR �+ CDREN + CDIN + CDAPU)
#� � � � � FORCE = CDX * Q * SW / (ZNE1 + ZNE2)
#�
#Estimating the Yawing moment coefficient (CN) and computing the Yaw drag
#�
#� � � � IF (DOENG .AND. .NOT. WODDRG) THEN
#� � � � � XMEOUT = (FORCE * DELTA + WDMDRG) * XLEOUT
#� � � � ELSEIF (DOENG .AND. WODDRG) THEN
#� � � � � XMEOUT = (FORCE + WDMDRG) * XLEOUT
#� � � � ENDIF
#� � � � CN = ABS(XMEOUT + XMPOD) / (Q * SW * SPAN)
#� � � � CDYAW = 0.
#� � � � IF (ISTTAB(4) .GT. 0) THEN
#� � � � � CDYAW = SKEW10(CN,XMOM,XCDPSI,NINT(DEGYAW),NXMOM)
#� � � � � IF(ITERR.NE.0)THEN
#� � � � � �ITERR = 9704805
#� � � � � �GOTO 900
#� � � � � ENDIF
#� � � � ELSE
#�
#� � � � IF (ABS(FORCE - FORCEL) .LE. 10) �GOTO 999
#� � � � FORCEL = FORCE
#100 � CONTINUE
