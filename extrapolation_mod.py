import numpy as np
from scipy.interpolate import UnivariateSpline
from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt


xwant = 5
xarr1 = [1, 2, 3, 4]
yarr1 = [1, 4, 9, 16]

"""
extrapolator = UnivariateSpline(threepoint_xarr1, threepoint_yarr1, k=2 )
yant1 = extrapolator(xwant)
print(label, ywant1)
plot( dayswanted, y, label=label  )  # pylab
"""
  
  
parabspline1 = UnivariateSpline(xarr1, yarr1, k=2, ext = 0)
ywant = parabspline1(xwant)
print("ywant is", ywant)
if not 0 < xwant < 0.5 :
    print('Warning, xwant outside the range of xlist. Ywant will be the result of extrapolation') 