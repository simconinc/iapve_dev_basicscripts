# Trialobyte is the overall control program for the IAPVE software. 
# Trialobyte is responsible for reading interpolation script options 
# and their input files from the configuration file  

import sys
from ConfigParser import SafeConfigParser

# Set default name for configuration file
# config_file_name = 'config_trialobyte.ini'
# print("config_file_name is", config_file_name)

# Access command line arguments for user to designate ini file 
# The user can enter the name of the .ini config file as an command line argument
args = sys.argv

for ar in args: 
  print(ar)
  if(ar.find('.ini') >= 0):
    print("ar = ", ar)
    #config_file_name = ar  
#print("config_file_name =", config_file_name)

##### Read in from Configuration File ####
# Create parser object
parser = SafeConfigParser()

#Read from Configuration file 
parser.read(config_file_name)

#Set IAPVE folder to the correct script directory
scrpt_dir  = parser.get('directories','scrpt_dir')
print("scrpt_dir = ", scrpt_dir)

#Set py_scrpt to the name of the correct IAPVE interpolation program
py_scrpt = parser.get('python script','py_scrpt')
if py_scrpt != "NA":
    print("Please make sure python script file is present in directory")
else:
    print("py_scrpt = ", py_scrpt)


# Establish table data xlist(s), ylist(s), and xwant
# Note: Eventually we need to get this value from Sulu
xwant = 16.72
print("xwant is", xwant)

# 2 lists of x values, one for each parabolic equation
# all xlists and ylists have 9 items
#xlist1 = [2,4,6,8,10,12,14,18,24]
#xlist2 = [1,3,5,9,12,16,23,29,31]
xlist1 = 
xlist2 =
print("xlist1 is", xlist1)
print("xlist2 is", xlist2)

# Use python list comprehension feature to create 2 y lists based on 
# applying a parabolic equation to the list x 
ylist1 =[(a1*(xv**2) + b1*xv + c1) for xv in xlist1]
ylist2 =[(a2*(xv**2) + b2*xv + c2) for xv in xlist2]
print("ylist1 is", ylist1)
print("ylist2 is", ylist2)
