import csv
import numpy as np
import sys
import json
import math

#This part tries out np.fromfile
#newarray = np.fromfile('FNCLB101_tbl.txt')
#print ("newarray")
#print (newarray)
#the above print statement prints out what look to be random numbers

#Initialize variables and structures
count = 0
disa = []
mach = []
alt = []
FNCLBod = []
part1 = []
part2 = []

# Parsing the input/output filename arguments from trialobyte
print ('Number of arguments received from trialobyte: ', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
# assign input filename
dp = sys.argv[0]
# assign output filename
fn = sys.argv[1]

#This code section uses the method to create a reader object using csv.reader and then
#use the reader object to create a list using dalist = [data for data in data_iter]
#
#**** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE input file
#dp = 'JBx37800W_FNCLB101_tbl.txt'
#dp = 'P8clean_FNCLB101_tbl.txt'
#dp = 'P8podup_FNCLB101_tbl.txt'
#dp = '37800WCFM56_57_FNCLB101_tbl.txt'
#****
#
if (dp[0:2] == "P8"):
   p8 = True
   dividby = 23.0
   numdisa = 8
else:
   p8 = False
   dividby = 14.0
   numdisa
#in files with names ending in _FNCLB101_tbl.txt, * is the deliminter
#between the 4 subtables (FNod,mach,alt,WF) 
dl = '*'
with open(dp,'rU') as csvfile:
   data_iter = csv.reader(csvfile,delimiter=dl)
#  for row in data_iter:
#   print ("row ",row)
   dalist = [data for data in data_iter] #creates the list dalist

# The list named dalist is made up of the following:
# 1st element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire mach block
#     element 2 is ''
# 2nd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire altitude block
#     element 2 is ''
# 3rd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire disa block
#     element 2 is ''
# 4th - nth elements in dalist are each a list with a single member that consists of 
# a string created from an entire line of FNCLBod data found in .._FNCLB101_tbl.txt  
icount = 0
for i in dalist:
  print ("just after for i in dalist")
  print (i)
  if (count == 0 and len(i) > 0): 
   print ("splitting up the members of the mach string using the delimiter 1#") 
   k = i[0].split("1#")
   for ar in k:
    mach.append(ar.split())
   print ("k")
   print (k)
   print ("Here is mach")
   print (mach)
  if (count == 1 and len(i) > 0): 
   print ("splitting up the members of the mach string using the delimiter 1#") 
   k = i[0].split("1#")
   for ar in k:
    alt.append(ar.split())
   print ("k")
   print (k)
   print (" ")
   print ("Here is alt")
   print (alt)
  if (count == 2 and len(i) > 0): 
   disa.append(i[0].split())
   print ("do not need to split up the disa string")
   print ("Here is disa")
   print (disa)
  if (count >=3 and len(i) > 0):
   print ("splitting up the members of the FNCLBod string") 
   print ("THE ENTIRE i ",i)
   print ("AIAI i[0] ",i[0])
   print ("i[0] SPLIT ",i[0].split())
   if (not p8):
    FNCLBod.append(i[0].split())
   else:
    if (icount == 0):
     part1 = i[0].split()
    if (icount == 1):
     part2 = i[0].split()
     FNCLBod.append(part1 + part2[0:1])
     print ("P8 FNCLBod so far ",FNCLBod)
    if (icount == 0):
     icount = 1
    else:
     icount = 0
     part1 = []
     part2 = []
  count = count + 1
  print(" ")
print("Done with Section 1: Here is mach ",mach)
#
print(" ")
print ("Here is FNCLBod")
print(FNCLBod) 
#So at this point we have built 4 list objects.
#  mach - contains multiple elements: each element is a list made up of data from a sub group within the mach section of the FNCLB101 table 
#  alt - contains multiple elements: each element is a list made up of data from a sub group within the altitude section of the FNCLB101 table 
#  disa - contains multiple elements: each element is a list made up of data from a sub group within the disa section of the FNCLB101 table
#  FNCLBod - contains multiple elements: each element is a list made up of data taken from a string created from an entire line of the FNCLB section 
#          of the FNCLB101 table 
#
#The following section of code populates the dictionary named FNCLBod_dict
#The structure of FNCLBod_dict is as follows:
#Each key in FNCLBod_dict is one of the disa's from the disa section of the FNCLB101 table
#The value associated with each disa key is a dictionary named alt_FNCLBod_dic.
#The keys in alt_FNCLBod_dic are the altitudes and associated with each altitude key is a
#list of two lists. List 1 consists of mach values and list 2 consists of corresponding 
#FNCLBod values
#For example, in the FNCLB101 table stored in JBx37800W.dat,
#the disa values are:
#-72 -18 0 18 27 36 54 72
#and the altitude values are: 
#0   5000  10000  15000  20000  25000  29000  31000  33000  35000 36089  37000  39000  41000 
#
#So taking our JBx37800W.dat FNCLB101 table example we have:
#8 disa's
#14 altitudes
#14 sets of mach values with 10 machs per set
#At altitude 37000 the applicable machs are:
#  .5000  .6000  .7000  .7200  .7500  .7800  .8000  .8500  .8700  .9000
#Since there are 8 disa's there are 8 blocks of FNCLBod values with each
#block consisting of 14 sets of FNCLBod values.
#disa 0 is the 3rd disa value so its FNCLBod sets are in the 3rd block
#Altitude 37000 is the 12th altitude in the 14 altitude set so this means
#that for disa = 0 and altitude = 37000 the 12th set of FNCLBod values in 
#the 3rd block is the applicable FNCLBod set.
#The 3rd block is:
"""
  22487  20249  18405  16909  15542  14470  13474  12638  11922  12034
  24702  22371  20551  18897  17281  16165  15229  14386  13610  13820
  26657  24308  22418  20898  19160  18047  17121  16275  15552  15856
  27801  25554  23647  22201  20847  19922  19043  18312  17605  17984   
  28715  26525  24756  23222  22058  21199  20667  20147  19513  20034
  29119  26950  25239  23976  23105  22459  22018  21686  21457  22268
  29081  26921  25215  23957  23170  22874  23002  22851  22831  23694
  22994  23232  23386  23417  23435  23411  23449  23941  24267  24589
  23026  23268  23760  23851  23983  24059  24093  24642  24976  25304
  23074  23318  23908  24076  24313  24594  24623  25197  25546  25894
  23122  23359  23923  24110  24378  24740  24744  25334  25685  26035
  23151  23383  23929  24130  24424  24854  24840  25432  25786  26137   
  23232  23446  24002  24173  24413  24726  24727  25321  25673  26022
  23431  23559  24058  24195  24381  24596  24614  25199  25549  25897  0
"""   
#The 12th set of FNCLBod values (corresponding to altitude 37000) in 
#the 3rd block is:
# 23151  23383  23929  24130  24424  24854  24840  25432  25786  26137 
#Because there are 10 machs applicable to altitude 37000 the above
#10 FNCLBod values have a one to one correspondence to these 10 machs. 
#
#So,for for example, the portion of FNCLBod_dict pertaining to  
#disa 0 and altitude 37000 should be as follows
#},{"0": {"37000": ["5000","6000","7000","7200","7500","7800","8000","8500","8700","9000"]
#[23151,23383,23929,24130,24424,24854,24840,25432,25786,26137]},{... 
#"0" is the disa key which contains multiple dictionaries, each
#one keyed by a an altitude key.
#"37000" is the altitude key for the above portion and it contains two lists
#The 1st list consists of mach values and the 2nd list consists of FNCLBod values 
#in one to one correspondence with the mach values in the 1st list.
#Thus, for example, FNCLBod = F(disa = 0, alttiude=37000, mach=.780) = 24854
#Note that .780 is represented in FNCLBod_dict as "7800" and which is the 6th
#member of the mach list for the key "37000".  The corresponding 6th member
#of the FNCLBod list for the key "37000" is 24854.    
#
hit_zero = False
hit_one = False
#mach_cnt = 0
machlist = []
mach_cnt_listmbrs = 0
alt_cnt = 0
dcnt = 0
alt_FNCLBod_dic = {}
FNCLBod_values = []
FNCLBod_dict = {}
fcount = 0
gcount = 0
disa_count = -1
for f in FNCLBod:
 print ("f IS THIS ",f)
 if (len(f) == 0):
  print("No more data")
  break
 alt_key = str(alt[0][fcount])
 print("GENCO alt_key = ",alt_key)
 for g in f:
  print ("g IS THIS ",g)
  FNCLBod_values.append(float(g))
 machlist = mach[fcount]
 print ("fcount = ",fcount," HERE IS JAZZY machlist ",machlist)
 ruple_tuple = (machlist,FNCLBod_values)
 alt_FNCLBod_dic.update({alt_key:ruple_tuple})
 print ("catemount alt_FNCLB0d_dic ",alt_FNCLBod_dic)
 fcount = fcount + 1
 print ("fcount dividby floatof ",fcount," ",dividby," ",math.fmod(float(fcount),dividby))
 if (fcount > 0 and math.fmod(float(fcount),dividby) == 0):
  disa_count = disa_count + 1
  disa_key = str(disa[0][disa_count])
  print ("JOHNLEEHOOKER disa_key = ",disa_key)
  FNCLBod_dict.update({disa_key:alt_FNCLBod_dic})
  print ('THE FNCLBod Dictionary: FNCLBod_dict{disa_key:alt_FNCLBod_dic} is \n',\
          FNCLBod_dict.get(disa_key))
  fcount = 0
  alt_FNCLBod_dic = {}
  if (disa_count == numdisa):
   disa_count = -1
 FNCLBod_values = []
print("Victory")
#
#Use json.dumps to create a JSON object and then write it to .._FNCLB101_JSON.txt
#fn = "JBx37800W_FNCLB101_JSON.txt"
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file
#fn = "37800WCFM56_57_FNCLB101_JSON.txt"
#fn = "P8clean_FNCLB101_JSON.txt"
#fn = "P8podup_FNCLB101_JSON.txt"
#fn = "notneeded_P8clean_FNCLB101_JSON.txt"
#***
target = open(fn,'w')
target.truncate()
target.write(json.dumps(FNCLBod_dict))

print ("FINISHED with FNCLB101 JSON Maker")



  
