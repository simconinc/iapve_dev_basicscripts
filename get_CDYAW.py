import Linear
#
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
print ("TIRGRE THE GOOD ac_model_config ",ac_model_config)
if (ac_model_config[0:2] == "p8"):
 P8 = True
else:
 P8 = False
if (ac_model_config == "37800W\n"):
 P8 = False
 JBx37800W = True
#
def get_CDYAW(cn,aircraft_config):
 if (aircraft_config != 2):
  print ("YOU HAVE NO BUSINESS CALLING get_CDWM since aircraft_config not= 2")
  return(0,0,0,0,0)
 print ("JUST STARTING get_CDYAW and aircraft_config = ",aircraft_config)
#
 if (P8):
  CDPSI_file = open('P8clean_CDPSI01_txt.txt')
 elif (JBx37800W):
  CDPSI_file = open('JBx37800W_CDPSI01_txt.txt')
 else:
  print ("NOT A P8 or JBx37800W aircraft so not ready yet to determine CDYAW")
  return(0,0,0,0,0)
#
 cnstr_list = []
 cdyawstr_list = []
#
 cnfloat_list = []
 cdyawfloat_list = []
 count = 0
#
 a1string = CDPSI_file.readline()
 print("a1string = ",a1string)
 a2string = CDPSI_file.readline()
 print("a2string = ",a2string)
#
 j = 1
 while (j == 1):
  cnstr_list.append(a1string[count:(count+7)])
  cdyawstr_list.append(a2string[count:(count+7)])
  count = count + 7
  if (count > 84):
   j = 0 
 cnstr_list.pop(-1)
 cdyawstr_list.pop(-1)
 #print (cnstr_list)
 #print (cdyawstr_list)
 for x in cnstr_list:
  cnfloat_list.append(float(x))
 for y in cdyawstr_list:
  cdyawfloat_list.append(float(y))
 print (cnfloat_list)
 print (cdyawfloat_list)
 CDYAW = Linear.Linear(cnfloat_list,cdyawfloat_list,cn)
 print ("CDYAW = ",CDYAW)
 return(CDYAW)
