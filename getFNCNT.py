import get_fromFNCNT
import IAPVEutils
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2
from Linear import Linear
#
def getFNCNT(falt,fmach,ftemperature,aircraft_config):
    FNCNTodvalue_disa_alt_list = []
    FNCNTmachlist = []
    FNCNTodlist = []
    FNCNTalts_list = []
    FNCNTod_list = []
    FNCNTalts_count = 0
    available_FNCNTdisa_alts_dict = {}
    intdisa = []
    intalt = []
#
    fidisaF = IAPVEutils.get_disa_degF(falt,ftemperature)
#
    available_FNCNTdisa_alts_dict = get_fromFNCNT.getdisa_fromFNCNT104()
    for iug in available_FNCNTdisa_alts_dict.keys():
     print ("available_FNCNTdisa_alts_dict key = ",iug)
     print ("available_FNCNTdisa_alts_dict value = ",available_FNCNTdisa_alts_dict[iug])
#
    for strdisa in available_FNCNTdisa_alts_dict.keys():
     intdisa.append(int(strdisa))
    intdisa.sort()
    print ("intdisa = ",intdisa)
#
# cycle through available_FNCNTdisa_alts_dict
    for idi in intdisa: 
     for stralt in available_FNCNTdisa_alts_dict[str(idi)]:
      intalt.append(int(stralt))
     intalt.sort()
     for iai in intalt:
      (FNCNTmachlist,FNCNTodlist) = get_fromFNCNT.get_lists_fromFNCNTod(str(idi),str(iai),aircraft_config)
      print ("Ahead of the 1st BiParabolic or Linear call for determining FNCNTod")
      print("FNCNTmachlist ",FNCNTmachlist)
      print("FNCNTodlist ",FNCNTodlist)
      print("fmach = ",fmach)
      if (FNCNTmachlist[0] !=  -888):
       fmachle =[float(xv) for xv in FNCNTmachlist]
      #for a given disa and altitude apply Biparabolic to the applicable
      #FNCNTmachlist list and FNCNTodlist to get a value for FNCNTodvalue.
      #This value is then added to FNCNTod_alts_list 
       #FNCNTodvalue = Biparabolic1(fmachle,FNCNTodlist,fmach,'maxclimbthrust')
       FNCNTodvalue = Linear(fmachle,FNCNTodlist,fmach)
       print ("After 1st Biparabolic call and FNCNTodvalue = ",FNCNTodvalue)
       FNCNTod_list.append(FNCNTodvalue)
       FNCNTalts_list.append(iai)
       FNCNTalts_count = FNCNTalts_count + 1
       print ("AT THE END OF THE INNER LOOP for iai = ",iai)
       print ("  ") 
     print ("For idi = ",idi)
     print ("Ahead of the 2nd Biparabolic or Linear call for FNCNT and falt = ",falt)
     print ("FNCNTalts_list = ",FNCNTalts_list)
     print ("FNCNTod_list = ",FNCNTod_list)
     #FNCNTodvalue_disa = Biparabolic1(FNCNTalts_list,FNCNTod_list,falt,'maxclimbthrust')
     FNCNTodvalue_disa = Linear(FNCNTalts_list,FNCNTod_list,falt)
     #alt_dict_forprint[str(ialt)] = mach_dict_forprint  
     print ("")
     FNCNTodvalue_disa_alt_list.append(FNCNTodvalue_disa)
     print ("After 2nd Biparabolic call")
     print ("falt and fmach = ",falt," ",fmach," and strdisa = ",strdisa)
     print ("FNCNTodvalue_disa = ",FNCNTodvalue_disa)
     print ("FNCNTodvalue_disa_alt_list = ",FNCNTodvalue_disa_alt_list)
#
     FNCNTmachlist = []
     FNCNTodlist = []
     FNCNTalts_list = []
     FNCNTod_list = []
     FNCNTalts_count = 0
     intalt = []
#
    print("Finished the outer loop and FNCNTodvalue_disa = ",FNCNTodvalue_disa)
    print("intdisa = ",intdisa)
    print (" ")
    print ("FNCNT JAGUAR TERRITORY")
    ISAtempdev_list =[float(xv) for xv in intdisa]
    #ISAtempdev_list.sort()
    print ("Ahead of the 3rd BiParabolic or Linear call for FNCNTodvalue")
    print ("FNCNTodvalue_disa_alt_list = ",FNCNTodvalue_disa_alt_list)
    print ("ISAtempdev_list = ",ISAtempdev_list)
    print ("fidisaF = ",fidisaF)
    #theFNCNTodvalue = Biparabolic1(ISAtempdev_list,FNCNTodvalue_disa_alt_list,fidisa,'maxclimbthrust') 
    theFNCNTodvalue = Linear(ISAtempdev_list,FNCNTodvalue_disa_alt_list,fidisaF)   
    print ("theFNCNTodvalue =  ",theFNCNTodvalue)
    engfac = 1.0
    #totalavlbclimbthrust = theFNCNTodvalue * 2.0
    totalavlbcontcruisethrustod = theFNCNTodvalue * engfac
    print ("END OF FNCNT JAGUAR TERRITORY")
    print ("totalavlbcontcruisethrustod = ",totalavlbcontcruisethrustod)
    return(totalavlbcontcruisethrustod)

