#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     TRIALOBYTE MASTER CONTROL PROGRAM FOR VERIFICATION/VALIDATION SOFTWARE
#
#			                Trialobyte version 1.0.1 
# Created on Dec 31, 2015 by JBaron based on an update to Trialobyte version 1.0.0
# This update consisted of fixes to the table maker portion of Trialobyte version 1.0.0.
# The fixes came from JBaron's latest version of table_maker.py.
#
# DESCRIPTION
# Trialobyte is the overall control program for the IAPVE software. and is responsible 
# for managing the process flow of data input starting with the table maker program 
# reading in a .dat file, followed by the creation of .txt files containing table data 
# that will be converted to JSON strings by the JSON maker program. Statistical analysis 
# is performed by an R script once the JSON strings have been converted into files.

# Trialobyte also includes a interpolation selector module. This module can either automatically
# identify the type of file being processed and select the appropriate interpolation 
# method or be switched to manual mode in whic the user selects the interpolation method.

# The table maker program, get_INFLT_tab_v4.py has been transferred from being executed
# from the ini configuration file to being fully embedded into the Trialobyte script

# Due to the number of individual JSON maker programs, JSON maker script selection
# is handled via the .ini configuration file.

############################
# FILE REVISION HISTORY
# 
# 7/2/2015 - File created through the combination of one Table Maker and Json Maker module
# 7/9/2015 - Added JB's recommended .py files for incorporation
# 7/12/2015 - Incorporated the function of the table maker program, get_INFLT_tab_v4.py
# 7/18/2015 - version 0.2 - Added ability to pass .dat file name string between modules
# 8/16/2015 - Swapped Table maker module with updated version from table_maker.py from JB
# 9/23/2015 - version 0.3 - Added JSON maker program selection capability to trialobyte. 
# 10/7/2015 - version 0.4 - JSON maker input/output file names are constructed by trialobyte and passed to each json maker script
# 10/12/2015 - version 0.5 - Table Maker module now reads and writes Yaw drag (CDPSI01) data
# 10/15/2015 - version 0.5 - Table Maker module now reads and writes Windmilling drag (DODWM and CDWM) data
# 10/25/2015 - version 0.6 - Takes the name of a spreadsheet created by IAPVE.py and executes statistical analysis program
# 10/30/2015 - version 0.7 - Incorporated a more updated version of table_maker.py
# 11/4/2015 - version 0.75 - Revised table maker to construct output file names automatically instead of reading from config file, like in the json makers
# 11/8/2015 - version 0.75 - JSON Maker file name read-in now based on newline separation instead of comma separation
# 11/12/2015 - version 0.8 - Table maker module searches for table data headers with the same depenent & indpendent vars from a pre-established list then loops through processing each table in the same way
# 11/13/2015 - version 0.8 - Improved "VerCAT" statistical analysis module using Mega_plug's AFPRAT calling structure as a base
# 11/17/2015 - version 0.8 - Changed JSON Maker module to accept user designated Header names from config file, not full json maker program file names. First production version of the overall VerCAT software
# 11/24/2015 - version 1.0.0 - Trialobyte now executes the VerCAT R-based statistical analysis program and specifies a VerCAT input file
# 11/28/2015 - version 1.0.0 - Added function jm_argsender to send arguments using sys.argv locally without interfering sending input arguments to VerCAT
# 11/28/2015 - version 1.0.0 - Added function vc_argreadder to locally read VerCAT input filenames from command line using sys.argv


#	(c) 2015 Simcon Technology Corporation, All rights reserved.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


########################################
###### Import Packages and Modules #####
########################################

import os
import json
import sys
import getopt
import re
import csv
import numpy as np
from configparser import SafeConfigParser # Use for python3
#from ConfigParser import SafeConfigParser # Use for python2.7
 

############################# 
###### Define Functions #####
############################# 

# jm_argsender executes a Json Maker program, jm and passes its input and output filename strings as arguments 
def jm_argsender(jm, jm_input, jm_output):
    #Pass input/output filenames as arguments to each Json Maker
    sys.argv = [jm, jm_input, jm_output]
    print("executing json maker ", jm , "using arguments ", sys.argv)
    #execfile(jm)                 # For python 2.7
    exec(open(jm).read())         # For python 3  

    #Add name of Json Maker program to the list of Json Makers run
    jsnmkr_programs_run.append(jm)
    #Add Json Maker output file to list for later use by IAPVE.py
    jm_outputs_list.append(jm_output)
 
# vc_argreader reads in a user specified .csv filename from the command line. The .csv file will be the input to the VerCAT R program
def vc_argreader(argv):
   vercat_input = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile="])
   except getopt.GetoptError:
      print ('trialobyte_1.0.0.py -i <vercat_input>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('trialobyte_1.0.0.py -i <vercat_input>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         vercat_input = arg
   print ('VerCAT input file =', vercat_input)


##########################################
##### Read in from Configuration File ####
##########################################

# Create parser object
parser = SafeConfigParser()

# Default configuration file name
config_file_name = "config_trialobyte.ini"

print ("config_file_name = ", config_file_name)

# Read from Configuration file 
parser.read(config_file_name)

# Read in directory paths from configuration file
scrpt_dir  = parser.get("script directory","scrpt_dir")
inpt_dir  = parser.get("input_file_directory","inpt_dir")
outpt_dir  = parser.get("output_file_directory","outpt_dir")
print("scrpt_dir = ", scrpt_dir)
print("input_dir = ", inpt_dir)
print("outpt_dir = ", outpt_dir)

# Read in Verification Comparative Analysis Tool (VerCAT) program name
vercat_prgrm = parser.get("vercat","vercat_prgrm")

###################################
### Establish File Name Strings ###
###################################

# Get the data headers requested for analysis from the config file
user_chosen_headers = parser.get('data_table_headers','user_chosen_headers')
print ("\n user_chosen_headers =", user_chosen_headers, "\n")
separated_header_names = user_chosen_headers.split("\n")
print ("separated_header_names =", separated_header_names, "\n")


"""
MARKED FOR DELTETION. This is the old way of getting the json maker program file names
### JSON Maker file read in
# Get the file data header names from the list of JSON makers in the config file
# Read in list of JSON maker programs
jsonmaker_pgrmlist = parser.get('jsonmaker_all','jsnmkrs')
print ("jsonmaker_pgrmlist =", jsonmaker_pgrmlist)

separated_jsnmkrs = jsonmaker_pgrmlist.split("\n")
print ("Separated_jsnmkrs =", separated_jsnmkrs)

# Get the table header names from the list of configuration files 
header_names_raw = [i.split('_table_to_JSON.py')[0] for i in separated_jsnmkrs] 
separated_header_names = [i.split('INFLT_')[1] for i in header_names_raw] 
print('The table separated_header_names are ', separated_header_names)
"""

### Read in the .dat file and retrieve aircraft and plan information 
# Find user designated .dat input file in the .ini configuration file
tbl_in = parser.get('table_maker_input', 'tbl_in')
print ("INFLT .dat input file is ", tbl_in, "\n")
tbl_data = open(tbl_in)
print ("tbl_data is ", tbl_data, "\n")

# Store the .dat input file name as a string. Use the character string before '.dat'. 
# This string will be used to generate file names throughout trialobyte
dat_string = tbl_in[:-4]
print("The .dat filename string is", dat_string, "\n")


############################
#### Table Maker module ####
############################
# Module derived from incorporating table_maker.py into Trialobyte. 
# Table Maker reads from a INFLT .dat file, parses certain tables, then
# writes them to WFTAB101_tbl.txt and CDBASE01_tbl.txt or to similarly named
# output files.
                  
i = 1
foundtable = False
#astr is where each line of a given table is appended to in order to create
#one long string making up that table.  astr is always initialized to null
astr = ""

tables = []  #for storing a list of tables, each a string
tbl_dict = []   #for storing a list of dictionaries
subtab = []
independent_vars = []
index11 = []
fuel_table = []
WFlist = []
dict = {}
crawdad = {}
k = 0

# The data table headers in the lists below use identical dependent and independent variable combinations
fuelflow_headers = ["WFTAB101","WFTAB111","WFTAB104","TABCORF01"]
dragco_headers = ["CDBASE01","CDBASE02","CDBASE03","CDBASE04","CDBASE05","CDBASE06", "CDWM01"]
fnod_headers = ["FNCNT104", "FNCLB101", "FNCRU101"]

#the following while loop extracts certain tables and stores them as 
#strings in the tables list
#The while loop below stays in effect until i is set to 0
while i:
    astring = tbl_data.readline()
#   print "astring = ",astring
#Check if astring contains a start indicator for a table
#The string /TBLU indicates the start of the very 1st table
#An H or a / indicates end of a table and the possible start of a new table
    if (not foundtable and astring[0:5] == "/TBLU"):
        foundtable = True 
#       print "FOUND TBLU"
    elif(foundtable and (astring[0:1] == "H" or astring[0:1] == '/')):
        tables.append(astr)
#       print "astr[0:7] and tables[k] ",astr[0:7]," ",tables[k][0:27]
        foundtable = False 
#       print "foundtable astring[0:1] = H or / ", foundtable," ",astring[0:1]
        k = k + 1
        astr = ""
    elif(foundtable and astring != ""):
        astr = astr + astring
#astring == "" means we have reached the end of the file 
    if (astring == ""):
        tables.append(astr)
        i = 0
#print "fplans equals", fplans           
#print "out of while loop"

tables_index = 0
for t in tables:
    print ("START of entry", tables_index, "in tables")
# when you specify '\n' python treats it as hex0a
# when you specify '\r' python treats it as hex0D
# therefore
# p.replace('\n', ' ') replaces all occurrences of hex0A with hex20
# p.replace('\r', ' ') replaces all occurrences of hex0D with hex20
# it is necessary to replace in the string p all occurences of hex0A with hex20 and all occurences hex0D with hex20 in order for json.loads(p) to work properly
#   t = t.replace('\n', ' ').replace('\r', ' ')
    tables_index += 1  
    print ("t is", t[0:272])
    print ("END OF THE TABLE") 
print ("Finished reading the file")  
#
#At this point in the process each member of the tables list is a string consisting
#of an entire table starting with the table identifier (e.g. CDBASE01)


### Loop through list of header names. If the name matches a data header specified by the user
### then parse the corresponding data table and write to a XXX_tbl.txt file

### BEGIN WF SECTION. 
#WF data table headers use WF as the dependent variable and mach, alt, fnod as the independent variables

#Find members of the tables list that contain a string
#designating it a fuel flow (WF) table, then process that table. 

for w in tables:
 #if (w[0:8] == 'WFTAB101' or w[0:8] == 'WFTAB104' or w[0:8] == 'WFTAB111' or w[0:9] == 'TABCORF01'):
 #if (w[0:5] == 'WFTAB' or w[0:7] == 'TABCORF'): 
  
 #Check if w[0:5] is in the list of fuel flow table headers 
 if w[0:w.find("\n")] in fuelflow_headers and user_chosen_headers:
  wf_name_var = w[0:w.find("\n")]  #We want the full header string. w[0:5] is just used so there's less matching
  print ("found ", wf_name_var)
  
  # Open a file to which the WF table will be written  
  tbl_out_WF = dat_string + '_' + wf_name_var + '_tbl.txt'
  print("WF Table Maker Output/JSON Maker Input file = ", tbl_out_WF)
  target = open(tbl_out_WF,'w')
  target.truncate()

  #*** HARD CODED OPTION for determining the table maker output file for WFTAB data
  #tbl_out_WF = "37800WCFM56_57_WFTAB101_tbl.txt"
  #tbl_out_WF = "P8podup_WFTAB111_tbl.txt"
  #***
  
###The INFLT USER MANUAL states the WF equation is WF = F(W/delta,mach,alt)
#Therefore, after locating a WF table we want to split it into 4 parts; 
#part 1 consists of values for the independent variable FNod (also called FN/δ)
#part 2 consists of values for the independent variable mach 
#part 3 consists of values for the independent variable altitude 
#part 4 consists of values for the dependent variable WF  
  
  #Split w into independent variable data blocks using 11\n as the delimiter
  independent_vars = w[8:].split("11\n")
  #print ("independent_vars list for ", wf_name_var , " is " , independent_vars)
  print ("indpendent_var[0] =", independent_vars[0])
  print ("indpendent_var[1] =", independent_vars[1])
  print ("indpendent_var[2] =", independent_vars[2])
  FNod = independent_vars[0]
  FNod = FNod.replace('1\n', '1#').replace('\r','')
  FNod = FNod.replace('\n','')
  target.write(FNod)
  target.write('*\n')
  mach = independent_vars[1]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[2]
  alt = alt.replace('\n','').replace('\r','')
  target.write(alt)
  target.write('*\n') 
  print ("FNod ",FNod)
  print ("mach ",mach)
  print ("alt ",alt)

#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("index11[2] ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  #WF = w[index11[3]:]  SHOULD NEVER HAVE BEEN w[index11[3]:] for any of the WFTABij's
  WF = w[index11[2]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(WF)
  target.write('*')
  print ("WF VALUES")
  print (WF)
  print ("DONE PRINTING WF VALUES for", wf_name_var)
# print "Complete Fuel Table"
# print fuel_table

# End of WF Section
#
#!#%


# BEGIN Thust Over Delta (FNOD) Section.
#Includes all data table headers with FNOD as the dependent variable 
#and mach, alt, delta temp as the independent variables

for w in tables:
 #if (w[0:8] == 'FNCLB101' or w[0:8] == 'FNCNT104' or w[0:8] == 'FNCRU101'): 
 if w[0:w.find("\n")] in fnod_headers and user_chosen_headers: 
  fnod_name_var = w[0:w.find("\n")]
  print ("found ", fnod_name_var)

#*** HARD CODED OPTION for determining the table maker output file for FNOD data
  #fn = "JBx37800W_FNCLB101_tbl.txt"
  #fn = "P8clean_FNCLB101_tbl.txt"
#***

  #Open file to write the FNOD table to 
  tbl_out_fnod = dat_string + '_' + fnod_name_var + '_tbl.txt' 
  target = open(tbl_out_fnod,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  print ("indpendent_var[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the fnod values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMEJAGS ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  FNCLB = w[index11[2]:]
  target.write(FNCLB)
  target.write('*')
  print (fnod_name_var, " VALUES ")
  print ("DONE PRINTING", fnod_name_var, "VALUES")
#End Thust Over Delta (FNOD) Section.


#
#
#BEGIN APUWF01 Section.
#Includes data table headers with WF as the dependent variable 
#and alt, mach as the independent variables

for w in tables:
 if w[0:5] == "APUWF" and w[0:w.find("\n")] in user_chosen_headers: 
  apuwf_name_var = w[0:w.find("\n")]
  print ("found APUWF data type", apuwf_name_var)

#*** HARD CODED option for determining the table maker output file for APUWF data
  #fn = "JBx37800W_APUWF01_tbl.txt"
  #fn = "P8clean_APUWF01_tbl.txt"
#***

  #Open file to write the APUWF01 table to 
  tbl_out_apuwf = dat_string + '_' + apuwf_name_var + '_tbl.txt'
  print("APUWF Table Maker Output/JSON Maker Input file = ", tbl_out_apuwf)
  target = open(tbl_out_apuwf,'w')  
  target.truncate()
  independent_vars = w[8:].split("11\n")
  #independent_vars = w[8:].split("11 ALT", "11MACH")
  #independent_vars = w[8:].split("11MACH")
  print ("independent_vars are ", independent_vars)
  print ("\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  #print ("indpendent_var[2] ",independent_vars[2])
  alt = independent_vars[0] + "1#"
  alt = alt.replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  print ("alt ",alt)
  mach = independent_vars[1] + "1#"
  #mach = mach.replace('11\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  print ("mach ",mach)

#
#Now we want to find the index in w which is the start of the FNCNTod values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("index11[0] index11[1] ",index11[0],\
         " ",index11[1])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  #outer2 = index11[2] + 100
  print ("HOMEJAGS ",w[index11[1]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  #print ('2 ',w[index11[2]:outer2]) 
  APUWF = w[index11[1]:]
  target.write(APUWF)
  target.write('*')
  print ("APUWF VALUES ")
  print (APUWF)
  print ("DONE PRINTING APUWF VALUES")
#!#%
#
#END APUWF01 
#
#
#BEGIN WFIDL101 Section.
#Includes data table headers with WF as the dependent variable 
#and alt, mach as the independent variables

print("BEGIN WFIDL101 Section")
for w in tables:
 if w[0:5] == 'WFIDL' and w[0:w.find(" ")] in user_chosen_headers: 
  wfidl_name_var = w[0:w.find(" ")]
  print ("found ", wfidl_name_var)

#*** HARD CODED OPTION for determining the output file for WFIDL101
  #fn = "JBx37800W_WFIDL101_tbl.txt"
  #fn = "P8clean_WFIDL101_tbl.txt"
#***

  #Open file to write the WFIDL101 table to 
  tbl_out_wfidl = dat_string + '_' + wfidl_name_var + '_tbl.txt'
  target = open(tbl_out_wfidl,'w')  
  target.truncate()
  independent_vars = w[8:].split("11\n")
  #independent_vars = w[22:].split("11\n")
  print ("indpendent_var[0] ",independent_vars[0])
  print ("indpendent_var[1] ",independent_vars[1])
  print ("indpendent_var[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMER ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  WFIDL = w[index11[2]:]
  target.write(WFIDL)
  target.write('*')
  print ("WFIDL VALUES ")
  print (WFIDL)
  print ("DONE PRINTING WFIDL VALUES")
#!#%
#
#END WFIDL101 section
#
#


#BEGIN Drag SECTION.
#Includes all data table headers with Drag Coefficient (CD) as the dependent variable 
#and CL (lift coefficient), and mach as the independent variables
 #if (w[0:8] == 'CDBASE01' or w[0:8] == 'CDBASE02' or w[0:8] == 'CDBASE03' or w[0:8] == 'CDBASE04'\
 #or w[0:8] == 'CDBASE05' or w[0:8] == 'CDBASE06') or w[0:6] == 'CDWM01':
#NOTE - there is a separate code block for processing the CDWM01 table.  This is because for all .dat files
#the delimiters indicating the end of the CL and mach sections of the CDWM01 table are always '11\n' 
 if w[0:w.find("\n")] in dragco_headers and user_chosen_headers: 
  cd_name_var = w[0:w.find("\n")] 
  print ("found ", cd_name_var)

#*** HARD CODED OPTION for determining the table maker output file for drag coefficient data
  #fn = "JBx37800W_CDBASE04_tbl.txt"
  #fn = "P8clean_CDBASE06_tbl.txt"
  #fn = "P8clean_CDBASE04_tbl.txt"
  #fn = "P8clean_CDBASE05_tbl.txt"
  #fn = "P8podup_CDBASE01_tbl.txt"
  #fn = "37800WCFM56_57_CDBASE01_tbl.txt"
  #fn = "P8clean_CDBASE06_tbl.txt"
  #fn = "P8clean_CDBASE04_tbl.txt"

  #Open the file that the drag coefficent table data will be written to
  tbl_out_CD = dat_string + '_' + cd_name_var + '_tbl.txt'
  print("CD Table Maker Output/JSON Maker Input file = ", tbl_out_CD)
  target = open(tbl_out_CD,'w')  
  target.truncate()
  #
  independent_vars = w[9:].split("1cl\n")
  print (cd_name_var, "independent_vars is ", independent_vars, "\n")
  cl = independent_vars[0]
  print (cd_name_var, "cl is ", cl, "\n")
  cl = cl.replace('cl\n', 'cl#').replace('\r','')
  print (cd_name_var, "cl is now ",cl, "\n")
  target.write(cl)
  target.write('*\n')
  #
  #On Dec 30 2015 JB renewed the method of getting mach and cd using 
  #independent_vars[1].split("1mach\n")
  #The reason is that it is now appropriate for processing CDBASEij tables for all
  #.dat files including JBx37800W.dat because JB made sure that all 
  #CDBASEij tables in JBx37800W.dat and in all P8 tables have 1cl\n as the 
  #delimiter ending the cl section and 1mach\n as the delimiter ending the
  #mach section. And JB made sure that all CDBASEij tables for all .dat file have 'cl\n' 
  #as the delimiter indicating the end of an indivdual cl line and 'mach\n' as the 
  #delimiter indicating the end of an individual mach line. 

  mach_and_cd = independent_vars[1].split("1mach\n")
  mach = mach_and_cd[0]
  print ("mach_and_cd = ",mach_and_cd)
  print ("mach = ",mach)
  mach = mach.replace('mach\n', 'mach#').replace('\r','')
  target.write(mach)
  target.write('*\n')
  
  cd = mach_and_cd[1]
  target.write(cd)
  target.write('*') 

#Now we are going to find the member of the tables list containing the string
#for the table CDPSI01 and process that table 
#we know from the INFLT User Manual that CDPSI01 is based on CD = f(CN) 
#where CN = yawing moment coefficient
#The CDPSI01 table has only one part because there is only 1 indpendent variable 
#
#CDPSI01 is the table header for PSI data
independent_vars = []
for w in tables:
 print ("TRYING TO FIND CDPSI01 and w = ",w)
 print ("w[0:7]",w[0:7]) 
 if (w[0:7] == 'CDPSI01'):
  print ("found CDPSI01")
#
#the parser.get command is what is used in trialobyte to determine the output file
  tbl_out_CDPSI01 = parser.get('table_maker_output_CDPSI01', 'tbl_out_CDPSI01')
#open file to write the CDPSI01 table to 
  target = open(tbl_out_CDPSI01,'w')
  target.truncate()
#
  print ("w 8: ",w[8:])
  independent_vars = w[8:].split("1cn\n")
  cn = independent_vars[0]
  cn = cn.replace('  cn\n','').replace('\r','')
  print ("cn = ",cn)
  target.write(cn)
  target.write('*\n')
  cdpsi = independent_vars[1]
  cdpsi = cdpsi.replace('\n','').replace('\r','')
  print ("cdpsi = ",cdpsi)
  target.write(cdpsi)
#
#The following is the old method for finding the table CDPSI01 and processing that table. 
#It is completely commented out
#for w in tables:
 #if w[0:5] in 'CDPSI' and w[0:w.find("\n")] in user_chosen_headers:
  #cdpsi_name_var = w[0:w.find("\n")]
  #print ("found", cdpsi_name_var)
  ##Open the file that the CDPSI01 table will be written to
  ##Construct table maker output/json maker input file name instead of reading from config file
  #tbl_out_cdpsi = dat_string + '_' + cdpsi_name_var + '_tbl.txt  
  #target = open(tbl_out_cdpsi,'w')
  #target.truncate()
  #independent_vars = w[8:].split("11\n")  
  #print ("independent_vars[0] ",independent_vars[0])
  #CD = independent_vars[0]
  #CD = CD.replace('1\n', '1#').replace('\r','')
  #CD = CD.replace('\n','')
  #target.write(CD)
  #target.write('*\n')
#
#Now we are going to find the member of the tables list containing the string
#for the table FNBLDF01 and process that table 
#we know from the INFLT User Manual that FNBLDF01 is based on FN = F(mach,alt)
#
#We want to split the FNBLDF01 table into 3 parts; part 1 consists of the independent variable 
#mach values, part 2 consists of the independent variable alt values, and part 3 consists of the 
#dependent variable FN values.
#
for w in tables:
 if w[0:6] == 'FNBLDF' and w[0:w.find("\n")] in user_chosen_headers:
  fnbldf_name_var = w[0:w.find("\n")]
  print ("found ", fnbldf_name_var)

#*** HARD CODED OPTION FOR DETERMINING table maker output file for FNBLDF01
  #fn = "JBx37800W_FNBLDF01_tbl.txt"
  #fn = "P8podup_FNBLDF01_tbl.txt"
  #fn = "37800WCFM56_57_WFTAB101_tbl.txt"
#***

#Open file to write the FNBLDF table to 
  tbl_out_FNBLDF = dat_string + '_' + fnbldf_name_var + '_tbl.txt'
  print("FNBLDF Table Maker Output/JSON Maker Input file = ", tbl_out_FNBLDF)  
  target = open(tbl_out_FNBLDF,'w')  
  target.truncate()
  independent_vars = w[8:].split("11\n")
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  print ("HERE IS ALT ",alt) 
  #alt = mach.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  index11 = []
#Now we want to find the index in w which is the start of the FN values
#this would be right after the 2nd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
# print "index11[2] ",index11[2]
# print "Here"  
# outer = index11[2] + 100
# print w[index11[2]:outer]  
  FNbld = w[index11[1]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(FNbld)
  target.write('*')
  print ("FNBLDF Values")
  print (FNbld)
  print ("DONE PRINTING FNBLDF VALUES")
#
#Now we are going to find the member of the tables list containing the string
#for the table CDWM01 and process that table 
#The CDWM01 table has two one parts because there are 2 indpendent variables 
#
#open file to write the CDWM01 table to
#BEGIN CDWM01, this is the table header for Yaw Drag data
#open file to write the CDWM01 table to 
independent_vars = []
index11 = []
#
for w in tables:
 if (w[0:6] == 'CDWM01'):
  print ("found CDWM01")
#the parser.get command is what used in trialobyte  
  tbl_out_CDWM01 = parser.get('table_maker_output_CDWM01', 'tbl_out_CDWM01')
  #tbl_out_CDWM01 = "P8clean_CDWM01_tbl.txt"
  #tbl_out_CDWM01 = "P8poddn_CDWM01_tbl.txt"
  #tbl_out_CDWM01 = "JBx37800W_CDWM01_tbl.txt"
  #tbl_out_CDWM01 = "notneeded.txt"
  target = open(tbl_out_CDWM01,'w')
  target.truncate()
  independent_vars = w[7:].split("11\n")  
  print ("independent_vars[0] ",independent_vars[0])
  CL = independent_vars[0]
  CL = CL.replace('1\n', '1#').replace('\r','')
  CL = CL.replace('\n','')
  target.write(CL)
  target.write('*\n')
  mach = independent_vars[1]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
#Now we want to find the index in w which is the start of the CD values
#this would be right after the 2nd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS")
  print ("index11[1] ",index11[1])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  #outer2 = index11[2] + 100
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  #print ('2 ',w[index11[2]:outer2]) 
  CD = w[index11[1]:]
  print ("CD VALUES ")
  print (CD)
  target.write(CD)
  target.write('*')
  print ("DONE PRINTING CD VALUES")
# print "Complete CDWM Table"
#
#!!!
#At this point in the process we have written out four string objects
#to the files WFTAB101_tbl.txt, CDBASE01_tbl.txt and/or to similarly named
#output files.
#For WFTAB101 these four string objects are:
#   FNod - contains the entire set of thrust over delta values as one string
#   mach - contains the entire set of mach values as one string
#   alt - contains all of the altitude values as one string
#   WF - contains the entire set of fuel flow values as one string
#Within WFTAB101_tbl.txt, the four strings are separated by the delimiter '*'
#

print ("Done with Table Maker Module")

#####################################
#### JSON Maker Execution module ####
#####################################
# Following the execution of the table maker module, we have a group of .txt files 
# containing data for a specific dependent variable. These files need to be prcoessed
# by the corresponding JSON Maker program for that dependent variable data type.

# Instatiate list of json maker programs being requested to run
jsnmkr_programs_run = []

# Instatiate list of json maker programs being requested to run
jsnmkr_programs_run = []

# Instatiate list of json maker outputs
jsnmkr_outputs_list = []

# Run a Json Maker for each data table header selected in the configuration file.
for header in separated_header_names:
    #Construct Jspm Maker program name
    jm = "INFLT_" + header + "_table_to_JSON.py"
    print ("Now opening Json Maker", jm, "\n") 
    
    # Construct Json Maker input/output file names to send as arguments to the Json Maker program
    jsnmkr_input = dat_string + '_' + header + '_tbl.txt'
    jsnmkr_output = dat_string + '_' + header + '_JSON.txt'
    print('JSON Maker input/output filenames =', jsnmkr_input, jsnmkr_output)
    
    #Pass Json Maker input/output filenames to each Json maker as arguments
    sys.argv = [jsnmkr_input, jsnmkr_output]
    print("executing json maker ", jm , "using arguments ", sys.argv)
    #execfile(jm)                 # For python 2.7
    exec(open(jm).read())         # For python 3  

    #Add name of Json Maker program to the list of Json Makers run
    jsnmkr_programs_run.append(jm)
    #Add Json Maker output file to list for later use by IAPVE.py
    jsnmkr_outputs_list.append(jsnmkr_output)


##############################
### IAPVE Module ###
##############################
"""
# Following the execution of the Json makers module, we have a group of _JSON.txt files. 
# IAPVE.py is a separate program responsible for reading from the INFLT tables stored 
# as Json strings in the _JSON.txt files.

#Execute IAPVE.py
exec(open('IAPVE.py').read()) 

iapve_output = open('multiple_city_pairs_July23_737-800W.csv','a',newline='')
writer = csv.writer(outfile,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
header_row = ["PointID","WFvalue","Fuel_flow","Temperature","Weight","Mach","Altitiude","CL",\
              "CD","Lift","Drag","Thrust-per-eng/delta","Total_Thrust/delta","Plan_fuel_flow","Delta_fuel_flow","% diff"] 
writer.writerow(header_row)
"""


######################################################
### Verification Comparison Analysis Tool (VerCAT) ###
######################################################

# Read in name of VerCAT program from config file
parser.read(config_file_name)
vercat_prgrm = parser.get("vercat","vercat_prgrm")
print ("VerCAT program is ", vercat_prgrm)

#*** HARD CODED OPTION for Getting Statistical Analysis path, input file, and program name ***#
#StatAnalyprgrm = '/Users/Daniel/Documents/SIMCON/Git/bitbucket/IAPVE_dev/StatisticalAnalysisProgram'
vercat_input = "Nov 6 2015 Validation P8clean  Hold Scenarios multpl_disas_winds.csv"

# Call vc_argread to get the VerCAT input filename from command line
#print ("Calling vc_argread \n")
#if __name__ == "__vc_argread__":
#   vc_argread(sys.argv[1:])
#   print ("vc_argread function found input file:", vercat_input)


# Run VerCAT using the user specified .csv input file as an argument
vercat_cmd = "Rscript" + scrpt_dir + "/" + vercat_prgrm + " " + vercat_input
print ("Full vercat_cmd is", vercat_cmd)
os.system(vercat_cmd)   

#*** VERCAT INPUT SPECIFIED BY IAPVE/TRIALOBYTE OPTION ***#
# Set the input file names to be sent to VerCat as arguments
#new_iapve_files = []
#for iar_file in iapve_outputs:
# if ('iar' in iar_file):
#    new_iapve_files.append(iar_file)
#print('new_iapve_files are ', new_iapve_files)
#iapve_filename1 = new_iapve_files[0]
#iapve_filename2 = new_iapve_files[1]

print ("Completed running VerCAT")
print ("Finished Trialobyte \n")
