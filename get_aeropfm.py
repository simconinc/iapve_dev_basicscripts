#get_aeropfm
#This is a library of functions that accepts the following inputs
#of independent variables and returns values for dependent variables
#such as required thrust over delta and max climb thrust: 
#
#NOTE:
#keys obtained from dictionaries loaded via json.loads
#are always strings. But the value associated with any 
#given key maintains its original type
#
import json, sys
import std_atm
import getCDWF
import reynolds
import CLBuffet
import get_CDWM
import get_yawdragcoefficient
#from Biparabolic1 import Biparabolic1
from Biparabolic1_pflg import Biparabolic1_pflag
from badgerzorro import Biparabolic2
#
import math
import get_fromFNCLB
import IAPVEutils
from Linear import Linear
#
import math
#
import get_fromFNCNT
#
#S is the surface area of the wing in square feet
S = 1341.0
#G is the acceleration of gravity in ft/sec**2
G = 32.17405
#
pflag = 0
#
def get_FNod_cruise(geefactor,ftemperature,fmach,falt,fweight,fcl,aircraft_model,aircraft_config,pflag):
#
#This function determines and returns lift coefficient, total drag coefficient,
#lift, drag, and required thrust over delta for a trajectory in which the aircraft
#is cruising at a constant flight level at a constant speed.  
# 
#Input Parameters are:
#ftemperature - temperature in deg C
#fmach - mach speed (e.g. 0.77)
#falt - altitude in feet (e.g. 38000.00)
#fcl - lift coefficient
#      if known provide
#      if not known set as 0.0 and the get_FNod will calculate cl and include it in the return
#aircraft_mode - aircraft model (e.g. '37800W')
#aircraft_config - aircraft configuration number (typically a number between 1 and 6)
#
#The values returned are:
#cl - lift coefficient
#theCDvalue - total drag coefficient (basic drag coefficient + reynolds drag coefficient +
#yaw drag coefficient + windmill drag coefficient).
#Note - yaw drag coefficient and windmill drag coefficient are only computed when
#aircraft_config =2. aircraft_config =2 signifies 1LE operation.
#
 #pflag = 0
 pflag = 1
 P8clean = False
 P8podup = False
 P8poddn = False
 JBx37800W = False
 if (aircraft_model == "p8clean" or aircraft_model == "P8clean"):
  P8clean = True
  JBx37800W = False
  P8podup = False
 if (aircraft_model == "37800W"):
  P8clean = False
  JBx37800W = True
  P8podup = False
 if (aircraft_model == "p8podup" or aircraft_model == "P8podup"):
  P8clean = False
  JBx37800W = False
  P8podup = True
 if (aircraft_model == "p8poddn" or aircraft_model == "P8poddn"):
  P8clean = False
  JBx37800W = False
  P8poddn = True
#
 if (pflag): print ("JUST STARTING get_FNod and aircraft_config = ",aircraft_config)
 if (pflag): print ("BLACK JAGUAR CAT JBx37800W = ",JBx37800W)
 if (pflag): print ("P8clean = ",P8clean)
 if (pflag): print ("P8podup = ",P8podup)
 CD = {}
#WF = {}
#
#CDBASE TABLES ARE UNIQUE FOR EACH MODEL 
#FOR A GIVEN MODEL, CONFIG 1&2 USE THE SAME CDBASE TABLE AND
#FLAPS 1, 5, 10, 15 (CONFIGS 3,4,5,6) USE UNIQUE TABLES
#
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8clean_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8clean_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8clean_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8clean_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8clean_CDBASE06_JSON.txt')
  #WF_file = open('P8clean_WFTAB101_JSON.txt')
#
 if (P8podup):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8podup_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8podup_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8podup_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8podup_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8podup_CDBASE06_JSON.txt')
  #WF_file = open('P8podup_WFTAB111_JSON.txt')
#
 if (P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8poddn_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8poddn_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8poddn_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8poddn_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8poddn_CDBASE06_JSON.txt')
  #WF_file = open('P8podup_WFTAB111_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('JBx37800W_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('JBx37800W_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('JBx37800W_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('JBx37800W_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('JBx37800W_CDBASE06_JSON.txt')
  #WF_file = open('JBx37800W_WFTAB101_JSON.txt')
#
 a1string = CD_file.readline()
 CD = json.loads(a1string)
 #a2string = WF_file.readline()
 #WF = json.loads(a2string)
 theta_ambient = (ftemperature + 273.15)/288.15
#fmach = ftas/(661.4786 * (theta_ambient ** (0.5)))
 int_mach = int((fmach*100.0) + .5)
 mach = str(int_mach)
 int_alt = int(falt)
 alt = str(int_alt)
#Compute the Lift Coefficient cl
#Use Eqn 9 of Chapter 9 in Jet Transport Performance Methods.pdf
 L =  fweight*geefactor
 print ("WARYBERRY fweight geefactor ",fweight," ",geefactor)
 dlta = std_atm.alt2press_ratio(falt)
 print("L falt dlta fmach S = ",L," ",falt," ",dlta," ",fmach," ",S)
 clnogee = fweight/(1481.4*(fmach**2)*dlta*S)
 print ('clnogee = ',clnogee)
 if (fcl == 0.0):
  try:
   #cl = L/(1481.4*(fmach**2)*dlta*S)
   cl = L/(1481.35*(fmach**2)*dlta*S)
  except:
   cl = 1.80
  print ("COMPUTED CL = ",cl)
 else:
  cl = fcl
  print ("CL from calling program = ",cl)
 print ("final cl = ",cl)
 if (P8clean or P8podup or P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffetP8_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffetP8_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffetP8_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffetP8_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffetP8_06(cl,fmach)
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffet37800W_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffet37800W_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffet37800W_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffet37800W_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffet37800W_06(cl,fmach)
# 
 if (pflag): print ("clmax = ",clmax)
 if (clmax >= (cl*0.001)):
  cl_list = []
  cd_list = []
  available_CDmachs = []
  CDvalue_list = []
  available_CDmachs = getmach_fromCDBASE(aircraft_model,aircraft_config,pflag)
  if (pflag): print(" ")
  if (pflag): print("available_CDmachs = ",available_CDmachs)
  for mgrab in available_CDmachs:
   if (pflag): print("From CDBASE01 ")
   if (pflag): print ("CD[mgrab] ",CD[str(mgrab)])
   cl_list = CD[str(mgrab)][0]
   cdho_list = CD[str(mgrab)][1]
   cd_list = [xcd*0.000001 for xcd in cdho_list]
   if (pflag): print ("cl = ",cl," ahead of getting CDvalue from Biparabolic1")
   if (isinstance(cl_list[0],str)):
    if (pflag): print ("ripper cat")
    clfloat = []
    for ichg in cl_list:
     clfloat.append(float(ichg))
    cl_list = clfloat
   if (pflag): print ("GOBLUE ",cl_list," ",cd_list)
   CDvalue = Biparabolic1_pflag(cl_list,cd_list,cl,'cd',pflag)
   CDvalue_list.append(CDvalue)
   if (pflag): print("For mgrab = ",mgrab," CDvalue from Biparabolic1 = ",CDvalue)
   if (pflag): print(" ")
 else:
  CDvalue_list = [-888]
#CDvalue = 0.22285
#
 if (CDvalue_list[0] == -888):
  theCDvalue = -888
  FFNod = -888
  D = -888
  return(cl,theCDvalue,L,D,FFNod)
 else:
  if (pflag): print("NOODLE call to Biparabolic1 where cl = ",cl)
  fmachable_list =[(float(xv)*0.001) for xv in available_CDmachs]
  if (pflag): print("fmachable_list = ",fmachable_list) 
  if (pflag): print("fmach and CDvalue_list = ",fmach," ",CDvalue_list)
  theCDBASEvalue = Biparabolic1_pflag(fmachable_list,CDvalue_list,fmach,'cd',pflag)
  #if (pflag): print("theCDBASEvalue = ",theCDBASEvalue)
  if (aircraft_config <= 6):
   theReyCDvalue = reynolds.reynolds((fweight*geefactor),fmach,falt,ftemperature)
  else:
   if (pflag): print ("NO REYNOLDS CORRECTION FOR THIS CONFIG")
   theReyCDvalue = 0.0
  if (pflag): print("theReyCDvalue = ",theReyCDvalue," geefactor = ",geefactor)
  CDbasic = theCDBASEvalue + theReyCDvalue
#aircraft_config ==2 means 1LE
  if (aircraft_config == 2):
   CDwm = get_CDWM.get_CDWM(fmach,falt,cl,aircraft_config)
   CDbasic_plus_CDWM = CDbasic + CDwm
   if (pflag): print ("CDbasic_plus_CDWM CDbasic CDwm ",CDbasic_plus_CDWM," ",CDbasic," ",CDwm)
   estDrag = CDbasic_plus_CDWM * (1481.4*(fmach**2)*dlta*S)
   wingspan = 112.58
   moment_arm = 16.14
   #sqb =  1481.4*(fmach**2)*dlta*S*wingspan
   CDyaw = get_yawdragcoefficient.get_ydc(estDrag,dlta,S,wingspan,moment_arm,fmach,falt,aircraft_config)
   #if (fmach > 0.45):
    #theCDvalue = theCDvalue*1.061
   #else:
    #theCDvalue = theCDvalue*1.07
   theCDvalue = CDbasic_plus_CDWM + CDyaw
   if (pflag): print ("END OF 1LE section in get_FNod and theCDvalue = ",theCDvalue,\
          " CDbasic = ",CDbasic," CDwm = ",CDwm,\
          " CDbasic_plus_CDWM = ",CDbasic_plus_CDWM," CDyaw = ",CDyaw)
  else:
   CDwm = 0.0
   CDyaw = 0.0
   theCDvalue = CDbasic
#
  pflag = 1
  if (pflag): print ("theCDvalue theCDBASEvalue theReyCDvalue ",theCDvalue," ",theCDBASEvalue," ",theReyCDvalue)
  if (pflag): print("AGAIN L falt dlta fmach = ",L," ",falt," ",dlta," ",fmach)
#Now compute the Drag
#See A-15 in Jet Transport Performance Methods.pdf
#
  #D = theCDvalue * (1481.4*(fmach**2)*dlta*S) 
  D = theCDvalue * (1481.35*(fmach**2)*dlta*S) 
  if (pflag): print ("Drag = ",D)
  if (aircraft_config == 2):
   FFNod = (D/dlta)
  else:
   FFNod = (D/dlta)/2.0
  if (pflag): print ("Cole Younger FFNod from get_FNod = ",FFNod,)
  #FFNod = 27000.00
  pflag = 0
  return(cl,clmax,theCDvalue,L,D,FFNod)
#
#
def getmach_fromCDBASE(aircraft_model,aircraft_config,pflag):
#returns a list of the available machs from CDBASE01_JSON.txt
# 
 CD = {}
 available_machs = []
 if (pflag): print ("In getmach_fromCDBASE and aircraft_config = ",aircraft_config)
#CDBASE TABLES ARE UNIQUE FOR EACH MODEL AND UNIQUE FOR EACH FLAP SETTING FOR A GIVEN MODEL
#
 P8clean = False
 P8podup = False
 P8poddn = False
 JBx37800W = False
 pflag = 0
#
 if (aircraft_model == '37800W'):
  JBx37800W = True
#
 if (aircraft_model == 'P8clean' or aircraft_model == 'p8clean'):
  P8clean = True
#
 if (aircraft_model == 'P8podup' or aircraft_model == 'p8podup'):
  P8podup = True
 if (aircraft_model == 'P8poddn' or aircraft_model == 'p8poddn'):
  P8poddn = True
#
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8clean_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8clean_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8clean_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8clean_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8clean_CDBASE06_JSON.txt')
   WF_file = open('P8clean_WFTAB101_JSON.txt')
#
 if (P8podup):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8podup_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8podup_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8podup_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8podup_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8podup_CDBASE06_JSON.txt')
#WFTAB111 and WFTAB114 tables are the same for podup and poddn
  if (aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt')
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8poddn_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8poddn_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8poddn_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8poddn_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8poddn_CDBASE06_JSON.txt')
#WFTAB111 and WFTAB114 tables are the same for podup and poddn
  if (aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt')
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('JBx37800W_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('JBx37800W_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('JBx37800W_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('JBx37800W_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('JBx37800W_CDBASE06_JSON.txt')
  WF_file = open('P8clean_WFTAB101_JSON.txt')
#
 a1string = CD_file.readline()
 if (pflag): print ("a1string = ",a1string)
 CD = json.loads(a1string)
 available_machs = CD.keys()
 str_mach = []
 int_mach = [] 
 for iamachs in available_machs:
  #if (pflag): print ("BARN CATS iamachs = ",iamachs)
  str_mach.append(iamachs)
 for jamachs in str_mach:
  int_mach.append(int(jamachs)) 
 int_mach.sort()
 return(int_mach)
#
#
def get_maxclimbthrust(falt,fmach,temperature,aircraft_model,aircraft_config,pflag):
    if (pflag): print ("MACABEE DETERMINING THE VALUE for max climb thrust over delta")
    if (pflag): print ("falt fmach temperature aircraft_model aircraft_config as follows")
    if (pflag): print (falt," ",fmach," ",temperature," ",aircraft_model," ",aircraft_config)
    fidisa = IAPVEutils.get_disa_degF(falt,temperature) 
    if (pflag): print ("fidisa = ",fidisa)
    if (aircraft_config == 2):
     engfac = 1.0
    else:
     engfac = 2.0
#
    FNCLBodvalue_disa_alt_list = []
#
    FNCLBmachlist = []
    FNCLBodlist = []
#
    FNCLBalts_list = []
    FNCLBod_list = []
    FNCLBalts_count = 0
    intdisa = []
    intalt = []
    pflag = 0
#
#Establish FNCLBdisa_alts dictionary for use further down
    available_FNCLBdisa_alts_dict = {}
    available_FNCLBdisa_alts_dict = getdisa_fromFNCLB101(aircraft_model,aircraft_config,pflag)
#
    for strdisa in available_FNCLBdisa_alts_dict.keys():
     intdisa.append(int(strdisa))
    intdisa.sort()
    if (pflag): print ("intdisa = ",intdisa)
#
# cycle through available_FNCLBdisa_alts_dict
    for idi in intdisa: 
     for stralt in available_FNCLBdisa_alts_dict[str(idi)]:
      intalt.append(int(stralt))
     intalt.sort()
     for iai in intalt:
      #if (pflag): print ("idi & iai ",idi," ",iai," AND THE FIXID IS",adict["PointID"])
      if (pflag): print ("idi & iai ",idi," ",iai)
      (FNCLBmachlist,FNCLBodlist) = get_lists_fromFNCLBod(str(idi),str(iai),aircraft_model,aircraft_config,pflag)
      if (pflag): print ("Ahead of the 1st BiParabolic or Linear call for determining FNCLBod")
      if (pflag): print("FNCLBmachlist ",FNCLBmachlist)
      if (pflag): print("FNCLBodlist ",FNCLBodlist)
      if (pflag): print("fmach = ",fmach)
      if (FNCLBmachlist[0] !=  -888):
       fmachle =[float(xv) for xv in FNCLBmachlist]
      #for a given disa and altitude apply Biparabolic to the applicable
      #FNCLBmachlist list and FNCLBodlist to get a value for FNCLBodvalue.
      #This value is then added to FNCLBod_alts_list 
       #FNCLBodvalue = Biparabolic1_pflag(fmachle,FNCLBodlist,fmach,'maxclimbthrust',pflag)
       FNCLBodvalue = Linear(fmachle,FNCLBodlist,fmach)
       if (pflag): print ("After 1st Biparabolic or Linear call and FNCLBodvalue = ",FNCLBodvalue)
       FNCLBod_list.append(FNCLBodvalue)
       FNCLBalts_list.append(iai)
       FNCLBalts_count = FNCLBalts_count + 1
       if (pflag): print ("AT THE END OF THE INNER LOOP for iai = ",iai)
       if (pflag): print ("  ") 
     if (pflag): print ("For idi = ",idi)
     if (pflag): print ("Ahead of the 2nd Biparabolic or Linear call for FNCLB and falt = ",falt)
     if (pflag): print ("FNCLBalts_list = ",FNCLBalts_list)
     if (pflag): print ("FNCLBod_list = ",FNCLBod_list)
     #FNCLBodvalue_disa = Biparabolic1_pflag(FNCLBalts_list,FNCLBod_list,falt,'maxclimbthrust',pflag)
     FNCLBodvalue_disa = Linear(FNCLBalts_list,FNCLBod_list,falt)
     #alt_dict_forif (pflag): print[str(ialt)] = mach_dict_forif (pflag): print  
     if (pflag): print ("")
     FNCLBodvalue_disa_alt_list.append(FNCLBodvalue_disa)
     if (pflag): print ("After 2nd Biparabolic or Linear call")
     if (pflag): print ("falt and fmach = ",falt," ",fmach," and strdisa = ",strdisa)
     if (pflag): print ("FNCLBodvalue_disa = ",FNCLBodvalue_disa)
     if (pflag): print ("FNCLBodvalue_disa_alt_list = ",FNCLBodvalue_disa_alt_list)
#
     FNCLBmachlist = []
     FNCLBodlist = []
     FNCLBalts_list = []
     FNCLBod_list = []
     FNCLBalts_count = 0
     intalt = []
#
    if (pflag): print("Finished the outer loop and FNCLBodvalue_disa = ",FNCLBodvalue_disa)
    if (pflag): print("intdisa = ",intdisa)
    if (pflag): print (" ")
    if (pflag): print ("FNCLB JAGUAR TERRITORY")
    ISAtempdev_list =[float(xv) for xv in intdisa]
    #ISAtempdev_list.sort()
    if (pflag): print ("Ahead of the 3rd BiParabolic call for FNCLBodvalue")
    if (pflag): print ("FNCLBodvalue_disa_alt_list = ",FNCLBodvalue_disa_alt_list)
    if (pflag): print ("ISAtempdev_list = ",ISAtempdev_list)
    if (pflag): print ("fidisa = ",fidisa)
    #theFNCLBodvalue = Biparabolic1_pflag(ISAtempdev_list,FNCLBodvalue_disa_alt_list,fidisa,'maxclimbthrust',pflag) 
    theFNCLBodvalue = Linear(ISAtempdev_list,FNCLBodvalue_disa_alt_list,fidisa)   
    #if (pflag): print ("FOR ",adict["PointID"])  
    if (pflag): print ("theFNCLBodvalue =  ",theFNCLBodvalue)
    #totalavlbclimbthrust = theFNCLBodvalue * 2.0
    totalavlbclimbthrust = theFNCLBodvalue * engfac
    if (pflag): print ("END OF FNCLB JAGUAR TERRITORY")
    return(totalavlbclimbthrust)
#@#$************************************************************
#
def get_lists_fromFNCLBod(strdisa,stralt,aircraft_model,aircraft_config,pflag):
 pflag = 0
 FNCLB = {}
 mach_list = {}
 FNCLBod_list = {}
#For now we default to JBx37800W and we don't bother with aircraft_config
 if (aircraft_model == '37800W'):
  FNCLB_file = open('JBx37800W_FNCLB101_JSON.txt')
#
 if (aircraft_model == 'P8clean' or aircraft_model == 'p8clean'):
  FNCLB_file = open('P8clean_FNCLB101_JSON.txt')
#
#THERE IS ONLY ONE FNCLB01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_FNCLB01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup AND P8poddn
 if (aircraft_model == 'P8podup' or aircraft_model == 'p8podup'\
     or aircraft_model == 'P8poddn' or aircraft_model == 'p8poddn'):
  FNCLB_file = open('P8podup_FNCLB101_JSON.txt')
#
 a2string = FNCLB_file.readline()
 FNCLB = json.loads(a2string)
 if (pflag): print ("In get_lists_fromFNCLBod and stralt strdisa = ",stralt," ",strdisa)
 try:
  mach_list = FNCLB[strdisa][stralt][0]
  FNCLBod_list = FNCLB[strdisa][stralt][1]
 except:
  mach_list = [-888]
  FNCLBod_list = [-888]
 if (pflag): print("AT THE END OF get_max_FNCLBod for these strdisa and stralt values ",\
        strdisa," ",stralt)
 return(mach_list,FNCLBod_list)
#
def getdisa_fromFNCLB101(aircraft_model,aircraft_config,pflag):
#Returns the dictionary available_alts_dict that is structured as follows:
#top level keys are disa values such as '-72'
#The value associated with each top level key is a list of the available alts for that
#altitude. The alts in each list are each strings such as '37000' representing. 
#The reason they are strings is that they themselves will serve as keys
# 
 pflag = 0
 diag = 3
 #P8clean = True
 #JBx37800W = False
 #P8podup = False
 FNCLB = {}
 available_disa = [] 
 available_alts_list = []
 available_alts_dict = {}
 if (aircraft_model == 'P8clean' or aircraft_model == 'p8clean'):
  FNCLB_file = open('P8clean_FNCLB101_JSON.txt') 
#
#THERE IS ONLY ONE FNCLB01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_FNCLB01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup AND P8poddn
 if (aircraft_model == 'P8podup' or aircraft_model == 'p8podup'\
     or aircraft_model == 'P8poddn' or aircraft_model == 'p8poddn'):
  FNCLB_file = open('P8podup_FNCLB101_JSON.txt') 
#
 if (aircraft_model == '37800W'):
  FNCLB_file = open('JBx37800W_FNCLB101_JSON.txt')
 a1string = FNCLB_file.readline()
 FNCLB = json.loads(a1string)
 available_disas = FNCLB.keys()
 #str_disa = []
 #for strdisas in available_disas:
  #str_disa.append(strdisas)
 for jdisas in available_disas:
  if (pflag): print("FNCLB[jdisas]keys = ",FNCLB[jdisas].keys())
  for striaa in FNCLB[jdisas].keys():
   #if (pflag):
    #if (pflag): print ("striaa = ",iaa)
   available_alts_list.append(striaa)
   if (pflag): print ("available_alts_list  ",available_alts_list)
  available_alts_dict.update({jdisas:available_alts_list})
  available_alts_list = []
  if (pflag): print ("jdisas & available_alts_dict[jdisas] = ",jdisas," ",available_alts_dict[jdisas])
 return(available_alts_dict)
#
#
def get_FNodclbdesc(ftemperature,fmach,falt,fweight,fgama,faccelerate,aircraft_model,aircraft_config,pflag):
 exact_match = False
 CD = {}
 WF = {}
#
 if (aircraft_config == 1 or aircraft_config == 2):
  diffcheck = 0.25
 else:
  diffcheck = 0.9
#
 P8clean = False
 P8podup = False
 P8poddn = False
 JBx37800W = False
 if (aircraft_model == "p8clean" or aircraft_model == "P8clean"):
  P8clean = True
  JBx37800W = False
  P8podup = False
 if (aircraft_model == "37800W"):
  P8clean = False
  JBx37800W = True
  P8podup = False
 if (aircraft_model == "p8podup" or aircraft_model == "P8podup"):
  P8clean = False
  JBx37800W = False
  P8podup = True
 if (aircraft_model == "p8poddn" or aircraft_model == "P8poddn"):
  P8clean = False
  JBx37800W = False
  P8poddn = True
#
#CDBASE TABLES ARE UNIQUE FOR EACH MODEL
#WITHIN A MODEL, THE CDBASE TABLES ARE ALSO 
#UNIQUE FOR EACH FLAPS SETTING 
#(flaps up, 1, 5, 10, 15)
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8clean_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8clean_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8clean_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8clean_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8clean_CDBASE06_JSON.txt')
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR ALL NON POD MODELS
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR THE POD MODELS
  WF_file = open('P8clean_WFTAB101_JSON.txt')
  if (aircraft_config == 2):
   WF_file = open('P8clean_WFTAB104_JSON.txt')
#
 if (P8podup):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8podup_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8podup_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8podup_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8podup_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8podup_CDBASE06_JSON.txt')
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR ALL NON POD MODELS
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR THE POD MODELS
  WF_file = open('P8podup_WFTAB111_JSON.txt')
  if (aircraft_config == 2):
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8poddn_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8poddn_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8poddn_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8pddn_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8poddn_CDBASE06_JSON.txt')
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR ALL NON POD MODELS
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR THE POD MODELS
  WF_file = open('P8podup_WFTAB111_JSON.txt')
  if (aircraft_config == 2):
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('JBx37800W_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('JBx37800W_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('JBx37800W_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('JBx37800W_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('JBx37800W_CDBASE06_JSON.txt')
  WF_file = open('JBx37800W_WFTAB101_JSON.txt')
#
 a1string = CD_file.readline()
 CD = json.loads(a1string)
 a2string = WF_file.readline()
 WF = json.loads(a2string)
 theta_ambient = (ftemperature + 273.15)/288.15
#fmach = ftas/(661.4786 * (theta_ambient ** (0.5)))
 int_mach = int((fmach*100.0) + .5)
 mach = str(int_mach)
 int_alt = int(falt)
 alt = str(int_alt)
#Compute the Lift Coefficient cl
#Use Eqn 9 of Chapter 9 in Jet Transport Performance Methods.pdf
 L =  fweight * math.cos(fgama)
 dlta = std_atm.alt2press_ratio(falt)
 if (pflag): print("L falt dlta fmach S = ",L," ",falt," ",dlta," ",fmach," ",S)
 try:
  cl = L/(1481.4*(fmach**2)*dlta*S)
 except:
  cl = 0.80
 if (pflag): print ("COMPUTED CL = ",cl)
#
#CLEAN P8, P8podup, and P8poddn ALL USE THE SAME BUFFET TABELS
#
 if (P8clean or P8podup or P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffetP8_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffetP8_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffetP8_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffetP8_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffetP8_06(cl,fmach)
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffet37800W_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffet37800W_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffet37800W_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffet37800W_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffet37800W_06(cl,fmach)
# 
 if (pflag): print ("clmax = ",clmax)
 if (clmax >= (cl*0.001)):
#
  cl_list = []
  cd_list = []
  available_CDmachs = []
  CDvalue_list = []
  available_CDmachs = getCDWF.getmach_fromCDBASE(aircraft_config)
  if (pflag): print(" ")
  if (pflag): print("available_CDmachs = ",available_CDmachs)
#deterine sensible machs
  sensible_mach_list = []
  for trymach in available_CDmachs:
   checkmach = float(trymach)/1000.00
   differ = abs(fmach - checkmach)
   #if (differ <= 0.25):
   if (differ <= diffcheck): 
    sensible_mach_list.append(trymach)
   print("ELY MN sensible_mach_list = ",sensible_mach_list)
#
# for mgrab in available_CDmachs:
  for mgrab in sensible_mach_list:
#  if (pflag): print("From CDBASE01 ")
#  if (pflag): print ("CD[mgrab] ",CD[str(mgrab)])
   cl_list = CD[str(mgrab)][0]
   cdho_list = CD[str(mgrab)][1]
   cd_list = [xcd*0.000001 for xcd in cdho_list]
   #if (pflag): print ("cl = ",cl," ahead of getting CDvalue from Biparabolic1")
   if (isinstance(cl_list[0],str)):
    #if (pflag): print ("ripper cat")
    clfloat = []
    for ichg in cl_list:
     clfloat.append(float(ichg))
    cl_list = clfloat
   CDvalue = Biparabolic1_pflag(cl_list,cd_list,cl,'cd',pflag)
   CDvalue_list.append(CDvalue)
   if (pflag): print("For mgrab = ",mgrab," CDvalue from Biparabolic1 = ",CDvalue)
   if (pflag): print("SO FAR CDvalue_list = ",CDvalue_list)
   if (pflag): print(" ")
 else:
  CDvalue_list.append(-888)
#CDvalue = 0.22285
#
 if (CDvalue_list[0] == -888):
  theCDvalue = -888
  FFNod = -888
  D = -888
  return(cl,theCDvalue,L,D,FFNod)
 else:
  if (pflag): print("NOODLE call to Biparabolic1 where cl = ",cl)
  #fmachable_list =[(float(xv)*0.001) for xv in available_CDmachs] 
  fmachable_list =[(float(xv)*0.001) for xv in sensible_mach_list]
  if (pflag): print("fmachable_list = ",fmachable_list) 
  if (pflag): print("fmach and CDvalue_list = ",fmach," ",CDvalue_list)
  theCDBASEvalue = Biparabolic1_pflag(fmachable_list,CDvalue_list,fmach,'cd',pflag)
  #if (pflag): print("theCDBASEvalue = ",theCDBASEvalue)
  if (aircraft_config <= 2):  
   theReyCDvalue = reynolds.reynolds(fweight,fmach,falt,ftemperature)
  else:
   if (pflag): print ("NO REYNOLDS CORRECTION FOR THIS CONFIG")
   theReyCDvalue = 0.0
  if (pflag): print("theReyCDvalue = ",theReyCDvalue)
  theCDvalue = theCDBASEvalue + theReyCDvalue
#TEMPORARY FOR 1LE
  if (aircraft_config == 2):
   theCDvalue = theCDvalue*1.07
  if (pflag): print ("theCDvalue theCDBASEvalue theReyCDvalue ",theCDvalue," ",theCDBASEvalue," ",theReyCDvalue)
  if (pflag): print("AGAIN L falt dlta fmach = ",L," ",falt," ",dlta," ",fmach)
#Now compute the Drag
#See A-15 in Jet Transport Performance Methods.pdf
#
  mass = fweight/G
  D = theCDvalue * (1481.4*(fmach**2)*dlta*S) 
  eyow = G*math.sin(fgama)
  if (pflag): print ("mass faccelerate fgama Gtimessinofgama ",mass," ",faccelerate," ",fgama," ",eyow)
#since G is in ft/sec**2 we need faccelerate in ft/sec**2
  Thrust = D + mass*(faccelerate + G*math.sin(fgama)) 
  if (pflag): print ("Drag = ",D," Thrust = ",Thrust)
  if (aircraft_config == 2):
   FFNod = (Thrust/dlta)
  else:
   FFNod = (Thrust/dlta)/2.0
  #FFNod = 27000.00
  return(cl,theCDvalue,L,D,FFNod)
#
#
def get_lists_fromFNCRUod(strdisa,stralt,aircraft_model,aircraft_config,pflag):
 FNCRU = {}
 mach_list = {}
 FNCRUod_list = {}
#For now we default to JBx37800W and we don't bother with aircraft_config
 #P8clean = False
 #P8clean = True
 #P8podup = False
 #JBx37800W = True
 #JBx37800W = False
 P8clean = False
 P8podup = False
 P8poddn = False
 JBx37800W = False
 if (aircraft_model == "p8clean" or aircraft_model == "P8clean"):
  P8clean = True
  JBx37800W = False
  P8podup = False
 if (aircraft_model == "37800W"):
  P8clean = False
  JBx37800W = True
  P8podup = False
 if (aircraft_model == "p8podup" or aircraft_model == "P8podup"):
  P8clean = False
  JBx37800W = False
  P8podup = True
 if (aircraft_model == "p8poddn" or aircraft_model == "P8poddn"):
  P8clean = False
  JBx37800W = False
  P8poddn = True
#
 if (JBx37800W):
  FNCRU_file = open('JBx37800W_FNCRU101_JSON.txt')
#
 if (P8clean):
  FNCRU_file = open('P8clean_FNCRU101_JSON.txt')
#
#THERE IS ONLY ONE FNCRU01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_FNCRU01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup AND P8poddn
 if (P8podup or P8poddn):
  FNCRU_file = open('P8podup_FNCRU101_JSON.txt')
#
 a2string = FNCRU_file.readline()
 FNCRU = json.loads(a2string)
 if (pflag): print ("In get_lists_fromFNCRUod and stralt strdisa = ",stralt," ",strdisa)
 try:
  mach_list = FNCRU[strdisa][stralt][0]
  FNCRUod_list = FNCRU[strdisa][stralt][1]
 except:
  mach_list = [-888]
  FNCRUod_list = [-888]
 if (pflag): print("AT THE END OF get_max_FNCRUod for these strdisa and stralt values ",\
        strdisa," ",stralt)
 return(mach_list,FNCRUod_list)
#
def getdisa_fromFNCRU101(aircraft_model,aircraft_config,pflag):
#Returns the dictionary available_alts_dict that is structured as follows:
#top level keys are disa values such as '-72'
#The value associated with each top level key is a list of the available alts for that
#altitude. The alts in each list are each strings such as '37000' representing. 
#The reason they are strings is that they themselves will serve as keys
# 
 diag = 3
 #P8clean = True
 #JBx37800W = False
 #P8podup = False
 P8clean = False
 P8podup = False
 P8poddn = False
 JBx37800W = False
 if (aircraft_model == "p8clean" or aircraft_model == "P8clean"):
  P8clean = True
  JBx37800W = False
  P8podup = False
 if (aircraft_model == "37800W"):
  P8clean = False
  JBx37800W = True
  P8podup = False
 if (aircraft_model == "p8podup" or aircraft_model == "P8podup"):
  P8clean = False
  JBx37800W = False
  P8podup = True
 if (aircraft_model == "p8poddn" or aircraft_model == "P8poddn"):
  P8clean = False
  JBx37800W = False
  P8poddn = True
#
 FNCRU = {}
 available_disa = [] 
 available_alts_list = []
 available_alts_dict = {}
 if (pflag): print ("IN get_fromFNCRU and P8clean P8podup JBx3700W ",\
         P8clean," ",P8podup," ",JBx37800W)
 if (P8clean):
  FNCRU_file = open('P8clean_FNCRU101_JSON.txt') 
#
#THERE IS ONLY ONE FNCRU01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_FNCRU01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup AND P8poddn
 if (P8podup or P8poddn):
  FNCRU_file = open('P8podup_FNCRU101_JSON.txt') 
#
 if (JBx37800W):
  FNCRU_file = open('JBx37800W_FNCRU101_JSON.txt')
 a1string = FNCRU_file.readline()
 FNCRU = json.loads(a1string)
 available_disas = FNCRU.keys()
 #str_disa = []
 #for strdisas in available_disas:
  #str_disa.append(strdisas)
 for jdisas in available_disas:
  if (pflag): print("FNCRU[jdisas]keys = ",FNCRU[jdisas].keys())
  for striaa in FNCRU[jdisas].keys():
   if (diag == 4):
    if (pflag): print ("striaa = ",iaa)
   available_alts_list.append(striaa)
   if (diag == 4): 
    if (pflag): print ("available_alts_list  ",available_alts_list)
  available_alts_dict.update({jdisas:available_alts_list})
  available_alts_list = []
  if (pflag): print ("jdisas & available_alts_dict[jdisas] = ",jdisas," ",available_alts_dict[jdisas])
 return(available_alts_dict)
#*****************************************************************
#
def get_maxcruisethrust(ftemperature,falt,fmach,aircraft_model,aircraft_config,pflag):
#Determine the value for max cruise thrust over delta (FNCRUod)
    if (aircraft_config == 2):
     engfac = 1.0
    else:
     engfac = 2.0
    if (pflag): print ("MACABEE DETERMINING THE VALUE for max cruise thrust over delta")
    #std_atm_temperature = std_atm.alt2temp(falt)
    #deviation = ftemperature - std_atm_temperature
    #if (deviation < 0.0):
     #deviation = deviation - 0.5
    #if (deviation > 0.0):
     #deviation = deviation + 0.5
    #idisa = int(deviation)
    #fidisa = float(idisa)
    fidisa = IAPVEutils.get_disa_degF(falt,ftemperature)
#Establish FNCRUdisa_alts dictionary for use further down
    available_FNCRUdisa_alts_dict = {}
    available_FNCRUdisa_alts_dict = getdisa_fromFNCRU101(aircraft_model,aircraft_config,pflag)
#
    FNCRUodvalue_disa_alt_list = []
#
    FNCRUmachlist = []
    FNCRUodlist = []
#
    FNCRUalts_list = []
    FNCRUod_list = []
    FNCRUalts_count = 0
    intdisa = []
    intalt = []
    for strdisa in available_FNCRUdisa_alts_dict.keys():
     intdisa.append(int(strdisa))
    intdisa.sort()
    if (pflag): print ("intdisa = ",intdisa)
#
# cycle through available_FNCRUdisa_alts_dict
    for idi in intdisa: 
     for stralt in available_FNCRUdisa_alts_dict[str(idi)]:
      intalt.append(int(stralt))
     intalt.sort()
     for iai in intalt:
      if (pflag): print ("idi & iai ",idi," ",iai)
      (FNCRUmachlist,FNCRUodlist) = get_lists_fromFNCRUod(str(idi),str(iai),aircraft_model,aircraft_config,pflag)
      #if (pflag): print ("Ahead of the 1st BiParabolic call for determining FNCRUod")
      if (pflag): print ("Ahead of the 1st Linear call for determining FNCRUod")
      if (pflag): print("FNCRUmachlist ",FNCRUmachlist)
      if (pflag): print("FNCRUodlist ",FNCRUodlist)
      if (pflag): print("fmach = ",fmach)
      if (FNCRUmachlist[0] !=  -888):
       fmachle =[float(xv) for xv in FNCRUmachlist]
      #for a given disa and altitude apply Biparabolic to the applicable
      #FNCRUmachlist list and FNCRUodlist to get a value for FNCRUodvalue.
      #This value is then added to FNCRUod_alts_list 
       #FNCRUodvalue = Biparabolic1_pflag(fmachle,FNCRUodlist,fmach,'maxcruisethrust',pflag)
       #if (pflag): print ("After 1st Biparabolic call and FNCRUodvalue = ",FNCRUodvalue)
       FNCRUodvalue = Linear(fmachle,FNCRUodlist,fmach)
       if (pflag): print ("After 1st Linear call and FNCRUodvalue = ",FNCRUodvalue)
       FNCRUod_list.append(FNCRUodvalue)
       FNCRUalts_list.append(iai)
       FNCRUalts_count = FNCRUalts_count + 1
       if (pflag): print ("AT THE END OF THE INNER LOOP for iai = ",iai)
       if (pflag): print ("  ") 
     if (pflag): print ("For idi = ",idi)
     #if (pflag): print ("Ahead of the 2nd Biparabolic call for FNCRU and falt = ",falt)
     if (pflag): print ("Ahead of the 2nd Linear call for FNCRU and falt = ",falt)
     if (pflag): print ("FNCRUalts_list = ",FNCRUalts_list)
     if (pflag): print ("FNCRUod_list = ",FNCRUod_list)
     #FNCRUodvalue_disa = Biparabolic1_pflag(FNCRUalts_list,FNCRUod_list,falt,'maxcruisethrust',pflag)
     FNCRUodvalue_disa = Linear(FNCRUalts_list,FNCRUod_list,falt)
     #alt_dict_forif (pflag): print[str(ialt)] = mach_dict_forif (pflag): print  
     if (pflag): print ("")
     FNCRUodvalue_disa_alt_list.append(FNCRUodvalue_disa)
     if (pflag): print ("After 2nd Linear call")
     if (pflag): print ("falt and fmach = ",falt," ",fmach," and strdisa = ",strdisa)
     if (pflag): print ("FNCRUodvalue_disa = ",FNCRUodvalue_disa)
     if (pflag): print ("FNCRUodvalue_disa_alt_list = ",FNCRUodvalue_disa_alt_list)
#
     FNCRUmachlist = []
     FNCRUodlist = []
     FNCRUalts_list = []
     FNCRUod_list = []
     FNCRUalts_count = 0
     intalt = []
#
    if (pflag): print("Finished the outer loop and FNCRUodvalue_disa = ",FNCRUodvalue_disa)
    if (pflag): print("intdisa = ",intdisa)
    if (pflag): print (" ")
    if (pflag): print ("FNCRU JAGUAR TERRITORY")
    ISAtempdev_list =[float(xv) for xv in intdisa]
    #ISAtempdev_list.sort()
    #if (pflag): print ("Ahead of the 3rd BiParabolic call for FNCRUodvalue")
    if (pflag): print ("Ahead of the 3rd Linear call for FNCRUodvalue")
    if (pflag): print ("FNCRUodvalue_disa_alt_list = ",FNCRUodvalue_disa_alt_list)
    if (pflag): print ("ISAtempdev_list = ",ISAtempdev_list)
    if (pflag): print ("fidisa = ",fidisa)
    #theFNCRUodvalue = Biparabolic1_pflag(ISAtempdev_list,FNCRUodvalue_disa_alt_list,fidisa,'maxcruisethrust',pflag)  
    theFNCRUodvalue = Linear(ISAtempdev_list,FNCRUodvalue_disa_alt_list,fidisa) 
    if (pflag): print ("theFNCRUodvalue =  ",theFNCRUodvalue)
    maxcruisethrust = theFNCRUodvalue * engfac
    if (pflag): print ("END OF FNCRU JAGUAR TERRITORY")
    return(maxcruisethrust)
#@#$************************************************************
#
def CL_to_buffet(fmach,aircraft_model,aircraft_config,pflag):
#
 P8clean = False
 P8podup = False
 P8poddn = False
 JBx37800W = False
 if (aircraft_model == "p8clean" or aircraft_model == "P8clean"):
  P8clean = True
  JBx37800W = False
  P8podup = False
 if (aircraft_model == "37800W"):
  P8clean = False
  JBx37800W = True
  P8podup = False
 if (aircraft_model == "p8podup" or aircraft_model == "P8podup"):
  P8clean = False
  JBx37800W = False
  P8podup = True
 if (aircraft_model == "p8poddn" or aircraft_model == "P8poddn"):
  P8clean = False
  JBx37800W = False
  P8poddn = True
#
 cl = 1.0
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffetP8_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffetP8_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffetP8_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffetP8_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffetP8_06(cl,fmach)
#
 if (P8podup or P8poddn):
#P8podup and P8poddn use the same CLB buffet tables as for P8Clean
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffetP8_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffetP8_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffetP8_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffetP8_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffetP8_06(cl,fmach)
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffet37800W_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffet37800W_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffet37800W_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffet37800W_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffet37800W_06(cl,fmach)
 return(clmax)
#
#
def get_maxconthrust(ftemperature,falt,fmach,aircraft_model,aircraft_config,pflag):
    FNCNTodvalue_disa_alt_list = []
    FNCNTmachlist = []
    FNCNTodlist = []
    FNCNTalts_list = []
    FNCNTod_list = []
    FNCNTalts_count = 0
    available_FNCNTdisa_alts_dict = {}
    intdisa = []
    intalt = []
#
    if (aircraft_config == 2):
     engfac = 1.0
    else:
     engfac = 2.0
#
    fidisaF = IAPVEutils.get_disa_degF(falt,ftemperature)
#
    available_FNCNTdisa_alts_dict = get_fromFNCNT.getdisa_fromFNCNT104()
    for iug in available_FNCNTdisa_alts_dict.keys():
     if (pflag): print ("available_FNCNTdisa_alts_dict key = ",iug)
     if (pflag): print ("available_FNCNTdisa_alts_dict value = ",available_FNCNTdisa_alts_dict[iug])
#
    for strdisa in available_FNCNTdisa_alts_dict.keys():
     intdisa.append(int(strdisa))
    intdisa.sort()
    print ("intdisa = ",intdisa)
#
# cycle through available_FNCNTdisa_alts_dict
    for idi in intdisa: 
     for stralt in available_FNCNTdisa_alts_dict[str(idi)]:
      intalt.append(int(stralt))
     intalt.sort()
     for iai in intalt:
      (FNCNTmachlist,FNCNTodlist) = get_fromFNCNT.get_lists_fromFNCNTod(str(idi),str(iai),aircraft_config)
      if (pflag): print ("Ahead of the 1st BiParabolic or Linear call for determining FNCNTod")
      if (pflag): print("FNCNTmachlist ",FNCNTmachlist)
      if (pflag): print("FNCNTodlist ",FNCNTodlist)
      if (pflag): print("fmach = ",fmach)
      if (FNCNTmachlist[0] !=  -888):
       fmachle =[float(xv) for xv in FNCNTmachlist]
      #for a given disa and altitude apply Biparabolic to the applicable
      #FNCNTmachlist list and FNCNTodlist to get a value for FNCNTodvalue.
      #This value is then added to FNCNTod_alts_list 
       #FNCNTodvalue = Biparabolic1(fmachle,FNCNTodlist,fmach,'maxclimbthrust')
       FNCNTodvalue = Linear(fmachle,FNCNTodlist,fmach)
       if (pflag): print ("After 1st Linear call and FNCNTodvalue = ",FNCNTodvalue)
       FNCNTod_list.append(FNCNTodvalue)
       FNCNTalts_list.append(iai)
       FNCNTalts_count = FNCNTalts_count + 1
       if (pflag): print ("AT THE END OF THE INNER LOOP for iai = ",iai)
       print ("  ") 
     print ("For idi = ",idi)
     if (pflag): print ("Ahead of the 2nd  Linear call for FNCNT and falt = ",falt)
     if (pflag): print ("FNCNTalts_list = ",FNCNTalts_list)
     if (pflag): print ("FNCNTod_list = ",FNCNTod_list)
     #FNCNTodvalue_disa = Biparabolic1(FNCNTalts_list,FNCNTod_list,falt,'maxclimbthrust')
     FNCNTodvalue_disa = Linear(FNCNTalts_list,FNCNTod_list,falt)
     #alt_dict_forprint[str(ialt)] = mach_dict_forprint  
     if (pflag): print ("")
     FNCNTodvalue_disa_alt_list.append(FNCNTodvalue_disa)
     if (pflag): print ("After 2nd Linear call")
     if (pflag): print ("falt and fmach = ",falt," ",fmach," and strdisa = ",strdisa)
     if (pflag): print ("FNCNTodvalue_disa = ",FNCNTodvalue_disa)
     if (pflag): print ("FNCNTodvalue_disa_alt_list = ",FNCNTodvalue_disa_alt_list)
#
     FNCNTmachlist = []
     FNCNTodlist = []
     FNCNTalts_list = []
     FNCNTod_list = []
     FNCNTalts_count = 0
     intalt = []
#
    if (pflag): print("Finished the outer loop and FNCNTodvalue_disa = ",FNCNTodvalue_disa)
    if (pflag): print("intdisa = ",intdisa)
    if (pflag): print (" ")
    if (pflag): print ("FNCNT JAGUAR TERRITORY")
    ISAtempdev_list =[float(xv) for xv in intdisa]
    #ISAtempdev_list.sort()
    if (pflag): print ("Ahead of the 3rd Linear call for FNCNTodvalue")
    if (pflag): print ("FNCNTodvalue_disa_alt_list = ",FNCNTodvalue_disa_alt_list)
    if (pflag): print ("ISAtempdev_list = ",ISAtempdev_list)
    if (pflag): print ("fidisaF = ",fidisaF)
    #theFNCNTodvalue = Biparabolic1(ISAtempdev_list,FNCNTodvalue_disa_alt_list,fidisa,'maxclimbthrust') 
    theFNCNTodvalue = Linear(ISAtempdev_list,FNCNTodvalue_disa_alt_list,fidisaF)   
    if (pflag): print ("theFNCNTodvalue =  ",theFNCNTodvalue)
    #totalavlbclimbthrust = theFNCNTodvalue * 2.0
    totalavlbcontcruisethrustod = theFNCNTodvalue * engfac
    print ("END OF FNCNT JAGUAR TERRITORY")
    print ("totalavlbcontcruisethrustod = ",totalavlbcontcruisethrustod)
    return(totalavlbcontcruisethrustod)

