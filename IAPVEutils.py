import std_atm
def get_disa_degF(falt,ftemperature):
 print("In get_disa_degF falt and ftemperature are ",falt," ",ftemperature)
 std_atm_temperature = std_atm.alt2temp(falt)
 #print("std_atm_temperature in deg C = ",std_atm_temperature)
 ftemperature_degF = ((ftemperature*9.0)/5.0) + 32.0
 #print("OAT in deg F at falt = ",ftemperature_degF)
 std_atm_temperature_degF = ((std_atm_temperature*9.0)/5.0) + 32.0
 #print ("std_atm_temperature in deg F = ",std_atm_temperature_degF)
 deviation = ftemperature_degF - std_atm_temperature_degF
 #print ("deviation_degF = ",deviation)
 fidisa = deviation
 if (deviation < 0.0):
  deviation = deviation - 0.5
 if (deviation > 0.0):
  deviation = deviation + 0.5
 idisa = int(deviation)
 #fidisa = float(idisa)
 print ("idisa and fidisa ",idisa," ",fidisa)
 return(fidisa)
def get_disa(falt,ftemperature):
 print("In get_disa falt and ftemperature are ",falt," ",ftemperature)
 std_atm_temperature = std_atm.alt2temp(falt)
 deviation = ftemperature - std_atm_temperature
 fidisa = deviation
 if (deviation < 0.0):
  deviation = deviation - 0.5
 if (deviation > 0.0):
  deviation = deviation + 0.5
 idisa = int(deviation)
 #fidisa = float(idisa)
 print ("idisa and fidisa ",idisa," ",fidisa)
 return(fidisa)
