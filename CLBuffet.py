#CLBUFF
from Biparabolic1 import Biparabolic1
import Linear
#import lagrange
from scipy.interpolate import lagrange
def CLBuffetP8_01(cl,fmach):
 mach_list = [.200000,.250000,.300000,.340000,.380000,.420000,.460000,.500000,.540000,.580000,\
 .600000,.620000,.660000,.700000,.720000,.740000,.752000,.760000,.780000,.820000]
 #old cl_tobuff = [1.37295,1.35221,1.25688,1.20841,1.15142,1.09535,1.04233,.991582,.954011,.912641,\
 #.886740,.898918,.915199,.919445,.907130,.884049,.860226,.840352,.776696,.646839]
 cl_tobuff = [1.31957,1.30703,1.25688,1.20841,1.15142,1.09535,1.04233,.991582,.954011,.912641,\
 .886740,.898918,.915199,.919445,.907130,.884049,.860226,.840352,.776696,.646839]
#
 #mach_list1 = [.200000,.250000,.300000,.340000,.380000,.420000,.460000]
 #mach_list2 = [.460000,.500000,.540000,.580000,.600000,.620000,.660000]
 #mach_list3 = [.660000,.700000,.720000,.740000,.752000,.760000,.780000,.820000]
 #cl_tobuff1 = [1.31957,1.30703,1.25688,1.20841,1.15142,1.09535,1.04233]
 #cl_tobuff2 = [1.04233,0.991582,.954011,.912641,.886740,.898918,.915199]
 #cl_tobuff3 = [.915199,.919445,.907130,.884049,.860226,.840352,.776696,.646839]
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 print ("IN CLBuffet and mach_list cl_tobuff fmach are")
 print (mach_list," ",cl_tobuff," ",fmach)
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 #i = 0
 #data_points = []
 #mach_value = []
 #mach_value.append(float(fmach))
 #print ("IN CLBuffet37800W_01 and mach_value = ",mach_value)
 #for x in mach_list:
  #data_points.append((x,cl_tobuff[i]))
  #i = i + 1
 #L = lagrange(mach_list,cl_tobuff)
 #L1 = lagrange(mach_list1,cl_tobuff1)
 #L2 = lagrange(mach_list2,cl_tobuff2)
 #L3 = lagrange(mach_list3,cl_tobuff3)
 #print ("mach_value = ",mach_value)
 #if (mach_value[0] > 0.66):
  #q = map(L3, mach_value)
  #for qresult in q:
   #clmax = qresult
   #print ('clmax based on L3 = ',clmax)
 #if (mach_value[0] > 0.46 and mach_value[0] <= 0.66):
  #q = map(L2, mach_value)
  #for qresult in q:
   #clmax = qresult
   #print ('clmax based on L2 = ',clmax)
 #if (mach_value[0] <= 0.46):
  #q = map(L1, mach_value)
  #for qresult in q:
   #clmax = qresult
   #print ('clmax based on L1 = ',clmax)
 return(clmax)
#
def CLBuffetP8_03(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
.300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [1.81955,1.92089,1.92089,1.92089,1.92089,1.91917,1.91244,1.88477,1.85710,1.82943,\
1.80058,1.77042,1.74025,1.71009,1.67871,1.64689] 
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 return(clmax)
#
def CLBuffetP8_04(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [1.89524,2.00442,2.00442,2.00442,2.00442,1.99956,1.99357,1.96963,1.94542,1.92103,\
 1.89663,1.87223,1.84784,1.82344,1.79904,1.77465]
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 return(clmax)
#
def CLBuffetP8_05(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]                            
 cl_tobuff = [1.96177,2.06625,2.06625,2.06625,2.06570,2.06041,2.05474,2.03205,2.00935,1.98666,\
 1.96396,1.94002,1.91534,1.89067,1.86599,1.84131] 
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 return(clmax)
#
def CLBuffetP8_06(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [2.01741,2.11874,2.11874,2.11874,2.11854,2.11327,2.10800,2.08694,2.06587,2.04376,\
 2.02050,1.99724,1.97398,1.95072,1.92596,1.90073]
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 return(clmax)
#
#THE BELOW DEFS ARE NOT NEEDED AS OF OCT 2015 BECAUSE P8clean, P8podup, P8poddn 
#ALL USE THE SAME BUFFET TABLES - THIS MIGHT CHANGE IN THE FUTURE
def CLBuffetP8podup_01(cl,fmach):
 mach_list = [.200000,.250000,.300000,.340000,.380000,.420000,.460000,.500000,.540000,.580000,\
 .600000,.620000,.660000,.700000,.720000,.740000,.752000,.760000,.780000,.820000]
 cl_tobuff = [1.37295,1.35221,1.25688,1.20841,1.15142,1.09535,1.04233,.991582,.954011,.912641,\
 .886740,.898918,.915199,.919445,.907130,.884049,.860226,.840352,.776696,.646839]
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 return(clmax)
#
def CLBuffetP8podup_03(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
.300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [1.81955,1.92089,1.92089,1.92089,1.92089,1.91917,1.91244,1.88477,1.85710,1.82943,\
 1.80058,1.77042,1.74025,1.71009,1.67871,1.64689]  
 clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 return(clmax)
#
def CLBuffetP8podup_04(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [1.89524,2.00442,2.00442,2.00442,2.00442,1.99956,1.99357,1.96963,1.94542,1.92103,\
 1.89663,1.87223,1.84784,1.82344,1.79904,1.77465] 
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 return(clmax)
#
def CLBuffetP8podup_05(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]                            
 cl_tobuff = [1.96177,2.06625,2.06625,2.06625,2.06570,2.06041,2.05474,2.03205,2.00935,1.98666,\
 1.96396,1.94002,1.91534,1.89067,1.86599,1.84131] 
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 return(clmax)
#
def CLBuffetP8podup_06(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [2.01741,2.11874,2.11874,2.11874,2.11854,2.11327,2.10800,2.08694,2.06587,2.04376,\
 2.02050,1.99724,1.97398,1.95072,1.92596,1.90073]
 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 clmax = Linear.Linear(mach_list,cl_tobuff,fmach)
 return(clmax)
#
def CLBuffet37800W_01(cl,fmach):
 #mach_list = [.200000,.250000,.300000,.340000,.380000,.420000,.460000,.500000,.540000,.580000,\
 #.600000,.620000,.660000,.700000,.720000,.740000,.752000,.760000,.780000,.820000]
 #cl_tobuff = [1.37295,1.35221,1.25688,1.20841,1.15142,1.09535,1.04233,.991582,.954011,.912641,\
 #.886740,.898918,.915199,.919445,.907130,.884049,.860226,.840352,.776696,.646839]
 mach_list = [.200000,.280000,.360000,.420000,.460000,.500000,.540000,.580000,.610000,.630000,\
  .650000,.670000,.690000,.710000,.730000,.750000,.770000,.790000,.810000,.820000]
 cl_tobuff = [1.35395,1.27694,1.19993,1.14165,1.10314,1.06464,1.02613,.98763,.96057,.94496,\
  .93247,.92206,.91269,.90125,.88772,.86690,.83672,.78989,.72329,.67958]
 mach_list1 = [.200000,.280000,.360000,.420000,.460000,.500000,.540000]
 cl_tobuff1 = [1.35395,1.27694,1.19993,1.14165,1.10314,1.06464,1.02613]
 mach_list2 = [0.54,0.58,0.61,0.63,.650000,.670000,.690000,.710000]
 cl_tobuff2 = [1.02613,.98763,.96057,.94496,.93247,.92206,.91269,.90125]
 mach_list3 = [.710000,.730000,.750000,.770000,.790000,.810000,.820000]
 cl_tobuff3 = [.90125,.88772,.86690,.83672,.78989,.72329,.67958]

 #clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 i = 0
 data_points = []
 mach_value = []
 mach_value.append(float(fmach))
 print ("IN CLBuffet37800W_01 and mach_value = ",mach_value)
 for x in mach_list:
  data_points.append((x,cl_tobuff[i]))
  i = i + 1
  #print ("data_points = ",data_points)
 #L = lagrange.lagrange(data_points)
 L = lagrange(mach_list,cl_tobuff)
 L1 = lagrange(mach_list1,cl_tobuff1)
 L2 = lagrange(mach_list2,cl_tobuff2)
 L3 = lagrange(mach_list3,cl_tobuff3)
 print ("mach_value = ",mach_value)
 if (mach_value[0] > 0.71):
  q = map(L3, mach_value)
  for qresult in q:
   clmax = qresult
   print ('clmax based on L3 = ',clmax)
 if (mach_value[0] > 0.54 and mach_value[0] <= 0.71):
  q = map(L2, mach_value)
  for qresult in q:
   clmax = qresult
   print ('clmax based on L2 = ',clmax)
 if (mach_value[0] <= 0.54):
  q = map(L1, mach_value)
  for qresult in q:
   clmax = qresult
   print ('clmax based on L1 = ',clmax)
 return(clmax)

#
def CLBuffet37800W_03(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [1.81955,1.92089,1.92089,1.92089,1.92089,1.91917,1.91244,1.88477,1.85710,1.82943,\
 1.80058,1.77042,1.74025,1.71009,1.67871,1.64689] 
 clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 return(clmax)
#
def CLBuffet37800W_04(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [1.89524,2.00442,2.00442,2.00442,2.00442,1.99956,1.99357,1.96963,1.94542,1.92103,\
 1.89663,1.87223,1.84784,1.82344,1.79904,1.77465]
 clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 return(clmax)
#
def CLBuffet37800W_05(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]                            
 cl_tobuff = [1.96177,2.06625,2.06625,2.06625,2.06570,2.06041,2.05474,2.03205,2.00935,1.98666,\
 1.96396,1.94002,1.91534,1.89067,1.86599,1.84131] 
 clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 return(clmax)
#
def CLBuffet37800W_06(cl,fmach):
 mach_list = [.150000,.180000,.200000,.205000,.210000,.215000,.220000,.240000,.260000,.280000,\
 .300000,.320000,.340000,.360000,.380000,.400000]
 cl_tobuff = [2.01741,2.11874,2.11874,2.11874,2.11854,2.11327,2.10800,2.08694,2.06587,2.04376,\
 2.02050,1.99724,1.97398,1.95072,1.92596,1.90073]
 clmax = Biparabolic1(mach_list,cl_tobuff,fmach,'cl')
 return(clmax)

