import json,sys
import std_atm
import math
#P8clean = False
P8clean = True
P8podup = False
P8poddn = False
#JBx37800W = True
JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
#
def get_lists_fromFNCLBod(strdisa,stralt,aircraft_config):
 FNCLB = {}
 mach_list = {}
 FNCLBod_list = {}
#For now we default to JBx37800W and we don't bother with aircraft_config
 if (JBx37800W):
  FNCLB_file = open('JBx37800W_FNCLB101_JSON.txt')
#
 if (P8clean):
  FNCLB_file = open('P8clean_FNCLB101_JSON.txt')
#
#THERE IS ONLY ONE FNCLB01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_FNCLB01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup AND P8poddn
 if (P8podup or P8poddn):
  FNCLB_file = open('P8podup_FNCLB101_JSON.txt')
#
 a2string = FNCLB_file.readline()
 FNCLB = json.loads(a2string)
 print ("In get_lists_fromFNCLBod and stralt strdisa = ",stralt," ",strdisa)
 try:
  mach_list = FNCLB[strdisa][stralt][0]
  FNCLBod_list = FNCLB[strdisa][stralt][1]
 except:
  mach_list = [-888]
  FNCLBod_list = [-888]
 print("AT THE END OF get_max_FNCLBod for these strdisa and stralt values ",\
        strdisa," ",stralt)
 return(mach_list,FNCLBod_list)
#
def getdisa_fromFNCLB101():
#Returns the dictionary available_alts_dict that is structured as follows:
#top level keys are disa values such as '-72'
#The value associated with each top level key is a list of the available alts for that
#altitude. The alts in each list are each strings such as '37000' representing an altitude in feel. 
#The reason they are strings is that they themselves will serve as keys
# 
 diag = 3
 #P8clean = True
 #JBx37800W = False
 #P8podup = False
 FNCLB = {}
 available_disa = [] 
 available_alts_list = []
 available_alts_dict = {}
 if (P8clean):
  FNCLB_file = open('P8clean_FNCLB101_JSON.txt') 
#
#THERE IS ONLY ONE FNCLB01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_FNCLB01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup AND P8poddn
 if (P8podup or P8poddn):
  FNCLB_file = open('P8podup_FNCLB101_JSON.txt') 
#
 if (JBx37800W):
  FNCLB_file = open('JBx37800W_FNCLB101_JSON.txt')
 a1string = FNCLB_file.readline()
 FNCLB = json.loads(a1string)
 available_disas = FNCLB.keys()
 #str_disa = []
 #for strdisas in available_disas:
  #str_disa.append(strdisas)
 for jdisas in available_disas:
  print("FNCLB[jdisas]keys = ",FNCLB[jdisas].keys())
  for striaa in FNCLB[jdisas].keys():
   if (diag == 4):
    print ("striaa = ",iaa)
   available_alts_list.append(striaa)
   if (diag == 4): 
    print ("available_alts_list  ",available_alts_list)
  available_alts_dict.update({jdisas:available_alts_list})
  available_alts_list = []
  print ("jdisas & available_alts_dict[jdisas] = ",jdisas," ",available_alts_dict[jdisas])
 return(available_alts_dict)

