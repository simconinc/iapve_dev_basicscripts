import CLBuffet
def CL_to_buffet(fmach,aircraft_config):
 #P8clean = False
 P8clean = True
 #JBx37800W = True
 JBx37800W = False
 P8podup = False
 P8poddn = False
 fi = open('aircraft_type_for_IAPVE.txt')
 ac_model_config = fi.readline()
 if (ac_model_config == "p8clean\n"):
  P8clean = True
  JBx37800W = False
  P8podup = False
 if (ac_model_config == "37800W\n"):
  P8clean = False
  JBx37800W = True
  P8podup = False
 if (ac_model_config == "p8podup\n"):
  P8clean = False
  JBx37800W = False
  P8podup = True
 if (ac_model_config == "p8poddn\n"):
  P8clean = False
  JBx37800W = False
  P8poddn = True
#
 cl = 1.0
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffetP8_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffetP8_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffetP8_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffetP8_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffetP8_06(cl,fmach)
#
 if (P8podup or P8poddn):
#P8podup and P8poddn use the same CLB buffet tables as for P8Clean
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffetP8_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffetP8_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffetP8_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffetP8_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffetP8_06(cl,fmach)
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffet37800W_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffet37800W_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffet37800W_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffet37800W_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffet37800W_06(cl,fmach)
 
 return(clmax)
