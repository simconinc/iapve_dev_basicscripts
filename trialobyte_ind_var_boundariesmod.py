# This module is to allow for user inputs to establish upper and lower limits for 
# a specific independent variable, such as weight, and create a new data set of
# dependent variable values corresponding to independent variables within that range

import sys
from configparser import SafeConfigParser

# Establish xlists 
xlist1 = [2,4,6,8,10,12,14,18,24]
xlist2 = [1,3,5,9,12,16,23,29,31]

#set default for name of the configuration file
config_file_name = 'config_trialobyte.ini'
print("config_file_name is", config_file_name)

#Access command line arg to mega_biparab identifying name of the .ini config file
args = sys.argv
lower_lim = args[1]
upper_lim = args[2]

for ar in args: 
    #print("User entered limits are", ar)
    if args[1] == '':
        print("ERROR - Please enter lower limit")
    if args[2] == '':
        print("ERROR - Please enter upper limit")        
    if len(args) > 3:
        print("ERROR - Too many values entered. Please enter only an upper and lower limit")
print("lower limit is", lower_lim)
print("upper limit is", upper_lim)

#Check that the upper and lower limits set by the user exist, are integers, and 
# are in the order of lower limit < upper limit
try:
    lower_lim = int(lower_lim)
except ValueError:
    print("ERROR - Lower limit not a integer. Please enter a integer value")

try:
    upper_lim = int(upper_lim)
except ValueError:
    print("ERROR - Upper limit not a integer. Please enter a integer value")

#Check that the upper limit is greater than the lower limit
if upper_lim < lower_lim:
    print("ERROR - Lower limit must be less than upper limit")

### Now apply limits to the xlists and create new lists
lim_xlist1 = xlist1[lower_lim:upper_lim]
lim_xlist2 = xlist2[lower_lim:upper_lim]

if lim_xlist1[0] < lower_lim or upper_lim > lim_xlist1[len(lim_xlist1) - 1]:
    print("ERROR - One or more limit outisde range of xlist1")
if lim_xlist2[0] < lower_lim or upper_lim > lim_xlist2[len(lim_xlist2) - 1]:
    print("ERROR - One or more limit outisde range of xlist2")    

print("lim_xlist1 is", lim_xlist1)
print("lim_xlist2 is", lim_xlist2)


