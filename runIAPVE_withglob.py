##########################################################
##### Process User Arguments
##########################################################
from __future__ import print_function
import os
import sys
import subprocess
import glob
#aircraft_model = "37800W"
aircraft_model = "p8clean"
#
with open('aircraft_type_for_IAPVE.txt','w') as aircraft:
 aircraft.write(aircraft_model + "\n")
#The above code represents creation and use of a context manager.
#The 'open(....) ...' portion of the 'with open(...) ...' command 
#constructs a context manager object that wraps the file 'aircraft_type_for_IAVPE.txt".
#The context manager object includes:
#    an __enter__ method
#    an __exit__ method
#    a read method
#    a write method
#    other methods
#The 'as aircraft' portion of the 'with open(...) ...' command casues aircraft to be
#the reference to the context manager object.
#The with portion of the 'with open(...) ...' command also causes the context 
#manager object's __enter__ method to be executed.
#aircraft.write causes the context manager's write method to be executed 
#and when the end of the with block is reach the context manager object's
#__exit__ method gets executed.
#The above code is equivalent to:
#   aircraft = open('aircraft_type_for_IAPVE.txt','w')
#   try:
#       aircraft.write(aircraft_model + "\n")
#   finally:
#       aircraft.close()
#
#aircraft = open('aircraft_type_for_IAPVE.txt',"w")
#aircraft.write("p8podup\n")
#aircraft.write("p8poddn\n")
#aircraft.write("37800W\n")
#aircraft.close()
run_num = 0
#plan_num = "9003"
#plan_num = "2862"
#plan_num = "3000"
#plan_num = "9103.7"
#Previous from Jan 12 2016 plan_num = "1500"
#Previous from Jan 12 2016 early afternoon plan_num = "1627"
#crzmode = "FLAPS05"
#crzmode = "FLAPSupHOLD"
#Previous from Jan 12 2016 crzmodelist = ["MRC"]
#Previous from Jan 12 2016 crzmode = "MRC"
#Previous from Jan 12 2016 early afternoon crzmodelist = ["M73"]
#Previous from Jan 12 2016 early afternoon crzmode = "M73"
#PREVIOUS from Jan 12 2016 mid afternoon plan_num = "1540"
#PREVIOUS from Jan 12 2016 mid afternoon crzmode = "IP40"
#PREVIOUS from Jan 12 2016 mid afternoon crzmodelist = ["IP40"]
#plan_num = "1627.1"
#plan_num = "1627"
#plan_num = "9900"
#crzmode = "M73"
#plan_num = "9903"
#plan_num = "9902"
#plan_num = "1527"
#NOTE THAT 1527 is in the Dec 21 CAT1 directory
#crzmodelist = ["M73"]
#crzmodelist = ["1LE"]
#plan_num = "1900"
plan_num = "1900.2"
#plan_num = "1900.1"
crzmode = "1LE"
crzmodelist = ["1LE"]
#crzmodelist = ["FLAPSupHOLD"]
#crzmodelist = ["FLAPS01","FLAPS05","FLAPS10","FLAPS15","HOLD"]
#crzmodelist = ["M70","M73","M77","M79"]
#previous crzmodelist = ["MRC","IP40"]
#crzmodelist = ["IK15"]
#crzmode = "M78"
#crzmode = "IP999"
#crzmode = "1LE"
#crzmodelist = ["IP40"]
#plan_num = "1540"
#crzmode = "IP40"
#crzmodelist = ["IP40"]
#plan_num = "1645"
#crzmode = "IP40"
#plan_num = "9003"
#crzmode = "IK15"
#previous Jan 10 2016 plan_num = "9000"
#previous Jan 10 2016 crzmode = "M82"
#for crzmode in crzmodelist:
 #print ("crzmode = ",crzmode)
#for name in glob.glob('Cat1Cat2_flapsup_2eng_medhvy_high_Nov_2015_podup_plans/*_Ouput.json'):
#for name in glob.glob('Cat3Cat4_flapsup_2eng_medhvy_low_Nov_2015_podup_plans/*_Ouput.json'):
#for name in glob.glob('Nov 6 2015 clean P8 1LE/*_Ouput.json'):
#for name in glob.glob('Dec 10 clean P8 1LE inflight start pt plans DTSCH/*_Ouput.json'):
#for name in glob.glob('Dec 10-11 clean P8 1LE inflight start pt plans CUDDY/*_Ouput.json'):
#for name in glob.glob('Cat5_flapsup_1LE_Nov_2015_podup_plans/*_Ouput.json'):
#for name in glob.glob('Cat6_hold_Dec8_2015_poddn_plans/*_Ouput.json'):
#PREVIOUS form Jan 19 2016 for name in glob.glob('Dec 21 Rerun P8 clean Cat2 plans from netwrk/*_Ouput.json'):
#Previous from Jan 18 2016 morning for name in glob.glob('xB738 Constant Mach Plans/*_Ouput.json'):
#previous for late on Jan 10 2016 for name in glob.glob('737-800W check roclatestfix/*_Ouput.json'):
#previous Jan 10 2016 for name in glob.glob('Oct 27 2015 737-800W M82 High Alt/*_Ouput.json'):
#PREVIOUS JAN 28 2016 latepm for name in glob.glob('Dec 21 Rerun P8 clean Cat1 plans from netwrk/*_Ouput.json'):
for name in glob.glob('Jan 11 2016 Rerun P8 clean Cat5aka1LE plans from netwrk/*_Ouput.json'):
#for name in glob.glob('737-800W check roc/*_Ouput.json'):
#PREVIOUS FROM JAN 26 2016 earlyafternoon for name in glob.glob('737-800W check roclatestfix/*_Ouput.json'):
#for name in glob.glob('Cat1Cat2_flapsup_2eng_medhvy_high_Dec9_2015_poddn_plans/*_Ouput.json'):
#for name in glob.glob('Cat1Cat2_flapsup_2eng_medhvy_high_Dec8_2015_poddn_plans/*_Ouput.json'):
#for name in glob.glob('Cat3Cat4_flapsup_2eng_medhvy_low_Dec8_2015_poddn_plans/*_Ouput.json'):
#for name in glob.glob('Cat6_flaps0_1_5_10_15_2eng_allholdscenario_Nov_2015_podup_plans/*_Ouput.json'):
  name = name.replace(" ","!")
  print ("name = ",name)
  #if (crzmode in name):
  if (plan_num in name):
   run_num = run_num + 1
   if (run_num == 1):
    hdr = "HDRYES"
   else:
    hdr = "HDRNO"
   str_arg = str(run_num) + " " + "GLOB" + " " + name + " " +  crzmode + " " + hdr + " " + aircraft_model
 #print ("str_arg = ",str_arg)
  #IAPVEcmd = "python IAPVE.py " + str_arg
   IAPVEcmd = "python newIAPVE.py " + str_arg
   print ("IAPVEcmd  = ",IAPVEcmd)
   #os.system(IAPVEcmd)
   status = -888
   try:
    status = subprocess.call(IAPVEcmd)
    if (status < 0):
     print ("Child was terminated by signal ",status)
    else:
     print ("Child returned ",status)
   except:
#    print message to standard error which defaults to the console
     print ("Execution failed",status,file=sys.stderr)
print ("ALL DONNNN")
#
#note - to redirect standard error to a file do something like
#the following:
#
#import sys
#f = open("myfile.log","w")
#original_stderr = sys.stderr
#sys.stderr = f
#print("lapodapo",file=sys.stderr)
#sys.stderr = original_stderr
#f.close()


