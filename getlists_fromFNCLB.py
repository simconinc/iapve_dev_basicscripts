import json, sys
import std_atm
S = 1341.0
diag = 3
#P8clean = False
P8clean = True
P8podup = False
#JBx37800W = True
JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
#
def getdisa_fromFNCLB101():
#Returns the dictionary available_machs_dict that is structured as follows:
#top level keys are altitudes such as '27000'
#The value associated with each top level key is a list of the available machs for that
#altitude. The machs in each list are each strings such as '400' representing the
#mach as mach*1000.  The reason they are strings is that they themselves will serve
#as keys
# 
 FNCLB = {}
 available_disa = [] 
 available_alts_list = []
 available_alts_dict = {}
 if (P8clean):
  FNCLB_file = open('P8clean_FNCLBTAB101_JSON.txt') 
 if (P8podup):
  FNCLB_file = open('P8podup_FNCLBTAB111_JSON.txt') 
 if (JBx37800W):
  FNCLB_file = open('JBx37800W_FNCLBTAB101_JSON.txt')
 a1string = FNCLB_file.readline()
 FNCLB = json.loads(a1string)
 available_disas = FNCLB.keys()
 str_disa = []
 for iadisas in available_disas:
  str_disa.append(iadisas)
 for jdisas in str_disa:
  print("FNCLB[jdisas]keys = ",FNCLB[jdisas].keys())
  for iaa in FNCLB[jdisas].keys():
   if (diag == 4):
    print ("iaa = ",iaa)
   available_alts_list.append(int(iaa))
   if (diag == 4): 
    print ("available_alts_list  ",available_alts_list)
  available_alts_dict.update({jdisas:available_alts_list})
  available_alts_list = []
  available_alts_dict[jaalts].sort()
  print ("jdisas & available_alts_dict[jdisas] = ",jdisas," ",available_alts_dict[jdisas])
 return(available_alts_dict)
