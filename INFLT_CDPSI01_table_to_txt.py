import csv

#Initialize variables and structures

# Parsing the input/output filename arguments from trialobyte
print ('Number of arguments received from trialobyte: ', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
# assign input filename
dp = sys.argv[0]
# assign output filename
fn = sys.argv[1]

#This code section uses the method to create a reader object using csv.reader and then
#use the reader object to create a list using dalist = [data for data in data_iter]
#
#**** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE input file
#dp = 'JBx37800W_CDPSI01_tbl.txt'
#dp = 'P8clean_CDPSI01_tbl.txt'
#dp = '37800WCFM56_57_CDPSI01_tbl.txt'
#****
#
if (dp[0:2] == "P8"):
   p8 = True
else:
   p8 = False
#in files with names ending in _CDPSI01_tbl.txt, * is the deliminter
#between the 2 subtables (CD,CN) 
dl = '*'
with open(dp,'rU') as csvfile:
   data_iter = csv.reader(csvfile,delimiter=dl)
   #for row in data_iter:
    #print ("row ",row)
   dalist = [data for data in data_iter] #creates the list dalist
   print("dalist = ",dalist)
# The list named dalist is made up of the following:
# 1st element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire CN block
#     element 2 is ''
# 2nd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire CD block
#
icnt = 0
for i in dalist:
  print ("just after for i in dalist")
  print (i)
  if (icnt == 0):
#for a string s = 'abcdef      ' s.split produces ['abcdef']  
   c = i[0].split()
   CN = c[0]
   print("CN = ",CN)
  if (icnt == 1):
   CD = i[0]
   print("CD = ",CD)
  icnt = icnt + 1 
print ("ALL DONE")
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file
#CDPSI IS COMMON TO ALL P8 MODELS SO THEREFORE WE ONLY NEED P8clean_CDPSI01_JSON.txt
#FOR ANY/ALL P8 1LE PLANS
#fn = "P8clean_CDPSI01_txt.txt"
#***
target = open(fn,'w')
target.truncate()
target.write(CN)
target.write('\n')
target.write(CD)

print ("Finished with CDPSI01 text maker")

  
