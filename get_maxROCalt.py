import get_aeropfm
import std_atm
import math
import accel_factor
#def get_maxROCalt(ftemperature,ftas,fmach,falt,fweight,aircraft_model,aircraft_config):
def get_maxROCalt(fidisa,ftas,fmach,falt,fweight,aircraft_model,aircraft_config,pflag):
#G is the acceleration of gravity in ft/sec**2
 pflag = 1
 accel_fac = accel_factor.accel_factor_constmach(falt,fmach,fidisa)
 G = 32.17405
 G_ft_per_min_sqrd = 32.17405*3600.00
 ROCmin = 300.0
 #ROCmin = 100.0
 ftascROCalt = True
 #ftascROCalt = False
 ftemperature = std_atm.isa2temp(fidisa,falt)
 if (pflag): print ('fidisa ftemperature ',fidisa,' ',ftemperature)
 cROCalt = falt
 if (aircraft_config == 2):
  ROCdelta = 32000.00 - falt
 else:
  ROCdelta = 42000.00 - falt
 higher = False
 lower = False
 i = 1
 while1_cnt = 0
 while2_cnt = 0
#ITERATE ON ALTITUDE
 while (i==1 and while1_cnt < 25):
  while1_cnt = while1_cnt + 1
  if (pflag): print ("cROCalt at top = ",cROCalt)
  ftemperature = std_atm.isa2temp(fidisa,cROCalt)
  if (pflag): print ('fidisa ftemperature ',fidisa,' ',ftemperature)
#
  if (aircraft_config == 2):
   maxthrustod = get_aeropfm.get_maxconthrust(ftemperature,cROCalt,fmach,aircraft_model,aircraft_config,pflag)
  else:
   maxthrustod = get_aeropfm.get_maxclimbthrust(cROCalt,fmach,ftemperature,aircraft_model,aircraft_config,pflag)
  if (pflag): print ("MAXIMUS")
  dlta = std_atm.alt2press_ratio(cROCalt)
  if (pflag): print ("dlta = ",dlta)
  maxthrust = maxthrustod * dlta
  if (pflag): print("maxthurstod maxthrust ",maxthrustod," ",maxthrust)
  fcl = 0.0
#
  geefactor = 1.0
  (lift_coeff,cl_max,drag_coeff,Lift,Drag,ffnod) = get_aeropfm.get_FNod_cruise(geefactor,ftemperature,fmach,\
                                                   cROCalt,fweight,fcl,aircraft_model,aircraft_config,pflag)
  if (pflag): print ("below are lift_coeff,cl_max,drag_coeff,Lift,Drag")
  if (pflag): print (lift_coeff," ",cl_max," ",drag_coeff," ",Lift," ",Drag)
  if (pflag): print("maxthrust Drag ",maxthrust," ",Drag)
  if (pflag): print ("WHITE WALKERS ftas fweight fidisa ftemperature ",ftas," ",fweight," ",fidisa," ",ftemperature)
#Estimate the rate of climb at cROCalt  
#This assumes 0 acceleration
  estROC_geo = ftas*((maxthrust-Drag)/fweight) 
  cROCalt_ftemperature = std_atm.isa2temp(fidisa,cROCalt)
  cROCalt_stdatm_temperature = std_atm.isa2temp(0.0,cROCalt)
  #stdatm2 = std_atm.isa2temp(0.0,(cROCalt+1000.0))
  if (pflag): print("cROCalt_ftemperature = ",cROCalt_ftemperature," cROCalt_stdatm_temperature = ",cROCalt_stdatm_temperature)
  speedofsound_at_cROCalt = std_atm.temp2speed_of_sound(cROCalt_ftemperature,speed_units='ft/s')
  print ("speedofsound_at_cROCalt = ",speedofsound_at_cROCalt)
  ftas_at_cROCalt = (fmach*speedofsound_at_cROCalt)*60.0 
  if (pflag): print ("Spurs ftas = ",ftas," ftas_at_cROCalt = ",ftas_at_cROCalt)
  if (ftascROCalt):
   estROC_geo = (ftas_at_cROCalt*((maxthrust-Drag)/fweight))/accel_fac
   print ("KING CROW estROC_geo = ",estROC_geo)
  #T_ISA_avg = (cROCalt_stdatm_temperature + stdatm2)/2.0
  tratio = (cROCalt_stdatm_temperature+273.15)/(cROCalt_stdatm_temperature + 273.15 + fidisa)
  #tratio = (T_ISA_avg + 273.15)/(T_ISA_avg + 273.15 + fidisa)
  estROC_palt = estROC_geo * tratio
  #ambientKelvin = (ftemperature + 273.15) 
  #std_atm_ambientKelvin = ambientKelvin - fidisa
  #estROC_palt = (estROC_palt*ambientKelvin)/std_atm_ambientKelvin
  if (pflag): print ("MAIN STOP ROCmin falt cROCalt ROCdelta estROC_palt ",ROCmin," ",falt," ",\
                      cROCalt," ",ROCdelta," ",estROC_palt)
  #True_estROC = estROC_palt
  estROC = estROC_palt 
#
  if (abs(estROC - ROCmin) < 0.5):
   i = 0
  else:
   if (pflag): print("cROCalt estROC ROCmin ROCdelta",cROCalt," ",estROC," ",ROCmin," ",ROCdelta)
   if (estROC > ROCmin and Drag > 0.0):
    cROCalt = cROCalt + ROCdelta
    higher = True
   if (estROC < ROCmin or Drag <= 0.0):
    cROCalt = cROCalt - ROCdelta
    lower = True
   if (pflag): print ("cROCalt = ",cROCalt)
   ROCdelta = ROCdelta/2.0
   if (abs(ROCdelta) < 0.01 and abs(estROC-ROCmin) > 0.5):
    print ("MAXROCALT THROWING IN THE TOWL ON CONVERGENCE with estROC = ",estROC," ROCdelta = ",ROCdelta) 
    cROCalt = -999
    estlift_coeff = -999
    estdrag_coeff = -999
    estLift = -999
    estDrag = -999
    i = 0
   #ROCdelta = ROCdelta/1.5
 return(cROCalt,estROC,maxthrust,lift_coeff,drag_coeff,Lift,Drag)
