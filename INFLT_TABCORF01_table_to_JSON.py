import csv
import numpy as np
import sys
import json

#This part tries out np.fromfile
#newarray = np.fromfile('TABCORF01_tbl.txt')
#print ("newarray")
#print (newarray)
#the above print statement prints out what look to be random numbers

#Initialize variables and structures
count = 0
FNod = []
mach = []
alt = []
CORF = []

# Parsing the input/output filename arguments from trialobyte
print ('Number of arguments received from trialobyte: ', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
# assign input filename
dp = sys.argv[0]
# assign output filename
fn = sys.argv[1]


#This code section uses the method to create a reader object using csv.reader and then
#use the reader object to create a list using dalist = [data for data in data_iter]
#
#**** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE input file
#dp = 'P8clean_TABCORF01_tbl.txt'
#dp = 'P8podup_TABCORF01_tbl.txt'
#dp = 'JBx37800W_TABCORF01_tbl.txt'
#dp = '37800WCFM56_57_TABCORF01_tbl.txt'
#****
#
special = False
#in files with names ending in _TABCORF01_tbl.txt, * is the deliminter
#between the 4 subtables (FNod,mach,alt,WF) 
dl = '*'
with open(dp,'rU') as csvfile:
   data_iter = csv.reader(csvfile,delimiter=dl)
#  for row in data_iter:
#   print ("row ",row)
   dalist = [data for data in data_iter] #creates the list dalist

# The list named dalist is made up of the following:
# 1st element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire FNod block
#     element 2 is ''
# 2nd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire mach block
#     element 2 is ''
# 3rd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire altitude block
#     element 2 is ''
# 4th - nth elements in dalist are each a list with a single member that consists of 
# a string created from an entire line of WF data found in .._TABCORF01_tbl.txt  

for i in dalist:
  print ("just after for i in dalist")
  print (i)
  if (count == 0 and len(i) > 0): 
   print ("splitting up the members of the FNod string using the delimiter 1#") 
   k = i[0].split("1#")
   for ar in k:
    FNod.append(ar.split())
   print ("k")
   print (k)
   print ("Here is FNod")
   print (FNod)
  if (count == 1 and len(i) > 0): 
   print ("splitting up the members of the mach string using the delimiter 1#") 
   k = i[0].split("1#")
   for ar in k:
    mach.append(ar.split())
   print ("k")
   print (k)
   print (" ")
   print ("Here is mach")
   print (mach)
  if (count == 2 and len(i) > 0): 
   alt.append(i[0].split())
   print ("do not need to split up the alt string")
   print ("Here is alt")
   print (alt)
  if (count >=3 and len(i) > 0):
   print ("splitting up the members of the WF string") 
   print ("THE ENTIRE i ",i)
   print ("AIAI i[0] ",i[0])
   print ("i[0] SPLIT ",i[0].split())
   CORF.append(i[0].split())
  count = count + 1
  print(" ")
print("Done with Section 1: Here is mach ",mach)
#
print(" ")
print ("Here is CORF")
print(CORF) 
#So at this point we have built 4 list objects.
#  FNod - contains multiple elements: each element is a list made up of data from a sub group within the FNod section of the TABCORF01 table 
#  mach - contains multiple elements: each element is a list made up of data from a sub group within the mach section of the TABCORF01 table 
#  alt - contains multiple elements: each element is a list made up of data from a sub grup within the alt section of the TABCORF01 table
#        note - as an example, the TABCORF01 table extracted from the file JBx37800W.dat the alt section of the TABCORF01
#               table has only one sub group so there is only one element in the alt list which
#               is made up of data from that one sub group 
# CORF - contains multiple elements: each element is a list made up of data taken from a string created from an entire line of the CORF section 
#        of the TABCORF01 table 
#
#Now we take advantage of the knowledge that every three elements of the CORF list when taken
#together make up a sub group from the CORF section of the TABCORF01 table
#Furthermore, we know that the third of these three lists will always have just three members; 
#these are:
#      the final CORF value for the mach number that the sub group applies to
#      the value of the mach number
#      A number in the range 1 to 15 followed by /n
#
#The following section of code populates the dictionary named WF_dict
#The structure of WF_dict is as follows:
#Each key in WF_dict is one of the altitudes from the altitude section of the TABCORF01 table
#These are 0, 5000, 10000, ...., 41000 
#The value associated with each altitude key is a dictionary named mach_fuel_burn_dic.
#The keys in mach_fuel_burn_dic are the mach numbers (stored as mach number * 1000) applicable for
#the corresponding altitude. For example, mach .400 is stored as 400
#The value associated with each mach number key is a list made up of the fuel burn rate (FBR) values
#applicable to the corresponding combination of alitude and mach number.  Each individual FBR value
#is associated with a particular FNod value that is in a corresponding list stored in FNod. 
#
hit_zero = False
hit_one = False
#mach_cnt = 0
mach_cnt_listmbrs = 0
alt_cnt = 0
mach_fuel_burn_dic = {}
fuel_burn_values = []
CORF_dict = {}
gcount = 1
for f in CORF:
 if (len(f) == 0):
  print("No more data")
  break
 print ("f IS THIS ",f)
 for g in f:
  earlyfind = False
  print ("g IS THIS ",g," gcount = ",gcount)
  val = float(g)
  print ("val IS THIS ",val)
  if (gcount <= 21):
    fuel_burn_values.append(val)
  if (not special):
   if (gcount == 21 and len(f) == 12):
    print ("Kligons gcount = ",gcount,"f = ",f)
    if (val < 1.0): 
     earlyfind = True
     mach_key = int(val*1000)
     print ("WARF mach_key = ",mach_key)
   if (gcount == 22):
    mach_key = int(val*1000)
    print ("mach_key = ",mach_key)
# For special check for True or False
  print ("SULU gcount = ",gcount)
  if (special):
   print("This is a special case and val = ",val)
   if (val >= 1.0 and val <= 15.0 and (hit_one or hit_zero)):
    #This is the case of 0 1 or 1 1 or 1 2 or 1 3 etc which means we set mach_key to the appropriate int
    print("######## val is 1.0 and hit_one is False")
    print ("HERE IS mach[alt_cnt] ",mach[alt_cnt])
    print ("alt_cnt = ",alt_cnt,"mach_cnt_listmbrs = ",mach_cnt_listmbrs)
    mach_key = mach[alt_cnt][mach_cnt_listmbrs]
    mach_key = mach_key[1:]
    print ("mach_key and type(mach_key ",mach_key," ",type(mach_key))
    mach_key = (int(mach_key))*10
    print ("mach_key in ######## is ",mach_key)
    if ((mach_cnt_listmbrs+1) == len(mach[alt_cnt])):
     mach_cnt_listmbrs = 0
    else:
     mach_cnt_listmbrs = mach_cnt_listmbrs + 1
    print ("mach_key = ",mach_key," alt_cnt = ",alt_cnt," mach_cnt_listmbrs = ",mach_cnt_listmbrs)
    val = -27
   if (val == 0.0):
    #this is the case of encountering a 0 for the 2nd to last value
    print("******* Val is 0.0") 
    hit_zero = True
    hit_one = False
   if (val == 1.0 and not hit_one and not hit_zero):
    #this is the case of encountering a 1 for the 2nd to last value
    hit_one = True
   print("hit_zero hit_one alt_cnt ",hit_zero," ",hit_one," ",alt_cnt)
   print("WARRIORS p8 val ",p8," ",val)
  print("BEFORE CHECK FOR gcount == 23") 
  if (gcount == 23 or earlyfind):
   gcount = 0
   print("GIANTS")
   if (special):
    hit_one = False
    hit_zero = False
   print ("fuel_burn_values ",fuel_burn_values)
   FNod_fuelburn_tuple = (FNod[alt_cnt],fuel_burn_values)
#  mach_fuel_burn_dic.update({mach_key:fuel_burn_values})
   mach_fuel_burn_dic.update({mach_key:FNod_fuelburn_tuple})
   fuel_burn_values = []
   print ("mach_fuel_burn_dic ",mach_fuel_burn_dic.get(mach_key))
   print ("alt_cnt and mach[alt_cnt][-1] ",alt_cnt," ",mach[alt_cnt][-1])
   fv = float(mach[alt_cnt][-1])
   hmn = int(fv*1000)
   print ("fv = ",fv,"hmn = ",hmn)
   print ("mach_key and highest_mach_number ",mach_key," ",hmn)
   if (mach_key == hmn):
    alt_key = int(alt[0][alt_cnt])
    print('alt_key = ',alt_key,"alt_cnt = ",alt_cnt)
    CORF_dict.update({alt_key:mach_fuel_burn_dic})
    print ('THE CORF Dictionary: CORF_dict{alt_key:mach_fuel_burn_dic} is \n',CORF_dict.get(alt_key))
    mach_fuel_burn_dic = {}
    alt_cnt = alt_cnt + 1
   print ("AFTER GIANTS gcount = ",gcount)
  gcount = gcount + 1   
  print ("gcount = ",gcount)
print("Victory")
count = 0
while (count <= (alt_cnt-1)):
  print ("count alt[0][count] ",count," ",alt[0][count])
  the_alt_key = int(alt[0][count])
  print ("the_alt_key = ",the_alt_key)
  print (CORF_dict.get(the_alt_key))
  count = count + 1
  print ("count = ",count,"alt_cnt = ",alt_cnt)

#Use json.dumps to create a JSON object and then write it to .._TABCORF01_JSON.txt
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file
#fn = "P8clean_TABCORF01_JSON.txt"
#fn = "JBx37800W_TABCORF01_JSON.txt"
#fn = "P8podup_TABCORF01_JSON.txt"
#***
target = open(fn,'w')
target.truncate()
target.write(json.dumps(CORF_dict))
print ("FINISHED with TABCORF01 JSON Maker")



  
