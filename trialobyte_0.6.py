#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     TRIALOBYTE MASTER CONTROL PROGRAM FOR VERIFICATION/VALIDATION SOFTWARE
#
#			                Trialobyte version 0.5

# DESCRIPTION
# Trialobyte is the overall control program for the IAPVE software. and is responsible 
# for managing the process flow of data input starting with the table maker program 
# reading in a .dat file, followed by the creation of .txt files containing table data 
# that will be converted to JSON strings by the JSON maker program. Statistical analysis 
# is performed by an R script once the JSON strings have been converted into files.

# Trialobyte also includes a interpolation selector module. This module can either automatically
# identify the type of file being processed and select the appropriate interpolation 
# method or be switched to manual mode in whic the user selects the interpolation method.

# The table maker program, get_INFLT_tab_v4.py has been transferred from being executed
# from the ini configuration file to being fully embedded into the Trialobyte script

# Due to the number of individual JSON maker programs, JSON maker script selection
# is handled via the .ini configuration file.

############################
# FILE REVISION HISTORY
# 
# 7/2/2015 - File created through the combination of one tablemaker and jsonmaker module
# 7/9/2015 - Added JB's recommended .py files for incorporation
# 7/12/2015 - Incorporated the function of the table maker program, get_INFLT_tab_v4.py
# 7/18/2015 - version 0.2 - Added ability to pass .dat file name string between modules
# 8/16/2015 - Swapped Table maker module with updated version from table_maker.py from JB
# 9/23/2015 - version 0.3 - Added JSON maker program selection capability to trialobyte. 
# 10/7/2015 - version 0.4 - JSON maker input/output file names are constructed by trialobyte and passed to each json maker script
# 10/12/2015 - version 0.5 - Table Maker module now reads and writes Yaw drag (CDPSI01) data
# 10/15/2015 - version 0.5 - Table Maker module now reads and writes Windmilling drag (DODWM and CDWM) data
# 10/25/2015 - version 0.6 - Takes the name of a spreadsheet created by IAPVE.py and executes statistical analysis program


#	(c) 2015 Simcon Technology Corporation, All rights reserved.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


########################################
###### Import Packages and Modules #####
########################################

import os
import json
import sys
import re
import csv
import numpy as np
from configparser import SafeConfigParser # Use for python3
#from ConfigParser import SafeConfigParser # Use for python2.7
 

##########################################
##### Read in from Configuration File ####
##########################################

# Create parser object
parser = SafeConfigParser()

# Default configuration file name
config_file_name = "config_trialobyte.ini"

print ("config_file_name = ", config_file_name)

# Read from Configuration file 
parser.read(config_file_name)

# Read in directory paths from configuration file
scrpt_dir  = parser.get("script directory","scrpt_dir")
inpt_dir  = parser.get("input_file_directory","inpt_dir")
outpt_dir  = parser.get("output_file_directory","outpt_dir")
print("scrpt_dir = ", scrpt_dir)
print("input_dir = ", inpt_dir)
print("outpt_dir = ", outpt_dir)


############################
#### Table Maker module ####
############################
# Module derived from incorporating get_INFLT_tab.py into Trialobyte. 
# Table Maker reads from a INFLT .dat file, parses certain tables, then
# writes them to WFTAB101_tbl.txt and CDBASE01_tbl.txt or to similarly named
# output files.

# Read .dat input file from .ini configuration file
tbl_in = parser.get('table_maker_input', 'tbl_in')
print ("INFLT .dat input file is ", tbl_in)

# Store the .dat input file name as a string. 
# This string will be used to generate file names throughout trialobyte
dat_string = tbl_in[:-4]
print(".dat filename string is", dat_string)

tbl_data = open(tbl_in)
print ("tbl_data is ", tbl_data)

tbl_bool = 1
foundtable = False
#astr is where each line of a given table is appended to in order to create
#one long string making up that table.  astr is always initialized to null
astr = ""

tables = []  #for storing a list of tables, each as a string
tbl_dict = []   #for storing a list of dictionaries
subtab = []
independent_vars = []
index11 = []
fuel_table = []
WFlist = []
dict = {}
crawdad = {}
k = 0
depvarindex = 0 # for cycling through dependent variables when constructing Json Maker input/output file names

# The following while loop extracts certain tables and stores them as 
# strings in the tables list.
#The while loop below stays in effect until i is set to 0
while tbl_bool:
    astring = tbl_data.readline()
    print ("astring = ",astring)
    
#Check if astring contains a start indicator for a table
#The string /TBLU indicates the start of the very 1st table
#An H or a / indicates end of a table and the possible start of a new table
    if (not foundtable and astring[0:5] == "/TBLU"):
        foundtable = True 
#       print "FOUND TBLU"
    elif(foundtable and (astring[0:1] == "H" or astring[0:1] == '/')):
        tables.append(astr)
#       print "astr[0:7] and tables[k] ",astr[0:7]," ",tables[k][0:27]
        foundtable = False 
#       print "foundtable astring[0:1] = H or / ", foundtable," ",astring[0:1]
        k = k + 1
        astr = ""
    elif(foundtable and astring != ""):
        astr = astr + astring
#astring == "" means we have reached the end of the file 
    if (astring == ""):
        tables.append(astr)
        tbl_bool = 0
#print "fplans equals", fplans           
#print "out of while loop"

for t in tables:
    print ("START of t in tables")
# when you specify '\n' python treats it as hex0a
# when you specify '\r' python treats it as hex0D
# therefore
# p.replace('\n', ' ') replaces all occurrences of hex0A with hex20
# p.replace('\r', ' ') replaces all occurrences of hex0D with hex20
# it is necessary to replace in the string p all occurences of hex0A with hex20 and all occurences hex0D with hex20 in order for json.loads(p) to work properly
#   t = t.replace('\n', ' ').replace('\r', ' ')
    print ("t ",t[0:272])
    print ("END OF THE TABLE t")   
print ("Finished reading the file")  
#
#At this point in the process each member of the tables list is a string consisting
#of an entire table starting with the table identifier (e.g. CDBASE01)
#
#Now we are going to find the member of the tables list containing the string
#for the table WFTAB101 and process that table 
#we know from the INFLT User Manual that WFTAB101 is based on WF = F(W/delta,mach,alt)
#
#We want to split the WFTAB101 table into 4 parts; part 1 consists of the independent variable 
#FNod values, part 2 consists of the independent variable mach values, part 3 consists of the
#independent variable altitude values, and part 4 consists of the dependent variable WF values.
#
#open file to write the WFTAB101 table to 

#Here we should change the hard coded f (w[0:8] == 'WFTAB101' to IF w[0:8] == the first
# 7-8 characters in the jsnmkrs config file list
#But make sure the jsnmkrs list is in the same order as it is here
for w in tables:
 if (w[0:8] == 'WFTAB101' or w[0:8] == 'WFTAB111'):
  if (w[0:8] == 'WFTAB101'):
   print ("found WFTAB101")
  if (w[0:8] == 'WFTAB111'):
   print ("found WFTAB111")
 
  tbl_out_WFTAB101 = parser.get('table_maker_output_WFTAB101', 'tbl_out_WFTAB101')
  target = open(tbl_out_WFTAB101,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")  
  print ("independent_vars[0] ",independent_vars[0])
  print ("independent_vars[1] ",independent_vars[1])
  print ("independent_vars[2] ",independent_vars[2])
  FNod = independent_vars[0]
  FNod = FNod.replace('1\n', '1#').replace('\r','')
  FNod = FNod.replace('\n','')
  target.write(FNod)
  target.write('*\n')
  mach = independent_vars[1]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[2]
  alt = alt.replace('\n','').replace('\r','')
  target.write(alt)
  target.write('*\n') 
  print ("FNod ",FNod)
  print ("mach ",mach)
  print ("alt ",alt)

#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS")
  print ("index11[2] ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  WF = w[index11[3]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(WF)
  target.write('*')
  print ("WF VALUES ")
  print (WF)
  print ("DONE PRINTING WF VALUES")
# print "Complete Fuel Table"
# print fuel_table
#
#!#%
#BEGIN FNCLB101
#open file to write the FNCLB101 table to 
for w in tables:
 if (w[0:8] == 'FNCLB101'): 
  print ("found FNCLB101")

  tbl_out_FNCLB101 = parser.get('table_maker_output_FNCLB101', 'tbl_out_FNCLB101')
  target = open(tbl_out_FNCLB101,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("independent_vars[0] ",independent_vars[0])
  print ("independent_vars[1] ",independent_vars[1])
  print ("independent_vars[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the FNCLBod values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS AND PANTHERS")
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMEJAGS ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  FNCLB = w[index11[2]:]
  target.write(FNCLB)
  target.write('*')
  print ("FNCLB VALUES ")
  print (FNCLB)
  print ("DONE PRINTING FNCLB VALUES")
#!#%
#
#BEGIN FNCRU101
#open file to write the FNCRU101 table to 
for w in tables:
 if (w[0:8] == 'FNCRU101'): 
  print ("found FNCRU101")


  tbl_out_FNCRU101 = parser.get('table_maker_output_FNCRU101', 'tbl_out_FNCRU101')

  target = open(tbl_out_FNCRU101,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("independent_vars[0] ",independent_vars[0])
  print ("independent_vars[1] ",independent_vars[1])
  print ("independent_vars[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the FNCLBod values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are more JAGUARS AND PANTHERS")
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMEJAGS ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  FNCRU = w[index11[2]:]
  target.write(FNCRU)
  target.write('*')
  print ("FNCRU VALUES ")
  print (FNCRU)
  print ("DONE PRINTING FNCRU VALUES")
#!#%
#
#BEGIN WFIDL101
#open file to write the WFIDL101 table to 
for w in tables:
 if (w[0:8] == 'WFIDL101'): 
  print ("found WFIDL101")

  tbl_out_WFIDL101 = parser.get('table_maker_output_WFIDL101', 'tbl_out_WFIDL101')
  target = open(tbl_out_WFIDL101,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  print ("independent_vars[0] ",independent_vars[0])
  print ("independent_vars[1] ",independent_vars[1])
  print ("independent_vars[2] ",independent_vars[2])
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  alt = alt.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  disa = independent_vars[2]
  disa = disa.replace('\n','').replace('\r','')
  target.write(disa)
  target.write('*\n') 
  print ("mach ",mach)
  print ("alt ",alt)
  print ("disa ",disa)
#
#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  index11 = []
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS AND ALIENS")
  print ("index11[0] index11[1] index11[2] ",index11[0],\
         " ",index11[1]," ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ("HOMER ",w[index11[2]:])
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  WFIDL = w[index11[2]:]
  target.write(WFIDL)
  target.write('*')
  print ("WFIDL VALUES ")
  print (WFIDL)
  print ("DONE PRINTING WFIDL VALUES")
#!#%
#
#END WFIDL101 section
#
 if (w[0:8] == 'CDBASE01'):
  print ("found CDBASE01")

  tbl_out_CDBASE01 = parser.get('table_maker_output_CDBASE01', 'tbl_out_CDBASE01')
  target = open(tbl_out_CDBASE01,'w')
  target.truncate()    
  independent_vars = w[9:].split("1cl\n")
  cl = independent_vars[0]
  cl = cl.replace('cl\n', 'cl#').replace('\r','')
  print ("cl = ",cl)
  target.write(cl)
  target.write('*\n')

  mach_and_cd = independent_vars[1].split("1mach\n")
  mach = mach_and_cd[0]
  mach = mach.replace('mach\n', 'mach#').replace('\r','')
  target.write(mach)
  target.write('*\n')
  
  cd = mach_and_cd[1]
  target.write(cd)
  target.write('*') 
#
#Now we are going to find the member of the tables list containing the string
#for the table FNBLDF01 and process that table 
#we know from the INFLT User Manual that FNBLDF01 is based on FN = F(mach,alt)
#
#We want to split the FNBLDF01 table into 3 parts; part 1 consists of the independent variable 
#mach values, part 2 consists of the independent variable alt values, and part 3 consists of the 
#dependent variable FN values.
#
#open file to write the FNBLDF01 table to 
independent_vars = []
index11 = []
for w in tables:
 if (w[0:8] == 'FNBLDF01'):
  print ("found FNBLDF01")

  tbl_out_FNBLDF01 = parser.get('table_maker_output_FNBLDF01', 'tbl_out_FNBLDF01')
  target = open(tbl_out_FNBLDF01,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  mach = independent_vars[0]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[1]
  print ("HERE IS ALT ",alt) 
  #alt = mach.replace('1\n', '1#').replace('\r','')
  alt = alt.replace('\n','')
  target.write(alt)
  target.write('*\n')
  index11 = []
#Now we want to find the index in w which is the start of the FN values
#this would be right after the 2nd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
# print "index11[2] ",index11[2]
# print "Here"  
# outer = index11[2] + 100
# print w[index11[2]:outer]  
  FNbld = w[index11[1]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(FNbld)
  target.write('*')
  print ("FNbld Values")
  print (FNbld)
  print ("DONE PRINTING FNbld VALUES")
#!!!
#
#Now we are going to find the member of the tables list containing the string
#for the table TABCORF01 and process that table 
#we know from the INFLT User Manual that TABCORF01 is based on WFdelta = F(W/delta,mach,alt)
#
#We want to split the TABCORF01 table into 4 parts; part 1 consists of the independent variable 
#FNod values, part 2 consists of the independent variable mach values, part 3 consists of the
#independent variable altitude values, and part 4 consists of the dependent variable WF values.
#
#open file to write the TABCORF01 table to 
independent_vars = []
index11 = []
fuel_table = []
CORFlist = []
for w in tables:
 #print ("TRYING TO FIND TABCORF01 and w = ",w)
 #print ("w[0:9] = ",w[0:9]) 
 if (w[0:9] == 'TABCORF01'):
  print ("found TABCORF01")

  tbl_out_TABCORF01 = parser.get('table_maker_output_TABCORF01', 'tbl_out_TABCORF01')
  target = open(tbl_out_TABCORF01,'w')
  target.truncate()
  independent_vars = w[9:].split("11\n")
  FNod = independent_vars[0]
  FNod = FNod.replace('1\n', '1#').replace('\r','')
  FNod = FNod.replace('\n','')
  target.write(FNod)
  target.write('*\n')
  mach = independent_vars[1]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[2]
  alt = alt.replace('\n','').replace('\r','')
  target.write(alt)
  target.write('*\n') 
  print ("FNod ",FNod)
  print ("mach ",mach)
  print ("alt ",alt)

#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
# print "index11[2] ",index11[2]
# print "Here"  
# outer = index11[2] + 100
# print w[index11[2]:outer]  
  CORF = w[index11[2]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(CORF)
  target.write('*')
  print ("CORF VALUES ")
  print (CORF)
  print ("DONE PRINTING CORF VALUES")
# print "Complete Fuel Table"
# print fuel_table
#
#!!!
#At this point in the process we have written out four string objects
#to the files WFTAB101_tbl.txt and CDBASE01_tbl.txt or to similarly named
#output files.
#For WFTAB101 these four string objects are:
#   FNod - contains the entire set of thrust over delta values as one string
#   mach - contains the entire set of mach values as one string
#   alt - contains all of the altitude values as one string
#   WF - contains the entire set of fuel flow values as one string
#Within WFTAB101_tbl.txt, the four strings are separated by the delimiter '*'
#



#
#Now we are going to find the member of the tables list containing the string
#for the table CDPSI01 and process that table 
#we know from the INFLT User Manual that CDPSI01 is based on CD = f(CN) 
#where CN = yawing moment coefficient
#The CDPSI01 table has only one part because there is only 1 indpendent variable 
#
#!#%
#open file to write the CDPSI01 table to
#BEGIN CDPSI01, this is the table header for Yaw Drag data
#open file to write the CDPSI01 table to 
for w in tables:
 if (w[0:7] == 'CDPSI01'):
  if (w[0:7] == 'CDPSI01'):
   print ("found CDPSI01")
  tbl_out_CDPSI01 = parser.get('table_maker_output_CDPSI01', 'tbl_out_CDPSI01')
  target = open(tbl_out_CDPSI01,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")  
  print ("independent_vars[0] ",independent_vars[0])
  CD = independent_vars[0]
  CD = CD.replace('1\n', '1#').replace('\r','')
  CD = CD.replace('\n','')
  target.write(CD)
  target.write('*\n')

#
#Now we are going to find the member of the tables list containing the string
#for the table CDWM01 and process that table 
#The CDWM01 table has two one parts because there are 2 indpendent variables 
#
#!#%
#open file to write the CDWM01 table to
#BEGIN CDWM01, this is the table header for Yaw Drag data
#open file to write the CDWM01 table to 
for w in tables:
 if (w[0:6] == 'CDWM01'):
  if (w[0:6] == 'CDWM01'):
   print ("found CDWM01")
  tbl_out_CDWM01 = parser.get('table_maker_output_CDWM01', 'tbl_out_CDWM01')
  target = open(tbl_out_CDWM01,'w')
  target.truncate()
  independent_vars = w[7:].split("11\n")  
  print ("independent_vars[0] ",independent_vars[0])
  CD = independent_vars[0]
  CD = CD.replace('1\n', '1#').replace('\r','')
  CD = CD.replace('\n','')
  target.write(CD)
  target.write('*\n')

#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
  print ("Here are JAGUARS")
  print ("index11[2] ",index11[2])  
  outer0 = index11[0] + 100
  outer1 = index11[1] + 100
  outer2 = index11[2] + 100
  print ('0 ',w[index11[0]:outer0])  
  print ('1 ',w[index11[1]:outer1]) 
  print ('2 ',w[index11[2]:outer2]) 
  WF = w[index11[3]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(WF)
  target.write('*')
  print ("WF VALUES ")
  print (WF)
  print ("DONE PRINTING WF VALUES")
# print "Complete Fuel Table"
# print fuel_table

###########################
#### JSON Maker Execution module ####
###########################
# Following the execution of the table maker module, we have a group of .txt files 
# containing data for a specific dependent variable. These files need to be prcoessed
# by the corresponding JSON Maker program for that dependent variable data type.

# Read in list of JSON maker programs
jsonmaker_pgrmlist = parser.get('jsonmaker_all','jsnmkrs')
separated_jsnmkrs = jsonmaker_pgrmlist.split(",")
print ("Separated_jsnmkrs are", separated_jsnmkrs)

# Get the dependent variable names from the list of configuration files 
table_names_raw = [i.split('_table_to_JSON.py')[0] for i in separated_jsnmkrs] 
table_names = [i.split('INFLT_')[1] for i in table_names_raw] 
print('The table_names are ', table_names)

# Create list for 
jsnmkr_outputs_list = []

# Run each json maker selected in the configuration file. Input/output filenames are automatically constructed
for f in separated_jsnmkrs:
    print ("Opening Json Maker", f) 
    
    # Construct JSON Maker input/output file names
    v = table_names[depvarindex]  # depvarindex set to 0 at top of script
    jsnmkr_input = dat_string + '_' + v + '_tbl.txt'
    jsnmkr_output = dat_string + '_' + v + '_JSON.txt'
    print('JSON Maker input/output filenames =', jsnmkr_input, jsnmkr_output)
    
    #Add JSON Maker output file to list for later use by IAPVE.py
    jsnmkr_outputs_list.append(jsnmkr_output)
    
    #Pass JSON Maker input/output filenames to each Json maker as arguments
    sys.argv = [jsnmkr_input, jsnmkr_output]
    #execfile(f)                 # For python 2.7
    exec(open(f).read())         # For python 3  
    depvarindex += 1


##############################
### IAPVE Module ###
##############################
# Following the execution of the JSON makers module, we have a group of _JSON.txt files. 
# IAPVE.py is a separate program responsible for reading from the INFLT tables stored 
# as JSON strings in the _JSON.txt files.

#Execute IAPVE.py
exec(open('IAPVE.py').read()) 

outfile = open('multiple_city_pairs_July23_737-800W.csv','a',newline='')
writer = csv.writer(outfile,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
header_row = ["PointID","WFvalue","Fuel_flow","Temperature","Weight","Mach","Altitiude","CL",\
              "CD","Lift","Drag","Thrust-per-eng/delta","Total_Thrust/delta","Plan_fuel_flow","Delta_fuel_flow","% diff"] 
writer.writerow(header_row)



###################################
### Statistical Analysis Module ###
###################################

### Hard coded filename for IAPVE output/statistical analysis input
iapve_output = 'iapve_output.csv'

# Read in list of statistical analysis program
parser.read(config_file_name)
stat_analy_prgm = parser.get('statistical_analysis_programs','stat_analy_prgm')
print ("Statistical Analysis Program is ", stat_analy_prgm)

stat_analy_dir = parser.get('statistical_analysis_program_directory','stat_analy_dir')
os.chdir(stat_analy_dir)
print ("Changed working directory to the Statistical Analysis Program directory")

stat_analy_prgm_full = stat_analy_dir + '/' + stat_analy_prgm

### Direct hard coded path and program name for Statistical Analysis
#StatAnalyPrgm = '/Users/Daniel/Documents/SIMCON/Git/bitbucket/IAPVE_dev/StatisticalAnalysisProgram'

#Now we create the entire command for running GetMerge
cmdSA = 'Rscript ' + stat_analy_prgm_full + " " + iapve_output  
print(cmdSA)
print("Before os.sytem(cmdSA)")
os.system(cmdSA)
print("os.sytem(cmdSA) is complete")



"""
##############################
### Interpolation Selector ###
##############################

#Manually set py_scrpt to the name of the correct IAPVE interpolation program
#py_scrpt = parser.get('python script','py_scrpt')
#if py_scrpt != "NA":
#    print("Please make sure python script file is present in directory")
#else:
#    print("py_scrpt = ", py_scrpt)
"""
