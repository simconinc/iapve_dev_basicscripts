import csv
import numpy as np
import sys
import json

#Initialize variables and structures
count = 0
FNbld = []
mach = []
alt = []

# Parsing the input/output filename arguments from trialobyte
print ('Number of arguments received from trialobyte: ', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
# assign input filename
dp = sys.argv[0]
# assign output filename
fn = sys.argv[1]

#This code section uses the method to create a reader object using csv.reader and then
#use the reader object to create a list using dalist = [data for data in data_iter]
#
#**** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE input file
#dp = 'P8podup_FNBLDF01_tbl.txt'
#dp = 'JBx37800W_FNBLDF01_tbl.txt'
#****
#
if (dp[0:2] == "P8"):
   p8 = True
else:
   p8 = False
#in files with names ending in _FNBLDF01_tbl.txt, * is the deliminter
#between the 3 subtables (mach,alt,FNbld) 
dl = '*'
with open(dp,'rU') as csvfile:
   data_iter = csv.reader(csvfile,delimiter=dl)
#  for row in data_iter:
#   print ("row ",row)
   dalist = [data for data in data_iter] #creates the list dalist

# The list named dalist is made up of the following:
# 1st element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire mach block
#     element 2 is ''
# 2nd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire alt block
#     element 2 is ''
# 3rd - nth elements in dalist are each a list with a single member that consists 
# of a string created from an entire line of FNbld data found in .._FNBLDF01_tbl.txt  

for i in dalist:
  print ("just after for i in dalist")
  print (i)
  if (count == 0 and len(i) > 0): 
   print ("splitting up the members of the mach string using the delimiter 1#") 
   k = i[0].split("1#")
   for ar in k:
    mach.append(ar.split())
   print ("k")
   print (k)
   print ("Here is mach")
   print (mach)
  if (count == 1 and len(i) > 0): 
   print ("splitting up the members of the alt string using the delimiter 1#") 
   k = i[0].split("1#")
   for ar in k:
    alt.append(ar.split())
   print ("k")
   print (k)
   print (" ")
   print ("Here is alt")
   print (alt)
  if (count >=2 and len(i) > 0):
   print ("splitting up the members of the FNbld string") 
   FNbld.append(i[0].split())
  count = count + 1
  print(" ")
print("Done with Section 1: Here is mach ",mach)
print ("And here is alt ",alt)
#
print(" ")
print ("Here is FNbld")
print(FNbld) 
#So at this point we have built 3 list objects.
#  mach - contains multiple elements: each element is a list made up of data from a sub group within the mach section of the FNBLDF01 table 
#  alt - contains multiple elements: each element is a list made up of data from a sub grup within the alt section of the WFTAB101 table
#        note - as an example, in the FNBLDF01 table extracted from the file JBx37800W.dat the alt section of the FNBLDF01
#               table has only one sub group so there is only one element in the alt list which
#               is made up of data from that one sub group 
#  FNbld - contains multiple elements: each element is a list made up of data taken from a string created from an entire line of the 
#          FNbld section of the FNBLDF01 table 
#
#Now we take advantage of the knowledge that each element of the WF list 
#makes up a sub group from the FNbld section of the FNBLDF01 table
#Furthermore, we know that the third of these three lists will always have just three members; 
#
#The following section of code populates the dictionary named FNbld_dict
#The structure of WF_dict is as follows:
#Each key in WF_dict is one of the altitudes from the altitude section of the FNBLDF01 table
#These are 0, 5000, 10000, ...., 41000 
#The value associated with each altitude key is a dictionary named mach_fuel_burn_dic.
#The keys in mach_fuel_burn_dic are the mach numbers (stored as mach number * 1000) applicable for
#the corresponding altitude. For example, mach .400 is stored as 400
#The value associated with each mach number key is a list made up of the fuel burn rate (FBR) values
#applicable to the corresponding combination of alitude and mach number.  Each individual FBR value
#is associated with a particular FNod value that is in a corresponding list stored in FNod. 
#
hit_zero = False
hit_one = False
mach_cnt_listmbrs = 0
mach_cnt = 0
alt_cnt = 0
mach_fuel_burn_dic = {}
fuel_burn_values = []
FNbld_values = []
FNbld_dict = {}
for f in FNbld:
 if (len(f) == 0):
  print("No more data")
  break
 print ("f IS THIS ",f)
 for g in f:
  print ("g IS THIS ",g)
  val = float(g)
  print ("val IS THIS ",val)
  if (val > 15.0):
    FNbld_values.append(val)
 print ("END OF THIS f and FNbld_values = ",FNbld_values)
 Mach_FNbld_tuple = (mach[mach_cnt],FNbld_values)
 alt_key = int(alt[0][alt_cnt])
 print('alt_key = ',alt_key,"alt_cnt = ",alt_cnt)
 FNbld_dict.update({alt_key:Mach_FNbld_tuple})
 FNbld_values = []
 mach_cnt = mach_cnt + 1
 alt_cnt = alt_cnt + 1
#          
print("Victory")
print (FNbld_dict.keys())
print (FNbld_dict)
"""
count = 0
while (count <= (alt_cnt-1)):
  print ("count alt[0][count] ",count," ",alt[0][count])
  the_alt_key = int(alt[0][count])
  print ("the_alt_key = ",the_alt_key)
  print (WF_dict.get(the_alt_key))
  count = count + 1
  print ("count = ",count,"alt_cnt = ",alt_cnt)
"""
#Use json.dumps to create a JSON object and then write it to .._WFTAB101_JSON.txt
#fn = "JBx37800W_FNBLDF01_JSON.txt"
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file
#fn = "P8podup_FNBLDF01_JSON.txt"
#fn = "JBx37800W_FNBLDF01_JSON.txt"
#***
target = open(fn,'w')
target.truncate()
target.write(json.dumps(FNbld_dict))
print ("FINISHED with FNBLDF01 JSON Maker")



  
