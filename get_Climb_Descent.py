#NOTE:
#keys obtained from dictionaries loaded via json.loads
#are always strings. But the value associated with any 
#given key maintains its original type
#
import json, sys
import math
import std_atm
import getCDWF
import reynolds
import CLBuffet
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2
#P8clean = False
P8clean = True
P8podup = False
P8poddn = False
#JBx37800W = True
JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
#
#S is the surface area of the wing in square feet
S = 1341.0
#G is the acceleration of gravity in ft/sec**2
G = 32.17405

def get_FNodclbdesc(ftemperature,fmach,falt,fweight,fgama,faccelerate,aircraft_config):
 exact_match = False
 CD = {}
 WF = {}
#CD_file = open('CDBASE01_JSON.txt')
#WF_file = open('WFTAB101_JSON.txt')
#CD_file = open('37800WCFM56_57_CDBASE01_JSON.txt')
#WF_file = open('37800WCFM56_57_WFTAB101_JSON.txt')
#CD_file = open('P8clean_CDBASE01_JSON.txt')
#WF_file = open('P8clean_WFTAB101_JSON.txt')
#
 if (aircraft_config == 1 or aircraft_config == 2):
  diffcheck = 0.25
 else:
  diffcheck = 0.9
#CDBASE TABLES ARE UNIQUE FOR EACH MODEL
#WITHIN A MODEL, THE CDBASE TABLES ARE ALSO 
#UNIQUE FOR EACH FLAPS SETTING 
#(flaps up, 1, 5, 10, 15)
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8clean_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8clean_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8clean_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8clean_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8clean_CDBASE06_JSON.txt')
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR ALL NON POD MODELS
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR THE POD MODELS
  WF_file = open('P8clean_WFTAB101_JSON.txt')
  if (aircraft_config == 2):
   WF_file = open('P8clean_WFTAB104_JSON.txt')
#
 if (P8podup):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8podup_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8podup_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8podup_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8podup_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8podup_CDBASE06_JSON.txt')
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR ALL NON POD MODELS
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR THE POD MODELS
  WF_file = open('P8podup_WFTAB111_JSON.txt')
  if (aircraft_config == 2):
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8poddn_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8poddn_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8poddn_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8pddn_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8poddn_CDBASE06_JSON.txt')
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR ALL NON POD MODELS
#THERE IS A WFTAB101 AND A WFTAB104 COMMON FOR THE POD MODELS
  WF_file = open('P8podup_WFTAB111_JSON.txt')
  if (aircraft_config == 2):
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('JBx37800W_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('JBx37800W_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('JBx37800W_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('JBx37800W_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('JBx37800W_CDBASE06_JSON.txt')
  WF_file = open('JBx37800W_WFTAB101_JSON.txt')
#
 a1string = CD_file.readline()
 CD = json.loads(a1string)
 a2string = WF_file.readline()
 WF = json.loads(a2string)
 theta_ambient = (ftemperature + 273.15)/288.15
#fmach = ftas/(661.4786 * (theta_ambient ** (0.5)))
 int_mach = int((fmach*100.0) + .5)
 mach = str(int_mach)
 int_alt = int(falt)
 alt = str(int_alt)
#Compute the Lift Coefficient cl
#Use Eqn 9 of Chapter 9 in Jet Transport Performance Methods.pdf
 L =  fweight * math.cos(fgama)
 dlta = std_atm.alt2press_ratio(falt)
 print("L falt dlta fmach S = ",L," ",falt," ",dlta," ",fmach," ",S)
 print("AQUA aircraft_config P8clean P8podup P8poddn JBx37800W ",aircraft_config," ",P8clean," ",P8podup," ",P8poddn," ",JBx37800W)
 try:
  cl = L/(1481.4*(fmach**2)*dlta*S)
 except:
  cl = 0.80
 print ("COMPUTED CL = ",cl)
#
#CLEAN P8, P8podup, and P8poddn ALL USE THE SAME BUFFET TABELS
#
 if (P8clean or P8podup or P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   clmax = CLBuffet.CLBuffetP8_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffetP8_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffetP8_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffetP8_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffetP8_06(cl,fmach)
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   print ("CALLING CLBuffet37800W_01 with cl and fmach = ",cl," ",fmach)
   clmax = CLBuffet.CLBuffet37800W_01(cl,fmach)
  if (aircraft_config == 3):
   clmax = CLBuffet.CLBuffet37800W_03(cl,fmach)
  if (aircraft_config == 4):
   clmax = CLBuffet.CLBuffet37800W_04(cl,fmach)
  if (aircraft_config == 5):
   clmax = CLBuffet.CLBuffet37800W_05(cl,fmach)
  if (aircraft_config == 6):
   clmax = CLBuffet.CLBuffet37800W_06(cl,fmach)
# 
 print ("clmax = ",clmax)
 if (clmax >= (cl*0.001)):
#
  cl_list = []
  cd_list = []
  available_CDmachs = []
  CDvalue_list = []
  available_CDmachs = getCDWF.getmach_fromCDBASE(aircraft_config)
  print(" ")
  print("available_CDmachs = ",available_CDmachs)
#deterine sensible machs
  sensible_mach_list = []
  for trymach in available_CDmachs:
   checkmach = float(trymach)/1000.00
   differ = abs(fmach - checkmach)
   #if (differ <= 0.25):
   if (differ <= diffcheck): 
    sensible_mach_list.append(trymach)
   print("sensible_mach_list = ",sensible_mach_list)
#
# for mgrab in available_CDmachs:
  for mgrab in sensible_mach_list:
#  print("From CDBASE01 ")
#  print ("CD[mgrab] ",CD[str(mgrab)])
   cl_list = CD[str(mgrab)][0]
   cdho_list = CD[str(mgrab)][1]
   cd_list = [xcd*0.000001 for xcd in cdho_list]
   #print ("cl = ",cl," ahead of getting CDvalue from Biparabolic1")
   if (isinstance(cl_list[0],str)):
    #print ("ripper cat")
    clfloat = []
    for ichg in cl_list:
     clfloat.append(float(ichg))
    cl_list = clfloat
   CDvalue = Biparabolic1(cl_list,cd_list,cl,'cd')
   CDvalue_list.append(CDvalue)
   print("For mgrab = ",mgrab," CDvalue from Biparabolic1 = ",CDvalue)
   print("SO FAR CDvalue_list = ",CDvalue_list)
   print(" ")
 else:
  CDvalue_list.append(-888)
#CDvalue = 0.22285
#
 if (CDvalue_list[0] == -888):
  theCDvalue = -888
  FFNod = -888
  D = -888
  return(cl,theCDvalue,L,D,FFNod)
 else:
  print("NOODLE call to Biparabolic1 where cl = ",cl)
  #fmachable_list =[(float(xv)*0.001) for xv in available_CDmachs] 
  fmachable_list =[(float(xv)*0.001) for xv in sensible_mach_list]
  print("fmachable_list = ",fmachable_list) 
  print("fmach and CDvalue_list = ",fmach," ",CDvalue_list)
  theCDBASEvalue = Biparabolic1(fmachable_list,CDvalue_list,fmach,'cd')
  #print("theCDBASEvalue = ",theCDBASEvalue)
  if (aircraft_config <= 2):  
   theReyCDvalue = reynolds.reynolds(fweight,fmach,falt,ftemperature)
  else:
   print ("NO REYNOLDS CORRECTION FOR THIS CONFIG")
   theReyCDvalue = 0.0
  print("theReyCDvalue = ",theReyCDvalue)
  theCDvalue = theCDBASEvalue + theReyCDvalue
#TEMPORARY FOR 1LE
  if (aircraft_config == 2):
   theCDvalue = theCDvalue*1.07
  print ("theCDvalue theCDBASEvalue theReyCDvalue ",theCDvalue," ",theCDBASEvalue," ",theReyCDvalue)
  print("AGAIN L falt dlta fmach = ",L," ",falt," ",dlta," ",fmach)
#Now compute the Drag
#See A-15 in Jet Transport Performance Methods.pdf
#
  mass = fweight/G
  D = theCDvalue * (1481.4*(fmach**2)*dlta*S) 
  eyow = G*math.sin(fgama)
  print ("mass faccelerate fgama Gtimessinofgama ",mass," ",faccelerate," ",fgama," ",eyow)
#since G is in ft/sec**2 we need faccelerate in ft/sec**2
  Thrust = D + mass*(faccelerate + G*math.sin(fgama)) 
  print ("Drag = ",D," Thrust = ",Thrust)
  if (aircraft_config == 2):
   FFNod = (Thrust/dlta)
  else:
   FFNod = (Thrust/dlta)/2.0
  #FFNod = 27000.00
  return(cl,theCDvalue,L,D,FFNod)


