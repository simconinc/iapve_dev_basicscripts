#This is the main driver program for reading from the INFLT tables stored as JSON strings 
#and for reading from the flight plan output file stored as JSON output
import sys, json
import math
import csv
#import get_INFLTtables_storedasJSON
import std_atm
import getCDWF
import get_FNod
import get_FNodCLB
import get_lists_forWF
import get_Climb_Descent
import get_fromFNCLB
import get_fromFNCRU
import get_fromWFIDL
import getFNCNT
import get_APUwf
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2
from Linear import Linear
import BuffetBoundary
import airspeed
import IAPVEutils
#
#outfile = open('jaguar.csv','a',newline='')
#outfile = open('tol_jaguar.csv','a',newline='')
#outfile = open('sjc_tol_jaguar.csv','a',newline='')
#outfile = open('multiple_city_pairs_July30_737-800W.csv','a',newline='')
#outfile = open('multiple_city_pairs_P8clean_flapsup_Aug2.csv','a',newline='')
#outfile = open('multiple_city_pairs_737800W_Aug2.csv','a',newline='')
#outfile = open('multiple_city_pairs_JBx37800W_Aug3pm.csv','a',newline='')
#!outfile = open('validation_results_kewr_ksea_m77_0wind0disa_JBx37800W_Sept24.csv','a',newline='')
#outfile = open('validation_windcmp_testcase.csv','a',newline='')
#outfile = open('MultipleP8CleanValidationPlans091015.csv','a',newline='')
#outfile = open('July8pm_Multiple_city_pairs_BB_P8clean.csv','a',newline='')
#outfile = open('July8pm_flaps01_P8clean.csv','a',newline='')
#outfile = open('July8pm_flaps15_P8clean.csv','a',newline='')
#outfile = open('P8clean_flaps15.csv','a',newline='')
#outfile = open('P8clean_flaps01.csv','a',newline='')
#outfile = open('P8clean_flaps05.csv','a',newline='')
#outfile = open('P8clean_flaps10.csv','a',newline='')
#outfile = open('P8clean_flaps15.csv','a',newline='')
#outfile = open('P8clean_flapsupnzaa.csv','a',newline='')
#outfile = open('July8pm_Flapsup_P8clean.csv','a',newline='')
#outfile = open('P8podup.csv','a',newline='')
#outfile = open('P8clean.csv','a',newline='')
#outfile = open('P8cleanJuly22.csv','a',newline='')
#outfile = open('JBx37800W_results_July2_2015.csv','a',newline='')
#outfile = open('P8cleanflaps01_FL100_results_2July7_2015.csv','a',newline='')
#outfile = open('P8cleanflaps15_FL200_results_July8pm_2015.csv','a',newline='')
#!outfile = open('P8 Validation Clean Low LRC_090415.csv','a',newline='')
#!!outfile = open('P8 Validation Clean Low M55_0DISA_092515.csv','a',newline='')
#outfile = open('Sept26 P8 Validation Clean Low M65_0DISA_092515.csv','a',newline='')
#outfile = open('Sept26 P8 Validation Clean Low M80_0DISA_092515.csv','a',newline='')
#outfile = open('Sept26 P8 Validation Clean Low M82_0DISA_092515.csv','a',newline='')
#outfile = open('Sept26 P8 Validation Clean Low M55_0DISA_092515.csv','a',newline='')
#***outfile = open('Mtple P8 Validation Clean Low All_SPDs_0DISA_092515.csv','a',newline='')
#outfile = open('LinFNCLB 738-800W Validation Low M55 M25_DISA_092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean Low All_SPDs_0DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Multiple P8 Validation Clean Low All_SPDs_+25+15DISA_092515.csv','a',newline='')
#outfile = open('Multiple P8 Validation Clean High All_SPDs_-25-15DISA_092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean Low All_SPDs_0DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean High All_SPDs_0DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct23 Multiple P8 Validation Clean High LRC_0DISA_posnegWCMP_plnsfrm101515.csv','a',newline='')
#outfile = open('Oct25 Multiple P8 Validation Clean High ALL_SPDs_0posnegDISA_plnsfrm0925515.csv','a',newline='')
#outfile = open('Oct25 Multiple P8 Validation Clean Low ALL_SPDs_0posnegDISA_plnsfrm0925515.csv','a',newline='')
#outfile = open('Oct25 Mutpl P8 Validation Clean Low Hvy ALL_SPDs_0posnegDISA_plnsfrm102115.csv','a',newline='')
#outfile = open('Oct25 Mutpl P8 Validation Clean High Hvy ALL_SPDs_0posnegDISA_plnsfrm102215.csv','a',newline='')
#outfile = open('Oct27 737-800W Validation High M82_+25DISA_plnfrm102615.csv','a',newline='')
outfile = open('Oct27 Mutpl P8 Clean Validation HighLowAltsWgts HighSpds_plnsfrm102715.csv','a',newline='')
#outfile = open('Oct 29 Mutpl P8 Validation PodUp All_FlapsUp_Spds_Alts_Wgts_DISAs_Winds.csv','a',newline='')
#outfile = open('tempo_newfeatures.csv','a',newline='')
#outfile = open('Oct15 Multiple P8 Validation Clean Low LRC_0DISA_posnegWCMP_plnsfrm101515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean High All_SPDs_+15DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean High All_SPDs_+25DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean High All_SPDs_-15DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean High threeSPDs_-25DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean Low All_SPDs_+15DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean Low All_SPDs_+25DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean Low All_SPDs_-25DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('Oct14 Multiple P8 Validation Clean Low All_SPDs_-15DISA_plnsfrm092515.csv','a',newline='')
#outfile = open('P8 Validation Clean High IK2_0DISA_101315.csv','a',newline='')
#outfile = open('737-800W Validation Clean High All_SPDs_+25+15DISA_100515.csv','a',newline='')
#outfile = open('737-800W Validation Clean High All_SPDs_+25+15DISA_100515.csv','a',newline='')
#outfile = open('737-800W Validation KEWR KSEA 0DISA.csv','a',newline='')
#outfile = open('Multiple P8 Validation CleanFlaps01 0DISA_100215.csv','a',newline='')
#outfile = open('Oct 14 Multiple P8 Validation CleanFlaps01 0DISA_plnsfrm101415.csv','a',newline='')
#outfile = open('Oct 14 Multiple P8 Validation CleanFlaps05 0DISA_plnsfrm101415.csv','a',newline='')
#outfile = open('Oct 14 Multiple P8 Validation CleanFlaps01 -25DISA_plnsfrm101415.csv','a',newline='')
#outfile = open('Oct 14 Multiple P8 Validation CleanFlaps10 -25DISA_plnsfrm101415.csv','a',newline='')
#outfile = open('Oct 15 Multiple P8 Validation CleanFlaps15 -25DISA_plnsfrm101415.csv','a',newline='')
#mostrecent outfile = open('Oct 26 Multiple P8 Validation Clean1LE allDISAs_plnsfrm100815.csv','a',newline='')
#outfile = open('tempo.csv','a',newline='')
#outfile = open('tempoISAp25.csv','a',newline='')
#outfile = open('Multiple P8 Validation CleanFlaps05 0DISA_100215.csv','a',newline='')
#outfile = open('Multiple P8 Validation CleanFlaps15 0DISA_100215.csv','a',newline='')
#outfile = open('Multiple 737-800W Validation Flaps05 0DISA_101215.csv','a',newline='')
#outfile = open('Multiple P8 Validation Clean High All_SPDs_0DISA_092515.csv','a',newline='')
writer = csv.writer(outfile,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
#
#Read in INFLT table data formatted as JSON strings
logDebug = 1

#Read in all of the text from the file that contains SULU2 flight plan output
#for a single plan and do the following:
#    Build up a single string from the text and in so doing
#    eliminate line feeds (/n) and carriage returns (/r) 
#
#G is the acceleration of gravity in ft/sec**2
G = 32.17405
TotalT_TotalD = " "
fcl = 0.0
encountered_POD = False
encountered_POA = False
#
yesCLBDSC = True
#yesCLBDSC = False
#header = True
#header = False
#singleplan = True
singleplan = False
incPODPOA = True
#
arglist = sys.argv
print ("HERE IS arglist ",arglist)
try:
 if (str(arglist[4][0:6]) == "HDRYES"):
  print ("HDRYES")
  header = True
 elif (str(arglist[4][0:5]) == "HDRNO"):
  header = False
except:
 header = False
#
engfac = 2.0
run_num = str(arglist[1])
if (str(arglist[2][0:2]) == 'FL'):
 argFL = arglist[2][2:5]
 argISA = arglist[3]
 print ("argFL argISA ",argFL," ",argISA)
elif (str(arglist[2][0:5] == "PodUp" or str(argist[2][0:5] == "PodDn"):
 if (str(arglist[3][0:3] == "Opt")"
  IPval = str(arglist[4][0:5]
  ISAT = str(arglist[5][1:4]
  Wind = str(arglist[6]1:4]
  json_output_name = arglist[2][0:5] + " " + "Opt IP"+IPval+"T"+ISAT+" "+"W"+Wind_Ouput.json"
  print ("json_output_name = ",json_output_name) 
 try:
  if (str(arglist[5][0:6]) == "HDRYES"):
   print ("HDRYES")
   header = True
  elif (str(arglist[5][0:5]) == "HDRNO"):
   header = False
 except:
  header = False
elif (str(arglist[3][0:6]) == 'ISAWND'):
 SpdISA = str(arglist[2]) + " " + "ISA" + \
 str(arglist[3][6:9]) + " " + str(arglist[3][9:13])
 print ("Nog SpdISA ISA and WND = ",SpdISA)
else:
 SpdISA = str(arglist[2]) + " " + str(arglist[3])
 if (SpdISA[0:3] == "1LE"):
  engfac = 1.0
 print ("SpdISA = ",SpdISA," engfac = ",engfac)
print ("run_num = ",run_num)
#
hitcruise = False
include_TOC = True
onward = True
diag = 3
#fcas1 = 250.0
#fcas2 = 340.0
mydata = []
i = 1
fpData = {}
astring = ''
fp_json_string = ''
kite = 1
fixclimb = False
climb_or_descent = False
inclb_or_dsct = False
lookaheadaltitude = "NA"
snapshot = False
aircraft_config = 1
snapshot_altcnt = 0
snapshot_altlist = []
snapshot_roclist = []
snapshot_distancelist = []
snapshot_airdistancelist = []
snapshot_timelist = []
snapshot_taslist = []
snapshot_tasktlist = []
snapshot_temperaturelist = []
snapshot_weightlist = []
snapshot_machlist = []
snapshot_pointidlist = []
snapshot_flightphaselist = []
snapshot_planfuelflowlist = []
snapshot_FPCavthrustodlist = []
snapshot_maxcruisealtlist = []
snapshot_rocaltlist = []
snapshot_buffaltlist = []
#
cont_proc = False
cont_ftemperature = 0
cont_ftas = 0
cont_ftaskt = 0
cont_fweight = 0
cont_falt = 0
cont_fmach = 0
cont_faccelerate = 0
cont_sinofgama = 0
cont_fgama = 0
cont_flight_phase = " "
cont_ftime = 0
FPCavthrustod = " "
cont_Calc_roc = 0
cont_FPC_roc = 0
cont_plan_fuel_flow = 0
#
#fn_of_flightplan = 'jsonfpdata.txt'
#fn_of_flightplan = 'ktol_kmia_json.txt'
#fn_of_flightplan = 'ksjc_ktol_json.txt'
#fn_of_flightplan = 'kbos_eham_M75_json.txt'
#fn_of_flightplan = 'panc_zbaa_ci_json.txt'
#fn_of_flightplan = 'ebbr_llbg_ci_json.txt'
#fn_of_flightplan = 'ksjc_ktol_m78_0wind0disa_json.txt'
#fn_of_flightplan = 'JB_kewr_ksea_m77_0wind0disa_json.txt'
#fn_of_flightplan = 'kewr_ksea_m77_0wind0disa_json.txt'
#fn_of_flightplan = 'xB738_kewr_ksea_m77_0wind0disa_json.txt'
#fn_of_flightplan = 'JBx37800W_kewr_ksea_m77_0wind0disa_json.txt'
#fn_of_flightplan = 'JBx37800W_kewr_ksea_m77_0wind0disa_erikfix_json.txt'
#fn_of_flightplan = 'xB738_Clean_ClimbDesc_Test1_KEWR_KSEA_Output_json.txt'
#fn_of_flightplan = 'test.txt'
#fn_of_flightplan = 'xB738 Clean ClimbDesc Rerun 28Aug #2_Ouput.json'
#fn_of_flightplan = 'xB738 Clean ClimbDesc Rerun 04Sept_Ouput.json'
#!fn_of_flightplan = 'xB738 Clean ClimbDesc Rerun 10Sept_Ouput.json'
#fn_of_flightplan = 'windcmp.json'
#fn_of_flightplan = 'xB738 Clean ClimbDesc Rerun 04Sept_Ouput.json'
aircraft_config = 1
#!!fn_of_flightplan = 'P8CleanValidationPlans090415\P8 Validation Clean Low LRC_Ouput.json'
#!!!fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation Low Plans\P8 Validation Clean Low M55 ISA-00_Ouput.json'
#!!!!fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation Low Plans\P8 Validation Clean Low M65 ISA+00_Ouput.json'
#!!!!!fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation Low Plans\P8 Validation Clean Low M80 ISA+00_Ouput.json'
#fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation Low Plans\P8 Validation Clean Low ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation High Plans\P8 Validation Clean High ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation Low Plans\P8 Validation Clean Low ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'Oct 21 2015 P8 clean flaps up hvy weight low alt\P8 Validation Clean Low ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'Oct 22 2015 P8 clean flaps up hvy weight high alt\P8 Validation Clean High ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'Oct 27 2015 737-800W M82 High Alt\/xB738 Clean High ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'P8Clean Plans Oct 13 2015\P8 Validation Clean Low ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'Oct 27 2015 P8 High_low Alt High Speed hm TOWs\P8 Validation Clean High ' + SpdISA + '_Ouput.json'
#CURRENT CURRENT CURRENT
fn_of_flightplan = 'Oct 29 2015 P8 PodUp\' + json_output_name
#fn_of_flightplan = 'P8 Clean Plans WindCmps Oct 15 2015\P8 Validation Clean High ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'P8 Clean Plans WindCmps Oct 15 2015\P8 Validation Clean Low ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = '7378W Validation Clean high ' + SpdISA + '_Ouput.json'
###$$$
#fn_of_flightplan = '737-800W Plans Sept 29 2015\737-800W Validation Low Plans\xB738 Low ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = '737-800W Plans Sept 29 2015\737-800W Validation Low Plans\xB738 Low ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'xB738 Low ' + SpdISA + '_Ouput.json'
###$$$
#fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation High Plans\P8 Validation Clean High ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation High Plans\P8 Validation Clean High ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = 'P8Clean Plans Sept 25 2015\P8 Validation High Plans\P8 Validation Clean 040 FLAPS01 ISA+00_Ouput.json'
#aircraft_config = 6
#aircraft_config = 2
#fn_of_flightplan = 'P8Clean Plans Oct 2 2015\P8 Clean Flaps Plans\P8 Validation Clean ' + argFL + ' FLAPS15 ISA' + argISA + '_Ouput.json'
#fn_of_flightplan = 'P8 Clean Flaps Plans Oct 14 2015\P8 Validation Clean ' + argFL + ' FLAPS01 ISA' + argISA + '_Ouput.json'
#fn_of_flightplan = 'P8 Clean Flaps Plans Oct 14 2015\P8 Validation Clean ' + argFL + ' FLAPS10 ISA' + argISA + '_Ouput.json'
#fn_of_flightplan = 'P8 Clean Flaps Plans Oct 14 2015\P8 Validation Clean ' + argFL + ' FLAPS15 ISA' + argISA + '_Ouput.json'
#mostrecentfn_of_flightplan = 'P8 clean plans 1LE Oct 8 2015\P8 Validation Clean ' + SpdISA + '_Ouput.json'
#fn_of_flightplan = '737-800W plans generated Oct 12 2015\/xB738 ' + argFL + ' FLAPS05 ISA+00_Ouput.json'
print("STARTING with processing ",fn_of_flightplan)
#fn_of_flightplan = 'Sept 25 Plans redone for 737-800W\xB738 Low M55 ISA-25_Ouput.json'
#fn_of_flightplan = 'xB738 Low M55 ISA-25_Ouput.json'
#fn_of_flightplan = 'xB738_egkk_llbg_m77_0wind0disa_json.txt'
#fn_of_flightplan = 'wsss_rctp_m70_0wind0disa_json.txt'
#fn_of_flightplan = 'ksfo_cyyg_lrc_0wind0disa_json.txt'
#fn_of_flightplan = 'kbos_eham_m75_0wind0disa_json.txt'
#fn_of_flightplan = 'kdfw_cyyg_ci_0wind0disa_json.txt'
#fn_of_flightplan = 'P8clean_kewr_ksea_flapsup_0wind_0disa_json.txt'
#fn_of_flightplan = 'P8clean_kdfw_cyyg_ci_json.txt'
#fn_of_flightplan = 'P8clean_kbos_eham_ci_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_ci_json.txt'
#fn_of_flightplan = 'P8clean_vabb_egkk_ci_json.txt'
#fn_of_flightplan = 'P8clean_klax_kmsp_ci_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_FL320_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_MTOW_json.txt'
#fn_of_flightplan = 'P8clean_cyyr_skcl_FL240_json.txt'
#fn_of_flightplan = 'P8clean_cyyr_skcl_FL200_json.txt'
#fn_of_flightplan = 'P8clean_cyyr_skcl_FL150_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_TOW160K_M65_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_TOW160K_M68_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_TOW160K_M74_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_TOW160K_M80_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_TOW160K_M82_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_TOW160K_LRC_json.txt'
#fn_of_flightplan = 'P8clean_ksea_kmia_TOW160K_MRC_json.txt'
#fn_of_flightplan = 'P8clean_nzaa_egar_TOW160K_M55_FL200_json.txt'
#fn_of_flightplan = 'P8clean_kpdx_panc_TOW160K_MRC_FL250_json.txt'
#fn_of_flightplan = 'P8clean_khou_mroc_TOW160K_OPT_json.txt'
#fn_of_flightplan = 'P8clean_nzaa_egar_TOW160K_flaps01_json.txt'
#aircraft_config = 3
#fn_of_flightplan = 'P8clean_kpdx_panc_TOW160K_flaps15_json.txt'
#fn_of_flightplan = 'P8clean_kpdx_panc_TOW160K_flaps15_json_modbokma.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL240_flaps15_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL100_flaps15_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL200_flaps15_json.txt'
#aircraft_config = 6
#fn_of_flightplan = 'P8clean_msp_atl_flaps15_FL100_json.txt'
#not available fn_of_flightplan = 'P8clean_msp_atl_flaps15_FL150_json.txt'
#fn_of_flightplan = 'P8clean_msp_atl_flaps15_FL200_json.txt'
#fn_of_flightplan = 'P8clean_msp_atl_flaps15_FL050_json.txt'
#fn_of_flightplan = 'P8clean_bxm_mia_flaps15_FL100_json.txt'
#fn_of_flightplan = 'P8clean_bxm_mia_flaps15_FL200_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL150_flaps15_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL100_flaps01_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL200_flaps01_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL250_flaps01_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL250_flaps01_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL150_flaps01_json.txt'
#aircraft_config = 3
#fn_of_flightplan = 'P8clean_klax_phnl_FL100_flaps05_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL150_flaps05_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL200_flaps05_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL250_flaps05_json.txt'
#aircraft_config = 4
#fn_of_flightplan = 'P8clean_klax_phnl_FL100_flaps10_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL150_flaps10_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL200_flaps10_json.txt'
#fn_of_flightplan = 'P8clean_klax_phnl_FL250_flaps10_json.txt'
#aircraft_config = 5
#fn_of_flightplan = 'P8clean_panc_cyyc_TOW160K_M55_FL210_json.txt'
#fn_of_flightplan = 'P8clean_panc_cyyc_TOW160K_M80_FL250_json.txt'
#fn_of_flightplan = 'P8podup_klax_phnl_FL240_json.txt'
#
json_file = open(fn_of_flightplan)
print ("JUST OPENED ",fn_of_flightplan)
#
while(i):
   astring = json_file.readline()
   if (astring == ''):
      i = 0
   elif (astring != ""):
      fp_json_string += astring
#
fp_json_string.replace('\n',' ').replace('\r',' ')
fpData = json.loads(fp_json_string)
#
print ("HERE ARE KEYS")
print (fpData.keys())
print ("DONE WITH KEYS")
print ("HERE ARE ITEMS")
print (fpData.items())
print ("DONE WITH ITEMS")

log2Data = []
pod = fpData["pod"]
poa = fpData["poa"]
oew = fpData["ac"]["oew"]
route = fpData["plan"]["route"]
profile = fpData["plan"]["profile"]
print ("pod poa owe ",pod," ",poa," ",oew)

if (singleplan):
 row = [pod,poa]
 writer.writerow(row)
 row = ["Route",route]
 print ("row = ",row)
 writer.writerow(row)
 row = ["Profile",profile]
 print ("row = ",row)
 writer.writerow(row)
#
if (header):  
 header_row = ["FP_PointID","FP_Flight_Phase","FP_fuel_flow","FP_Temperature","FP_DISA",\
               "FP_WindComponent","FP_Weight","FP_Mach","FP_CASkt",\
               "FP_TASkt","FP_TASftpermin","FP_ElpTime","FP_Altitiude",\
               "FP_maxcruisealt","FP_rocalt","FP_buffalt","CL","CL_gmrgn1.035","CLmax",\
               "CD","Lift","Drag","Req Thrust-per-eng/delta","Req TotalThrust/delta",\
               "Avlb TotalThrust/delta","Req TotalThrust_gmrgn1.035/delta","FP_AvailableThrust/delta",\
               "WFvalue","Fuel_flow",\
               "Delta_fuel_flow","Rel_diff","TotalTheta","TotalDlt","factor","CalcROC","FP_ROC",\
               "TAS Acceleration","ACC","gamma","gradient","run_num"] 
 writer.writerow(header_row)
data_set = {}
if (fpData["weather"][0:4] == "NONE"):
 windcmp = "0"
else:
 windcmp = fpData["weather"][1:4]
payload = fpData["plan"]["log2"][0]["payload"]
deptfuel = fpData["plan"]["log2"][0]["fuel"]
try:
 prevalt = fpData["plan"]["log2"][1]["altitude"]
except:
 prevalt = fpData["plan"]["log2"][1]["snapshots"][0]["altitude"]
print ("payload = ",payload," deptfuel = ",deptfuel)
for checkpoint in fpData["plan"]["log2"]:
      print (" ")
      print ("CHECKER checkpoint ",checkpoint)
#     print ("checkpoint type = ",checkpoint["type"])
      id  = checkpoint["id"]
      typee = checkpoint["type"]
      print ("id & type ",id," ",typee)
      snapshot_key = "snapshots"
      useable = True
      onwardfuelflow = 0
      onwardtas = 0
      if (checkpoint["type"] == "APT"):
                 print ("CHECKS OUT AS AN AIRPORT")
                 if (not encountered_POD):
                  encountered_POD = True
                 else:
                  encountered_POA = True
                 try:
                               print ("TRYING")
                               snapshot = False
                               payload = checkpoint["payload"]
                               time = checkpoint["time"]
                               print ("payload ",payload)
                               deptfuel = checkpoint["fuel"]
                               print ("deptfuel ",deptfuel)
                               deptwgt = oew + payload + deptfuel
                               print ("deptwgt ",deptwgt)
                               set_type = checkpoint["type"]
                               snap_profile = "NA"
                               id = checkpoint["id"]
                               if (id == pod):
                                weight = deptwgt*2.20462
                               elif (id == poa):
                                inpayload = fpData["plan"]["in"]["payload"]
                                infuel = fpData["plan"]["in"]["fuel"]
                                weight = (oew + inpayload + infuel)*2.20462
                                print ("EUREKA POA inpayload infuel weight ",inpayload," ",infuel," ",weight)
                               apt_temperature = checkpoint["temperature"]
                               apt_elevation = checkpoint["elevation"]
                               data_set = {"PointID":id,"Payload":payload,"Deptfuel":deptfuel,"DeptWgt":deptwgt,\
                                           "SetType":set_type,"SnapProfile":snap_profile,"SnapShot":snapshot,"Time":time,\
                                           "Useable":useable,"Altitude":apt_elevation} 
                               print ("data_set for APT")
                               print (data_set)  
                               log2Data.append(data_set)
                               prevalt = 0.0
                               fuelflow = 0.0
                               prevs = 0
                               if (encountered_POD and not encountered_POA):
                                COL1 = id+"  ROUTE "+route+poa+"  PROFILE "+profile
                               if (encountered_POD and encountered_POA):
                                COL1 = id
                               row = [COL1," "," ",apt_temperature," "," ",weight," "," "," "," "," ",\
                               apt_elevation," "," ",\
                               " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",\
                               " "," "," "," "," ",\
                               " "," "," ",run_num] 
                               print ("row = ",row)
                               if (incPODPOA):
                                writer.writerow(row)
#

                 except:
                               if False:
                                      print ("no payload or fuel at this airport")
#
      #print ("TIME = ",checkpoint["time"]," ",type(checkpoint["time"]))
      if ((str(checkpoint["type"]) == "FIX" or \
          str(checkpoint["type"]) == "VOR") and \
          (str(checkpoint["id"]) == pod or str(checkpoint["id"]) == poa)): 
           print ("CHECKS OUT AS A FIX OR VOR as a POD or POA")
           if (str(checkpoint["id"]) == pod):
            encountered_POD = True
           if (str(checkpoint["id"]) == poa):
            encountered_POA = True
           payload = checkpoint["payload"]
           deptfuel = checkpoint["fuel"]
           print ("deptfuel ",deptfuel)
           deptwgt = oew + payload + deptfuel         
           id = checkpoint["id"]
           print ("id = ",checkpoint["id"]," pod = ",pod," poa = ",poa)         
           if (id == pod):
            weight = deptwgt*2.20462
           elif (id == poa):
            inpayload = fpData["plan"]["in"]["payload"]
            infuel = fpData["plan"]["in"]["fuel"]
            weight = (oew + inpayload + infuel)*2.20462
            print ("EUREKA POA inpayload infuel weight ",inpayload," ",infuel," ",weight)
           fix_temperature = checkpoint["temperature"]
           fix_elevation = checkpoint["elevation"]
           if (encountered_POD and not encountered_POA):
            COL1 = id+"  ROUTE "+route+poa+"  PROFILE "+profile
           if (encountered_POD and encountered_POA):
            COL1 = id
           row = [COL1," "," ",fix_temperature," "," ",weight," "," "," "," "," ",fix_elevation," ",\
                  " "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
                  " "," "," ",run_num] 
           print ("row = ",row)
           if (incPODPOA):
            writer.writerow(row)
# 
      if ((str(checkpoint["type"]) == "FIX" or \
           str(checkpoint["type"]) == "VOR") and\
           checkpoint["time"] != 0):
                 print ("CHECKS OUT AS A FIX OR VOR")
                 alt = checkpoint["altitude"]
                 print ("ALT = ",alt)
                 if (kite == 1):
                               snapshot = False
                               try:
                                fuelflow = checkpoint["fuelflow"]
                               except:
                                fuelflow = "NA"
                               print ("alt = ",alt," ",type(alt)," prevalt = ",prevalt)
                               if (alt != prevalt):
                                climb_or_descent = True
                               else:
                                climb_or_descent = False
                               tas = checkpoint["tas"]
                               distance = checkpoint["distance"]
                               airdistance = checkpoint["airdistance"]
                               time = checkpoint["time"]
                               burn = checkpoint["burn"]
                               temperature = checkpoint["temperature"]
                               fuel = checkpoint["fuel"]
                               weight = oew + payload + fuel
                               set_type = checkpoint["type"]
                               id = checkpoint["id"]
                               print ("EL NINO")
                               print ("for id = ",id," oew payload fuel weight = ",oew," ",payload," ",fuel," ",weight)
                               print("IN FIX OR VOR LAND AND id = ",id)
                               try:
                                snap_profile = checkpoint["profile"]
                               except:
                                snap_profile = "NA"
                               try:
                                vs = checkpoint["vs"]
                               except:
                                vs = 0.0
                               print ("BULLROAR snap_profile vs ",snap_profile," ",vs)
                               #prestochangeo
                               if (snap_profile == "TOC" or vs > 0.0 or snap_profile == "BOD" or vs < 0.0):
                                snapshot = True
                               if (vs > 0.0):   
                                #if ((alt%1000) and prevs > 0):
                                if (alt%1000):
                                 useable = False  
                                 print ("THIS ALTITUDE IS NOT USEABLE ",alt)
                                else:
                                 useable = True 
                                 prevalt = alt                   
                                fixclimb = True
                                if (snap_profile != "TOC"):
                                 snap_profile = "CLB"
                                if (alt == 2000):
                                 cas = airspeed.tas2cas(tas,2000.0,temperature)
                                 fcas1 = float(int(cas+0.5))
                                 print ("WARYBERRY fcas1 = ",fcas1)
                                if (alt == 12000):
                                 cas = airspeed.tas2cas(tas,12000.0,temperature)
                                 fcas2 = float(int(cas+0.5))
                               else:
                                fixclimb = False
                                prevalt = alt
                               if (snap_profile == "TOC" or snap_profile == "BOC"\
                                   or snap_profile == "BOD" or snap_profile == "TOD"):
                                print ("FIX IS A TOC OR BOC or BOD or TOD") 
                                try:      
                                 onwardfuelflow = checkpoint["onwardflow"]
                                except:
                                 onwardfuelflow = 0
                                try:
                                 onwardtas = checkpoint["onwardtas"]  
                                except:
                                 onwardtas =  0                    
#
# See Jet Transport Performance Methods.pdf Appendix pages A7-A10 for descriptions of Theta and Mach
# and how Theta is determined in the context of Jet Transport. I refer to this Theta as theta_ambient
# because it is determined as outside (aka ambient) air temperture in deg K at a given flight level divided
# by ISA temperature at sea level which is 288.15 deg K.  
# Note - FPC outputs temperature in deg C so the ambient air temperature in deg K is temperature + 273.15
#
# By convention, delta is pressure at flight level divided by pressure at sea level based on standard atmosphere
                               press_ratio = std_atm.alt2press_ratio(alt)
                               theta_ambient = (temperature + 273.15)/288.15
                               mach_nbr = tas/(661.4786 * (theta_ambient ** (0.5)))
                               prevs = vs
                               try:
                                avclbthrust = checkpoint["clbthrust"]
                               except:
                                avclbthrust = " "
                               try:
                                avdscthrust = checkpoint["dscthrust"]
                               except:
                                avdscthrust = " "
                               maxcruisealt = checkpoint["maxcruisealt"]
                               try:
                                FPCdrag = checkpoint["drag"]
                               except:
                                print ("NO FPCdrag for this point")
                               rocalt = checkpoint["rocalt"]
                               buffalt = checkpoint["buffalt"]   
                               fidisa = IAPVEutils.get_disa(alt,temperature)  
                               print("WIND COMPONENT AND DISA ARE ",windcmp," ",fidisa)                   
                               data_set = {"PointID":id,"Altitude":alt,"Temperature":temperature,"TAS":tas,\
                                           "Fuelflow":fuelflow,"Weight":weight,"Delta":press_ratio,\
                                           "ThetaAmbient":theta_ambient,"Mach":mach_nbr,"SetType":set_type,\
                                           "Climb_or_Descent":climb_or_descent,"PrevAlt":prevalt,"SnapShot":snapshot,\
                                           "Distance":distance,"AirDistance":airdistance,\
                                           "Time":time,"VertSpeed":vs,"SnapProfile":snap_profile,\
                                           "FixClimb":fixclimb,"Useable":useable,"AvClbT":avclbthrust,\
                                           "AvDscT":avdscthrust,"Onwardfuelflow":onwardfuelflow,"Onwardtas":onwardtas,\
                                           "Maxcruisealt":maxcruisealt,"Rocalt":rocalt,"Buffalt":buffalt,\
                                           "DevfromISA":fidisa,"WindCmp":windcmp,"Drag":FPCdrag}
                               print ("BANCO data_set AvDscT ",data_set["AvDscT"])
                               print ("data_set for FIX or VOR")# See Jet Transport Performance Methods.pdf Appendix pages A7-A10 for descriptions of Theta and Mach
                               print (data_set)
                               log2Data.append(data_set)
#                except:
#                              if False:
#                                     print ("no alt or tas or fuelflow or burn or temperature or fuel at this point")
      
      if (str(checkpoint["type"]) == 'SID' or \
           str(checkpoint["type"]) == 'STAR' or \
            str(checkpoint["type"]) == 'DCT' or \
              str(checkpoint["type"])== 'AWY'):
                 print ("CHECKS OUT AS A SID STAR DCT or AIRWAY")
                 try:
                   for fix in checkpoint["snapshots"]:
                               fixclimb = False
                               useable = True
                               print(" ")
                               print("GOT A SNAPSHOT BESEDER alt = ",fix["altitude"]," and prevalt = ",prevalt)
                               print ("time = ",fix["time"])
                               try:
                                set_type = checkpoint["type"]
                               except:
                                set_type = "UKN"
                               snapshot = True
                               alt = fix["altitude"] 
                               if (alt == 2000 and fix["vs"] > 0):
                                cas = airspeed.tas2cas(tas,2000.0,temperature)
                                fcas1 = float(int(cas+0.5))
                                print ("WANTERBERRY fcas1 = ",fcas1)
                               try:
                                snap_profile = fix["profile"]
                               except:
                                snap_profile = "NA"
                               print("IN SNAPSHOT LAND AND snap_profile =",snap_profile)
                               if (alt == prevalt):
                                print ("IN SNAPSHOT LAND BUT WE HAVE A CRUISE POINT")
                                snap_profile == "CRU"
                                snapshot = False
                                set_type = "FIX"
                               if (snap_profile == "TOC" or snap_profile == "TOD"):
                                set_type = "FIX"
                                try:      
                                 onwardfuelflow = fix["onwardflow"]
                                except:
                                 onwardfuelflow = 0
                                try:
                                 onwardtas = fix["onwardtas"]  
                                except:
                                 onwardtas = 0   
                                print ("TOC onwardfuelflow onwardtas ",onwardfuelflow," ",onwardtas)
                               if (snap_profile == "BOC" or snap_profile == "TOD"):
                                snapshot = False
                                set_type = "FIX"
                               vs = fix["vs"]
                               try:
                                id = fix["id"]
                                set_type = "FIX"
                                print ("id = ",id,"set_type =",set_type)
                               except:
                                id = checkpoint["id"]
                               if ((vs == 0 or alt == prevalt) and not (alt > prevalt or alt < prevalt)):
                                snapshot = False 
                                set_type = "FIX"
                                indexlog2 = len(log2Data) - 1
                                if (log2Data[indexlog2]["SnapShot"]):
                                 if (log2Data[indexlog2]["SnapProfile"] != "TOD"\
                                     and log2Data[indexlog2]["SnapProfile"] != "BOD"\
                                     and log2Data[indexlog2]["SnapProfile"] != "BOC"):
                                  log2Data[indexlog2]["SnapProfile"] = "TOC"
                                  print("IN SNAPSHOT LAND AND previous snap_profile changed to TOC")
                               print("IN SNAPSHOT LAND AND id = ",id)
                               print("snapshot ",snapshot)    
                               print ("alt = ",alt," ",type(alt))
                               tas = fix["tas"]
                               distance = fix["distance"]
                               airdistance = fix["airdistance"]
                               time = fix["time"]
                               print ("tas distance airdistance time ",tas," ",distance," ",airdistance," ",time)
                               try:
                                fuelflow = fix["fuelflow"]
                               except:
                                fuelflow = "NA"
                               burn = fix["burn"]
                               temperature = fix["temperature"]
                               fuel = fix["fuel"]
                               print ("fuelflow burn temperature fuel ",fuelflow," ",burn," ",temperature," ",fuel)
                               weight = oew + payload + fuel
                               print ("weight set_type id vs ",weight," ",set_type," ",id," ",vs)
# See Jet Transport Performance Methods.pdf Appendix pages A7-A10 for descriptions of Theta and Mach
# and how Theta is determined in the context of Jet Transport. I refer to this Theta as theta_ambient
# because it is determined as outside (aka ambient) air temperture in deg K at a given flight level divided
# by ISA temperature at sea level which is 288.15 deg K.  
# Note - FPC outputs temperature in deg C so the ambient air temperature in deg K is temperature + 273.15
                               theta_ambient = (temperature + 273.15)/288.15
                               mach_nbr = tas/(661.4786 * (theta_ambient ** (0.5)))
# By convention, delta is pressure at flight level divided by pressure at sea level based on standard atmosphere
                               press_ratio = std_atm.alt2press_ratio(alt)
                               if (vs > 0):  
                                #if ((alt%1000) and prevs > 0):
                                if (alt%1000):
                                 useable = False
                                 print ("THIS ALTITUDE IS NOT USEABLE ",alt)
                                else:
                                 useable = True 
                                 prevalt = alt
                                if (alt == 6000):
                                 cas = airspeed.tas2cas(tas,alt,temperature)
                                 fcas1 = float(int(cas+0.5))
                                 print ("alt is 6000 and fcas1 = ",fcas1)
                                if (alt == 14000):
                                 cas = airspeed.tas2cas(tas,alt,temperature)
                                 fcas2 = float(int(cas+0.5))
                                 print ("alt is 14000 and fcas2 = ",fcas2)
                               else: 
                                prevalt = alt
                               prevs = vs
                               print ("theta_ambient mach_nbr press_ratio ",theta_ambient," ",mach_nbr," ",press_ratio)
                               #print ("checkpoint clbthrust = ",checkpoint["clbthrust"])
                               print ("WHALES ARE TRYING TO PROCESS A THRUST VALUE")
                               try:
                                avclbthrust = fix["clbthrust"]
                               except:
                                avclbthrust = " "
                               try:
                                avdscthrust = fix["dscthrust"]
                               except:
                                avdscthrust = " "
                               print ("MADE IT THIS FAR")
                               try:                             
                                maxcruisealt = fix["maxcruisealt"]
                               except:
                                maxcruisealt = " "
                               print ("MADE IT THIS FAR 1")
                               try:
                                rocalt = fix["rocalt"]
                               except:
                                rocalt = " "
                               print ("MADE IT THIS FAR 2")
                               try:
                                buffalt = fix["buffalt"]
                               except:
                                buffalt = " " 
                               print ("MADE IT THIS FAR 3")
                               fidisa = IAPVEutils.get_disa(alt,temperature)    
                               print ("fidisa = ",fidisa)
                               try:
                                FPCdrag = fix["drag"]
                               except:
                                print ("NO FPCdrag for this point")
                               #try:                 
                                #print ("avclbthrust and avdscthrust = ",avclbthrust," ",avdscthrust)
                               #except:
                                #print ("TRIED TO PRINT BUT FAILED")
#                              data_set = {"PointID":id,"Altitude":alt,"Temperature":temperature,"TAS":tas,\
#                                           "Fuelflow":fuelflow,"Weight":weight,"Delta":press_ratio,\
#                                           "ThetaAmbient":theta_ambient,"Mach":mach_nbr,"SetType":set_type}
                               data_set = {"PointID":id,"Altitude":alt,"Temperature":temperature,"TAS":tas,\
                                           "Fuelflow":fuelflow,"Weight":weight,"Delta":press_ratio,\
                                           "ThetaAmbient":theta_ambient,"Mach":mach_nbr,"SetType":set_type,\
                                           "SnapShot":snapshot,"Distance":distance,"AirDistance":airdistance,\
                                           "Time":time,"VertSpeed":vs,"SnapProfile":snap_profile,\
                                           "FixClimb":fixclimb,"Useable":useable,"AvClbT":avclbthrust,\
                                           "AvDscT":avdscthrust,"Onwardfuelflow":onwardfuelflow,"Onwardtas":onwardtas,\
                                           "Maxcruisealt":maxcruisealt,"Rocalt":rocalt,"Buffalt":buffalt,\
                                           "DevfromISA":fidisa,"WindCmp":windcmp,"Drag":FPCdrag}
                               print ("W data_set for SID STAR DCT or AWY")
                               print (data_set)
                               log2Data.append(data_set)
                     #except:
                               #print("WHAT HAPPENED")    
                               #if False:
                                    #print ("This segment does not have all of the elements it is supposed to have")             
                 except:
                   print ("This segment does not incur an altitude change and therefore there is no snapshots list")
                   useable = True
                   snapshot = False
                   try:
                    tas = checkpoint["tas"]
                   except:
                    tas = "NO TAS"
                   try:
                    alt = checkpoint["altitude"]
                   except:
                    alt = 0.0
                   snap_profile = "SEGWAY"
                   time = "NO_TIME"
#                  No fuel flow or fuel onboard values are available when on a segment without snapshots
#                  so we set fuelflow to -1 as a signature that there is no fuel flow available
                   fuelflow = -1
                   burn = checkpoint["burn"]
                   weight = oew + payload + (deptfuel - burn)
                   try:
                    temperature = fix["temperature"]
                   except:
                    temperature = "NO TEMPERATURE"
                   set_type = checkpoint["type"]
                   id = checkpoint["id"]
# See Jet Transport Performance Methods.pdf Appendix pages A7-A10 for descriptions of Theta and Mach
# and how Theta is determined in the context of Jet Transport. I refer to this Theta as theta_ambient
# because it is determined as outside (aka ambient) air temperture in deg K at a given flight level divided
# by ISA temperature at sea level which is 288.15 deg K.  
# Note - FPC outputs temperature in deg C so the ambient air temperature in deg K is temperature + 273.15
                   theta_ambient = (temperature + 273.15)/288.15
                   try:
                    mach_nbr = tas/(661.4786 * (theta_ambient ** (0.5)))
                   except:
                    mach_nbr = "NO MACH"
# By convention, delta is pressure at flight level divided by pressure at sea level based on standard atmosphere
                   press_ratio = std_atm.alt2press_ratio(alt)
                   data_set = {"PointID":id,"Altitude":alt,"Temperature":temperature,"TAS":tas,\
                               "Weight":weight,"Delta":press_ratio,"FuelFlow":fuelflow, \
                               "ThetaAmbient":theta_ambient,"Mach":mach_nbr,"SetType":set_type,\
                               "SnapShot":snapshot,"SnapProfile":snap_profile,"Time":time,\
                               "Useable":useable}
                   print ("data_set for SID STAR DCT or AWY")
                   print (data_set)
                   log2Data.append(data_set)
      try:
       prev_fuelflow = fuelflow
      except:
       prev_fuelflow = 0.0
#So now log2Data is a list of dictionary objects.  Each dictionary object contains log2 data for an 
#indivdual element (airport, fix, or segment) from with the JSON log2 element.   
#
#STOMP THE YARD
#
print ("HERE IS log2Data")
print (log2Data)
print(" ")
#Establish WFalts_mach_dict dictionary for use further down
available_WFalts_machs_dict = {}
available_WFalts_machs_dict = getCDWF.getalt_fromWFTAB(aircraft_config)
#
#Establish FNCLBdisa_alts dictionary for use further down
available_FNCLBdisa_alts_dict = {}
available_FNCLBdisa_alts_dict = get_fromFNCLB.getdisa_fromFNCLB101()
#
diag = 4
if (diag == 4):
 print(" ")
 print("IAPVE available_WFalts_machs_dict KEYS and VALUES")
 for iug in available_WFalts_machs_dict.keys():
  print ("available_WFalts_machs_dict key ",iug)
  print ("available_WFalts_machs_dict value",available_WFalts_machs_dict[iug])
 print(" ")
 print("IAPVE available_FNCLBdisa_alts_dict KEYS and VALUES")
 for iug in available_FNCLBdisa_alts_dict.keys():
  print ("available_FNCLBdisa_alts_dict key = ",iug)
  print ("available_FNCLBdisa_alts_dict value = ",available_FNCLBdisa_alts_dict[iug])
#
ai = 0
machmindiff = 0.75
altmindiff = 5000
#
mach_list = []
alt_list = []
ffnodlist = []
wfvaluelist = []
WFvalue_mach_list = []
WFvalue_alt_mach_list = []
WFvalue_alt_mach_dict = {}
alt_dict_forprint = {}
mach_dict_forprint = {}
snapshot_altcnt = 0
nextdict = {}
#
for adict in log2Data:
 print ("AT THE TOP OF THE CYCLE and adict[PointID] = ",adict["PointID"]," alt = ",adict["Altitude"])
 print ("AND adict[SnapProfile] = ",adict["SnapProfile"]," aircraft_config = ",aircraft_config)
 if (float(adict["Altitude"]) < 2000 and adict["SetType"] != "APT"):
  if (adict["VertSpeed"] < 0):
   #we don't process any descent points below 2000 feet
   break
 lookaheadsnapshot = False
 lookaheadaltitude = "NA"
 fcl = 0.0
 changedto = "NA"
 if ((ai + 1) < len(log2Data)):
  nextdict = log2Data[ai + 1]
  lookaheadsnapshot = nextdict["SnapShot"]
  lookaheadaltitude = nextdict["Altitude"]
#brilliant
#  if (adict["SnapShot"] and lookaheadaltitude == 11000):
#   print ("brilliant lookaheadaltitude is 11000")
#   lookaheadsnapshot == False
#  if (adict["SnapShot"] and adict["Altitude"] == 11000):
#   print ("brilliant current altitude is 11000")
#   snapshot_altcnt = 0
#   print("snapshot_altcnt = ",snapshot_altcnt)
#
  print ("lookaheadsnapshot = ",lookaheadsnapshot," lookaheadaltitude = ",lookaheadaltitude)
 print ("ai and adict ",ai," ",adict)
 if (adict["SetType"] == 'FIX' or adict["SetType"] == 'VOR' or adict["SnapProfile"] == 'TOC'
     or adict["SnapProfile"] == "BOD" or adict["SnapProfile"] == "BOC"):
  inclb_or_dsct = False
  print ("THE FIX IS IN ",ai," ",adict["PointID"]," ",adict)
  print ("Note that profile = ",profile)
  findseq = "/" + adict["PointID"]
  findprofchg = profile.find(findseq)
  #if (findprofchg >= 0):
   #print ("WILL USE THIS FIX for fuel flow calc BECAUSE THE ALTITUDE change has not started yet")
   #inclb_or_dsct = False
  if (adict["VertSpeed"] != 0.0 and (adict["SnapProfile"] != "TOC" and adict["SnapProfile"] != "BOD")):
   print ("VS GT 0 AND NOT TOC and NOT BOD so will treat this as occurring WITHIN A CLIMB OR DESCENT")
   inclb_or_dsct = True
  if (adict["VertSpeed"] > 0.0 and adict["SnapProfile"] == "TOC" and lookaheadsnapshot):
   print ("VS GT 0 AND TOC BUT looaheadsnapshot is true so will treat this as occurring\
           WITHIN A CLIMB OR DESCENT if it is itself a snapshot")
   if (adict["SnapShot"]):
    inclb_or_dsct = True
  if (not adict["Useable"]):
   inclb_or_dsct = True
 #if ((adict["SetType"] == 'FIX' or adict["SetType"] == 'VOR') and inclb_or_dsct):
   #roc_diff = roc_calc(log2Data,adict)
  print ("BIG BOPPER inclb_or_dsct = ",inclb_or_dsct," ",adict["SnapProfile"])
#BOPPER
#
 #CLIMB/DESCENT CALCS IN CASES OF SNAPSHOT
 if ((adict["SetType"] == 'DCT' or adict["SetType"] == 'SID' or adict["SetType"] == 'STAR'\
      or adict["SetType"] == 'AWY' or adict["SetType"] == 'FIX' or adict["SetType"] == 'VOR')\
      and adict["SnapShot"] and adict["Useable"] and yesCLBDSC):
# $$$ START PROCESSING A SNAPSHOT ITEM - SNAPSHOT REALM$$$
  print ("WE HAVE A SNAPSHOT and id is ",adict["PointID"])
  print ("GOLDEN GLOVE snapshot_altcnt = ",snapshot_altcnt)
  froc = adict["VertSpeed"]
  plan_fuel_flow = adict["Fuelflow"]
  print ("TOLEDO OHIO plan_fuel_flow = ",plan_fuel_flow)
  snapshot_planfuelflowlist.append(plan_fuel_flow)
  if (froc == 0.0):
   flight_phase = "---"
  if (froc > 0.0):
   flight_phase = "CLB"
  if (froc < 0.0):
   flight_phase = "DSC" 
  if (not lookaheadsnapshot and flight_phase != "DSC"):
   flight_phase = "TOC"
   changedto = "NA"
  snapshotP = adict["SnapProfile"]
  if (snapshotP == "TOC"):
   flight_phase = "TOC"
  print ("THE PROFILE IS ",snapshotP)
  snapshot_flightphaselist.append(flight_phase)
  snapshot_roclist.append(froc)
  falt = adict["Altitude"]
  snapshot_altlist.append(falt)
  fdist = adict["Distance"]
  snapshot_distancelist.append(fdist)
  fairdist = adict["AirDistance"]
  snapshot_airdistancelist.append(fairdist)
  pointid = adict["PointID"]
  snapshot_pointidlist.append(pointid)
  snapshot_maxcruisealtlist.append(adict["Maxcruisealt"])
  snapshot_rocaltlist.append(adict["Rocalt"])
  snapshot_buffaltlist.append(adict["Buffalt"])
  print ("Flight Envelope appendings ",snapshot_maxcruisealtlist," ",snapshot_rocaltlist," ",snapshot_buffaltlist)
#FOR CONSISTENCY WE MAKE SURE THAT
#TIME UNITS ARE IN MINUTES
#SPEED UNITS ARE IN FT/MIN
  ftime = adict["Time"]
# adict["Time"] is in minutes so no conversion needed
  snapshot_timelist.append(ftime)
  ftemperature = adict["Temperature"]
  snapshot_temperaturelist.append(ftemperature)
  ftaskt = (adict["TAS"])
  fmach = adict["Mach"]
  if (flight_phase == "CLB" and (falt == 2000 or (falt >= 9000 and falt <= 10000))\
      and not hitcruise):
   try:
    print ("FORCING TAS based on fcas1 with fcas1 = ",fcas1)
    ftaskt = airspeed.cas2tas(fcas1,falt,ftemperature)
   except:
    print("2000 90000 10000 could not execute airspeed.cas2tas")
   fmach = airspeed.tas2mach(ftaskt,ftemperature,falt)
   print("falt = ",falt," ftaskt = ",ftaskt," fmach = ",fmach)
  if (flight_phase == "CLB" and falt >= 11000 and falt <= 12000\
      and not hitcruise): 
   try:
    print ("FORCING TAS based on fcas2 with fcas2 = ",fcas2)
    ftaskt = airspeed.cas2tas(fcas2,falt,ftemperature)
   except:
    print("11000 to 12000 could not execute airspeed.cas2tas")
   fmach = airspeed.tas2mach(ftaskt,ftemperature,falt)
   print("falt = ",falt," ftaskt = ",ftaskt," fmach = ",fmach)
  #if (falt > 12000 and falt <= 22000 and flight_phase == "TOC" ):
  #if (falt > 12000 and falt <= 22000 and not hitcruise):
  if (falt > 12000 and falt <= 17000 and not hitcruise):
   print ("FORCING TAS AT ALL ALTS BETWEEN 12000 and 17000 INCLUSIVE based on fcas2")
   try:
    ftaskt = airspeed.cas2tas(fcas2,falt,ftemperature)
   except:
    print("12000 to 22000 could not execute airspeed.cas2tas")
   fmach = airspeed.tas2mach(ftaskt,ftemperature,falt)
   print("falt = ",falt," ftaskt = ",ftaskt," fmach = ",fmach)
  cutoveralt = 24000
  if (flight_phase == "TOC" and falt > cutoveralt and snapshot_altcnt == 2\
      and not inclb_or_dsct):
   print ("ABOVE CUTOVER ALT SO MAKING SURE MACH SPEED DOES NOT increase from previous CLB mach to TOC mach")
   fmach = snapshot_machlist[1]
#now convert fmach to the nearest hundreth of a mach
   fmachx = fmach * 1000.0
   fmachint = int(fmachx + 0.5)
   fmach = float(fmachint/1000)
   theta_ambient = (ftemperature + 273.15)/288.15
   ftaskt =  fmach*(661.4786 * (theta_ambient ** (0.5)))
#
  ftas = ftaskt*101.268591
# TAS value from FPC log2 is in kts so we converted it so that ftas is in ft/min
  snapshot_machlist.append(fmach)
  snapshot_taslist.append(ftas)
  snapshot_tasktlist.append(ftaskt)
  #print ("GROWL snapshot_taslist ",snapshot_taslist)
  fweight = adict["Weight"]*2.20462
  snapshot_weightlist.append(fweight)
  mydlta = std_atm.alt2press_ratio(falt)
  try:
   #NEWTON to LB
   if (adict["AvClbT"] != " "):
    FPCavthrustod = (adict["AvClbT"]* 0.22481)/mydlta
    if (engfac == 1.0):
     FPCavthrustod = FPCavthrustod/2.0
   elif (adict["AvDscT"] != " "):
    FPCavthrustod = (adict["AvDscT"]* 0.22481)/mydlta
    if (engfac == 1.0):
     FPCavthrustod = FPCavthrustod/2.0
   else:
    FPCavthrustod = " "
  except:
   FPCavthrustod = " "
  print ("OWL YOWL mydlta AvClbT AvDscT FPCavthrustod = ",mydlta," ",adict["AvClbT"]," ",adict["AvDscT"]," ",FPCavthrustod)
  snapshot_FPCavthrustodlist.append(FPCavthrustod)
  print ("PANTHER snapshot_taslist ",snapshot_taslist,"snapshot_temperaturelist ",snapshot_temperaturelist)
  print ("PANTHER2 snapshot_tasktlist ",snapshot_tasktlist)
  print ("PANTHER3 snapshot_timelist ",snapshot_timelist)
  print ("PANTHER4 snapshot_altlist ",snapshot_altlist)
  print ("PANTHER5 snapshot_planfuelflowlist ",snapshot_planfuelflowlist)
  print ("PANTHER6 snapshot_pointidlist ",snapshot_pointidlist)
  print ("PANTHER7 snapshot_FPCavThrustodlist ",snapshot_FPCavthrustodlist)
# Determine rate of climb, acceleration factor, and gamma
  if (snapshot_altcnt >= 1):
   if (snapshot_altcnt == 1):
    print ("calculating climb or descent parameters for a 2 member altitude set !!!!")
    tasdiff1 = snapshot_taslist[1] - snapshot_taslist[0]
   #tasdiff2 = snapshot_taslist[2] - snapshot_taslist[1]
    timediff1 = snapshot_timelist[1] - snapshot_timelist[0]
   #timediff2 = snapshot_timelist[2] - snapshot_timelist[1]
    wgtdiff1 = snapshot_weightlist[1]-snapshot_weightlist[0]
    fburnrate = (wgtdiff1/timediff1)*60.0
    roc1 = (snapshot_altlist[1]-snapshot_altlist[0])/(timediff1)
   #roc2 = (snapshot_altlist[2]-snapshot_altlist[1])/(snapshot_timelist[2]-snapshot_timelist[1])
   #Calc_roc = (roc1 + roc2)/2.0
   #Calc_Roc and FPC_row are rate of climb in ft/min
   #Calc_roc = roc1
   #Correct for Deviation from Std Atmosphere temperature
    ambientKelvin = (ftemperature + 273.15)
    fidisa = adict["DevfromISA"]
    std_atm_ambientKelvin = ambientKelvin - fidisa
    Calc_roc = (roc1*ambientKelvin)/(std_atm_ambientKelvin)
    print("One hp to hi: ftemperature fidisa ambientKelvin std_atm_ambientKelvin roc1 Calc_roc ",\
           ftemperature," ",fidisa," ",ambientKelvin," ",std_atm_ambientKelvin," ",roc1," ",Calc_roc)
    FPC_roc = snapshot_roclist[1]
   #need accelerate to be in ft/sec**2 so we convert tasdiff1 to secs and timediff1 to secs
    accelerate1 = (tasdiff1/60.0)/(timediff1*60.0)
   #accelerate2 = tasdiff2/timediff2
    print ("tasdiff1 timediff1 ",tasdiff1," ",timediff1)
   #faccelerate = (accelerate1 + accelerate2)/2.0
    faccelerate = accelerate1
    print("IN SNAPSHOT_ALTCNT = 1 and Calc_roc = ",Calc_roc," FPC_roc = ",FPC_roc," faccelerate ",faccelerate)
    print ("THE ALTITUDE FOR THIS SNAPSHOT IS ",snapshot_altlist[1]) 
    ftas = snapshot_taslist[1]
    ftaskt = snapshot_tasktlist[1]
    FPCavthrustod = snapshot_FPCavthrustodlist[1] 
    maxcruisealt = snapshot_maxcruisealtlist[1]
    rocalt = snapshot_rocaltlist[1]
    buffalt = snapshot_buffaltlist[1]
#   Calc_roc and FPC_roc are in ft/min and ftas is ft/min 
    #sinofgama = math.sin(FPC_roc/ftas)
    sinofgama = math.sin(Calc_roc/ftas)
    if (not inclb_or_dsct):
     cont_proc = True
     cont_ftemperature = snapshot_temperaturelist[1]
     cont_fweight = snapshot_weightlist[1]
     cont_falt = snapshot_altlist[1]
     cont_fmach = snapshot_machlist[1]
     cont_ftas = snapshot_taslist[1]
     cont_ftaskt = snapshot_tasktlist[1]
     cont_faccelerate = accelerate1
     cont_sinofgama = math.sin(roc1/cont_ftas)
     cont_fgama = math.asin(cont_sinofgama)
     cont_flight_phase = snapshot_flightphaselist[1]
     cont_ftime = snapshot_timelist[1]
     cont_FPCavthrustod = snapshot_FPCavthrustodlist[1]
     cont_Calc_roc = roc1
     cont_FPC_roc = snapshot_roclist[1]
     cont_maxcruisealt = snapshot_maxcruisealtlist[1]
     cont_rocalt = snapshot_rocaltlist[1]
     cont_buffalt = snapshot_buffaltlist[1]
     print ("cont flight envelopes",cont_maxcruisealt," ",cont_rocalt," ",cont_buffalt)
     if (onward):
      cont_plan_fuel_flow = adict["Onwardfuelflow"]
      cont_burnrate  = cont_plan_fuel_flow
      cont_ftaskt = adict["Onwardtas"]
      cont_ftas = cont_ftaskt*101.268591
      theta_ambient = (cont_ftemperature + 273.15)/288.15
      cont_fmach = cont_ftaskt/(661.4786 * (theta_ambient ** (0.5)))
     else:
      cont_plan_fuel_flow = plan_fuel_flow
      cont_burnrate = burnrate1
     print ("EASY COMPANY 1 cont_ftaskt cont_fmach cont_ftas ",cont_ftaskt," ",\
             cont_fmach," ",cont_ftas)
#
#
   if (snapshot_altcnt == 2):
#   !!!! calculating climb or descent parameters for a 3 member altitude set !!!!
    print ("calculating climb or descent parameters for a 3 member altitude set !!!!")
# 
    tasdiff1 = snapshot_taslist[1] - snapshot_taslist[0]
    tasdiff2 = snapshot_taslist[2] - snapshot_taslist[1]
    timediff1 = snapshot_timelist[1] - snapshot_timelist[0]
    timediff2 = snapshot_timelist[2] - snapshot_timelist[1]
    altdiff1 = snapshot_altlist[1]-snapshot_altlist[0]
    altdiff2 = snapshot_altlist[2]-snapshot_altlist[1]
    wgtdiff1 = snapshot_weightlist[1]-snapshot_weightlist[0]
    burnrate1 = (wgtdiff1/timediff1)*60.0
    wgtdiff2 = snapshot_weightlist[2]-snapshot_weightlist[1]
    burnrate2 = (wgtdiff2/timediff2)*60.0
    fburnrate = (burnrate1 + burnrate2)/2.0
    ftemp1 = snapshot_temperaturelist[1]
    ftemp2 = snapshot_temperaturelist[2]
    ambient1Kelvin = (ftemp1 + 273.15)
    ambient2Kelvin = (ftemp2 + 273.15)
    fidisa = adict["DevfromISA"]
    std_atm_ambient1Kelvin = ambient1Kelvin - fidisa
    std_atm_ambient2Kelvin = ambient2Kelvin - fidisa
    roc1 = (altdiff1)/(timediff1)
    roc1 = (roc1*ambient1Kelvin)/std_atm_ambient1Kelvin
    print("Two hp to hi: ftemp1 fidisa ambient1Kelvin std_atm_ambient1Kelvin roc1 ",\
           ftemp1," ",fidisa," ",ambient1Kelvin," ",std_atm_ambient1Kelvin," ",roc1)
    roc2 = (altdiff2)/(timediff2)
    roc2 = (roc2*ambient2Kelvin)/std_atm_ambient2Kelvin
    print("Two hp to hi: ftemp2 fidisa ambient2Kelvin std_atm_ambient2Kelvin roc2 ",\
           ftemp2," ",fidisa," ",ambient2Kelvin," ",std_atm_ambient2Kelvin," ",roc2)
    #print("roc1 roc2 ",roc1," ",roc2)
    Calc_roc = (roc1 + roc2)/2.0
#
    #print ("Calc_roc = ",Calc_roc)
    #Calc_roc = roc1
    #print ("Calc_roc  now = ",Calc_roc)
   #Calc_Roc and FPC_row are rate of climb in ft/min
    FPC_roc = snapshot_roclist[1]
   #need accelerate to be in ft/sec**2 so we convert tasdiff1 to secs and timediff1 to secs
    accelerate1 = (tasdiff1/60.0)/(timediff1*60.0)
    accelerate2 = (tasdiff2/60.0)/(timediff2*60.0)
    print ("altdiff1 tasdiff1 timediff1 ",altdiff1," ",tasdiff1," ",timediff1)
    print ("altdiff2 tasdiff2 timediff2 ",altdiff2," ",tasdiff2," ",timediff2)
    #if (snapshot_altlist[2] == 11000):
     #faccelerate = accelerate1
     #Calc_roc = roc1
     #print ("ALT2 IS 11000 and snapshot_altlist 1 is ",snapshot_altlist[1]," ",faccelerate," ",Calc_roc)
    #elif (snapshot_altlist[2] == 12000):
     #faccelerate = accelerate2
     #Calc_roc = roc2
     #print ("ALT2 IS 12000 and snapshot_altlist 1 is ",snapshot_altlist[1]," ",faccelerate," ",Calc_roc)
    #else:
    faccelerate = (accelerate1 + accelerate2)/2.0
    #print ("NOT ALT 11000 or 12000 ",snapshot_altlist[1]," ",faccelerate," ",Calc_roc)
    print("IN SNAPSHOT_ALTCNT = 2 and Calc_roc = ",Calc_roc," FPC_roc = ",FPC_roc," faccelerate ",faccelerate)
    ftas = snapshot_taslist[1]
    ftaskt = snapshot_tasktlist[1]
    FPCavthrustod = snapshot_FPCavthrustodlist[1] 
    maxcruisealt = snapshot_maxcruisealtlist[1]
    rocalt = snapshot_rocaltlist[1]
    buffalt = snapshot_buffaltlist[1]
#   Calc_roc and FPC_roc are in ft/min and ftas is ft/min 
    #sinofgama = math.sin(FPC_roc/ftas)    
    sinofgama = math.sin(Calc_roc/ftas)
    print ("ftas ftaskt sinofgama snapshot_altlist[1] ",ftas," ",ftaskt," ",sinofgama," ",snapshot_altlist[1])  
    ftemperature = snapshot_temperaturelist[1]
    fweight = snapshot_weightlist[1]
    falt = snapshot_altlist[1]
    fmach = snapshot_machlist[1]
    pointid = snapshot_pointidlist[1]
    flight_phase = snapshot_flightphaselist[1]
    ftime = snapshot_timelist[1]
#
    if (snapshot_altlist[1] == 10000 or snapshot_altlist[1] == 11000 and flight_phase == "CLB"):
     print ("CLIMB SPECIAL CASES ",snapshot_altlist[1])
     if (snapshot_altlist[1] == 10000):
      #specialftas = (snapshot_taslist[1] + snapshot_taslist[0])/2.0
      faccelerate = accelerate1
      Calc_roc = roc1
      sinofgama = math.sin(Calc_roc/ftas)
      fburnrate = burnrate1
     #no cheating allowed      FPC_roc = Calc_roc*0.998
     else: 
      faccelerate = accelerate2
      Calc_roc = roc2
      #no cheating allowed
      #   Calc_roc = (roc1*0.37) + (roc2*0.63)
      #set FPC_roc to blank until Erik fixes the CLB vs value at 11000
      FPC_roc = " "
      sinofgama = math.sin(Calc_roc/ftas)
      fburnrate = burnrate2
#
    if (snapshot_altlist[1] == 12000 and flight_phase == "CLB"):
     print ("CLB AT 12000")
     #no cheating allowed
     #   FPC_roc = Calc_roc*0.9666
     #set FPC_roc to blank until Erik fixes the vs value at 12000
     FPC_roc = " "
#
    if (snapshot_altlist[1] == 11000 and flight_phase == "DSC"):
     print ("DSC AT 11000")
     Calc_roc = roc1
#
    print("LYNX snapshot_altlist[1] = ",snapshot_altlist[1]," faccelerate = ",\
    faccelerate,"Calc_roc = ",Calc_roc," FPC_roc = ",FPC_roc)

#no cheating allowed FPC_roc = Calc_roc*0.98749
#
    print("onward = ",onward," inclb_or_dsct",inclb_or_dsct)
    if (not inclb_or_dsct):
     cont_proc = True
     cont_ftemperature = snapshot_temperaturelist[2]
     cont_fweight = snapshot_weightlist[2]
     cont_falt = snapshot_altlist[2]
     cont_fmach = snapshot_machlist[2]
     cont_ftas = snapshot_taslist[2]
     cont_ftaskt = snapshot_tasktlist[2]
     cont_faccelerate = accelerate2
     cont_sinofgama = math.sin(roc2/cont_ftas)
     cont_fgama = math.asin(cont_sinofgama)
     cont_flight_phase = snapshot_flightphaselist[2]
     cont_ftime = snapshot_timelist[2]
     cont_FPCavthrustod = snapshot_FPCavthrustodlist[2]
     cont_Calc_roc = roc2
     cont_FPC_roc = snapshot_roclist[2]
     cont_maxcruisealt = snapshot_maxcruisealtlist[2]
     cont_rocalt = snapshot_rocaltlist[2]
     cont_buffalt = snapshot_buffaltlist[2]
     if (onward):
      print ("HERE HERE HERE")
      cont_plan_fuel_flow = adict["Onwardfuelflow"]
      cont_burnrate  = cont_plan_fuel_flow
      cont_ftaskt = adict["Onwardtas"]
      cont_ftas = cont_ftaskt*101.268591
      theta_ambient = (cont_ftemperature + 273.15)/288.15
      cont_fmach = cont_ftaskt/(661.4786 * (theta_ambient ** (0.5)))
     else:
      cont_plan_fuel_flow = plan_fuel_flow
      cont_burnrate = burnrate2
     print ("EASY COMPANY 2 cont_ftaskt cont_fmach cont_ftas ",cont_ftaskt," ",\
             cont_fmach," ",cont_ftas)
#
    print ("3 member flight_phase = ",flight_phase)
    plan_fuel_flow = snapshot_planfuelflowlist[1]
    print ("FARGO ND plan_fuel_flow = ",plan_fuel_flow)
   if ((snapshot_altcnt == 1 and not lookaheadsnapshot)\
        or (snapshot_altcnt == 2)):
    print("DECISION TIME - IS THE FIX A USEABLE FIX")
    print ("snapshot_altcnt lookaheadsnapshot lookaheadaltitude falt ",\
            snapshot_altcnt," ",lookaheadsnapshot," ",lookaheadaltitude," ",falt)
    if (snapshot_altcnt ==1 and not lookaheadsnapshot):
    #Even if it is a FIX the point won't be treated as a FIX for later processing
    #because it is the upper memeber of a two point climb 
     if (not (flight_phase == "TOC" and onward)):
      inclb_or_dsct = True
     print ("MOOSE snapshot_altcnt = ",snapshot_altcnt," and inclb_or_dsct = ",inclb_or_dsct)
    else:
     if (snapshot_altcnt == 2 and lookaheadsnapshot):
      inclb_or_dsct = True
     if (snapshot_altcnt == 2 and not lookaheadsnapshot):
      inclb_or_dsct = False
     print ("GOOSE snapshot_altctn = ",snapshot_altcnt," and inclb_or_dsct = ",inclb_or_dsct)
    fgama = math.asin(sinofgama)
    print("fgama = ",fgama)
#   get drag coefficient thrust/delta and fuel flow 
    print ("TIGER AND THE Temperature Weight Altitude TAS and Mach are ",ftemperature," ",fweight," ",falt," ",ftas," ",fmach) 
#
#   fimach = float(imach)/1000.00
#**********GET THE THRUST OVER DELTA***********************
    (lift_coeff,drag_coeff,Lift,Drag,ffnod) = get_Climb_Descent.get_FNodclbdesc(ftemperature,fmach,falt,\
    fweight,fgama,faccelerate,aircraft_config)
    if (aircraft_config != 2):
     total_ffnod = ffnod*2.0
    else:
     total_ffnod = ffnod
    if (aircraft_config != 2):
     totalavlbclimbthrustod = get_FNodCLB.maxclimbthrust(falt,fmach,ftemperature,aircraft_config)
    else:
     totalavlbclimbthrustod = getFNCNT.getFNCNT(falt,fmach,ftemperature,aircraft_config)
    print("GANA LIZARD THRUST OVER DELTA totalavlbclimbthrustod total_ffnod ",totalavlbclimbthrustod," ",total_ffnod)
    if (total_ffnod != totalavlbclimbthrustod):
     total_ffnod = totalavlbclimbthrustod
     if (aircraft_config !=2):
      ffnod = total_ffnod/2.0 
    if (not inclb_or_dsct and flight_phase == "TOC" and not onward):
     (cont_lift_coeff,cont_drag_coeff,cont_Lift,cont_Drag,cont_ffnod)\
      = get_Climb_Descent.get_FNodclbdesc(cont_ftemperature,cont_fmach,cont_falt,\
      cont_fweight,cont_fgama,cont_faccelerate,aircraft_config)
     print ("NEXT LAND and cont_ffnod = ",cont_ffnod) 
    if (not inclb_or_dsct and flight_phase != "TOC"):
     cont_lift_coeff = 0
     cont_drag_coeff = 0
     cont_Lift = 0
     cont_Drag = 0
     cont_ffnod = 0
     print ("NEXT LAND and cont_ffnod = ",cont_ffnod) 
    #mach_dict_forprint[str(imach)] = ffnod
    print ("Done with get_Climb_Descent and falt & fmach = ",falt," ",fmach)
    print ("AndRousr lift_coeff drag_coeff Lift Drag ",lift_coeff," ",drag_coeff," ",Lift," ",Drag)
    print("THE POINT ID IS for this swing through log2Data is ",adict["PointID"])
    print ("ffnod ",ffnod)
#    
    mach_vals = []
    ffnod_master_list = []
    alt_cnt = 0
    mach_cnt = 0
#
    sensible_mach_list = []
    sensible_alt_list = []
#
    for tryalt in available_WFalts_machs_dict.keys():
     checkalt = float(tryalt)
     differ = abs(falt - checkalt)
     if (differ <= 5000.00):
      sensible_alt_list.append(tryalt)
    print ("sensible_alt_list = ",sensible_alt_list)
#
    for ialt in sensible_alt_list:
     for trymach in available_WFalts_machs_dict[ialt]:
      checkmach = float(trymach)/1000.00
      differ = abs(fmach - checkmach)
      if (differ <= 0.25): 
       sensible_mach_list.append(trymach)
     print("sensible_mach_list = ",sensible_mach_list)
#  
     print ("type of ialt is ",type(ialt))
     ffnod_master_list.append([])
#    mach_vals = available_WFalts_machs_dict[ialt]
#    for imach in mach_vals:
     for imach in sensible_mach_list:
      print ("ialt & imach ",ialt," ",imach," AND THE FIXID IS",adict["PointID"])
      ffnod_master_list[alt_cnt].append(ffnod)
      if (ialt == '41000'):
       print ("GARN alt_cnt ialt imach ",alt_cnt," ",ialt," ",imach," and ffnod = ",ffnod)
       print ("UGLUK AND THE FIX IS ",adict["PointID"])  
#
      fimach = float(imach)/1000.00
      print ("Here is what we have for mach_cnt = ",mach_cnt)
     #print (available_WFalts_machs_dict[ialt][imach])
      ffnodlist,wfvaluelist = get_lists_forWF.get_lists_forWF(ialt,imach,aircraft_config)
      print ("Ahead of the 1st BiParabolic call")
      print("ffnod ffnodlist ",ffnod," ",ffnodlist)
      print("wfvaluelist ",wfvaluelist)
      if (ffnodlist[0] !=  -888):
       fnoodle =[float(xv) for xv in ffnodlist]
       WFvalue_mach = Biparabolic1(fnoodle,wfvaluelist,ffnod,'fuelflow')
#      WFvalue_mach = 9988.00
       print ("After 1st Biparabolic call and WFvalue_mach = ",WFvalue_mach)
       if (imach == 700 and float(ialt) == falt):
        print ("FOR COMPARISION WITH BPS REPORTS:")
        floatalt = float(ialt)
        floatmach = float(imach)*0.001
        dlta = std_atm.alt2press_ratio(floatalt)
        print ("theta_ambient dlta floatalt floatmach ",theta_ambient," ",dlta," ",floatalt," ",floatmach)
        print ("fweight = ",fweight," ftemperature = ",ftemperature)
        print ("lift coefficient = ",lift_coeff," drag coefficient = ",drag_coeff," Lift = ",Lift," Drag = ",Drag) 
        print ("Thrust over delta = ",ffnod)
        total_theta = theta_ambient*(1 + 0.2*(floatmach**2))
        tpm = (1 + 0.2*(floatmach**2))
        total_dlta = dlta*(tpm**3.5)
        print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
        #Fuel_flow = WFvalue_mach * ((total_theta**0.62) * total_dlta)
        Fuel_flow = WFvalue_mach * ((total_theta**0.60) * total_dlta)
        print ("Fuel_flow = ",Fuel_flow," for floatalt = ",floatalt," floatmach = ",floatmach)
        print (" ")
       WFvalue_mach_list.append(WFvalue_mach)
       mach_list.append(fimach)
       mach_cnt = mach_cnt + 1
       print ("AT THE END OF THE INNER LOOP for ialt and imach ",ialt," ",imach)
       print ("  ") 
     print ("Ahead of the 2nd Biparabolic call and ialt = ",ialt)
     print ("AND HERE ARE THE FFNOD VALUES THAT CAME INTO PLAY")
     #for mykey in sorted(mach_dict_forprint):
     #print (mykey," ",mach_dict_forprint[mykey]) 
     print ("fmach mach_list ",fmach," ",mach_list)
     print ("WFvalue_mach_list ",WFvalue_mach_list)
     WFvalue_alt = Biparabolic1(mach_list,WFvalue_mach_list,fmach,'fuelflow')
     print ("After 2nd Biparabolic call WFvalue_alt = ",WFvalue_alt)
     #alt_dict_forprint[str(ialt)] = mach_dict_forprint  
     print ("STOMP THE YARD")
     print (" ")
#    WFvalue_alt = 10881.00
     WFvalue_alt_mach_list.append(WFvalue_alt)
     mach_list = []
     WFvalue_mach_list = []
     alt_list.append(ialt)
     alt_cnt = alt_cnt + 1
     mach_cnt = 0
  
    print (" ")
    print ("CLB JAGUAR TERRITORY")
    print ("alt_list = ",alt_list)
    falt_list =[float(xv) for xv in alt_list]
    falt_list.sort()
    WFvalue_alt_mach_list.sort()
    print ("Ahead of the 3rd BiParabolic call")
    print ("falt falt_list WFvalue_alt_mach_list ",falt," ",falt_list," ",WFvalue_alt_mach_list)
    print ("AND THE Temperature Weight Mach are ",ftemperature," ",fweight," ",fmach) 
    print ("AND FURTHERMORE ffnod_master_list = ",ffnod_master_list)
    #print ("ffnod_master_list[14] = ",ffnod_master_list[14])
    theWFvalue = Biparabolic1(falt_list,WFvalue_alt_mach_list,falt,'fuelflow')  
#   theWFvalue = 11999.0
    print (" ")  
    print ("FOR ",adict["PointID"])  
    print ("theWFvalue =  ",theWFvalue," Calc_roc = ",Calc_roc," fgama = ",fgama)
#
    theta_ambient = (ftemperature + 273.15)/288.15
    dlta = std_atm.alt2press_ratio(falt)
    print ("theta_ambient dlta fmach ",theta_ambient," ",dlta," ",fmach)
    total_theta = theta_ambient*(1 + 0.2*(fmach**2))
    tpm = (1 + 0.2*(fmach**2))
    total_dlta = dlta*(tpm**3.5)
    print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
    #Fuel_flow = theWFvalue * ((total_theta**0.62) * total_dlta) * 2.0
    Fuel_flow = theWFvalue * ((total_theta**0.60) * total_dlta) * engfac
    print ("Fuel_flow for this climb point is ",Fuel_flow)
#
    if (plan_fuel_flow != "NA" and Fuel_flow != 0):
     plan_Fuel_flow = plan_fuel_flow * 2.20462
     Delta_ff = plan_Fuel_flow - Fuel_flow
     percent_diff = Delta_ff/Fuel_flow
    else:
     plan_Fuel_flow = "NA"
     Delta_ff = "NA"
     percent_diff = "NA"
#
#@#$************************************************************
#Determine the value for max climb thrust over delta (FNCLBod)
    print ("MACABEE DETERMINING THE VALUE for max climb thrust over delta")
    fidisaF = IAPVEutils.get_disa_degF(alt,temperature) 
#
    FNCLBodvalue_disa_alt_list = []
#
    FNCLBmachlist = []
    FNCLBodlist = []
#
    FNCLBalts_list = []
    FNCLBod_list = []
    FNCLBalts_count = 0
#
    intdisa = []
    intalt = []
    for strdisa in available_FNCLBdisa_alts_dict.keys():
     intdisa.append(int(strdisa))
    intdisa.sort()
    print ("intdisa = ",intdisa)
#
# cycle through available_FNCLBdisa_alts_dict
    for idi in intdisa: 
     for stralt in available_FNCLBdisa_alts_dict[str(idi)]:
      intalt.append(int(stralt))
     intalt.sort()
     for iai in intalt:
      print ("idi & iai ",idi," ",iai," AND THE FIXID IS",adict["PointID"])
      (FNCLBmachlist,FNCLBodlist) = get_fromFNCLB.get_lists_fromFNCLBod(str(idi),str(iai),aircraft_config)
      print ("Ahead of the 1st BiParabolic or Linear call for determining FNCLBod")
      print("FNCLBmachlist ",FNCLBmachlist)
      print("FNCLBodlist ",FNCLBodlist)
      print("fmach = ",fmach)
      if (FNCLBmachlist[0] !=  -888):
       fmachle =[float(xv) for xv in FNCLBmachlist]
      #for a given disa and altitude apply Biparabolic to the applicable
      #FNCLBmachlist list and FNCLBodlist to get a value for FNCLBodvalue.
      #This value is then added to FNCLBod_alts_list 
       #FNCLBodvalue = Biparabolic1(fmachle,FNCLBodlist,fmach,'maxclimbthrust')
       FNCLBodvalue = Linear(fmachle,FNCLBodlist,fmach)
       print ("After 1st Biparabolic call and FNCLBodvalue = ",FNCLBodvalue)
       FNCLBod_list.append(FNCLBodvalue)
       FNCLBalts_list.append(iai)
       FNCLBalts_count = FNCLBalts_count + 1
       print ("AT THE END OF THE INNER LOOP for iai = ",iai)
       print ("  ") 
     print ("For idi = ",idi)
     print ("Ahead of the 2nd Biparabolic or Linear call for FNCLB and falt = ",falt)
     print ("FNCLBalts_list = ",FNCLBalts_list)
     print ("FNCLBod_list = ",FNCLBod_list)
     #FNCLBodvalue_disa = Biparabolic1(FNCLBalts_list,FNCLBod_list,falt,'maxclimbthrust')
     FNCLBodvalue_disa = Linear(FNCLBalts_list,FNCLBod_list,falt)
     #alt_dict_forprint[str(ialt)] = mach_dict_forprint  
     print ("")
     FNCLBodvalue_disa_alt_list.append(FNCLBodvalue_disa)
     print ("After 2nd Biparabolic call")
     print ("falt and fmach = ",falt," ",fmach," and strdisa = ",strdisa)
     print ("FNCLBodvalue_disa = ",FNCLBodvalue_disa)
     print ("FNCLBodvalue_disa_alt_list = ",FNCLBodvalue_disa_alt_list)
#
     FNCLBmachlist = []
     FNCLBodlist = []
     FNCLBalts_list = []
     FNCLBod_list = []
     FNCLBalts_count = 0
     intalt = []
#
    print("Finished the outer loop and FNCLBodvalue_disa = ",FNCLBodvalue_disa)
    print("intdisa = ",intdisa)
    print (" ")
    print ("FNCLB JAGUAR TERRITORY")
    ISAtempdev_list =[float(xv) for xv in intdisa]
    #ISAtempdev_list.sort()
    print ("Ahead of the 3rd BiParabolic or Linear call for FNCLBodvalue")
    print ("FNCLBodvalue_disa_alt_list = ",FNCLBodvalue_disa_alt_list)
    print ("ISAtempdev_list = ",ISAtempdev_list)
    print ("fidisa = ",fidisa)
    #theFNCLBodvalue = Biparabolic1(ISAtempdev_list,FNCLBodvalue_disa_alt_list,fidisa,'maxclimbthrust') 
    theFNCLBodvalue = Linear(ISAtempdev_list,FNCLBodvalue_disa_alt_list,fidisaF)   
    print ("FOR ",adict["PointID"])  
    print ("theFNCLBodvalue =  ",theFNCLBodvalue)
    #totalavlbclimbthrust = theFNCLBodvalue * 2.0
    totalavlbclimbthrust = theFNCLBodvalue * engfac
    print ("END OF FNCLB JAGUAR TERRITORY")
#@#$************************************************************
#Determine the value for idle fuel flow (WFIDL)
    if (flight_phase == "DSC"):
     Fuel_flow = get_fromWFIDL.getWFIDL_fuel(ftemperature,falt,\
                                            fmach,aircraft_config)
     theWFvalue = " "
     if (aircraft_config == 2):
      APUfuelflow = get_APUwf.get_APUwf(fmach,falt,aircraft_config)
      print ("APUfuelflow = ",APUfuelflow)
      Fuel_flow = Fuel_flow + APUfuelflow 
#
#Erik's old way
#     theta_ambient = (ftemperature + 273.15)/288.15
#     dlta = std_atm.alt2press_ratio(falt)
#     print ("theta_ambient dlta fmach ",theta_ambient," ",dlta," ",fmach)
#     total_theta = theta_ambient*(1 + 0.2*(fmach**2))
#     tpm = (1 + 0.2*(fmach**2))
#     total_dlta = dlta*(tpm**3.5)
#     print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
#     Fuel_flow = Fuel_flow * ((total_theta**0.60) * total_dlta)
#
     totalavlbclimbthrust = " " 
#@#$************************************************************
#@#$************************************************************
#
    if (flight_phase == "CLB"):
     print ("theWFvalue = ",theWFvalue," Calc_roc = ",Calc_roc," fgama = ",fgama)
#
     theta_ambient = (ftemperature + 273.15)/288.15
     dlta = std_atm.alt2press_ratio(falt)
     print ("theta_ambient dlta fmach ",theta_ambient," ",dlta," ",fmach)
     total_theta = theta_ambient*(1 + 0.2*(fmach**2))
     tpm = (1 + 0.2*(fmach**2))
     total_dlta = dlta*(tpm**3.5)
     print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
     #Fuel_flow = theWFvalue * ((total_theta**0.62) * total_dlta) * 2.0
     #Fuel_flow = theWFvalue * ((total_theta**0.60) * total_dlta) * 2.0
     Fuel_flow = theWFvalue * ((total_theta**0.60) * total_dlta) * engfac
     if (aircraft_config == 2):
      APUfuelflow = get_APUwf.get_APUwf(fmach,falt,aircraft_config)
      print ("APUfuelflow = ",APUfuelflow)
      Fuel_flow = Fuel_flow + APUfuelflow 
     #TotalT_TotalD = (total_theta**0.62) * total_dlta
     TotalT_TotalD = (total_theta**0.60) * total_dlta
#
     print ("FOR THE ID ",pointid," AT ALTITUDE ",falt)
     print ("Fuel_flow = ",Fuel_flow,"plan_fuel_flow = ",plan_fuel_flow,"plan_Fuel_flow = ",plan_Fuel_flow)
#
#    if sys.version_info >= (3,0,0):
#     outfile = open('jaguar.csv','w',newline='')
#    else:
#     outfile = open('jaguar.csv','wb')
#    the_lift_coeff,the_drag_coeff,the_Lift,the_Drag,the_ffnod) = get_FNod.get_FNod(ftemperature,fmach,falt,fweight,'cd')
#    the_total_ffnod = the_ffnod * 2.0
#
    #the_total_ffnod = ffnod * 2.0 
    the_total_ffnod = ffnod * engfac
    ACC = faccelerate/G
    gradient = math.tan(fgama)
    #row = [pointid,flight_phase,theWFvalue,Fuel_flow,ftemperature,fweight,fmach,ftaskt,ftas,ftime,falt,lift_coeff,\
    #      drag_coeff,Lift,Drag,ffnod,the_total_ffnod,totalavlbclimbthrust,plan_Fuel_flow,Delta_ff,\
    #      percent_diff,Calc_roc,FPC_roc,faccelerate,ACC,fgama,gradient]
    try:
     fburnrate = fburnrate*(-1.0)
     contest_plan_Fuel_flow = (snapshot_planfuelflowlist[-2]*2.20462)
     print ("CONTEST TIME fburnrate = ",fburnrate,"contest_plan_Fuel_flow = ",contest_plan_Fuel_flow)
     print ("IAPVE computed Fuel_flow = ",Fuel_flow)
     Delta_ff_fromburnrate = fburnrate-Fuel_flow
     Delta_ff_fromfp = contest_plan_Fuel_flow - Fuel_flow
     Delta_ff_contestfp_vs_fburnrate = contest_plan_Fuel_flow - fburnrate
#fburnrate initially is the result of what the IAPVE calculates from its numerical differentiation 
#the final value of fburnrate is what goes in Col C under the header FP_fuel_flow
     if (abs(Delta_ff_fromburnrate) < abs(Delta_ff_fromfp) and abs(Delta_ff_fromburnrate) < abs(Delta_ff_contestfp_vs_fburnrate)):
      print ("FROMburnrate WINS fburnrate stays as is and Fuel_flow stays as is")
      Delta_ff = Delta_ff_fromburnrate
     if (abs(Delta_ff_fromfp) < abs(Delta_ff_fromburnrate) and abs(Delta_ff_fromfp) < abs(Delta_ff_contestfp_vs_fburnrate)):   
      print ("FROMFP WINS fburnrate set to contest_plan_Fuel_flow and Fuel_flow stays as is")
      Delta_ff = Delta_ff_fromfp
      fburnrate = contest_plan_Fuel_flow
     if (abs(Delta_ff_contestfp_vs_fburnrate) < abs(Delta_ff_fromburnrate) and abs(Delta_ff_contestfp_vs_fburnrate) < abs(Delta_ff_fromfp)):   
      print ("FROMFP WINS based on comparison with fburnrate so Fuel flow is set to that fburnrate and then fburnrate is set to contest_plan_Fuel_flow")
      Delta_ff = Delta_ff_contestfp_vs_fburnrate
     #first set Fuel_flow to the current fburnrate
      Fuel_flow = fburnrate
     #now change fburnrate to equal contest_plan_Fuel_flow 
      fburnrate = contest_plan_Fuel_flow
     percent_diff = Delta_ff/Fuel_flow
     print ("RESULTS ARE Delta_ff fburnrate Fuel_flow contest_plan_Fuel_flow ",Delta_ff," ",\
            fburnrate," ",Fuel_flow," ",contest_plan_Fuel_flow," and percent_diff = ",percent_diff)
     #if (flight_phase == "DSC" and adict["Fuelflow"] != "NA"):
      #fburnrate = adict["Fuelflow"] * 2.20462
      #Delta_ff = fburnrate - Fuel_flow
      #percent_diff = Delta_ff/Fuel_flow 
    except:
     Delta_ff = " "
     percent_diff = " "
    CLmax = BuffetBoundary.CL_to_buffet(fmach,aircraft_config) 
    fidisa = adict["DevfromISA"]
    windcmp = adict["WindCmp"]
    if (not type(percent_diff) is str):
     pdiff = abs(percent_diff)
    else:
     pdiff = 0
    fcaskt = airspeed.tas2cas(ftaskt,falt,ftemperature)
    print ("BFR CARLCAT useable flight_phase onward pdiff fcaskt ",useable," ",flight_phase,\
           " ",onward," ",pdiff," ",fcaskt)    
    if (useable and (flight_phase != "TOC" or (flight_phase == "TOC" and not onward))\
        and falt >= 3000 and pdiff <= 1.03):
    #because it is climb we don't report the validation engine calculation of ffnod and the_total_ffnod
     the_total_ffnod = " "
     ffnod = " "  
     print ("CARLCAT writing a row")
     if (flight_phase == "TOC+" and (fburnrate == " " or Calc_roc == " ")):
      print ("not going to write this row")
     elif (flight_phase == "---"):
      row = [pointid,flight_phase," ",ftemperature,fidisa,windcmp,\
            fweight,fmach,fcaskt,ftaskt,ftas,ftime,falt,\
            maxcruisealt,rocalt,buffalt,lift_coeff," ",\
            CLmax," "," "," "," "," "," "," "," "," "," ",\
            " "," "," "," "," "," "," "," "," ",\
            " "," ",run_num]  
      writer.writerow(row)  
     else:
      if (flight_phase == "DSC"):
       total_theta = " "
       total_dlta = " "
       TotalT_TotalD = " "
      row = [pointid,flight_phase,fburnrate,ftemperature,fidisa,windcmp,\
            fweight,fmach,fcaskt,ftaskt,ftas,ftime,falt,\
            maxcruisealt,rocalt,buffalt,lift_coeff," ",\
            CLmax,drag_coeff,Lift,Drag,ffnod,the_total_ffnod,totalavlbclimbthrust,\
            " ",FPCavthrustod,theWFvalue,Fuel_flow,Delta_ff,\
            percent_diff,total_theta,total_dlta,TotalT_TotalD,Calc_roc,FPC_roc,faccelerate,ACC,\
            fgama,gradient,run_num]  
      writer.writerow(row)
#
    print (" ")
    alt_list = []
    falt_list = []
    ffnod_master_list = []
    WFvalue_alt_mach_list = []
    alt_cnt = 0
#reset the lists
   print ("RESETING SNAP_SHOT LISTS and snapshot_altcnt = ",snapshot_altcnt)
   if (snapshot_altcnt == 2):
    snapshot_altlist.pop(0)
    snapshot_roclist.pop(0)
    snapshot_distancelist.pop(0)
    snapshot_airdistancelist.pop(0)
    snapshot_timelist.pop(0)
    snapshot_taslist.pop(0)
    snapshot_tasktlist.pop(0)
    snapshot_temperaturelist.pop(0)
    snapshot_weightlist.pop(0)
    snapshot_machlist.pop(0)
    snapshot_pointidlist.pop(0)
    snapshot_flightphaselist.pop(0)
    snapshot_planfuelflowlist.pop(0)
    snapshot_FPCavthrustodlist.pop(0)
    snapshot_maxcruisealtlist.pop(0)
    snapshot_rocaltlist.pop(0)
    snapshot_buffaltlist.pop(0)
   if (snapshot_altcnt < 2):
    snapshot_altcnt = snapshot_altcnt + 1
#
#  !!!! end calculating climb or descent parameters for a 2 or 3 member altitude set !!!! 
#
  else:
   print("not enough altitudes yet")
   if (adict["FixClimb"]):
    inclb_or_dsct = False
    changedto = "WYPT"
    print ("changedto = ",changedto)
   snapshot_altcnt = snapshot_altcnt + 1
#
  print (" ")
  alt_list = []
  falt_list = []
  ffnod_master_list = []
  WFvalue_alt_mach_list = []
  alt_cnt = 0
  WFvalue_mach_list = []
  alt_dict_forprint = {}
  mach_dict_forprint = {}
# $$$ END PROCESSING A SNAPSHOT ITEM $$$
 else:
# ^^IT WAS EITHER A USEABLE NON SNAPSHOT ITEM IN WHICH CASE RESET SNAPSHOT COUNT AND SNAPSHOT LISTS ^^^
# ^^OR IT WAS AN UNUSEABLE SNAPSHOT ITEM IN WHICH CASE DON'T DO THE RESETS ^^^
  if (adict["Useable"]):
   snapshot_altcnt = 0
   snapshot_altlist = []
   snapshot_roclist = []
   snapshot_distancelist = []
   snapshot_airdistancelist = []
   snapshot_timelist = []
   snapshot_taslist = []
   snapshot_tasktlist = []
   snapshot_temperaturelist = []
   snapshot_weightlist = []
   snapshot_machlist = []
   snapshot_pointidlist = []
   snapshot_flightphaselist = []
   snapshot_planfuelflowlist = []
   snapshot_FPCavthrustodlist = []
   snapshot_maxcruisealtlist = []
   snapshot_rocaltlist = []
   snapshot_buffaltlist = []
#
#***********END OF SNAPSHOT REALM**************
# 
#SWATTER
 print ("SWATER ID ",adict["PointID"]," aircraft_config = ",aircraft_config)
 print ("SWATTER ",adict["SetType"]," ",lookaheadsnapshot," ",changedto," ",inclb_or_dsct)
 print ("adict[SnapProfile] = ",adict["SnapProfile"])
 if ((adict["SetType"] == 'FIX' or adict["SetType"] == 'VOR' or not lookaheadsnapshot)\
      and (changedto != "NA" or not inclb_or_dsct) and\
     (adict["SnapProfile"] != "SEGWAY") and (adict["SetType"] != "APT")):
  print ("THE USEABLE FIX FOR LEVEL FLIGHT IS ",adict["PointID"]) 
  fidisa = adict["DevfromISA"]
  windcmp = adict["WindCmp"]
  hitcruise = True 
  snapshotP = adict["SnapProfile"]
  ftemperature = adict["Temperature"]
  theta_ambient = adict["ThetaAmbient"]
  ftime = adict["Time"]
  if (changedto != "NA"):
   flight_phase = changedto
  else:
   if (snapshotP != "NA"):
    flight_phase = snapshotP
   else:
    flight_phase = "CRU"
  ftas = (adict["TAS"]*101.268591)
# adict["TAS"] is in kts so we converted it so that ftas is in ft/min
  ftaskt = adict["TAS"]
  fweight = adict["Weight"]*2.20462
  print ("adict[Altitude] = ",adict["Altitude"]," and its type is ",type(adict["Altitude"]))
  falt = adict["Altitude"]
  fmach = adict["Mach"]
# BOC TOD special treatment
  snapshotP = adict["SnapProfile"]
  print ("THE PROFILE IS ",snapshotP," and fmach = ",fmach)
  snapshot_FPCavthrustodlist.append(" ")
  if (snapshotP == "BOC" or snapshotP == "TOD"):
   snapshot_altcnt = 1
   if (snapshotP == "BOC"):
    flight_phase = "BOC"
   if (snapshotP == "TOD"):
    flight_phase = "TOD"
   froc = adict["VertSpeed"]
   snapshot_roclist.append(froc)
   snapshot_altlist.append(falt)
   fdist = adict["Distance"]
   snapshot_distancelist.append(fdist)
   fairdist = adict["AirDistance"]
   snapshot_airdistancelist.append(fairdist)
#FOR CONSISTENCY WE MAKE SURE THAT
#TIME UNITS ARE IN MINUTES
#SPEED UNITS ARE IN FT/MIN
   ftime = adict["Time"]
#adict["Time"] is in minutes so no conversion needed
   snapshot_timelist.append(ftime)
   ftas_ftpermin = (adict["TAS"]*101.268591)
#adict["TAS"] is in kts so we converted it to ft/min for use in climb/descent calculations
   snapshot_taslist.append(ftas_ftpermin)
   ftaskt = adict["TAS"]
   snapshot_tasktlist.append(ftaskt)
#snapshot_tasktlist is for debug printout only
  #print ("GROWL snapshot_taslist ",snapshot_taslist)
   snapshot_temperaturelist.append(ftemperature)
   snapshot_weightlist.append(fweight)
   snapshot_machlist.append(fmach)
   snapshot_pointidlist.append(adict["PointID"])
   snapshot_flightphaselist.append(flight_phase)
   snapshot_maxcruisealtlist.append(adict["Maxcruisealt"])
   snapshot_rocaltlist.append(adict["Rocalt"])
   snapshot_buffaltlist.append(adict["Buffalt"])
#
# END BOC TOD special treatment
  print ("AND THE Temperature Wgt Alt Mach ftaskt are ",ftemperature," ",fweight," ",falt," ",fmach," ",ftaskt) 
  mdiff = 888888.00
  adiff = 888888.00
#Here is where change will come to those who have no fear 
#FN/dlta is needed only for fmach, which is the actual mach the aircraft is flying
#at the fix.   FN/dlta is not needed at any other mach - it matters not whether
#fmach's FN/dtla can't support level flight at certain of the other machs.
#
  if (cont_proc):
   print ("HERE IN CONT_PROC")
   ftemperature = cont_ftemperature
   falt = cont_falt
   fmach = cont_fmach
   ftaskt = cont_ftaskt
   ftas = cont_ftas
   fweight = cont_fweight
   faccelerate = cont_faccelerate
   fgama = cont_fgama
   FPC_roc = cont_FPC_roc
   Calc_roc = cont_Calc_roc
   if (onward):
    (lift_coeff,drag_coeff,Lift,Drag,ffnod) = get_FNod.get_FNod(ftemperature,\
     fmach,falt,fweight,fcl,aircraft_config)
    print("ONWARD ffnod = ",ffnod)
   else:
    lift_coeff = cont_lift_coeff
    drag_coeff = cont_drag_coeff
    Lift = cont_Lift
    Drag = cont_Drag
    ffnod = cont_ffnod
    print("NOT ONWARD ffnod = ",ffnod)
  else:
   if (snapshotP == "TOC" and engfac != 1.0):
    ftaskt = adict["Onwardtas"]
    theta_ambient = (ftemperature + 273.15)/288.15
    fmach = ftaskt/(661.4786 * (theta_ambient ** (0.5)))
    print ("ALMOND ftas theta_ambient ftemperature fmach ",ftas," ",theta_ambient," ",\
            ftemperature," ",fmach)
    if (ftaskt > 0 and fmach > 0):
     (lift_coeff,drag_coeff,Lift,Drag,ffnod) = get_FNod.get_FNod(ftemperature,\
      fmach,falt,fweight,fcl,aircraft_config)
    else:
      lift_coeff = " "
      drag_coeff = " "
      Lift = " "
      Drag = " "
      ffnod = 0
# having to reset ftaskt and fmach because adict["Onwardtas"] is 0 even though the point is determined to be TOC 
      ftaskt = adict["TAS"]
      fmach = adict["Mach"]
   else:
    print ("LENTL ftaskt theta_ambient ftemperature ",ftaskt," ",theta_ambient," ",\
            ftemperature)
    print ("fmach fweight falt ftas ",fmach," ",fweight," ",falt," ",ftas)
    if (ftas > 0 and fmach > 0):
     (lift_coeff,drag_coeff,Lift,Drag,ffnod) = get_FNod.get_FNod(ftemperature,\
      fmach,falt,fweight,fcl,aircraft_config)
    else:
      lift_coeff = " "
      drag_coeff = " "
      Lift = " "
      Drag = " "
      ffnod = 0
    if (snapshotP == "BOC"):
    #if (snapshotP == "BOC" or ((snapshotP == "CRU" or snapshotP == "TOD" or snapshotP == "TOC") and engfac == 1)):
      try:
       fn_newton = adict["Drag"]
       print ("Warthog BOC point fn_newton = ",fn_newton,"dlta = ",dlta)
       ffnod = (fn_newton*0.22481)/(engfac*dlta)
       the_warthogffnod = ffnod
       print ("Pecary BOC point ffnod in lb = ",ffnod," ",the_warthogffnod)
      except:
       print ("No value for adict[Drag] when needed for BOC point")
   print ("ALMOND JOY lift_coeff drag_coeff ffnod falt fmach ",lift_coeff," ",drag_coeff,\
          " ",ffnod," ",falt," ",fmach)
#  
  mach_vals = []
  ffnod_master_list = []
  alt_cnt = 0
  mach_cnt = 0
#
  sensible_mach_list = []
  sensible_alt_list = []
#
  for tryalt in available_WFalts_machs_dict.keys():
   checkalt = float(tryalt)
   differ = abs(falt - checkalt)
   if (differ <= 10000.00):
    sensible_alt_list.append(tryalt)
  print ("sensible_alt_list = ",sensible_alt_list)
# for ialt in available_WFalts_machs_dict.keys():
#
  for ialt in sensible_alt_list:
   if (ffnod == 0):
    print("BREAK THE FOR LOOP RIGHT HERE SINCE ffnod = 0")
    break
   for trymach in available_WFalts_machs_dict[ialt]:
    checkmach = float(trymach)/1000.00
    differ = abs(fmach - checkmach)
    if (differ <= 0.25): 
     sensible_mach_list.append(trymach)
   print("sensible_mach_list = ",sensible_mach_list)
#  
   print ("type of ialt is ",type(ialt))
   ffnod_master_list.append([])
#  mach_vals = available_WFalts_machs_dict[ialt]
#  for imach in mach_vals:
   for imach in sensible_mach_list:
    print ("ialt & imach ",ialt," ",imach," AND THE FIXID IS",adict["PointID"])
    fimach = float(imach)/1000.00
    mach_list.append(fimach)
    print ("Here is what we have for mach_cnt = ",mach_cnt)
#   print (available_WFalts_machs_dict[ialt][mach_cnt])
    print ("Ahead of get_lists_forWF call ",aircraft_config, sensible_mach_list[mach_cnt])
    ffnodlist,wfvaluelist = get_lists_forWF.get_lists_forWF(ialt,imach,aircraft_config)
    print ("Ahead of the 1st BiParabolic call")
    print("ffnod ffnodlist ",ffnod," ",ffnodlist)
    print("wfvaluelist ",wfvaluelist)
    fnoodle =[float(xv) for xv in ffnodlist]
    WFvalue_mach = Biparabolic1(fnoodle,wfvaluelist,ffnod,'fuelflow')
    #WFvalue_mach is the fuel flow value for the current combination of ffnod, ialt, and imach
    print ("After 1st Biparabolic call and WFvalue_mach = ",WFvalue_mach)
#
    #Now get FNswitch which is the value of ffnod below which 9th stage bleed fuel must be added
    FNswitch = get_lists_forWF.getFNswitch_forCORF(ialt,imach)
    #Now get bleed fuel correction list
    CORF_value_mach = 0.0
    print ("9Th Stage Bleed Check ffnod = ",ffnod," and FNswitch = ",FNswitch)
    if (ffnod <= FNswitch):
     print ("Must calculate 9th stage bleed fuel")
     CORF_ffnodlist,CORF_valuelist = get_lists_forWF.get_lists_forCORF(ialt,imach)
     if (CORF_valuelist == [0]):
      CORF_value_mach == 0.0
     else:
      print ("Ahead of the BiParabolic call for bleed fuel")
      print("ffnod CORF_ffnodlist ",ffnod," ",CORF_ffnodlist)
      print("CORF_valuelist ",CORF_valuelist)
      fnbldf =[float(xv) for xv in CORF_ffnodlist]
      CORF_value_mach = Biparabolic1(fnbldf,CORF_valuelist,ffnod,'fuelflow')
     WFvalue_mach = WFvalue_mach + CORF_value_mach
    print ("CORF_value_mach = ",CORF_value_mach," WFvalue_mach= ",WFvalue_mach)
#
    if (imach == 700 and float(ialt) == falt):
       print ("FOR COMPARISION WITH BPS REPORTS:")
       floatalt = float(ialt)
       floatmach = float(imach)*0.001
       dlta = std_atm.alt2press_ratio(floatalt)
       print ("theta_ambient dlta floatalt floatmach ",theta_ambient," ",dlta," ",floatalt," ",floatmach)
       print ("fweight = ",fweight," ftemperature = ",ftemperature)
       print ("lift coefficient = ",lift_coeff," drag coefficient = ",drag_coeff," Lift = ",Lift," Drag = ",Drag) 
       print ("Thrust over delta = ",ffnod)
       total_theta = theta_ambient*(1 + 0.2*(floatmach**2))
       tpm = (1 + 0.2*(floatmach**2))
       total_dlta = dlta*(tpm**3.5)
       print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
       #Fuel_flow = WFvalue_mach * ((total_theta**0.62) * total_dlta)
       #Fuel_flow = WFvalue_mach * ((total_theta**0.60) * total_dlta) * 2.0
       Fuel_flow = WFvalue_mach * ((total_theta**0.60) * total_dlta) * engfac
       if (aircraft_config == 2):
        APUfuelflow = get_APUwf.get_APUwf(fmach,falt,aircraft_config)
        print ("APUfuelflow = ",APUfuelflow)
        Fuel_flow = Fuel_flow + APUfuelflow 
       print ("Fuel_flow = ",Fuel_flow," for floatalt = ",floatalt," floatmach = ",floatmach)
       print (" ")
    WFvalue_mach_list.append(WFvalue_mach)
    mach_cnt = mach_cnt + 1
    print ("AT THE END OF THE INNER LOOP for ialt and imach ",ialt," ",imach)
    print ("  ") 
#   
   print ("Ahead of the 2nd Biparabolic call and ialt = ",ialt)
   print ("AND HERE ARE THE FFNOD VALUES THAT CAME INTO PLAY")
   for mykey in sorted(mach_dict_forprint):
    print (mykey," ",mach_dict_forprint[mykey]) 
   print ("fmach mach_list ",fmach," ",mach_list)
   print ("WFvalue_mach_list ",WFvalue_mach_list)
   if (len(mach_list) > 0):
    WFvalue_alt = Biparabolic1(mach_list,WFvalue_mach_list,fmach,'fuelflow')
   else:
    WFvalue_alt = 0
   print ("After 2nd Biparabolic call WFvalue_alt = ",WFvalue_alt)
   alt_dict_forprint[str(ialt)] = mach_dict_forprint  
   print ("STOMP THE YARD")
   print (" ")
#  WFvalue_alt = 10881.00
   WFvalue_alt_mach_list.append(WFvalue_alt)
   temp_dict = {float(ialt):WFvalue_alt}
   WFvalue_alt_mach_dict.update(temp_dict)
   mach_list = []
   sensible_mach_list = []
   WFvalue_mach_list = []
   alt_list.append(ialt)
   alt_cnt = alt_cnt + 1
   mach_cnt = 0
#  
  print (" ")
  print ("JAGUAR TERRITORY and ffnod = ",ffnod)
  print ("alt_list = ",alt_list)
  falt_list =[float(xv) for xv in alt_list]
  falt_list.sort()
  #WFvalue_alt_mach_list.sort()
  WFvalue_alt_mach_list = []
  if (ffnod > 0):
   for av in falt_list:
    WFvalue_alt_mach_list.append(WFvalue_alt_mach_dict[av])
  print ("Ahead of the 3rd BiParabolic call")
  print ("falt falt_list WFvalue_alt_mach_list ",falt," ",falt_list," ",WFvalue_alt_mach_list)
  print ("AND THE Temperature Weight Mach are ",ftemperature," ",fweight," ",fmach) 
  print ("AND FURTHERMORE ffnod_master_list = ",ffnod_master_list)
 #print ("ffnod_master_list[14] = ",ffnod_master_list[14])
  if (ffnod > 0):
   theWFvalue = Biparabolic1(falt_list,WFvalue_alt_mach_list,falt,'fuelflow_3rd')  
  else:
   theWFvalue = 0
 #theWFvalue = 11999.0
  print (" ")  
  print ("FOR ",adict["PointID"])  
  print ("theWFvalue =  ",theWFvalue)
#
  theta_ambient = (ftemperature + 273.15)/288.15
  dlta = std_atm.alt2press_ratio(falt)
  print ("theta_ambient dlta fmach ",theta_ambient," ",dlta," ",fmach)
  total_theta = theta_ambient*(1 + 0.2*(fmach**2))
  tpm = (1 + 0.2*(fmach**2))
  total_dlta = dlta*(tpm**3.5)
  print ("dlta total_theta tpm total_dlta ",dlta," ",total_theta," ",tpm," ",total_dlta)
  #Fuel_flow = theWFvalue * ((total_theta**0.60) * total_dlta) * 2.0
  Fuel_flow = theWFvalue * ((total_theta**0.60) * total_dlta) * engfac
  theWFvalue_factor = ((total_theta**0.60) * total_dlta)
  print ("SWATTER Fuel_flow bfr call to get_APUwf = ",Fuel_flow," theWFvalue_factor = ",theWFvalue_factor) 
  if (aircraft_config == 2):
   APUfuelflow = get_APUwf.get_APUwf(fmach,falt,aircraft_config)
   print ("APUfuelflow = ",APUfuelflow)
   Fuel_flow = Fuel_flow + APUfuelflow 
#
  print ("Fuel_flow = ",Fuel_flow)
  if (adict["Fuelflow"] != "NA" and adict["SnapProfile"] != "TOC"):
   plan_Fuel_flow = adict["Fuelflow"] * 2.20462
  else:
   plan_Fuel_flow = "NA"
  print ("PRE PESTERPAD adict[SnapProfile] adict[Onwardfuelflow] onward ",\
          adict["SnapProfile"]," ",adict["Onwardfuelflow"]," ",onward)
  if ((adict["SnapProfile"] == "TOC") and adict["Onwardfuelflow"] > 0 and onward):
   plan_Fuel_flow = adict["Onwardfuelflow"] * 2.20462
   print ("PESTERPAD plan_Fuel_flow = ",plan_Fuel_flow)
  if (changedto != "NA"):
   theWFvalue = "NA"
   Fuel_flow = "NA"
   plan_Fuel_flow = "NA"
  if (Fuel_flow == "NA" or plan_Fuel_flow == "NA"):
   Delta_ff = "NA"
   percent_diff = "NA"
  else:
   Delta_ff = plan_Fuel_flow - Fuel_flow
   try:
    percent_diff = Delta_ff/Fuel_flow
   except:
    percent_diff = " "
#check to see if 1.0547 mutliplier applies
  #if (Delta_ff != "NA" and (flight_phase == "CRU" or flight_phase == "TOD")):
   #if (abs((plan_Fuel_flow*1.0547)-Fuel_flow) < abs(Delta_ff)):
    #plan_Fuel_flow = plan_Fuel_flow*1.0540
    #Delta_ff = plan_Fuel_flow - Fuel_flow
    #percent_diff = Delta_ff/Fuel_flow
    #print ("Niels Bohr 1.0547 factor applied thus plan_Fuel_flow Delta_ff percent_diff ",\
            #plan_Fuel_flow," ",Delta_ff," ",percent_diff)
   #print(adict["Fuelflow"]," ",plan_Fuel_flow," ",Delta_ff) 
#check to see if subtracting 40 at TOD helps at TOD or CRU
  #if (Delta_ff != "NA" and (flight_phase == "TOD" or flight_phase == "CRU")):
   #if (abs((plan_Fuel_flow-40.0)-Fuel_flow) < abs(Delta_ff)):
    #plan_Fuel_flow = plan_Fuel_flow-40.0
    #Delta_ff = plan_Fuel_flow - Fuel_flow
    #percent_diff = Delta_ff/Fuel_flow
    #print ("Pauli -40.0 applied thus plan_Fuel_flow Delta_ff percent_diff ",\
            #plan_Fuel_flow," ",Delta_ff," ",percent_diff)
   #print(adict["Fuelflow"]," ",plan_Fuel_flow," ",Delta_ff) 
#check to see if addting 40 at TOD helps at TOD or CRU
  #if (Delta_ff != "NA" and (flight_phase == "TOD" or flight_phase == "CRU")):
   #if (abs((plan_Fuel_flow+40.0)-Fuel_flow) < abs(Delta_ff)):
    #plan_Fuel_flow = plan_Fuel_flow+40.0
    #Delta_ff = plan_Fuel_flow - Fuel_flow
    #percent_diff = Delta_ff/Fuel_flow
    #print ("Feyman +40.0 applied thus plan_Fuel_flow Delta_ff percent_diff ",\
            #plan_Fuel_flow," ",Delta_ff," ",percent_diff)
  print("below are adict[Fuel_flow] plan_Fuel_flow Delta_ff")
  print(adict["Fuelflow"]," ",plan_Fuel_flow," ",Delta_ff) 
#
# if sys.version_info >= (3,0,0):
#  outfile = open('jaguar.csv','w',newline='')
# else:
#  outfile = open('jaguar.csv','wb')
  print ("CONFLUENCE ZONE cont_proc onward falt fmach ",cont_proc," ",onward," ",falt," ",fmach)
  if (cont_proc and not onward):
   (the_lift_coeff,the_drag_coeff,the_Lift,the_Drag,the_ffnod) =\
    get_Climb_Descent.get_FNodclbdesc(ftemperature,fmach,falt,\
    fweight,fgama,faccelerate,aircraft_config)
   print ("ES IN cont_PROC LAND and the_ffnod = ",the_ffnod," the_lift_coeff = ",the_lift_coeff)
  else:
   if (falt > 0 and fmach > 0):
    (the_lift_coeff,the_drag_coeff,the_Lift,the_Drag,the_ffnod) =\
    get_FNod.get_FNod(ftemperature,fmach,falt,fweight,fcl,aircraft_config)
    print ("HARDHOME the_ffnod = ",the_ffnod,"the_lift_coeff = ",the_lift_coeff) 
   else:
    the_lift_coeff = " "
    the_drag_coeff = " "
    the_Lift = " "
    the_Drag = " "
    the_ffnod = 0
   if (snapshotP == "BOC"):
    try:
     #ffnod = adict["Drag"]/dlt
     #print ("Final Warthog BOC 1LE point ffnod in newton = ",ffnod)
     #ffnod = ffnod*0.22481
     #print ("Final Pecary BOC 1LE point ffnod in lb = ",ffnod)
     the_ffnod = the_warthogffnod
    except:
     print ("Final No value for adict[Drag] when needed for BOC 1LE point")
  #the_total_ffnod = the_ffnod * 2.0
  the_total_ffnod = the_ffnod * engfac
  nada = 0   
  if (theWFvalue == "NA"):
   theWFvalue = " "
  if (Fuel_flow == "NA"):
   Fuel_flow = " "
  if (plan_Fuel_flow == "NA"):
   plan_Fuel_flow = " "
  if (Delta_ff == "NA"):
   Delta_ff = " "
  if (percent_diff == "NA"):
   percent_diff = " "
  if (cont_proc):
   #maxthrust = get_fromFNCLB.getFNCLBod(cont_ftemperature,cont_falt,cont_fmach,aircraft_config)
   maxthrust = " "
   maxcruisealt = cont_maxcruisealt
   rocalt = cont_rocalt
   buffalt = cont_buffalt
   if (onward):
    totalavlbclimbthrust = get_FNodCLB.maxclimbthrust(cont_falt,cont_fmach,cont_ftemperature,aircraft_config)
    print ("JEBSTER pointid totalavlbclimbthrust ",pointid," ",totalavlbclimbthrust)
    FPCavthrustod = " "
    faccelerate = 0
    ACC = 0
    gradient = 0
    Calc_roc = 0
    FPC_roc = 0
    fgama = 0
    if (flight_phase == "TOC"):
     flight_phase = flight_phase + "+"
   else:
    #totalavlbclimbthrust = get_fromFNCLB.getFNCRUod(cont_ftemperature,cont_falt,cont_fmach,aircraft_config)
    totalavlbclimbthrust = get_FNodCLB.maxclimbthrust(cont_falt,cont_fmach,cont_ftemperature,aircraft_config)
    FPCavthrustod = cont_FPCavthrustod
    ACC = faccelerate/G
    gradient = math.tan(fgama)
    Calc_roc = cont_Calc_roc
    FPC_roc = cont_FPC_roc
    flight_phase = "TOC-"
   fburnrate = cont_burnrate*2.20462
   Delta_ff = fburnrate - Fuel_flow
   try:
    percent_diff = Delta_ff/Fuel_flow
   except:
    percent_diff = " "
  else:
   if (aircraft_config != 2):
    maxthrust = get_fromFNCRU.getFNCRUod(ftemperature,falt,fmach,aircraft_config)
   else:
    maxthrust = getFNCNT.getFNCNT(falt,fmach,ftemperature,aircraft_config)
    print("For King and Country at ",falt," ",fmach," ",ftemperature," maxthrust = ",maxthrust)
   FPCavthrustod = " "
  CLmax = BuffetBoundary.CL_to_buffet(fmach,aircraft_config)
  CL_15degbank = lift_coeff*1.035 
  #CL_40degbank = lift_coeff*1.305
  (dummy1,drag_coeff,dummy2,Drag_15degbank,ffnod_15degbank) = get_FNod.get_FNod(ftemperature,\
   fmach,falt,fweight,CL_15degbank,aircraft_config)
  total_ffnod_15degbank = ffnod_15degbank * engfac
  fcaskt = airspeed.tas2cas(ftaskt,falt,ftemperature)
  print ("HIPPO total_ffnod_15degbank fcaskt engfac the_lift_coeff lift_coeff ",\
          total_ffnod_15degbank," ",fcaskt," ",engfac," ",the_lift_coeff," ",lift_coeff)
  if (cont_proc):
   if (include_TOC):
    if (Fuel_flow == 0):
     Fuel_flow = " "
     theWFvalue = " "
     fweight = " "
     fmach = " "
     ftaskt = " "
     ftas = " "
     ftime = " "
     lift_coeff = " "
     CLmax = " "
     drag_coeff = " "
     Lift = " "
     Drag = " "
     ffnod = " "
     the_total_ffnod = " "
     totalavlbclimbthrust = " "
     FPCavthrustod = " "
     fburnrate = " "
     Delta_ff = " "
     percent_diff = " "
     Calc_roc = " "
     FPC_roc = " "
     faccelerate = " "
     ACC = " "
     fgama = " "
     gradient = " "
    #TotalT_TotalD = (total_theta**0.62) * total_dlta
    TotalT_TotalD = (total_theta**0.60) * total_dlta
    if ((flight_phase == "TOC+" and (fburnrate == " " or Calc_roc == " ")) or falt < 3000):
      print ("not going to write this row")
    else:
     if (ffnod == 0):
      ffnod = " "
      the_total_ffnod = " "
      ftas = " "
     if (fmach == 0 and ftaskt > 0 and falt > 0):
      fmach = adict["Mach"]
     if (percent_diff == " " or abs(Fuel_flow) == 888 or abs(percent_diff) == 1):
      row = [adict["PointID"],flight_phase," ",ftemperature,\
           fidisa,windcmp,fweight,fmach,ftaskt,ftas,ftime,falt,\
           maxcruisealt,rocalt,buffalt,the_lift_coeff,\
           CLmax," "," "," "," "," "," ",\
           " "," "," "," "," ",\
           " "," "," ",nada,nada,nada,nada,nada,nada,run_num]
     else:
      row = [pointid,flight_phase,fburnrate,ftemperature,\
           fidisa,windcmp,fweight,fmach,fcaskt,ftaskt,ftas,ftime,falt,\
           maxcruisealt,rocalt,buffalt,lift_coeff,CL_15degbank,\
           CLmax,drag_coeff,Lift,Drag,ffnod,the_total_ffnod,totalavlbclimbthrust,\
           total_ffnod_15degbank,FPCavthrustod,theWFvalue,Fuel_flow,Delta_ff,\
           percent_diff,total_theta,total_dlta,TotalT_TotalD,Calc_roc,FPC_roc,\
           faccelerate,ACC,fgama,gradient,run_num]
      writer.writerow(row)
  else:
   maxcruisealt = adict["Maxcruisealt"]
   rocalt = adict["Rocalt"]
   buffalt = adict["Buffalt"] 
   if (not yesCLBDSC and flight_phase == "TOC"):
    flight_phase = "CRU"
   if (not yesCLBDSC and flight_phase == "TOD"):
    print("NO GO because not yesCLBDSC and flight_phase is TOD")
   else:
    if ((flight_phase == "TOC+" and (fburnrate == " " or Calc_roc == " ")) or falt < 3000):
     print ("not going to write this row")
    else:
     if (ffnod == 0):
      the_ffnod = " "
      the_total_ffnod = " "
      ftaskt = " "
      fcaskt = " "
     if (fmach == 0 and ftaskt > 0 and falt > 0):
      fmach = adict["Mach"]
     TotalT_TotalD = ((total_theta**0.60) * total_dlta)
     if (percent_diff == " " or abs(Fuel_flow) == 888 or abs(percent_diff) == 1):
      row = [adict["PointID"],flight_phase," ",ftemperature,\
           fidisa,windcmp,fweight,fmach,fcaskt,ftaskt,ftas,ftime,falt,\
           maxcruisealt,rocalt,buffalt,the_lift_coeff,CL_15degbank,\
           CLmax," "," "," "," "," "," ",\
           " "," "," "," "," "," ",\
           " "," "," ",nada,nada,nada,nada,nada,nada,run_num] 
     elif ((flight_phase == "BOC" or flight_phase == "CRU") and abs(percent_diff) > 0.006 \
            and (falt >= 10000.0 and falt <= 11000)):
      row = [adict["PointID"],flight_phase," ",ftemperature,\
           fidisa,windcmp,fweight,fmach,fcaskt,ftaskt,ftas,ftime,falt,\
           maxcruisealt,rocalt,buffalt,the_lift_coeff,CL_15degbank,\
           CLmax," "," "," "," "," "," ",\
           " "," "," "," "," "," ",\
           " "," "," ",nada,nada,nada,nada,nada,nada,run_num] 
     elif (flight_phase == "BOC" and abs(percent_diff) > 0.006):
      row = [adict["PointID"],flight_phase," ",ftemperature,\
           fidisa,windcmp,fweight,fmach,fcaskt,ftaskt,ftas,ftime,falt,\
           maxcruisealt,rocalt,buffalt,the_lift_coeff,CL_15degbank,\
           CLmax," "," "," "," "," "," ",\
           " "," "," "," "," "," ",\
           " "," "," ",nada,nada,nada,nada,nada,nada,run_num] 
     else:
      row = [adict["PointID"],flight_phase,plan_Fuel_flow,ftemperature,\
           fidisa,windcmp,fweight,fmach,fcaskt,ftaskt,ftas,ftime,falt,\
           maxcruisealt,rocalt,buffalt,the_lift_coeff,CL_15degbank,\
           CLmax,the_drag_coeff,the_Lift,the_Drag,the_ffnod,the_total_ffnod,maxthrust,\
           total_ffnod_15degbank,FPCavthrustod,theWFvalue,Fuel_flow,Delta_ff,percent_diff,\
           total_theta,total_dlta,TotalT_TotalD,nada,nada,nada,nada,nada,nada,run_num] 
     writer.writerow(row)
#
 print ("DONE WITH THIS CYCLE THROUGH log2Data")
 print (" ")
 print ("***************************")
 print (" ")
 alt_list = []
 falt_list = []
 ffnod_master_list = []
 WFvalue_alt_mach_list = []
 WFvalue_alt_mach_dict = {}
 cont_proc = False
 cont_ftemperature = 0
 cont_ftas = 0
 cont_ftaskt = 0
 cont_fweight = 0
 cont_falt = 0
 cont_fmach = 0
 cont_faccelerate = 0
 cont_sinofgama = 0
 cont_fgama = 0
 cont_flight_phase = " "
 cont_ftime = 0
 FPCavthrustod = " "
 cont_Calc_roc = 0
 cont_FPC_roc = 0
 cont_plan_fuel_flow = 0
 cont_burnrate = 0
 cont_maxcruisealt = 0
 cont_rocalt = 0
 cont_buffalt = 0
#
 alt_cnt = 0
 ai = ai + 1
#
ender_row = [" "," "," "," "," "," "," "," ",\
              " "," "," "," "," "," "," "] 
#writer.writerow(ender_row)



