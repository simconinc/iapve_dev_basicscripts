#### Purpose ####
#Initial implementation of Biparapolic interpolation function


import numpy as np
from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt

# 2 sets of coefficients, one for each parabolic equation
a1 = 2
b1 = 4
c1 = 22

a2 = 1
b2 = 6
c2 = 33

### Moved this section to Trialobyte on 6/22/15. All interpolations use these vars
"""
# Set fixed variables
xwant = 16.72
print("xwant is", xwant)

# 2 lists of x values, one for each parabolic equation
# all xlists and ylists have 9 items
#xlist1 = [2,4,6,8,10,12,14,18,24]
#xlist2 = [1,3,5,9,12,16,23,29,31]
print("xlist1 is", xlist1)
print("xlist2 is", xlist2)

# Use python list comprehension feature to create 2 y lists based on 
# applying a parabolic equation to the list x 
ylist1 =[(a1*(xv**2) + b1*xv + c1) for xv in xlist1]
ylist2 =[(a2*(xv**2) + b2*xv + c2) for xv in xlist2]
print("ylist1 is", ylist1)
print("ylist2 is", ylist2)
"""

# Biparabolic() finds the 3 necessary points in each parabolic equation
# using one xlist/xarray then proceeds with the bibarbolic interpolation method
#
# Biparabolic2() finds the 3 necessary points in each of two parabolic equation
# using two xlists/xarrays then proceeds with the bibarbolic interpolation method

def Biparabolic2(xlist1, ylist1, xlist2, ylist2, xwant):

    # First determine if we have a direct match on
    # xwant within xlist1
    il_cnt = 0
    for ilook in xlist1:
     if (ilook == xwant):
      return(ylist1[il_cnt])
     il_cnt = il_cnt + 1
    #No direct match so proceed with xlist2

    # Now determine if we have a direct match on
    # xwant within xlist2
    il_cnt = 0
    for ilook in xlist2:
     if (ilook == xwant):
      return(ylist2[il_cnt])
     il_cnt = il_cnt + 1
    #No direct match so proceed with parabolics

    # Find the closest points in each xarray to xwant
    # Convert lists to arrays for use with the numpy find-closest-xpoint expression
    xarray1 = np.array(xlist1)
    xpos1 = (np.abs(xarray1 - xwant)).argmin()
    
    xarray2 = np.array(xlist2)
    xpos2 = (np.abs(xarray2 - xwant)).argmin()
    print("xpos1 is", xpos1)
    print("xpos2 is", xpos2)
    print("xlists1[xpos1] is", xlist1[xpos1])
    print("xlists2[xpos2] is", xlist2[xpos2])

    # test for edge cases.
    if not xlist1[0] < xwant < xlist1[len(xlist1) - 1] :
        print('Warning, xwant outside the range of xlist1. ywant1 will be the result of extrapolation') 
    if not xlist2[0] < xwant < xlist2[len(xlist2) - 1] :
        print('Warning, xwant outside the range of xlist2. ywant2 will be the result of extrapolation')

    if xpos1 == 0:
        return ylist1[0]
        # if xwant is less than any xlist1 values
        if xwant < xlist1[xpos1]:
            x1p1 = xlist1[xpos1]
            x1p2 = xlist1[xpos1 + 1]
            x1p3 = xlist1[xpos1 + 2] 
            y1p1 = ylist1[xpos1]
            y1p2 = ylist1[xpos1 + 1]
            y1p3 = ylist1[xpos1 + 2]

    if xpos1 == len(xlist1) - 1:
        return ylist1[xpos1]
        if xwant > xlist1[xpos1]:
            x1p1 = xlist1[xpos1]
            x1p2 = xlist1[xpos1 - 1]
            x1p3 = xlist1[xpos1 - 2] 
            y1p1 = ylist1[xpos1]
            y1p2 = ylist1[xpos1 - 1]
            y1p3 = ylist1[xpos1 - 2]  
    
    if xpos2 == 0:
        return ylist1[0]
        # if xwant is less than any xlist2 values
        if xwant < xlist2[xpos2]:
            x2p1 = xlist2[xpos2]
            x2p2 = xlist2[xpos2 + 1]
            x2p3 = xlist2[xpos2 + 2] 
            y2p1 = ylist2[xpos2]
            y2p2 = ylist2[xpos2 + 1]
            y2p3 = ylist2[xpos2 + 2]        
    if xpos2 == len(xlist2) - 1:
        return ylist2[xpos2]
        if xwant < xlist2[xpos2]:
            x2p1 = xlist2[xpos2]
            x2p2 = xlist2[xpos2 - 1]
            x2p3 = xlist2[xpos2 - 2] 
            y2p1 = ylist2[xpos2]
            y2p2 = ylist2[xpos2 + 1]
            y2p3 = ylist2[xpos2 + 2]
    
    # Build x1, y1, x2, y2 arrays
    # we don't need ypos because we can use xpos
    #use the same positional values in the list as xpos so point match up     
   
    try:    
        x1p1 = xlist1[xpos1 - 1]
        x1p2 = xlist1[xpos1]
        x1p3 = xlist1[xpos1 + 1]
        y1p1 = ylist1[xpos1 - 1]
        y1p2 = ylist1[xpos1]
        y1p3 = ylist1[xpos1 + 1]  
    except:
        return(999.99)   
    try:    
        x2p1 = xlist2[xpos2 - 1]
        x2p2 = xlist2[xpos2]
        x2p3 = xlist2[xpos2 + 1]
        y2p1 = ylist2[xpos2 - 1]
        y2p2 = ylist2[xpos2]
        y2p3 = ylist2[xpos2 + 1] 
    except:
        return(999.99)           
        
    threepoint_xarr1 = np.array([x1p1, x1p2, x1p3])
    threepoint_yarr1 = np.array([y1p1, y1p2, y1p3])
    threepoint_xarr2 = np.array([x2p1, x2p2, x2p3])
    threepoint_yarr2 = np.array([y2p1, y2p2, y2p3])
    print("threepoint_xarr1 is ", threepoint_xarr1)   
    print("threepoint_yarr1 is", threepoint_yarr1)         
    print("threepoint_xarr2 is", threepoint_xarr2)         
    print("threepoint_yarr2 is ", threepoint_yarr2)    

    # Generate interpolated splines and use them to calculate ywants
    parabspline1 = UnivariateSpline(threepoint_xarr1,threepoint_yarr1, k=2, ext = 0)
    parabspline2 = UnivariateSpline(threepoint_xarr2,threepoint_yarr2, k=2, ext = 0)
    ywant1 = parabspline1(xwant)
    ywant2 = parabspline2(xwant)
    
    #Now use the two ywant points to calculate a weighted average
    if xwant <= xarray1[xpos1]:       #xwant is smaller than xpos1, the so other point surrounding xwant is xpos - 1
        x1_weight = 1 - ((xarray1[xpos1] - xwant)/(xarray1[xpos1] - xarray1[xpos1 - 1])) 
        print("xwant > xarray1[xpos1]")
        print("x1_weight = ",x1_weight)
    else:  #xwant is greater than xpos1, the so other point surrounding xwant is xpos + 1
        x1_weight = 1 - ((xwant - xarray1[xpos1])/(xarray1[xpos1 + 1] - xarray1[xpos1])) 
        print("xwant < xarray1[xpos1]")
        print("x1_weight = ",x1_weight)
    if xwant <= xarray2[xpos2]:       #xwant is smaller than xpos2, the so other point surrounding xwant is xpos - 1
        x2_weight = 1 - ((xarray2[xpos2] - xwant)/(xarray2[xpos2] - xarray2[xpos2 - 1])) 
        print("xwant > xarray2[xpos2]") 
        print("x2_weight = ",x2_weight)
    else:  #xwant is greater than xpos2, the so other point surrounding xwant is xpos + 1
        x2_weight = 1 - ((xwant - xarray2[xpos2])/(xarray2[xpos2 + 1] - xarray2[xpos2])) 
        print("xwant < xarray2[xpos2]")
        print("x2_weight = ",x2_weight)       

    weighted_ave = ((x1_weight * ywant1) + (x2_weight * ywant2)) / (x1_weight + x2_weight)
    
    print("weighted_ave is ", weighted_ave)
   

r = Biparabolic2(xlist1, ylist1, xlist2, ylist2, xwant)
#print(r)

