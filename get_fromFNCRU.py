import json,sys
import std_atm
import math
import IAPVEutils
from Biparabolic1 import Biparabolic1
P8clean=False
JBx37800W=False
P8podup=False
P8poddn=False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
ttt = 27
print ("AT TOP OF get_fromFNCRU and P8clean P8podup JBx3700W ",\
         P8clean," ",P8podup," ",JBx37800W)
#
def get_lists_fromFNCRUod(strdisa,stralt,aircraft_config):
 FNCRU = {}
 mach_list = {}
 FNCRUod_list = {}
#For now we default to JBx37800W and we don't bother with aircraft_config
 #P8clean = False
 #P8clean = True
 #P8podup = False
 #JBx37800W = True
 #JBx37800W = False
 if (JBx37800W):
  FNCRU_file = open('JBx37800W_FNCRU101_JSON.txt')
#
 if (P8clean):
  FNCRU_file = open('P8clean_FNCRU101_JSON.txt')
#
#THERE IS ONLY ONE FNCRU01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_FNCRU01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup AND P8poddn
 if (P8podup or P8poddn):
  FNCRU_file = open('P8podup_FNCRU101_JSON.txt')
#
 a2string = FNCRU_file.readline()
 FNCRU = json.loads(a2string)
 print ("In get_lists_fromFNCRUod and stralt strdisa = ",stralt," ",strdisa)
 try:
  mach_list = FNCRU[strdisa][stralt][0]
  FNCRUod_list = FNCRU[strdisa][stralt][1]
 except:
  mach_list = [-888]
  FNCRUod_list = [-888]
 print("AT THE END OF get_max_FNCRUod for these strdisa and stralt values ",\
        strdisa," ",stralt)
 return(mach_list,FNCRUod_list)
#
def getdisa_fromFNCRU101():
#Returns the dictionary available_alts_dict that is structured as follows:
#top level keys are disa values such as '-72'
#The value associated with each top level key is a list of the available alts for that
#altitude. The alts in each list are each strings such as '37000' representing. 
#The reason they are strings is that they themselves will serve as keys
# 
 diag = 3
 #P8clean = True
 #JBx37800W = False
 #P8podup = False
 FNCRU = {}
 available_disa = [] 
 available_alts_list = []
 available_alts_dict = {}
 print ("COAL MINE ttt = ",ttt)
 print ("IN get_fromFNCRU and P8clean P8podup JBx3700W ",\
         P8clean," ",P8podup," ",JBx37800W)
 if (P8clean):
  FNCRU_file = open('P8clean_FNCRU101_JSON.txt') 
#
#THERE IS ONLY ONE FNCRU01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_FNCRU01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup AND P8poddn
 if (P8podup or P8poddn):
  FNCRU_file = open('P8podup_FNCRU101_JSON.txt') 
#
 if (JBx37800W):
  FNCRU_file = open('JBx37800W_FNCRU101_JSON.txt')
 a1string = FNCRU_file.readline()
 FNCRU = json.loads(a1string)
 available_disas = FNCRU.keys()
 #str_disa = []
 #for strdisas in available_disas:
  #str_disa.append(strdisas)
 for jdisas in available_disas:
  print("FNCRU[jdisas]keys = ",FNCRU[jdisas].keys())
  for striaa in FNCRU[jdisas].keys():
   if (diag == 4):
    print ("striaa = ",iaa)
   available_alts_list.append(striaa)
   if (diag == 4): 
    print ("available_alts_list  ",available_alts_list)
  available_alts_dict.update({jdisas:available_alts_list})
  available_alts_list = []
  print ("jdisas & available_alts_dict[jdisas] = ",jdisas," ",available_alts_dict[jdisas])
 return(available_alts_dict)
#*****************************************************************
#
def getFNCRUod(ftemperature,falt,fmach,aircraft_config):
#Determine the value for max cruise thrust over delta (FNCRUod)
    print ("MACABEE DETERMINING THE VALUE for max cruise thrust over delta")
    #std_atm_temperature = std_atm.alt2temp(falt)
    #deviation = ftemperature - std_atm_temperature
    #if (deviation < 0.0):
     #deviation = deviation - 0.5
    #if (deviation > 0.0):
     #deviation = deviation + 0.5
    #idisa = int(deviation)
    #fidisa = float(idisa)
    fidisa = IAPVEutils.get_disa_degF(falt,ftemperature) 
#Establish FNCRUdisa_alts dictionary for use further down
    available_FNCRUdisa_alts_dict = {}
    available_FNCRUdisa_alts_dict = getdisa_fromFNCRU101()
#
    FNCRUodvalue_disa_alt_list = []
#
    FNCRUmachlist = []
    FNCRUodlist = []
#
    FNCRUalts_list = []
    FNCRUod_list = []
    FNCRUalts_count = 0
    intdisa = []
    intalt = []
    for strdisa in available_FNCRUdisa_alts_dict.keys():
     intdisa.append(int(strdisa))
    intdisa.sort()
    print ("intdisa = ",intdisa)
#
# cycle through available_FNCRUdisa_alts_dict
    for idi in intdisa: 
     for stralt in available_FNCRUdisa_alts_dict[str(idi)]:
      intalt.append(int(stralt))
     intalt.sort()
     for iai in intalt:
      print ("idi & iai ",idi," ",iai)
      (FNCRUmachlist,FNCRUodlist) = get_lists_fromFNCRUod(str(idi),str(iai),aircraft_config)
      print ("Ahead of the 1st BiParabolic call for determining FNCRUod")
      print("FNCRUmachlist ",FNCRUmachlist)
      print("FNCRUodlist ",FNCRUodlist)
      print("fmach = ",fmach)
      if (FNCRUmachlist[0] !=  -888):
       fmachle =[float(xv) for xv in FNCRUmachlist]
      #for a given disa and altitude apply Biparabolic to the applicable
      #FNCRUmachlist list and FNCRUodlist to get a value for FNCRUodvalue.
      #This value is then added to FNCRUod_alts_list 
       FNCRUodvalue = Biparabolic1(fmachle,FNCRUodlist,fmach,'maxcruisethrust')
       print ("After 1st Biparabolic call and FNCRUodvalue = ",FNCRUodvalue)
       FNCRUod_list.append(FNCRUodvalue)
       FNCRUalts_list.append(iai)
       FNCRUalts_count = FNCRUalts_count + 1
       print ("AT THE END OF THE INNER LOOP for iai = ",iai)
       print ("  ") 
     print ("For idi = ",idi)
     print ("Ahead of the 2nd Biparabolic call for FNCRU and falt = ",falt)
     print ("FNCRUalts_list = ",FNCRUalts_list)
     print ("FNCRUod_list = ",FNCRUod_list)
     FNCRUodvalue_disa = Biparabolic1(FNCRUalts_list,FNCRUod_list,falt,'maxcruisethrust')
     #alt_dict_forprint[str(ialt)] = mach_dict_forprint  
     print ("")
     FNCRUodvalue_disa_alt_list.append(FNCRUodvalue_disa)
     print ("After 2nd Biparabolic call")
     print ("falt and fmach = ",falt," ",fmach," and strdisa = ",strdisa)
     print ("FNCRUodvalue_disa = ",FNCRUodvalue_disa)
     print ("FNCRUodvalue_disa_alt_list = ",FNCRUodvalue_disa_alt_list)
#
     FNCRUmachlist = []
     FNCRUodlist = []
     FNCRUalts_list = []
     FNCRUod_list = []
     FNCRUalts_count = 0
     intalt = []
#
    print("Finished the outer loop and FNCRUodvalue_disa = ",FNCRUodvalue_disa)
    print("intdisa = ",intdisa)
    print (" ")
    print ("FNCRU JAGUAR TERRITORY")
    ISAtempdev_list =[float(xv) for xv in intdisa]
    #ISAtempdev_list.sort()
    print ("Ahead of the 3rd BiParabolic call for FNCRUodvalue")
    print ("FNCRUodvalue_disa_alt_list = ",FNCRUodvalue_disa_alt_list)
    print ("ISAtempdev_list = ",ISAtempdev_list)
    print ("fidisa = ",fidisa)
    theFNCRUodvalue = Biparabolic1(ISAtempdev_list,FNCRUodvalue_disa_alt_list,fidisa,'maxcruisethrust')  
    print ("theFNCRUodvalue =  ",theFNCRUodvalue)
    maxcruisethrust = theFNCRUodvalue * 2.0
    print ("END OF FNCRU JAGUAR TERRITORY")
    return(maxcruisethrust)
#@#$************************************************************
