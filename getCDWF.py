#NOTE:
#keys obtained from dictionaries loaded via json.loads
#are always strings. But the value associated with any f
#given key maintains its original type
#
import json, sys
import std_atm
S = 1341.0
diag = 3
#P8clean = False
P8clean = True
P8podup = False
P8poddn = False
#JBx37800W = True
JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
if (ac_model_config[0:2] == "p8"):
 P8 = True
else:
 P8 = False
#
def getCDWF(ftemperature,fmach,falt,fweight,aircraft_config):
 exact_match = False
 CD = {}
 WF = {}
#
#CDBASE TABLES ARE UNIQUE FOR EACH MODEL AND UNIQUE FOR EACH FLAP SETTING FOR A GIVEN MODEL
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8clean_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8clean_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8clean_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8clean_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8clean_CDBASE06_JSON.txt')
  WF_file = open('P8clean_WFTAB101_JSON.txt')
# 
 if (P8podup):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8podup_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8podup_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8podup_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8podup_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8podup_CDBASE06_JSON.txt')
#WFTAB111 and WFTAB104 are the same for podup and poddn
  if (aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt')
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8poddn_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8poddn_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8poddn_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8poddn_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8poddn_CDBASE06_JSON.txt')
#WFTAB111 and WFTAB114 are the same for podup and poddn
  if (aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt')
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 1):
   CD_file = open('JBx37800W_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('JBx37800W_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('JBx37800W_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('JBx37800W_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('JBx37800W_CDBASE06_JSON.txt')
  WF_file = open('P8clean_WFTAB101_JSON.txt')
#
 a1string = CD_file.readline()
 CD = json.loads(a1string)
 print ("CD", CD)
 a2string = WF_file.readline()
 WF = json.loads(a2string)
 theta_ambient = (ftemperature + 273.15)/288.15
#fmach = ftas/(661.4786 * (theta_ambient ** (0.5)))
 int_mach = int((fmach*100.0) + .5)
 mach = str(int_mach)
 int_alt = int(falt)
 alt = str(int_alt)
#Compute the Lift Coefficient cl
#Use Eqn 9 of Chapter 9 in Jet Transport Performance Methods.pdf
 L =  fweight
 dlta = std_atm.alt2press_ratio(falt)
 print("L falt dlta fmach S = ",L," ",falt," ",dlta," ",fmach," ",S)
 cl = L/(1481.4*(fmach**2)*dlta*S)
 print ("COMPUTED CL = ",cl)
 print("From CDBASEij with mach = ",mach)
 print ("CD[mach] ",CD[mach])
 print(CD.keys())
#a = CD.keys
#print (type(a))
 first_lowest = -888
 last_lowest = -888
 first_highest = -889
 icl_cnt = 0
 count_last_lowest = 0
 for icl in CD[mach][0]:
  print ("icl = ",icl)
  if (icl == cl):
    print ("FOUND THE CL YOU WANTED ",icl)
    ifound = icl_cnt
    exact_match = True
  if (exact_match):
    break
  if (icl < cl):
    last_lowest = icl
    count_last_lowest = icl_cnt
  if (icl > cl):
    if(icl_cnt == (count_last_lowest + 1)):
      first_lowest  =  last_lowest
      first_highest = icl
      count_1st_highest = icl_cnt
  icl_cnt = icl_cnt + 1
 if (exact_match):
  CD_value = CD[mach][1][ifound]
  print ("AND HERE IS THE CD VALUE FOR YOUR MACH AND CL ",CD_value)
 if (not exact_match):
  print ("first_lowest first_highest",first_lowest," ",first_highest)
  CL_value_lowside = CD[mach][0][count_last_lowest]
  CL_value_highside = CD[mach][0][count_1st_highest]
  CD_value_lowside = CD[mach][1][count_last_lowest]
  CD_value_highside = CD[mach][1][count_1st_highest]
  totdiffCD = CD_value_highside - CD_value_lowside
  totdiffCL  = CL_value_highside - CL_value_lowside
  thisdiffCL = cl - CL_value_lowside
  linear_coeff = thisdiffCL/totdiffCL
  CDvalue = (CD_value_lowside + (linear_coeff*totdiffCD))* 0.000001 
  print("lowside and highside CD values = ",CD_value_lowside," ",CD_value_highside)
  print("CDvalue = ",CDvalue)
#
#Now compute the Drag
#See A-15 in Jet Transport Performance Methods.pdf
#
 D = CDvalue * (1481.4*(fmach**2)*dlta*S) 
 print ("Drag = ",D)
#
#Now determine WF from WFTAB101
#
 FFNod = D/dlta
 FNod = str(FFNod)
 print ("FFNod = ",FFNod," FNod = ",FNod)
#
 exact_match = False
 print ("fmach = ",fmach)
 fmach_thou = fmach * 1000.0
 print ("int of fmach_thou = ",int(fmach_thou))
 mach = str(int(fmach_thou))
 print ("mach for finding WF = ",mach)
 print ("alt for finding WF = ",alt)
 print("From WFTAB101 ")
 print ("Alt ",alt," WF[alt] ",WF[alt])
 print(WF.keys())
#a = CD.keys
#print (type(a))
 most_recent_lowest = -888
 last_lowest = -888
 first_highest = -889
 iw_cnt = 0
 for ifnod in WF[alt][mach][0]:
  print ("ifnod = ",ifnod)
  float_nod = float(ifnod)
  if (float_nod == FFNod):
    print ("FOUND THE FFNod YOU WANTED ",float_nod)
    ifound = iw_cnt
    exact_match = True
  if (exact_match):
    break
  if (float_nod < FFNod):
    most_recent_lowest = float_nod
    print("most_recent_lowest = ",most_recent_lowest)
    count_most_recent_lowest = iw_cnt
  if (float_nod > FFNod):
    if(iw_cnt == (count_most_recent_lowest + 1)):
      first_highest = float_nod
      last_lowest = most_recent_lowest
      print ("first_highest = ",first_highest)
      print ("last_lowest = ",last_lowest)
      count_1st_highest = iw_cnt
      count_last_lowest = count_most_recent_lowest
  iw_cnt = iw_cnt + 1
 if (exact_match):
  WF_value = float(WF[alt][mach][1][ifound])
  print ("AND HERE IS THE WF VALUE FOR YOUR ALT MACH AND FNOD ",WF_value)
 if (not exact_match and first_highest == -889 and most_recent_lowest > 0):
  WF_value = float(WF[alt][mach][1][iw_cnt-1])
  print("Using the highest WF_value which is ",WF_value)
 if (not exact_match and most_recent_lowest == -888): 
  WF_value = float(WF[alt][mach][1][0])
  print("Using the lowest WF_value which is ",WF_value)
 if (not exact_match and last_lowest > 0 and first_highest > 0):
  print ("last_lowest first_highest",last_lowest," ",first_highest)
  FFNod_value_lowside = float(WF[alt][mach][0][count_last_lowest])
  FFNod_value_highside = float(WF[alt][mach][0][count_1st_highest])
  WF_value_lowside = float(WF[alt][mach][1][count_last_lowest])
  WF_value_highside = float(WF[alt][mach][1][count_1st_highest])
  totdiffWF = WF_value_highside - WF_value_lowside
  totdiffFFNod  = FFNod_value_highside - FFNod_value_lowside
  thisdiffFFNod = FFNod - FFNod_value_lowside
  linear_coeff = thisdiffFFNod/totdiffFFNod
  WF_value = (WF_value_lowside + (linear_coeff*totdiffWF)) 
  print("lowside and highside WF values = ",WF_value_lowside," ",WF_value_highside)
  print("WF_value = ",WF_value)
# Need theta total and dlta total

 print ("theta_ambient dlta fmach ",theta_ambient," ",dlta," ",fmach)
 total_theta = theta_ambient*(1 + 0.2*(fmach**2))
 tpm = (1 + 0.2*(fmach**2))
 total_dlta = dlta*(tpm**3.5)
 print ("total_theta tpm total_dlta ",total_theta," ",tpm," ",total_dlta)
 Fuel_flow = WF_value * ((total_theta**0.62) * total_dlta)
 print ("Fuel_flow = ",Fuel_flow)
 return(fmach,cl,CDvalue,D,FFNod,Fuel_flow)
#
#
def getmach_fromCDBASE(aircraft_config):
#returns a list of the available machs from CDBASE01_JSON.txt
# 
 CD = {}
 available_machs = []
 print ("In getmach_fromCDBASE and aircraft_config = ",aircraft_config)
#CD_file = open('37800WCFM56_57_CDBASE01_JSON.txt')
#
#CDBASE TABLES ARE UNIQUE FOR EACH MODEL AND UNIQUE FOR EACH FLAP SETTING FOR A GIVEN MODEL
#
 if (P8clean):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8clean_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8clean_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8clean_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8clean_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8clean_CDBASE06_JSON.txt')
   WF_file = open('P8clean_WFTAB101_JSON.txt')
#
 if (P8podup):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8podup_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8podup_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8podup_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8podup_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8podup_CDBASE06_JSON.txt')
#WFTAB111 and WFTAB114 tables are the same for podup and poddn
  if (aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt')
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (P8poddn):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('P8poddn_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('P8poddn_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('P8poddn_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('P8poddn_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('P8poddn_CDBASE06_JSON.txt')
#WFTAB111 and WFTAB114 tables are the same for podup and poddn
  if (aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt')
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 1 or aircraft_config == 2):
   CD_file = open('JBx37800W_CDBASE01_JSON.txt')
  if (aircraft_config == 3):
   CD_file = open('JBx37800W_CDBASE03_JSON.txt')
  if (aircraft_config == 4):
   CD_file = open('JBx37800W_CDBASE04_JSON.txt')
  if (aircraft_config == 5):
   CD_file = open('JBx37800W_CDBASE05_JSON.txt')
  if (aircraft_config == 6):
   CD_file = open('JBx37800W_CDBASE06_JSON.txt')
  WF_file = open('P8clean_WFTAB101_JSON.txt')
#
 a1string = CD_file.readline()
 print ("a1string = ",a1string)
 CD = json.loads(a1string)
 available_machs = CD.keys()
 str_mach = []
 int_mach = [] 
 for iamachs in available_machs:
  print ("BARN CATS iamachs = ",iamachs)
  str_mach.append(iamachs)
 for jamachs in str_mach:
  int_mach.append(int(jamachs)) 
 int_mach.sort()
 return(int_mach)

def getalt_fromWFTAB(aircraft_config):
#Returns the dictionary available_machs_dict that is structured as follows:
#top level keys are altitudes such as '27000'
#The value associated with each top level key is a list of the available machs for that
#altitude. The machs in each list are each strings such as '400' representing the
#mach as mach*1000.  The reason they are strings is that they themselves will serve
#as keys
# 
 WF = {}
 available_alts = [] 
 available_machs_list = []
 available_machs_dict = {}
 if (P8clean):
  if (aircraft_config != 2):
   WF_file = open('P8clean_WFTAB101_JSON.txt')
  else:
   WF_file = open('P8clean_WFTAB104_JSON.txt')
   print ("IN getalt_fromWFTAB101 and using WFTAB104")
#
#WFTAB111 and WFTAB104 tables are the same for podup and poddn
 if (P8podup or P8poddn):
  if (aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt') 
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
   print ("IN getalt_fromWFTAB101 and using WFTAB104")
#
 if (JBx37800W):
  if (aircraft_config != 2):
   WF_file = open('JBx37800W_WFTAB101_JSON.txt')
  else:
   WF_file = open('P8clean_WFTAB104_JSON.txt')
   print ("IN getalt_fromWFTAB101 and using WFTAB104")
 a1string = WF_file.readline()
 WF = json.loads(a1string)
 available_alts = WF.keys()
 str_alt = []
#int_alt = [] 
 for iaalts in available_alts:
  str_alt.append(iaalts)
 for jaalts in str_alt:
  print("WF[jaalts]keys = ",WF[jaalts].keys())
  for iaa in WF[jaalts].keys():
   if (diag == 4):
    print ("iaa = ",iaa)
   available_machs_list.append(int(iaa))
   if (diag == 4): 
    print ("available_machs_list  ",available_machs_list)
  available_machs_dict.update({jaalts:available_machs_list})
  available_machs_list = []
# int_alt.append(int(jaalts)) 
  available_machs_dict[jaalts].sort()
  print ("jaalts & available_machs_dict[jaalts] = ",jaalts," ",available_machs_dict[jaalts])
#not needed: int_alt.sort()
#print ("int_alt = ",intalt)
 return(available_machs_dict)
#
def getmach_fromCDWM(aircraft_config):
#returns a list of the available machs from CDBASE01_JSON.txt
# 
 if (aircraft_config != 2):
  print ("YOU HAVE NO BUSINESS CALLING get_CDWM since aircraft_config not= 2")
  return(0)
 CD = {}
 available_machs = []
 print ("In getmach_fromCDWM and aircraft_config = ",aircraft_config)
#
#CDWM TABLES ARE FOR THE CDincrement due to windmill drag
#
 if (P8):
  if (aircraft_config == 2):
   CD_file = open('P8clean_CDWM01_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 2):
   CD_file = open('JBx37800W_CDWM01_JSON.txt')
#
 a1string = CD_file.readline()
 print ("a1string = ",a1string)
 CD = json.loads(a1string)
 available_machs = CD.keys()
 str_mach = []
 int_mach = [] 
 for iamachs in available_machs:
  print ("BARN CATS iamachs = ",iamachs)
  str_mach.append(iamachs)
 for jamachs in str_mach:
  int_mach.append(int(jamachs)) 
 int_mach.sort()
 return(int_mach)

