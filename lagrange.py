def lagrange(points):
       def P(x): 
              total = 0
              n = len(points)
              for i in range(n):
                     xi, yi = points[i]
                                                   
                     def g(i, n):

                             tot_mul = 1
                             for j in range(n):
                                   if (i == j):
                                               continue
                                   (xj, yj) = points[j]
                                   tot_mul *= (x - xj)/float(xi - xj)

                             return tot_mul
                     
                     total += yi * g(i,n)
              return total
       return P