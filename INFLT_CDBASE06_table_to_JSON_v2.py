import csv
import numpy as np
import sys
import json

#Initialize variables and structures
count = 0
cl = []
mach = []
cd = []

#This code section uses the method to create a reader object using csv.reader and then
#use the reader object to create a list using dalist = [data for data in data_iter]
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE input file
#dp = 'JBx37800W_CDBASE01_tbl.txt'
dp = "P8clean_CDBASE06_tbl.txt"
#dp = "P8clean_CDBASE04_tbl.txt"
#dp = "37800WCFM56_57_CDBASE01_tbl.txt"
#***
#
#for files with a name ending in _CDBASEij_tbl.txt, * is the deliminter between the 3 subtables (cl,mach,cd) 
dl = '*'
with open(dp,'rU') as csvfile:
   data_iter = csv.reader(csvfile,delimiter=dl)
#   for row in data_iter:
#    print ("row ",row)
   dalist = [data for data in data_iter] #creates the list dalist
print ("dalist = ",dalist)

# The list named dalist is made up of the following:
# 1st element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire cl block
#     element 2 is ''
# 2nd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire mach block
#     element 2 is ''
# 3rd - nth elements in dalist are each a list. Each such list consists of a single 
# element which is a string created from an entire line of cd data found in .._CDBASEij_tbl.txt  

real_cl = []
real_mach = []

for i in dalist:
  print ("just after for i in dalist")
  if (len(i) == 0):
   print("No more data")
   break
  print (i)
  if (count == 0): 
#Process cl
   print ("splitting up the members of the cl string using the delimiter #") 
   k = i[0].split("#")
   for ar in k:
    cl.append(ar.split())
   print ("k")
   print (k)
   print ("Here is cl")
   print (cl)
   bonafide_cl = []
   for c in cl:
#   realdeal = c[0].split('.')
#   realdeal = realdeal[1:]
    cha_cnt = 0
    for cha in range(0,len(c[0]),7):
     cha7 = cha + 7
     realdeal = c[0][cha:cha7]
     bonafide_cl.append(realdeal)
   print ("Here is bonafide_cl")
   print (bonafide_cl)
   cl_list = []
   for odcl in bonafide_cl:
    cl_list.append(odcl)
#  icl = 0
#  for odci in cl_list:
#   fcl = float(odci)*0.000001
#   cl_list[icl] = fcl
#   icl = icl + 1
   print ("cl_list ",cl_list)
#
  if (count == 1): 
#Process mach 
   print ("splitting up the members of the mach string using the delimiter #") 
   k = i[0].split("#")
   for ar in k:
    mach.append(ar.split())
   print ("k")
   print (k)
   print (" ")
   print ("Here is mach")
   print (mach)
   im = 0
   bonafide_mach = []
   for m in mach:
    realdeal = m[0].split('.')
    realdeal = realdeal[1:]
    bonafide_mach.append(realdeal)
    im = im + 1
   print ("Here is bonafide_mach")
   print (bonafide_mach)
   mach_list = []
   for odm in bonafide_mach:
    mach_list = mach_list + odm
   imh = 0
   for odmh in mach_list:
    fmh = float(odmh)*0.000001
    int_mh = int((fmh*1000)+ 0.5)
    mach_list[imh] = str(int_mh)
    imh = imh + 1
   print("mach_list ",mach_list)
#
#  hmn = float(mach[len(mach)-1])
#
  if (count >= 2): 
   print ("splitting up the members of the cd string") 
   cd.append(i[0].split('\n'))
  count = count + 1
  print(" ")
#
print ("Here is CD")
print(cd) 
#So at this point we have built 4 list objects.
#  cl - contains multiple elements: each element is a list made up of data from a sub group within the cl section of the CDBASEij table 
#  mach - contains multiple elements: each element is a list made up of data from a sub group within the mach section of the CDBASEij table 
#  cd - contains multiple elements: each element is a list made up of data taken from a string created from an entire line of the cd section 
#       of the CDBASEij table 
#
#Now we take advantage of the knowledge that every three elements of the cd list when taken
#together make up a sub group from the cd section of the CDBASEij table
#Furthermore, we know that the third of these three lists will always have just five members; 
#these are the final five cd values for the sub group:
#
#The following section of code populates the dictionary named cd_dic
#The structure of cd_dic is as follows:
#Each key in cd_dic is one of the mach numbers from the mach number section of the
#CDBASEij table
#
#The value associated with each mach number key is a list.
#The list is made up of the cd values each applicable to a corresponding combination 
#of cl and mach number.  
#
ic = 0
mach_cnt = 0
line_cnt = 0
g_cnt = 0
cd_dic = {}
cd_values = []
for c in cd:
 print ("c IS THIS ",c)
 cdints = c[0].split(".")
#get rid of the first member of the cdints list
 cdints = cdints[1:]
 print ("cdints IS THIS ",cdints)
 for g in cdints:
  print ("g = ",g) 
  g_cnt = g_cnt + 1
  print ("g_cnt = ",g_cnt)
  val = float(g)
  print ("line_cnt = ",line_cnt)
  if (g_cnt <= 10):
   cd_values.append(val)
  if (g_cnt == 6 and line_cnt == 1):
   line_cnt = line_cnt + 1
   #get rid of the last member of cd_values
   #cd_values.pop()
   g_cnt = 0
  if (g_cnt == 11):
   cd_values.append(val)
   line_cnt = line_cnt + 1
   print ("HERE inside if g_cnt == 11")
   print ("SO FAR cd_values = ",cd_values)
   g_cnt = 0
  print ("line_cnt = ",line_cnt)
  if (line_cnt == 2): 
   mach_key = mach_list[mach_cnt]
   print ("mach_key = ",mach_key)
   mach_cnt = mach_cnt + 1
   print ("cd_values ",cd_values)
   cl_cd_tuple = (cl_list,cd_values)
   cd_dic.update({mach_key:cl_cd_tuple})
   cd_values = []
   print ("cd_dic ",cd_dic.get(mach_key))
   line_cnt = 0
#  fv = float(mach[mach_cnt][-1])
#  hmn = int(fv*1000)
#  print ("fv = ",fv,"hmn = ",hmn)
#  print ("mach_key and highest_mach_number ",mach_key," ",hmn)
#  if (mach_key == hmn):
#    alt_key = int(alt[0][alt_cnt])
#    print('alt_key = ',alt_key,"alt_cnt = ",alt_cnt)
#    WF_dict.update({alt_key:mach_fuel_burn_dic})
#    print ('THE WF Dictionary: WF_dict{alt_key:mach_fuel_burn_dic} is \n',WF_dict.get(alt_key))
#    mach_fuel_burn_dic = {}
#    alt_cnt = alt_cnt + 1
#    mach_cnt = mach_cnt + 1 
print("Victory")
#count = 0
#while (count <= mach_cnt)
#  print ("count alt[0][count] ",count," ",alt[0][count])
#  the_alt_key = int(alt[0][count])
#  print ("the_alt_key = ",the_alt_key)
#  print (WF_dict.get(the_alt_key))
#  count = count + 1
#  print ("count = ",count,"alt_cnt = ",alt_cnt)

#Use json.dumps to create a JSON object and then write it to .._CDBASEij_JSON.txt
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file
#fn = "JBx37800W_CDBASE01_JSON.txt"
fn = "P8clean_CDBASE06_JSON.txt"
#fn = "37800WCFM56_57_CDBASE01_JSON.txt"
#***
target = open(fn,'w')
target.truncate()
target.write(json.dumps(cd_dic))
print ("FINISHED")



  
