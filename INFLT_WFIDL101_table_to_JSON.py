import csv
import numpy as np
import sys
import json
import math

#Initialize variables and structures
count = 0
disa = []
mach = []
alt = []
WFIDLval = []

# Parsing the input/output filename arguments from trialobyte
print ('Number of arguments received from trialobyte: ', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))
# assign input filename
dp = sys.argv[0]
# assign output filename
fn = sys.argv[1]

#This code section uses the method to create a reader object using csv.reader and then
#use the reader object to create a list using dalist = [data for data in data_iter]
#
#**** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE input file
#dp = 'JBx37800W_WFIDL101_tbl.txt'
#dp = 'P8clean_WFIDL101_tbl.txt'
#dp = 'P8podup_WFIDL101_tbl.txt'
#dp = '37800WCFM56_57_WFIDL101_tbl.txt'
#****
#
if (dp[0:2] == "P8"):
   p8 = True
   dividby = 11.0
   numdisa = 11
else:
   p8 = False
   dividby = 14.0
   numdisa = 9
#in files with names ending in _WFIDL101_tbl.txt, * is the deliminter
#between the 4 subtables (FNod,mach,alt,WF) 
dl = '*'
with open(dp,'rU') as csvfile:
   data_iter = csv.reader(csvfile,delimiter=dl)
#  for row in data_iter:
#   print ("row ",row)
   dalist = [data for data in data_iter] #creates the list dalist

# The list named dalist is made up of the following:
# 1st element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire mach block
#     element 2 is ''
# 2nd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire altitude block
#     element 2 is ''
# 3rd element in dalist is a list made up of two elements
#     element 1 is a string made up of the entire disa block
#     element 2 is ''
# 4th - nth elements in dalist are each a list with a single member that consists of 
# a string created from an entire line of WFIDLval data found in .._WFIDL101_tbl.txt  

for i in dalist:
  print ("just after for i in dalist")
  print (i)
  if (count == 0 and len(i) > 0): 
   print ("splitting up the members of the mach string using the delimiter 1#") 
   k = i[0].split("1#")
   for ar in k:
    mach.append(ar.split())
   print ("k")
   print (k)
   print ("Here is mach")
   print (mach)
  if (count == 1 and len(i) > 0): 
   print ("splitting up the members of the mach string using the delimiter 1#") 
   k = i[0].split("1#")
   for ar in k:
    alt.append(ar.split())
   print ("k")
   print (k)
   print (" ")
   print ("Here is alt")
   print (alt)
  if (count == 2 and len(i) > 0): 
   disa.append(i[0].split())
   print ("do not need to split up the disa string")
   print ("Here is disa")
   print (disa)
  if (count >=3 and len(i) > 0):
   print ("splitting up the members of the WFIDLval string") 
   print ("THE ENTIRE i ",i)
   print ("AIAI i[0] ",i[0])
   print ("i[0] SPLIT ",i[0].split())
   WFIDLval.append(i[0].split())
  count = count + 1
  print(" ")
#
print(" ")
print ("Here is WFIDLval")
print(WFIDLval) 
#So at this point we have built 4 list objects.
#  mach - contains multiple elements: each element is a list made up of data from a sub group within the mach section of the WFIDL101 table 
#  alt - contains multiple elements: each element is a list made up of data from a sub group within the altitude section of the WFIDL101 table 
#  disa - contains multiple elements: each element is a list made up of data from a sub group within the disa section of the WFIDL101 table
#  WFIDLval - contains multiple elements: each element is a list made up of data taken from a string created from an entire line of the WFIDL section 
#          of the WFIDL101 table 
#
#The following section of code populates the dictionary named WFIDLval_dict
#The structure of WFIDLval_dict is as follows:
#Each key in WFIDLval_dict is one of the disa's from the disa section of the WFIDL101 table
#The value associated with each disa key is a dictionary named alt_WFIDLval_dic.
#The keys in alt_WFIDLval_dic are the altitudes and associated with each altitude key is a
#list of two lists. List 1 consists of mach values and list 2 consists of corresponding 
#WFIDLval values
#For example, in the WFIDL101 table stored in JBx37800W.dat,
#the disa values are:
# -65.00 -40.00 -20.00    .00  18.00  27.00  36.00  51.00  61.00  71.00
#and the altitude values are: 
#0   5000  10000  15000  20000  25000  29000  31000  33000  35000 36089  37000  39000  41000 
#
#So taking our JBx37800W.dat WFIDL101 table example we have:
#10 disa's
#14 altitudes
#14 sets of mach values with 10 machs per set
#At altitude 37000 the applicable machs are:
#  .60000 .70000 .72000 .75000 .78000 .80000 .85000 .87000
#Since there are 10 disa's there are 10 blocks of WFIDLval values with each
#block consisting of 14 sets of WFIDLval values.
#disa 0 is the 4th disa value so its WFIDLval sets are in the 4th block
#Altitude 37000 is the 12th altitude in the 14 altitude set so this means
#that for disa = 0 and altitude = 37000 the 12th set of WFIDLval values in 
#the 4th block is the applicable WFIDLval set.
#
hit_zero = False
hit_one = False
#mach_cnt = 0
machlist = []
mach_cnt_listmbrs = 0
alt_cnt = 0
dcnt = 0
alt_WFIDLval_dic = {}
WFIDLval_values = []
WFIDLval_dict = {}
fcount = 0
gcount = 0
disa_count = -1
for f in WFIDLval:
 print ("f IS THIS ",f)
 if (len(f) == 0):
  print("No more data")
  break
 alt_key = str(alt[0][fcount])
 print("GENCO alt_key = ",alt_key)
 machlist = mach[fcount]
#remove "MIN" and "IDL"
 llm = 0
 removelist = []
 for m in machlist:
  print ("mamda ",m)
  if (m == "MIN" or m == "IDLE"):
   removelist.append(m)
 for r in removelist:
  machlist.remove(r)
 print ("machlist = ",machlist)
 for g in f:
  print ("g IS THIS ",g)
  if (len(WFIDLval_values) < len(machlist)):
   WFIDLval_values.append(float(g))
 print ("fcount = ",fcount," HERE IS JAZZY machlist ",machlist)
 ruple_tuple = (machlist,WFIDLval_values)
 alt_WFIDLval_dic.update({alt_key:ruple_tuple})
 print ("catemount alt_WFIDL0d_dic ",alt_WFIDLval_dic)
 fcount = fcount + 1
 if (fcount > 0 and math.fmod(float(fcount),dividby) == 0):
  disa_count = disa_count + 1
  disa_key = str(disa[0][disa_count])
  print ("JOHNLEEHOOKER disa_key = ",disa_key)
  WFIDLval_dict.update({disa_key:alt_WFIDLval_dic})
  print ('THE WFIDLval Dictionary: WFIDLval_dict{disa_key:alt_WFIDLval_dic} is \n',\
          WFIDLval_dict.get(disa_key))
  fcount = 0
  alt_WFIDLval_dic = {}
  if (disa_count == numdisa):
   disa_count = -1
 WFIDLval_values = []
print("Victory")
#
#Use json.dumps to create a JSON object and then write it to .._WFIDL101_JSON.txt
#
#*** THIS IS WHERE USE OF THE .INI FILE SHOULD TAKE ON THE RESPONSIBILITY FOR DETERMINING THE output file
#fn = "37800WCFM56_57_WFIDL101_JSON.txt"
#fn = "P8clean_WFIDL101_JSON.txt"
#fn = "P8podup_WFIDL101_JSON.txt"
#fn = "JBx37800W_WFIDL101_JSON.txt"
#fn = "notneeded_P8clean_WFIDL101_JSON.txt"
#***
target = open(fn,'w')
target.truncate()
target.write(json.dumps(WFIDLval_dict))

print ("FINISHED with WFIDL101 JSON Maker")



  
