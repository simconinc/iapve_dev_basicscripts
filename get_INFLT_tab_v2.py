#get_INFLT_tab.py
#This program reads from INFLT .dat file and parses certain tables
#and then writes their values to a spread sheet.

import json
import sys
import re

import csv
import numpy as np

INFLT_file = open('JBx37800W.dat')
i = 1
foundtable = False

#astr is where each line of a given table is appended to in order to create
#one long string making up that table.  astr is always initialized to null
astr = ""

tables = []  #for storing a list of tables, each a string
tbl_dict = []   #for storing a list of dictionaries
subtab = []
independent_vars = []
index11 = []
fuel_table = []
WFlist = []
dict = {}
crawdad = {}
k = 0

#the following while loop extracts certain tables and stores them as 
#strings in the tables list
#The while loop below stays in effect until i is set to 0
while(i):
    astring = INFLT_file.readline()
#   print "astring = ",astring
#Check if astring contains a start indicator for a table
#The string /TBLU indicates the start of the very 1st plan
#An H or a / indicates end of a table and the possible start of a new table
    if (not foundtable and astring[0:5] == "/TBLU"):
        foundtable = True 
#       print "FOUND TBLU"
    elif(foundtable and (astring[0:1] == "H" or astring[0:1] == '/')):
        tables.append(astr)
#       print "astr[0:7] and tables[k] ",astr[0:7]," ",tables[k][0:27]
        foundtable = False 
#       print "foundtable astring[0:1] = H or / ", foundtable," ",astring[0:1]
        k = k + 1
        astr = ""
    elif(foundtable and astring != ""):
        astr = astr + astring
#astring == "" means we have reached the end of the file 
    if (astring == ""):
        tables.append(astr)
        i = 0
#print "fplans equals", fplans           
#print "out of while loop"

for t in tables:
    print ("START of t in tables")
# when you specify '\n' python treats it as hex0a
# when you specify '\r' python treats it as hex0D
# therefore
# p.replace('\n', ' ') replaces all occurrences of hex0A with hex20
# p.replace('\r', ' ') replaces all occurrences of hex0D with hex20
# it is necessary to replace in the string p all occurences of hex0A with hex20 and all occurences hex0D with hex20 in order for json.loads(p) to work properly
#   t = t.replace('\n', ' ').replace('\r', ' ')
    print ("t ",t[0:272])
    print ("END OF THE TABLE t")   
print ("Finished reading the file")  
#
#At this point in the process each member of the tables list is a string consisting
#of an entire table starting with the table identifier (e.g. CDBASE01)
#
#Now we are going to find the member of the tables list containing the string
#for the table WFTAB101 and process that table 
#we know from the INFLT User Manual that WFTAB101 is based on WF = F(W/delta,mach,alt)
#
#We want to split the WFTAB101 table into 4 parts; part 1 consists of the independent variable 
#wod values, part 2 consists of the independent variable mach values, part 3 consists of the
#independent variable altitude values, and part 4 consists of the dependent variable WF values.
#
#open file to write the WFTAB101 table to 
fn = "WFTAB101_tbl.txt"
target = open(fn,'w')
target.truncate()
for w in tables:
 if (w[0:8] == 'WFTAB101'):
  print ("found WFTAB101")
  independent_vars = w[8:].split("11\n")
  print ("The length of independent"len(independent_vars)
  wod = independent_vars[0]
  wod = wod.replace('1\n', '1#').replace('\r','')
  wod = wod.replace('\n','')
  target.write(wod)
  target.write('*\n')
  mach = independent_vars[1]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[2]
  alt = alt.replace('\n','').replace('\r','')
  target.write(alt)
  target.write('*\n') 
  print ("wod ",wod)
  print ("mach ",mach)
  print ("alt ",alt)

#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
# print "index11[2] ",index11[2]
# print "Here"  
# outer = index11[2] + 100
# print w[index11[2]:outer]  
  WF = w[index11[2]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(WF)
  target.write('*')
  print ("WF VALUES ")
  print (WF)
  print ("DONE PRINTING WF VALUES")
# print "Complete Fuel Table"
# print fuel_table

#At this point in the process we have written out four string objects
#to the file WFTAB101_tbl.txt.
#These four string objects are:
#   wod - contains the entire set of weight over delta values as one string
#   mach - contains the entire set of mach values as one string
#   alt - contains all of the altitude values as one string
#   WF - contains the entire set of fuel flow values as one string
#Within WFTAB101_tbl.txt, the four strings are separated by the delimiter '*'
#
#
#The section below is kept for possible future use. However,
#it probably will not be needed as the program INFLTtable_to_JSON.py
#that reads from WFTAB101_tbl.txt makes use of an iterator object
#created using csv.reader with '*' as the delimiter.  This
#iterator object and follow on code provides a cleaner way
#for getting us what we need.
# 
#We now want to convert wod, mach, alt, and WF into lists
#
count = len(wod)

wodlist = wod.split('1\n')
machlist = mach.split('1\n')
altlist = alt.split('1\n')
#WF.replace('\n','')

print ("wodlist ",wodlist)
print ("machlist ",machlist)
print ("altlist ",altlist)

count_wodlist = len(wodlist)
count_machlist = len(machlist)
count_altlist = len(altlist)
j = 0
print ("count_wodlist count_machlist count_altlist ",count_wodlist," ",count_machlist," ",count_altlist)
while (j < count_wodlist):
  wodlist[j].replace('\n','')
  print (wodlist[j])
  j = j + 1
j = 0
while (j < count_machlist):
  machlist[j].replace('\n','')
  print (machlist[j])
  j = j + 1
j = 0
while (j < count_altlist):
  altlist[j].replace('\n','')
  print (altlist[j])
  j = j + 1

table_id = wodlist[0]
wodlist.pop(0)

#Split WF in stages
#
seps = ('1\n','2\n','3\n','4\n','5\n','6\n','7\n','8\n','9\n','10\n','11\n','12\n','13\n','14\n','15\n')
default_sep = '1\n'
print ("WFlist")
n = 0
for s in seps[1:]:
 WF = WF.replace(s,default_sep)
for i in WF.split(default_sep):
  WFlist.append(i)
  print (WFlist[n])
  n = n + 1

#now we create a list of lists
wod_2d_list = []
mach_2d_list = [[]]
WF_2d_list = [[]]
for m in wodlist:
  wod_2d_list.append(m.split())
print ("wod_2d_list")
print (wod_2d_list)
#wod.replace('\n','')
#mach.replace('\n','')
#alt.replace('\n','')
#WF.replace('\n','')
#