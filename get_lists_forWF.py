#NOTE:
#keys obtained from dictionaries loaded via json.loads
#are always strings. But the value associated with any 
#given key maintains its original type
#
#This function returns the appropriate FFnod list and associated WF_list for a 
#combination of ialt and imach as provided in the WFTAB101 table
#
import json, sys
from Linear import Linear 
#P8clean = False
P8clean = True
P8podup = False
P8poddn = False
#JBx37800W = True
JBx37800W = False
#
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
#
def get_lists_forWF(ialt,imach,aircraft_config):
 exact_match = False
 WF_list = []
 FF_nodlist = []
 if (JBx37800W):
  if (aircraft_config != 2):
   WF_file = open('JBx37800W_WFTAB101_JSON.txt')
  else:
   WF_file = open('JBx37800W_WFTAB104_JSON.txt')
   print ("IN get_lists_forWF and USING WFTAB104")
#
 if (P8clean):
  if (aircraft_config != 2):
   WF_file = open('P8clean_WFTAB101_JSON.txt')
  else:
   WF_file = open('P8clean_WFTAB104_JSON.txt')
   print ("IN get_lists_forWF and USING WFTAB104")
#
 if (P8podup or P8poddn):
  if (aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt')
   print ("IN get_lists_forWF and USING WFTAB111")
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
   print ("IN get_lists_forWF and USING WFTAB104")
#
 a2string = WF_file.readline()
 WF = json.loads(a2string)
 print ("IN get_lists_forWF and keys for WF are ",WF.keys())
 #for keym1 in WF.keys():
  #print("MAFIA keym1 WF[keymarch] ",keym1," ",WF[keym1]) 
  #for keym2 in WF[keym1]:
   #print("keym2 WF[keym1][keym2] ",keym2," ",WF[keym1][keym2])
 ialtitude = str(ialt)
 imachnumber = str(imach)
 print("MAFIA GUYS ialtitude imachnumber ",ialtitude," ",imachnumber)
 try:
  FFnod_list = WF[ialtitude][imachnumber][0]
  WF_list = WF[ialtitude][imachnumber][1]
 except:
  FFnod_list = [-888]
  WF_list = [-888]
 print("AT THE END OF get_lists_forWF for these ialtitude and imachnumber values ",ialtitude," ",imachnumber)
 return(FFnod_list,WF_list)
#print(FFnod_list)
#print(WF_list)
#
#
def getFNswitch_forCORF(ialt,imach,aircraft_config):
 ialtitude = str(ialt)
 imachnumber = str(imach)
 fmach = (float(imach)/1000.00)
 falt = float(ialt)
 altuse = -888
 lowsidediff = 1000000.0
 highsidediff = 1000000.0
 print("WELSH GUYS ialtitude imachnumber ",ialtitude," ",imachnumber)
 FNswitch = []
 FNBLDdict = {}
 CORF_list = []
 COR_FF_nodlist = []
 if (JBx37800W):
  WF_file = open('JBx37800W_WFTAB101_JSON.txt')
  FNBLD_file = open('JBx37800W_FNBLDF01_JSON.txt')
#WF_file = open('37800WCFM56_57_WFTAB101_JSON.txt')
 if (P8clean):
  WF_file = open('P8clean_WFTAB101_JSON.txt')
  FNBLD_file = open('P8clean_FNBLDF01_JSON.txt')
 if (P8podup or P8poddn):
  if(aircraft_config != 2):
   WF_file = open('P8podup_WFTAB111_JSON.txt')
  else:
   WF_file = open('P8podup_WFTAB114_JSON.txt')
  FNBLD_file = open('P8podup_FNBLDF01_JSON.txt')
#
 a2string = FNBLD_file.readline()
 FNBLD = json.loads(a2string)
 lowsidealt = highsidealt = falt
 for fn in FNBLD.keys():
  jalt = float(fn)
  if (jalt == falt):
   altuse = jalt 
   break
  if (altuse == -888):
   if ((jalt-falt) < 0.0):
    altdiff = jalt-falt
    if (abs(altdiff) < lowsidediff):
     lowsidediff = abs(altdiff)
     lowsidealt = jalt
   if ((jalt-falt) > 0.0):
    altdiff = jalt-falt
    if (altdiff < highsidediff):
     highsidediff = altdiff
     highsidealt = jalt  
 print ("IN get_lists_forWF.getFNswitch_forCORF falt fmach ",falt," ",fmach)
 print ("altuse = ",altuse)
 print ("lowsidealt = ",lowsidealt," highsidealt = ",highsidealt)
 if (altuse != -888): 
  altuse_str = str(int(altuse))
  MACH_list = FNBLD[altuse_str][0]
  fmach_list =[float(xv) for xv in MACH_list]
  FNBLD_list = FNBLD[altuse_str][1]
  FNswitch = Linear(fmach_list,FNBLD_list,fmach)
  print ("altuse != -888 and FNswitch = ",FNswitch)
 else:
  lowsidealt_str = str(lowsidealt)
  MACH_list = FNBLD[lowsidealt_str][0]
  fmach_list =[float(xv) for xv in MACH_list]
  FNBLD_list = FNBLD[lowsidealt_str][1]
  FNswitchlow = Linear(fmach_list,FNBLD_list,fmach)
  highsidealt_str = str(highsidealt)
  MACH_list = FNBLD[highsidealt_str][0]
  fmach_list =[float(xv) for xv in MACH_list]
  FNBLD_list = FNBLD[highsidealt_str][1]
  FNswitchhigh = Linear(fmach_list,FNBLD_list,fmach)
  FNswitch = (FNswitchlow + FNswitchhigh)/2.0
  print ("altuse = -888 and FNswitch = ",FNswitch)
 return(FNswitch)
#
#
def get_lists_forCORF(ialt,imach,aircraft_config):
 CORF_FNodlist = []
 CORF_list = []
#TABCORF01 IS COMMON FOR ALL P8 MODELS FOR CONFIGs 1,3,4,5,6
#BUT WE WILL NEED TO ADD A TABCORF04 FOR CONFIG 2 (1LE)
#ASSUMING THE THRUST EVER GETS THAT LOW FOR 1LE WHICH SEEMS DOUBTFULL
#FOR NOW WE USE TABCORF01 FOR ALL CONFIGS
 if (P8clean or P8podup or P8poddn):
  if (aircraft_config != 2):
   CORF_file = open('P8clean_TABCORF01_JSON.txt')
  else:
   CORF_file = open('P8clean_TABCORF01_JSON.txt')
#
 if (JBx37800W):
  CORF_file = open('JBx37800W_TABCORF01_JSON.txt')
 #if (P8podup or P8poddn):
  #CORF_file = open('P8podup_TABCORF01_JSON.txt')
 a2string = CORF_file.readline()
 CORF = json.loads(a2string)
 print ("OHIO KEYS CORF.keys() = ",CORF.keys())
 #for keym1 in CORF.keys():
  #print("OHIO MAFIA keym1 CORF[keym] ",keym1," ",CORF[keym1]) 
  #for keym2 in CORF[keym1]:
   #print("keym2 CORF[keym1][keym2] ",keym2," ",CORF[keym1][keym2])
 ialtitude = str(ialt)
 imachnumber = str(imach)
 print("OHIO BUCKEYES ialtitude imachnumber ",ialtitude," ",imachnumber)
 try:
  CORF_FNod_list = CORF[ialtitude][imachnumber][0]
  CORF_list = CORF[ialtitude][imachnumber][1]
 except:
  CORF_FNod_list = [0]
  CORF_list = [0]
 print("AT THE END OF get_lists_forWF for these ialtitude and imachnumber values ",ialtitude," ",imachnumber)
 return(CORF_FNod_list,CORF_list)
#print(CORF_FNod_list)
#print(CORF_list)
#
    

