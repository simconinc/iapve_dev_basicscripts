import get_aeropfm
import std_atm
import math
#def get_MAXBUFalt(ftemperature,ftas,fmach,falt,fweight,aircraft_model,aircraft_config,pflag):
def get_MAXBUFalt(disa,ftas,fmach,falt,fweight,aircraft_model,aircraft_config,pflag):
#G is the acceleration of gravity in ft/sec**2
 G = 32.17405
 G_ft_per_min_sqrd = 32.17405*3600.00
#
 ftemperature = std_atm.isa2temp(disa,falt)
 if (pflag): print ('disa ftemperature ',disa,' ',ftemperature)
 cMAXBUFalt = falt
 MAXBUF_alt_delta = 47000.00 - falt
 higher = False
 lower = False
 i = 1
 whilecnt = 0
#ITERATE ON ALTITUDE
 while (i==1 and whilecnt < 25):
  whilecnt = whilecnt + 1
  if (pflag): print ("cMAXBUFalt at top = ",cMAXBUFalt)
  ftemperature = std_atm.isa2temp(disa,cMAXBUFalt)
  if (pflag): print ('disa ftemperature ',disa,' ',ftemperature)
#
  fcl = 0.0
  geefactor = 1.305
  (lift_coeff,cl_max,drag_coeff,Lift,Drag,ffnod) = get_aeropfm.get_FNod_cruise(geefactor,ftemperature,fmach,cMAXBUFalt,\
                                                   fweight,fcl,aircraft_model,aircraft_config,pflag)
  if (pflag): print ("below are lift_coeff,cl_max,drag_coeff,Lift,Drag")
  if (pflag): print (lift_coeff," ",cl_max," ",drag_coeff," ",Lift," ",Drag)
#
# CLmax = get_aeropfm.CL_to_buffet(fmach,aircraft_model,aircraft_config,pflag) 
# if (pflag): print ("CLmax = ",CLmax)
#
  try: 
   CL_40degbank = lift_coeff
  except:
   CL_40degbank = cl_max
#
  if (pflag): print("CL_40degbank = ",CL_40degbank)
#
  CL_diff = cl_max - CL_40degbank
  if (pflag): print ("MAIN STOP CL_diff = ",CL_diff)
#
  if (abs(CL_diff) < 0.0005):
   i = 0
  else:
   #if (CL_diff > 0.0001):
   if (CL_diff > 0.0005):
    cMAXBUFalt = cMAXBUFalt + MAXBUF_alt_delta
    higher = True
   if (CL_diff < -0.0005):
    cMAXBUFalt = cMAXBUFalt - MAXBUF_alt_delta
    lower = True
   if (pflag): print ("cMAXBUFalt = ",cMAXBUFalt)
   MAXBUF_alt_delta = MAXBUF_alt_delta/2.0
  if (abs(MAXBUF_alt_delta) < 0.0001 and abs(CL_diff) > 0.0005):
   print ("MAXBUFALT THROWING IN THE TOWL ON CONVERGENCE with cMAXBUFalt = ",cMAXBUFalt," MAXBUF_alt_delta = ",MAXBUF_alt_delta) 
   cMAXBUFalt = -999
 return(cMAXBUFalt,CL_40degbank,cl_max)


