#Determine the value for max climb thrust over delta (FNCLBod)
import json,sys
import std_atm
import math
import get_fromFNCLB
import IAPVEutils
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2
from Linear import Linear
#
def maxclimbthrust(falt,fmach,temperature,aircraft_config):
    print ("MACABEE DETERMINING THE VALUE for max climb thrust over delta")
    if (aircraft_config == 2):
     engfac = 1.0
    else:
     engfac = 2.0
    fidisa = IAPVEutils.get_disa_degF(falt,temperature) 
#
    FNCLBodvalue_disa_alt_list = []
#
    FNCLBmachlist = []
    FNCLBodlist = []
#
    FNCLBalts_list = []
    FNCLBod_list = []
    FNCLBalts_count = 0
    intdisa = []
    intalt = []
#
#Establish FNCLBdisa_alts dictionary for use further down
    available_FNCLBdisa_alts_dict = {}
    available_FNCLBdisa_alts_dict = get_fromFNCLB.getdisa_fromFNCLB101()
#
    for strdisa in available_FNCLBdisa_alts_dict.keys():
     intdisa.append(int(strdisa))
    intdisa.sort()
    print ("intdisa = ",intdisa)
#
# cycle through available_FNCLBdisa_alts_dict
    for idi in intdisa: 
     for stralt in available_FNCLBdisa_alts_dict[str(idi)]:
      intalt.append(int(stralt))
     intalt.sort()
     for iai in intalt:
      #print ("idi & iai ",idi," ",iai," AND THE FIXID IS",adict["PointID"])
      print ("idi & iai ",idi," ",iai)
      (FNCLBmachlist,FNCLBodlist) = get_fromFNCLB.get_lists_fromFNCLBod(str(idi),str(iai),aircraft_config)
      print ("Ahead of the 1st BiParabolic or Linear call for determining FNCLBod")
      print("FNCLBmachlist ",FNCLBmachlist)
      print("FNCLBodlist ",FNCLBodlist)
      print("fmach = ",fmach)
      if (FNCLBmachlist[0] !=  -888):
       fmachle =[float(xv) for xv in FNCLBmachlist]
      #for a given disa and altitude apply Biparabolic to the applicable
      #FNCLBmachlist list and FNCLBodlist to get a value for FNCLBodvalue.
      #This value is then added to FNCLBod_alts_list 
       #FNCLBodvalue = Biparabolic1(fmachle,FNCLBodlist,fmach,'maxclimbthrust')
       FNCLBodvalue = Linear(fmachle,FNCLBodlist,fmach)
       print ("After 1st Biparabolic or Linear call and FNCLBodvalue = ",FNCLBodvalue)
       FNCLBod_list.append(FNCLBodvalue)
       FNCLBalts_list.append(iai)
       FNCLBalts_count = FNCLBalts_count + 1
       print ("AT THE END OF THE INNER LOOP for iai = ",iai)
       print ("  ") 
     print ("For idi = ",idi)
     print ("Ahead of the 2nd Biparabolic or Linear call for FNCLB and falt = ",falt)
     print ("FNCLBalts_list = ",FNCLBalts_list)
     print ("FNCLBod_list = ",FNCLBod_list)
     #FNCLBodvalue_disa = Biparabolic1(FNCLBalts_list,FNCLBod_list,falt,'maxclimbthrust')
     FNCLBodvalue_disa = Linear(FNCLBalts_list,FNCLBod_list,falt)
     #alt_dict_forprint[str(ialt)] = mach_dict_forprint  
     print ("")
     FNCLBodvalue_disa_alt_list.append(FNCLBodvalue_disa)
     print ("After 2nd Biparabolic or Linear call")
     print ("falt and fmach = ",falt," ",fmach," and strdisa = ",strdisa)
     print ("FNCLBodvalue_disa = ",FNCLBodvalue_disa)
     print ("FNCLBodvalue_disa_alt_list = ",FNCLBodvalue_disa_alt_list)
#
     FNCLBmachlist = []
     FNCLBodlist = []
     FNCLBalts_list = []
     FNCLBod_list = []
     FNCLBalts_count = 0
     intalt = []
#
    print("Finished the outer loop and FNCLBodvalue_disa = ",FNCLBodvalue_disa)
    print("intdisa = ",intdisa)
    print (" ")
    print ("FNCLB JAGUAR TERRITORY")
    ISAtempdev_list =[float(xv) for xv in intdisa]
    #ISAtempdev_list.sort()
    print ("Ahead of the 3rd BiParabolic call for FNCLBodvalue")
    print ("FNCLBodvalue_disa_alt_list = ",FNCLBodvalue_disa_alt_list)
    print ("ISAtempdev_list = ",ISAtempdev_list)
    print ("fidisa = ",fidisa)
    #theFNCLBodvalue = Biparabolic1(ISAtempdev_list,FNCLBodvalue_disa_alt_list,fidisa,'maxclimbthrust') 
    theFNCLBodvalue = Linear(ISAtempdev_list,FNCLBodvalue_disa_alt_list,fidisa)   
    #print ("FOR ",adict["PointID"])  
    print ("theFNCLBodvalue =  ",theFNCLBodvalue)
    totalavlbclimbthrust = theFNCLBodvalue * engfac
    print ("END OF FNCLB JAGUAR TERRITORY")
    return(totalavlbclimbthrust)
#@#$************************************************************