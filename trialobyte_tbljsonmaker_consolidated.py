###########################################
#### Trialobyte Master Control Program ####
###########################################

# Trialobyte is the overall control program for the IAPVE software. 
# Trialobyte is responsible for managing the process flow of data input starting 
# with the table maker program reading in a .dat file, followed by the creation of 
# .txt files containing table data that will be converted to JSON objects by the
# JSON maker program. Finally, statistical analysis is performed by an R script.

# Trialobyte also includes a interpolation selector module. This module can either automatically
# identify the type of file being processed and select the appropriate interpolation 
# method or be switched to manual mode in whic the user selects the interpolation method.

# The table maker program, get_INFLT_tab_v4.py has been transferred from being executed
# from the ini configuration file to being fully embedded into the Trialobyte script

# Due to the number of individual JSON maker programs, JSON maker script selection
# is handled via the .ini configuration file.

############################
# FILE HISTORY
# 7/2/2015 - File created using one tablemaker and jsonmaker modules
# 7/9/2015 - Added JB's recommended .py files for incorporation
# 7/12/2015 - Incorporated the function of the table maker program, get_INFLT_tab_v4.py
# 7/18/2015 - Added ability to pass .dat file name string between modules
############################

import json
import sys
import re
import csv
import numpy as np
from configparser import SafeConfigParser


##########################################
##### Read in from Configuration File ####
##########################################

# Create parser object
parser = SafeConfigParser()

# Default configuration file name
config_file_name = 'config_trialobyte.ini'
"""
# Alternately, use command line arguments to designate configuration file. 
# The user can enter the name of the .ini config file as an command line argument
args = sys.argv
for ar in args: 
  print(ar)
  if(ar.find('.ini') >= 0):
    config_file_name = ar
"""
print("config_file_name is = ", config_file_name)

# Read from Configuration file 
parser.read(config_file_name)

#Read in directory paths from configuration file
scrpt_dir  = parser.get('script directory','scrpt_dir')
inpt_dir  = parser.get('input_file_directory','inpt_dir')
outpt_dir  = parser.get('output_file_directory','outpt_dir')
print("scrpt_dir = ", scrpt_dir)
print("input_dir = ", inpt_dir)
print("outpt_dir = ", outpt_dir)


############################
#### Table Maker module ####
############################

#get_INFLT_tab.py
#This program reads from INFLT .dat file and parses certain tables
#and then writes their values to a spread sheet.

#data = numpy.loadtxt('JBx37800W.dat')
#dest_path = "C:/python276/WFTAB101_table.txt"
#dest_path = "C:/python276/P8Clean_WFTAB101_table.txt"

#newarray = np.fromfile('JBx37800W.dat',dtype=int)
#with open(newarray,'r'):
#    data_iter = csv.reader(dest_path, 
#                           delimiter = delimiter, 
#                           quotechar = '11/n')
#    data = [data for data in data_iter]
#data_array = np.asarray(data, dtype=None, order=None)

i = 1
foundtable = False

#astr is where each line of a given table is appended to in order to create
#one long string making up that table.  astr is always initialized to null
astr = ""

tables = []  #for storing a list of tables, each a string
tbl_dict = []   #for storing a list of dictionaries
subtab = []
independent_vars = []
index11 = []
fuel_table = []
WFlist = []
dict = {}
crawdad = {}
k = 0

#the following while loop extracts certain tables and stores them as 
#strings in the tables list
#The while loop below stays in effect until i is set to 0
while(i):
    astring = INFLT_file.readline()
#   print "astring = ",astring
#Check if astring contains a start indicator for a table
#The string /TBLU indicates the start of the very 1st plan
#An H or a / indicates end of a table and the possible start of a new table
    if (not foundtable and astring[0:5] == "/TBLU"):
        foundtable = True 
#       print "FOUND TBLU"
    elif(foundtable and (astring[0:1] == "H" or astring[0:1] == '/')):
        tables.append(astr)
#       print "astr[0:7] and tables[k] ",astr[0:7]," ",tables[k][0:27]
        foundtable = False 
#       print "foundtable astring[0:1] = H or / ", foundtable," ",astring[0:1]
        k = k + 1
        astr = ""
    elif(foundtable and astring != ""):
        astr = astr + astring
#astring == "" means we have reached the end of the file 
    if (astring == ""):
        tables.append(astr)
        i = 0
#print "fplans equals", fplans           
#print "out of while loop"

for t in tables:
    print ("START of t in tables")
# when you specify '\n' python treats it as hex0a
# when you specify '\r' python treats it as hex0D
# therefore
# p.replace('\n', ' ') replaces all occurrences of hex0A with hex20
# p.replace('\r', ' ') replaces all occurrences of hex0D with hex20
# it is necessary to replace in the string p all occurences #of hex0A with hex20 
# and all occurences hex0D with hex20 in order for json.loads(p) to work properly
#   t = t.replace('\n', ' ').replace('\r', ' ')
    print ("t ",t[0:272])
    print ("END OF THE TABLE t")   
print ("Finished reading the file")  
#
#At this point in the process each member of the tables list is a string consisting
#of an entire table starting with the table identifier (e.g. CDBASE01)
#
#Now we are going to find the member of the tables list containing the string
#for the table WFTAB101 and process that table 
#we know from the INFLT User Manual that WFTAB101 is based on WF = F(W/delta,mach,alt)
#
#We want to split the WFTAB101 table into 4 parts; part 1 consists of the independent variable 
#FNod values, part 2 consists of the independent variable mach values, part 3 consists of the
#independent variable altitude values, and part 4 consists of the dependent variable WF values.
#
#open file to write the WFTAB101 table to 
for w in tables:
 if (w[0:8] == 'WFTAB101'):
  print ("found WFTAB101")
  #fn = "JBx37800W_WFTAB101_tbl.txt"
  #fn = "P8clean_WFTAB101_tbl.txt"
  fn = "37800WCFM56_57_WFTAB101_tbl.txt"
  target = open(fn,'w')
  target.truncate()
  independent_vars = w[8:].split("11\n")
  FNod = independent_vars[0]
  FNod = FNod.replace('1\n', '1#').replace('\r','')
  FNod = FNod.replace('\n','')
  target.write(FNod)
  target.write('*\n')
  mach = independent_vars[1]
  mach = mach.replace('1\n', '1#').replace('\r','')
  mach = mach.replace('\n','')
  target.write(mach)
  target.write('*\n')
  alt = independent_vars[2]
  alt = alt.replace('\n','').replace('\r','')
  target.write(alt)
  target.write('*\n') 
  print ("FNod ",FNod)
  print ("mach ",mach)
  print ("alt ",alt)

#Now we want to find the index in w which is the start of the WF values
#this would be right after the 3rd occurrence of 11
  for m in re.finditer('11\n',w):
    index11.append(m.end())
# print "index11[2] ",index11[2]
# print "Here"  
# outer = index11[2] + 100
# print w[index11[2]:outer]  
  WF = w[index11[2]:]
# WF = WF.replace('\n', '#').replace('\r', ' ')
  target.write(WF)
  target.write('*')
  print ("WF VALUES ")
  print (WF)
  print ("DONE PRINTING WF VALUES")
# print "Complete Fuel Table"
# print fuel_table


 if (w[0:8] == 'CDBASE01'):
  print ("found CDBASE01")
  #fn = "JBx37800W_CDBASE01_tbl.txt"
  #fn = "P8clean_CDBASE01_tbl.txt"
  fn = "37800WCFM56_57_CDBASE01_tbl.txt"
  target = open(fn,'w')
  target.truncate()

  independent_vars = w[9:].split("1cl\n")
  cl = independent_vars[0]
  cl = cl.replace('cl\n', 'cl#').replace('\r','')
  print ("cl = ",cl)
  target.write(cl)
  target.write('*\n')

  mach_and_cd = independent_vars[1].split("1mach\n")
  mach = mach_and_cd[0]
  mach = mach.replace('mach\n', 'mach#').replace('\r','')
  target.write(mach)
  target.write('*\n')
  
  cd = mach_and_cd[1]
  target.write(cd)
  target.write('*') 

#At this point in the process we have written out four string objects
#to the files WFTAB101_tbl.txt and CDBASE01_tbl.txt or to similarly named
#output files.
#For WFTAB101 these four string objects are:
#   FNod - contains the entire set of thrust over delta values as one string
#   mach - contains the entire set of mach values as one string
#   alt - contains all of the altitude values as one string
#   WF - contains the entire set of fuel flow values as one string
#Within WFTAB101_tbl.txt, the four strings are separated by the delimiter '*'
#
#
#The section below is kept for possible future use. However,
#it probably will not be needed as the program INFLTtable_to_JSON.py
#that reads from WFTAB101_tbl.txt makes use of an iterator object
#created using csv.reader with '*' as the delimiter.  This
#iterator object and follow on code provides a cleaner way
#for getting us what we need.
# 
#We now want to convert FNod, mach, alt, and WF into lists
#
count = len(FNod)

FNodlist = FNod.split('1\n')
machlist = mach.split('1\n')
altlist = alt.split('1\n')
#WF.replace('\n','')

print ("FNodlist ",FNodlist)
print ("machlist ",machlist)
print ("altlist ",altlist)

count_FNodlist = len(FNodlist)
count_machlist = len(machlist)
count_altlist = len(altlist)
j = 0
print ("count_FNodlist count_machlist count_altlist ",count_FNodlist," ",count_machlist," ",count_altlist)
while (j < count_FNodlist):
  FNodlist[j].replace('\n','')
  print (FNodlist[j])
  j = j + 1
j = 0
while (j < count_machlist):
  machlist[j].replace('\n','')
  print (machlist[j])
  j = j + 1
j = 0
while (j < count_altlist):
  altlist[j].replace('\n','')
  print (altlist[j])
  j = j + 1

table_id = FNodlist[0]
FNodlist.pop(0)

#Split WF in stages
#
seps = ('1\n','2\n','3\n','4\n','5\n','6\n','7\n','8\n','9\n','10\n','11\n','12\n','13\n','14\n','15\n')
default_sep = '1\n'
print ("WFlist")
n = 0
for s in seps[1:]:
 WF = WF.replace(s,default_sep)
for i in WF.split(default_sep):
  WFlist.append(i)
  print (WFlist[n])
  n = n + 1

#now we create a list of lists
FNod_2d_list = []
mach_2d_list = [[]]
WF_2d_list = [[]]
for m in FNodlist:
  FNod_2d_list.append(m.split())
print ("FNod_2d_list")
print (FNod_2d_list)
#FNod.replace('\n','')
#mach.replace('\n','')
#alt.replace('\n','')
#WF.replace('\n','')
#

###########################
#### JSON Maker module ####
###########################


##############################
### Interpolation Selector ###
##############################

#Manually set py_scrpt to the name of the correct IAPVE interpolation program
py_scrpt = parser.get('python script','py_scrpt')
if py_scrpt != "NA":
    print("Please make sure python script file is present in directory")
else:
    print("py_scrpt = ", py_scrpt)

