import get_MAXCRUalt
#ftemperature = -56.5
disa = 0.0
aircraft_model = "p8clean"
aircraft_config = 1
#falt = 37000.00
#falt = 27000.00
#ftas = 43205.00076
#ftas = 42930.89
#ftas = 39137.1828423355
#fmach = 0.74383
#fmach = 0.73911
#
#for 30N37
falt = 35000.00
fweight = 141488.851586879
fmach = 0.734163633703063
ftas = 42855.4164310223
#
#for 33N27
falt = 37000.00
fweight = 134636.057283133
fmach = 0.743829658209804
ftas = 43205.0075568481
#
#for CEBAN
#falt = 27000.00
#fmach = 0.6474
#fweight = 155127.259567499
#ftas = 39137.1828423355
#
#for CEBAN
#falt = 27000.00
falt = 31375.0
#fmach = 0.6474
fmach = 0.73
#fweight = 155127.259567499
fweight = 156761.5719
#ftas = 46598.27793
ftas = 45828.51
fidisa = 27.0
#ftas = 39137.1828423355
#
pflag = 1 
#
(MAXCRUalt,FNCRUatalt,cl_15degb_atalt,MAXCRUaltcl_max,cd_15degb_atalt,lift_15degb_atalt,drag_15degb_atalt) = \
 get_MAXCRUalt.get_MAXCRUalt(fidisa,ftas,fmach,falt,fweight,\
 aircraft_model,aircraft_config,pflag)
#
print ("MAXCRUalt ",MAXCRUalt," maxthrust = ",FNCRUatalt," cl_15degb_atalt = ",cl_15degb_atalt)
print ("cd_15degb_atalt = ",cd_15degb_atalt," lift_15degb_atalt = ",lift_15degb_atalt)
print ("drag_15degb_atalt = ",drag_15degb_atalt)
