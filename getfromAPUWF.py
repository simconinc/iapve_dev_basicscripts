#NOTE:
#keys obtained from dictionaries loaded via json.loads
#are always strings. But the value associated with any 
#given key maintains its original type
#
import json, sys
import std_atm
S = 1341.0
diag = 3
#P8clean = False
P8clean = True
P8podup = False
P8poddn = False
#JBx37800W = True
JBx37800W = False
fi = open('aircraft_type_for_IAPVE.txt')
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
if (ac_model_config == "p8poddn\n"):
 P8clean = False
 JBx37800W = False
 P8poddn = True
#
def getmach_fromAPUWF(aircraft_config):
#returns a list of the available machs from APUWF01_JSON.txt
# 
 WF = {}
 available_machs = []
 print ("In getfromAPUWF and aircraft_config = ",aircraft_config)
#APUWF_FILE = open('37800WCFM56_57_APUWF01_JSON.txt')
#
#There is one APUWF01 file for all configs 
#P8clean needs APUWF01 for configs 2-6
 if (P8clean):
  if (aircraft_config == 2):
   APUWF_FILE = open('P8clean_APUWF01_JSON.txt')
  if (aircraft_config == 3):
   APUWF_FILE = open('P8clean_APUWF01_JSON.txt')
  if (aircraft_config == 4):
   APUWF_FILE = open('P8clean_APUWF01_JSON.txt')
  if (aircraft_config == 5):
   APUWF_FILE = open('P8clean_APUWF01_JSON.txt')
  if (aircraft_config == 6):
   APUWF_FILE = open('P8clean_APUWF01_JSON.txt')
#
#THERE IS ONLY ONE APUWF01 FILE FOR ALL P8 MODELS BUT
#WE CREATED P8podup_APUWF01_JSON.txt FOR P8podup SO WE USE IT
#FOR P8podup and P8poddn
#P8podup and P8poddn need APUWF01 for all configs 
 if (P8podup or P8poddn):
  if (aircraft_config > 0 and aircraft_config < 999):
   APUWF_FILE = open('P8podup_APUWF01_JSON.txt')
#
 if (JBx37800W):
  if (aircraft_config == 2):
   APUWF_FILE = open('JBx37800W_APUWF01_JSON.txt')
  if (aircraft_config == 3):
   APUWF_FILE = open('JBx37800W_APUWF03_JSON.txt')
  if (aircraft_config == 4):
   APUWF_FILE = open('JBx37800W_APUWF04_JSON.txt')
  if (aircraft_config == 5):
   APUWF_FILE = open('JBx37800W_APUWF05_JSON.txt')
  if (aircraft_config == 6):
   APUWF_FILE = open('JBx37800W_APUWF06_JSON.txt')
#
 a1string = APUWF_FILE.readline()
 print ("a1string = ",a1string)
 APUWF = json.loads(a1string)
 available_machs = APUWF.keys()
 str_mach = []
 int_mach = [] 
 for iamachs in available_machs:
  str_mach.append(iamachs)
 for jamachs in str_mach:
  int_mach.append(int(jamachs)) 
 int_mach.sort()
 return(int_mach)

