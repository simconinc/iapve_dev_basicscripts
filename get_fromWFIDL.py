import json,sys
import std_atm
import math
import IAPVEutils
from Biparabolic1 import Biparabolic1
from Linear import Linear
fi = open('aircraft_type_for_IAPVE.txt')
P8clean=False
JBx37800W=False
P8podup=False
ac_model_config = fi.readline()
if (ac_model_config == "p8clean\n"):
 P8clean = True
 JBx37800W = False
 P8podup = False
if (ac_model_config == "37800W\n"):
 P8clean = False
 JBx37800W = True
 P8podup = False
if (ac_model_config == "p8podup\n"):
 P8clean = False
 JBx37800W = False
 P8podup = True
#
def get_lists_fromWFIDLff(strdisa,stralt,aircraft_config):
 WFIDL = {}
 mach_list = {}
 WFIDLff_list = {}
#For now we default to P8clean and we don't bother with aircraft_config
 #P8clean = False
 #P8clean = True
 #P8podup = False
 #JBx37800W = True
 #JBx37800W = False
 if (JBx37800W):
  WFIDL_file = open('JBx37800W_WFIDL101_JSON.txt')
#WF_file = open('37800WCFM56_57_WFTAB101_JSON.txt')
 if (P8clean):
  WFIDL_file = open('P8clean_WFIDL101_JSON.txt')
 #if (P8podup):
  #WFIDL_file = open('P8podup_WFIDL101_JSON.txt')
#
 a2string = WFIDL_file.readline()
 WFIDL = json.loads(a2string)
 print ("In get_lists_fromWFIDLff and stralt strdisa = ",stralt," ",strdisa)
 try:
  mach_list = WFIDL[strdisa][stralt][0]
  WFIDLff_list = WFIDL[strdisa][stralt][1]
 except:
  mach_list = [-888]
  WFIDLff_list = [-888]
 print("AT THE END OF get_lists_fromWFIDLff for these strdisa and stralt values ",\
        strdisa," ",stralt)
 return(mach_list,WFIDLff_list)
#
def getdisa_fromWFIDL101():
#Returns the dictionary available_alts_dict that is structured as follows:
#top level keys are disa values such as '-72'
#The value associated with each top level key is a list of the available alts for that
#altitude. The alts in each list are each strings such as '37000' representing. 
#The reason they are strings is that they themselves will serve as keys
# 
 diag = 3
 #P8clean = True
 #JBx37800W = False
 #P8podup = False
 original_disakeys = {}
 WFIDL = {}
 available_disa = [] 
 available_alts_list = []
 available_alts_dict = {}
 if (P8clean):
  WFIDL_file = open('P8clean_WFIDL101_JSON.txt') 
 if (P8podup):
  WFIDL_file = open('P8podup_WFIDL101_JSON.txt') 
 if (JBx37800W):
  WFIDL_file = open('JBx37800W_WFIDL101_JSON.txt')
 a1string = WFIDL_file.readline()
 WFIDL = json.loads(a1string)
 available_disas = WFIDL.keys()
 #str_disa = []
 #for strdisas in available_disas:
  #str_disa.append(strdisas)
 for jdisas in available_disas:
  print("WFIDL[jdisas]keys = ",WFIDL[jdisas].keys())
  for striaa in WFIDL[jdisas].keys():
   if (diag == 4):
    print ("striaa = ",iaa)
   available_alts_list.append(striaa)
   if (diag == 4): 
    print ("available_alts_list  ",available_alts_list)
#make sure that the disa key for available_alts_dict is a character string representing an int
  if (jdisas.find(".") >= 0):
   if (jdisas.find(".") == 0):
    jkey = "0"
   else:
    pos = jdisas.find(".")
    jkey = jdisas[0:pos]
  else:
   jkey = jdisas
  original_disakeys.update({jkey:jdisas})
  available_alts_dict.update({jkey:available_alts_list})
  available_alts_list = []
  print ("type of jkey = ",type(jkey))
  print ("jkey & available_alts_dict[jkey] = ",jkey," ",available_alts_dict[jkey])
 return(available_alts_dict,original_disakeys)
#
#Determine the value for idle fuel flow (WFIDLff)
def getWFIDL_fuel(ftemperature,falt,fmach,aircraft_config):
    print ("GOPHERS DETERMINING THE VALUE for idle fuel flow WFIDL")
#
    available_WFIDLdisa_alts_dict = {}
    original_disakeys = {}
    (available_WFIDLdisa_alts_dict,original_disakeys) = getdisa_fromWFIDL101()
    #print(available_WFIDLdisa_alts_dict)
#
    #std_atm_temperature = std_atm.alt2temp(falt)
    #deviation = ftemperature - std_atm_temperature
    deviation = IAPVEutils.get_disa_degF(falt,ftemperature)
    if (deviation < 0.0):
     deviation = deviation - 0.5
    if (deviation > 0.0):
     deviation = deviation + 0.5
    idisa = int(deviation)
    fidisa = float(idisa)
#
    WFIDLffvalue_disa_alt_list = []
#
    WFIDLmachlist = []
    WFIDLfflist = []
#
    WFIDLalts_list = []
    WFIDLff_list = []
    WFIDLalts_count = 0
    intdisa = []
    intalt = []
    for strdisa in available_WFIDLdisa_alts_dict.keys():
     print("in getWFIDL_fuel and strdisa = ",strdisa," type of strdisa = ",type(strdisa))
     intdisa.append(int(strdisa))
    intdisa.sort()
    print ("intdisa = ",intdisa)
#
# cycle through available_WFIDLdisa_alts_dict
    for idi in intdisa: 
     mykey = str(idi)
     thekey = original_disakeys[mykey]
     for stralt in available_WFIDLdisa_alts_dict[mykey]:
      intalt.append(int(stralt))
     intalt.sort()
     for iai in intalt:
      print ("idi & iai ",idi," ",iai)
      (WFIDLmachlist,WFIDLfflist) = get_lists_fromWFIDLff(thekey,str(iai),aircraft_config,)
      print ("Ahead of the 1st BiParabolic call for determining WFIDLff")
      print("WFIDLmachlist ",WFIDLmachlist)
      print("WFIDLfflist ",WFIDLfflist)
      print("fmach = ",fmach)
      if (WFIDLmachlist[0] !=  -888):
       fmachle =[float(xv) for xv in WFIDLmachlist]
      #for a given disa and altitude apply Biparabolic to the applicable
      #WFIDLmachlist list and WFIDLfflist to get a value for WFIDLffvalue.
      #This value is then added to WFIDLff_alts_list 
      #WFIDLffvalue = Biparabolic1(fmachle,WFIDLfflist,fmach,'idlefuelflow')
       WFIDLffvalue = Linear(fmachle,WFIDLfflist,fmach)
       print ("After 1st Biparabolic or Linear call and WFIDLffvalue = ",WFIDLffvalue)
       WFIDLff_list.append(WFIDLffvalue)
       WFIDLalts_list.append(iai)
       WFIDLalts_count = WFIDLalts_count + 1
       print ("AT THE END OF THE INNER LOOP for iai = ",iai)
       print ("  ") 
     print ("For idi = ",idi)
     print ("Ahead of the 2nd Biparabolic call for WFIDL and falt = ",falt)
     print ("WFIDLalts_list = ",WFIDLalts_list)
     print ("WFIDLff_list = ",WFIDLff_list)
     #WFIDLffvalue_disa = Biparabolic1(WFIDLalts_list,WFIDLff_list,falt,'idlefuelflow')
     WFIDLffvalue_disa = Linear(WFIDLalts_list,WFIDLff_list,falt)
     #alt_dict_forprint[str(ialt)] = mach_dict_forprint  
     print ("")
     WFIDLffvalue_disa_alt_list.append(WFIDLffvalue_disa)
     print ("After 2nd Biparabolic call")
     print ("falt and fmach = ",falt," ",fmach," and strdisa = ",strdisa)
     print ("WFIDLffvalue_disa = ",WFIDLffvalue_disa)
     print ("WFIDLffvalue_disa_alt_list = ",WFIDLffvalue_disa_alt_list)
#
     WFIDLmachlist = []
     WFIDLfflist = []
     WFIDLalts_list = []
     WFIDLff_list = []
     WFIDLalts_count = 0
     intalt = []
#
    print("Finished the outer loop and WFIDLffvalue_disa = ",WFIDLffvalue_disa)
    print("intdisa = ",intdisa)
    print (" ")
    print ("WFIDL JAGUAR TERRITORY")
    ISAtempdev_list =[float(xv) for xv in intdisa]
    #ISAtempdev_list.sort()
    print ("Ahead of the 3rd BiParabolic call for WFIDLffvalue")
    print ("WFIDLffvalue_disa_alt_list = ",WFIDLffvalue_disa_alt_list)
    print ("ISAtempdev_list = ",ISAtempdev_list)
    print ("fidisa = ",fidisa)
    #theWFIDLffvalue = Biparabolic1(ISAtempdev_list,WFIDLffvalue_disa_alt_list,fidisa,'idlefuelflow')  
    theWFIDLffvalue = Linear(ISAtempdev_list,WFIDLffvalue_disa_alt_list,fidisa)  
    print ("theWFIDLffvalue =  ",theWFIDLffvalue)
    if (aircraft_config != 2):
     totalWFIDLfuelflow = theWFIDLffvalue * 2.0
    if (aircraft_config == 2):
     totalWFIDLfuelflow = theWFIDLffvalue
    print ("END OF WFIDL JAGUAR TERRITORY")
    return(totalWFIDLfuelflow)
