#G is the acceleration of gravity in ft/sec**2
import math
import IAPVEutils
import std_atm
G = 32.17405
def accel_factor_constmach(falt,fmach,fidisa):
 Tstd = std_atm_temperature = std_atm.alt2temp(falt,'ft','K')
 OAT_degK = Tstd + fidisa
 T_ratio = std_atm_temperature/(std_atm_temperature + fidisa) 
 print ("Tstd = ",Tstd," OAT_degK = ",OAT_degK," T_ratio = ",T_ratio)
 if (falt > 36089.24):
  accel_fac = 1.0
 else:
  accel_fac = 1.0 - (0.133184*(fmach*fmach)*(T_ratio))
 return(accel_fac)
   

