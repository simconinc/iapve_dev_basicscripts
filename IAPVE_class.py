
import sys, json
import csv
import std_atm
import getCDWF
import get_FNod
import get_lists_forWF
from Biparabolic1 import Biparabolic1
from badgerzorro import Biparabolic2

class iapve():    

    def __init__(self, iaobj, iapve_input_file):
        """Return a iapve object whose name is iaobj and starting
        input is iapve_input_file."""

    # Allows user to enter the name of the input file as an command line argument
    args = sys.argv
    for ar in args: 
        print(ar)
        if(ar.find('.ini') >= 0):
            iapve_input_file = ar




    def iapve_output(self, output_file):